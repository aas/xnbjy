@include('home.public.header')
    <!--top end-->
	<style>
	table tr td{border-bottom:1px solid #e1e1df;}	
	</style>
	<style>
.raise_list tr:hover{
	background-color:#FFE9D2;
	}
	
</style>
<div id="main">
	<div class="main_box">
		@include('home.public.left')
		<div class="raise right clearfix">
            <div class="ybc_list">
	<div class="ybcoin clearfix">
		<h2 class="left">第三方支付日志</h2>
		
	</div>
		<table class="raise_list" style="border:1px solid #e1e1df;" id="Transaction" align="center" border="0" cellpadding="0" cellspacing="0">
			<thead>
			<tr>
				<th>订单号</th>
				<th>金额(rmb)</th>
				<th>状态</th>
				<th>充值时间</th>
			
			</tr>
			</thead>
			<tbody>
			@foreach($list['data'] as $v)
			<tr>
				<td>{{$v['tradeno']}}</td>
				<td>{{$v['num']}}</td>
				<td>
					@if($v['status']==1)
					<span style="color:#393">成功</span>
					@else
					<span style="color:#e55600">失败</span>
					@endif
				</td>
				<td>
					{{date('Y-m-d H:i:s',$v['ctime'])}}
				</td>
			
			</tr>
			@endforeach
			</tbody>
									
		</table>
		<div class="page" style=" min-width:200px !important; float:right;">
			@if($list['last_page']>1)
				<div>
					<a class="prev" href="{{$list['prev_page_url']}}"><<</a>
					@foreach($list['pageNoList'] as $v)
						<a @if($v == $list['current_page'])
						   class="current"
						   @else
						   class="num"
						   @endif
						   href="{{urlHome('Fill','getIndex')}}?page={{$v}}">{{$v}}</a>
					@endforeach
					<a class="next" href="{{$list['next_page_url']}}">>></a>
				</div>
			@endif
              </div>
	</div>
		</div>
		<div class="clear"></div>
	</div>
</div> 
<script>
$(".menu138").addClass("uc-current");
</script>   
<!--footer start-->
@include('home.public.footer')
<!--footer end-->

