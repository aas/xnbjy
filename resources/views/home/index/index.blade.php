@extends('home/mainlayout')
@section('content')
    <!--top end-->
    <!--header end-->
    <style>
        .pull-left{ float:left;}
        .pull-right{ float:right;}
        .link a{ color:#000;}
        .link{ margin:0px auto; width:100%;}

    </style>
    <script type="text/javascript" src="/home/js/focus.js"></script>
    <script type="text/javascript" src="/home/js/Fnc.js"></script>
    <script type="text/javascript" src="/home/js/zc.js"></script>
    <script type="text/javascript" src="/home/js/1.js"></script>
    <script type="text/javascript" src="/home/js/bootstrap.js"></script>
    <script type="text/javascript" src="/home/js/jquery.flexslider-min.js"></script>
    <link rel="stylesheet" type="text/css" href="/home/css/hb_index.css">
    <link rel="stylesheet" type="text/css" href="/home/css/zc.css">
    <link rel="stylesheet" type="text/css" href="/home/css/flexslider.css">
    <link rel="shortcut icon" href="1.ico" />
    <!--banner start-->
    <div style="height:360px; width:100%; position:relative; overflow:hidden; min-width:1200px;">
        <div>
            <div class="flexslider">
                <ul class="slides">
                    @foreach($flash as $k=>$v)
                        <li><a href="http://{{!empty($v->jump_url)? $v->jump_url:'#'}}" target="_blank"><img src="{{$v->pic}}" alt="{{$v->title}}" style="height:320px;"></a></li>
                    @endforeach
                </ul>
            </div>
            <div class="ybcoin_volume">
                <div style="color:#fff;">
                    <p style=" font-size:16px; margin-bottom:5px; text-align: center;">风险提示</p>
                    <p style=" font-size:12px; line-height:22px;">{{$config['risk_warning']}}</p>
                </div>
                <div class="news_coin">
                    @if(empty(Session('USER_KEY_ID')))
                        <a href="{{urlHome('login','getIndex')}}">立即登录</a>
                    @else
                        <a href="{{urlHome('user','getIndex')}}">我的账户</a>
                    @endif
                </div>
                <p class="coin_reg">
                    @if(empty(Session('USER_KEY_ID')))
                        <a href="{{urlHome('reg','getIndex')}}" class="left">免费注册</a>
                        <!-- <a href="{:U('Oauth/index',array('type'=>'qq'))}" class="right">QQ登录  </a> -->
                    @endif
                    <a href="{{urlHome('dow','getNewcoin')}}" class="right">我要上新币</a>
                </p>
            </div>
        </div>
    </div>
    <!--banner end-->
    <div class="ybcoin_section clearfix" style="border:0;">
        <!--币币交易开始-->
        <div id="tags_coin" class="coinarea left" style="position:relative;">
            <div class="bgcolor" style="display:none;"></div>
            <div style="margin-top:30px;" id="tagContent">
                <!-- 对CNY交易区 结束-->
                <div class="tagContent selectTag" id="tagContent0">
                    <p style="color:#f00; font-size:14px; margin-bottom:10px;">{!! $config['friendship_tips'] !!}</p>
                    <table class="coin_list coinarea" border="0" cellpadding="0" cellspacing="0">
                        <thead>
                        <tr>
                            <th></th>
                            <th style="text-align:left;">币名</th>
                            <th class="header">最新价格(CNY)</th>
                            <th class="header">24H成交量</th>
                            <th class="header">24H成交额(CNY)</th>
                            <th class="header">总市值(CNY)</th>
                            <th class="header">24H涨跌</th>
                            <th class="header">7D涨跌</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($currency as $k=>$v)
                            <tr class="coin_num">
                                <td><a href="#"><img src="{{$v['currency_logo']}}" style="width:20px; height:20px;"></a></td>
                                <td class="coin_name"><a href="#">{{!empty($v['currency_name'])? $v['currency_name']:'虚拟币'}}{{!empty($v['currency_mark'])? $v['currency_mark']:''}}</a></td>
                                <td>
                                    @if($v['new_price_status'] == 0)
                                        <a href="#" class="buy">
                                            @else
                                                <a href="#" class="sell">
                                                    @endif
                                                    {{!empty($v['new_price'])? $v['new_price']:'暂无'}}
                                                    @if($v['new_price_status'] == 0)↓@else↑@endif</a>
                                </td>
                                <td><a href="#">{{!empty($v['24H_done_num'])? $v['24H_done_num']:'0'}}</a></td>
                                <td><a href="#">{{!empty($v['24H_done_money'])? $v['24H_done_money']:'0'}}</a></td>
                                <td><a href="#">{{!empty($v['currency_all_money'])? $v['currency_all_money']:'0'}}</a>
                                </td>
                                <td>
                                    {{--<gt name='vo.24H_change' value='0'>--}}
                                    @if($v['24H_change'] > 0)
                                        <a href="#" class="sell">
                                            @else
                                                <a href="#" class="buy">
                                                    @endif
                                                    {{!empty($v['24H_change'])? $v['24H_change']:'0'}}%
                                                </a>
                                </td>
                                <td>
                                    {{--<gt name='vo.7D_change' value='0'>--}}
                                    @if($v['7D_change'] > 0)
                                        <n href="#" class="sell">+@else<a href="#" class="buy">@endif{{!empty($v['7D_change'])? $v['7D_change']:'0'}}%</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--币币交易结束-->
        <div class="right coin_news">
            <div class="news_title clearfix">
                <h2 class="left">官方公告</h2>
                <a href="{{urlHome('art','getIndex')}}" class="right">更多</a>
            </div>
            <ul>
                @if($info_red1)
                    @foreach($info_red1 as $k=>$vo_red1)
                        <li>
                            <a href="{{urlHome('art','getDetails',['article_id'=>$vo_red1['article_id']],2)}}">
                                <span class="coin_news_title left" style="color:red;font-weight:bold;">{{$vo_red1['title']}}</span>
                                <span class="right">{{date('m-d',$vo_red1['add_time'])}}</span>
                            </a>
                        </li>
                    @endforeach
                @endif
                @if($info1)
                    @foreach($info1 as $k=>$vo1)
                        <li>
                            <a href="{{urlHome('art','getDetails',['article_id'=>$vo1['article_id']],2)}}">
                                <span class="coin_news_title left">{{$vo1['title']}}</span>
                                <span class="right">{{date('m-d',$vo1['add_time'])}}</span>
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>
            <div class="news_title clearfix" style="margin-top:20px;">
                <h2 class="left">市场动态</h2>
                <a href="{{urlHome('art','getIndex')}}" class="right">更多</a>
            </div>
            <ul>
                @if($info_red2)
                    @foreach($info_red2 as $k=>$vo_red2)
                        <li>
                            <a href="{{urlHome('art','getDetails',['article_id'=>$vo_red2['article_id']],2)}}">
                                <span class="coin_news_title left" style="color:red;font-weight:bold;">{{$vo_red2['title']}}</span>
                                <span class="right">{{date('m-d',$vo_red2['add_time'])}}</span>
                            </a>
                        </li>
                    @endforeach
                @endif
            </ul>
            <ul>
                @if($info2)
                    @foreach($info2 as $k=>$vo2)
                        <li><a href="{{urlHome('art','getDetails',['article_id'=>$vo2['article_id']],2)}}"><span class="coin_news_title left">{{$vo2['title']}}</span><span class="right">{{date('m-d',$vo2['add_time'])}}</span></a></li>
                    @endforeach
                @endif
            </ul>
            <!--<div style="margin-top:20px;"><a href="https://www.gemwallet.com/" target="_blank"><img src="images/coin_gemwallet.jpg" alt="竞付宝"></a></div>-->
        </div>
    </div>
    <div class="index_box_2 slogan" style="width:1200px; margin:0px auto; margin-top:30px;">

        <div class="slogan_title">选择{{!empty($config['name'])? $config['name']:'虚拟网'}},安全可信赖</div>
        <div class="slogan_tis">累计交易额:<span id="yi"><span>{{!empty($sum_money)? $sum_money:'0'}}</span></div>
        <div id="cumulative">
            <div class="number_box">
                @foreach($arr as $k=>$v)
                    <div
                            @if($k%3 == 0 && $k%12 == 0)
                            class="unit add_f"
                            @elseif($k%3 == 0)
                            class="unit add_w"
                            @else
                            class="unit"
                            @endif >
                        <div class="top"><span>{{$v}}</span></div>
                        <div style="" class="top"><span>{{$v}}</span></div>
                        <div class="btm"><span>{{$v}}</span></div>
                        <div style="transform: rotateX(0deg);" class="btm"><span>{{$v}}</span></div>
                    </div>
            @endforeach



            <!--<div class="unit add_f">-->
                <!--<div class="top"><span>3</span></div>-->
                <!--<div style="" class="top"><span>3</span></div>-->
                <!--<div class="btm"><span>3</span></div>-->
                <!--<div style="transform: rotateX(0deg);" class="btm"><span>3</span></div>-->
                <!--</div>-->
                <!--<div class="unit ">-->
                <!--<div class="top"><span>6</span></div>-->
                <!--<div style="" class="top"><span>6</span></div>-->
                <!--<div class="btm"><span>6</span></div>-->
                <!--<div style="transform: rotateX(0deg);" class="btm"><span>6</span></div>-->
                <!--</div>-->
                <!--<div class="unit ">-->
                <!--<div class="top"><span>8</span></div>-->
                <!--<div style="" class="top"><span>8</span></div>-->
                <!--<div class="btm"><span>8</span></div>-->
                <!--<div style="transform: rotateX(0deg);" class="btm"><span>8</span></div>-->
                <!--</div>-->
                <!--<div class="unit add_w">-->
                <!--<div class="top"><span>7</span></div>-->
                <!--<div style="" class="top"><span>7</span></div>-->
                <!--<div class="btm"><span>7</span></div>-->
                <!--<div style="transform: rotateX(0deg);" class="btm"><span>7</span></div>-->
                <!--</div>-->
                <!--<div class="unit ">-->
                <!--<div class="top"><span>4</span></div>-->
                <!--<div style="" class="top"><span>4</span></div>-->
                <!--<div class="btm"><span>4</span></div>-->
                <!--<div style="transform: rotateX(0deg);" class="btm"><span>4</span></div>-->
                <!--</div>-->
                <!--<div class="unit ">-->
                <!--<div class="top"><span>7</span></div>-->
                <!--<div style="" class="top"><span>7</span></div>-->
                <!--<div class="btm"><span>7</span></div>-->
                <!--<div style="transform: rotateX(0deg);" class="btm"><span>7</span></div>-->
                <!--</div>-->
                <!--<div class="unit add_w">-->
                <!--<div class="top"><span>9</span></div>-->
                <!--<div style="" class="top"><span>9</span></div>-->
                <!--<div class="btm"><span>9</span></div>-->
                <!--<div style="transform: rotateX(0deg);" class="btm"><span>9</span></div>-->
                <!--</div>-->
                <!--<div class="unit ">-->
                <!--<div class="top"><span>9</span></div>-->
                <!--<div style="" class="top"><span>9</span></div>-->
                <!--<div class="btm"><span>9</span></div>-->
                <!--<div style="transform: rotateX(0deg);" class="btm"><span>9</span></div>-->
                <!--</div>-->
                <!--<div class="unit ">-->
                <!--<div class="top"><span>5</span></div>-->
                <!--<div style="" class="top"><span>5</span></div>-->
                <!--<div class="btm"><span>5</span></div>-->
                <!--<div style="transform: rotateX(0deg);" class="btm"><span>5</span></div>-->
                <!--</div>-->

                <!--<div class="unit add_w">-->
                <!--<div class="top"><span>3</span></div>-->
                <!--<div style="" class="top"><span>3</span></div>-->
                <!--<div class="btm"><span>3</span></div>-->
                <!--<div style="transform: rotateX(0deg);" class="btm"><span>3</span></div>-->
                <!--</div>-->
                <!--<div class="unit ">-->
                <!--<div class="top"><span>5</span></div>-->
                <!--<div style="" class="top"><span>5</span></div>-->
                <!--<div class="btm"><span>5</span></div>-->
                <!--<div style="transform: rotateX(0deg);" class="btm"><span>5</span></div>-->
                <!--</div>-->
                <!--<div class="unit ">-->
                <!--<div class="top"><span>8</span></div>-->
                <!--<div style="" class="top"><span>8</span></div>-->
                <!--<div class="btm"><span>8</span></div>-->
                <!--<div style="transform: rotateX(0deg);" class="btm"><span>8</span></div>-->
                <!--</div>-->



            </div>
        </div>
    </div>
    <script type="text/javascript" src="/home/js/coinindex.js"></script>
    <script type="text/javascript" src="/home/js/tab.js"></script>
    <script type="text/javascript" src="/home/js/slide.js"></script>
    <script type="text/javascript" src="/home/js/hb_lang.js"></script>
    <script type="text/javascript" src="/home/js/hb_sea.js"></script>
    <script type="text/javascript" src="/home/js/hb_hm.js"></script>
    {{--<script src="%EF%BF%A52717.80%20_%20%E3%80%90%E7%81%AB%E5%B8%81%E3%80%91%E8%99%9A%E6%8B%9F%E8%B4%A7%E5%B8%81%E4%BA%A4%E6%98%93_%E6%AF%94%E7%89%B9%E5%B8%81%E4%BA%A4%E6%98%93%E5%B9%B3%E5%8F%B0_%E8%99%9A%E6%8B%9F%E5%B8%81%E8%8E%B1%E7%89%B9%E5%B8%81_%E6%AF%94%E7%89%B9%E5%B8%81%E5%AE%98%E7%BD%91_files/page_glb_var.js"></script>--}}
    <script>
        seajs.use("dist/page_index");
        /**/
        function change_vcode(e) {
            e.src = "/account/captcha?" + Math.random();
        }
        /**/

    </script>

    <script type="text/javascript">
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "//hm.baidu.com/hm.js?18784dd00dc1c9774528d08ae7943072";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
    <!-- 成功案例 -->
    <!--<div class="success-project">
        <div class="page-view">
             <div class="row">
               <p class="sp-title">众筹</p>
            </div>
            <div class="row" style="overflow: hidden;position: relative;">
                <span class="sp-prev"><img src="/home/images/suc_left_arrow.png"></span>
                <span class="sp-next"><img src="/home/images/suc_right_arrow.png"></span>

                <div class="sp-view">
                <volist name="issue_list" id="vo">
                    <div style="width: 400px;" class="sp-wrap">
                        <div class="sp-box">
                            <a href="{:U('Zhongchou/details',array('id'=>$vo['id']))}" target="_blank">
                                <div class="sp-info">
                                    <div class="pull-right pull-right1">
                                        <h4 style="font-weight: normal;font-size: 16px">{$vo['title']|mb_substr=###,0,8,'utf-8'}</h4>

                                        <p style="font-size: 12px;">{$vo['name']}</p>
                                    </div>
                                    <img src="{$vo['url1']}" style="width: 82px;height: 82px;">
                                    <!--                                    <img src="--><!--" style="width: 82px;height: 82px;">-->
    <!-- </div>
         </a>

         <div class="sp-step">
             <div class="pull-left sp-time">
                 <p><span>{$vo['end_time']|date="Y-m-d",###}</span></p>
                 <p><span>{$vo['add_time']|date="Y-m-d",###}</span></p>
                 <p><span>{$vo['ctime']|date="Y-m-d",###}</span></p>
             </div>
             <div class="pull-right">
                 <p>众筹完成</p>
                 <p>众筹开始</p>
                 <p>初创项目</p>
             </div>
         </div>
     </div>
 </div>
</volist>
 <!--<div style="width: 400px;" class="sp-wrap">-->
    <!--<div class="sp-box">-->
    <!--<a href="https://www.yuanbaohui.com/project_index/?id=209" target="_blank">-->
    <!--<div class="sp-info">-->
    <!--<div class="pull-right pull-right1">-->
    <!--<h4 style="font-weight: normal;font-size: 16px">第二次元宝币与太一股认...</h4>-->

    <!--<p style="font-size: 12px;">元宝币兑换太一股</p>-->
    <!--</div>-->
    <!--<img src="/home/images/c3aa0c2605711db444b4fbf2fd235b4a.png" style="width: 82px;height: 82px;">-->
    <!--&lt;!&ndash;                                    <img src="&ndash;&gt;&lt;!&ndash;" style="width: 82px;height: 82px;">&ndash;&gt;-->
    <!--</div>-->
    <!--</a>-->

    <!--<div class="sp-step">-->
    <!--<div class="pull-left sp-time">-->
    <!--<p><span>2015-08-12</span></p>-->
    <!--<p><span>2015-08-10</span></p>-->
    <!--<p><span>2015-07-30</span></p>-->
    <!--</div>-->
    <!--<div class="pull-right">-->
    <!--<p>众筹完成</p>-->
    <!--<p>众筹开始</p>-->
    <!--<p>初创项目</p>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!---->
    <!--<div style="width: 400px;" class="sp-wrap">-->
    <!--<div class="sp-box">-->
    <!--<a href="https://www.yuanbaohui.com/project_index/?id=201" target="_blank">-->
    <!--<div class="sp-info">-->
    <!--<div class="pull-right pull-right1">-->
    <!--<h4 style="font-weight: normal;font-size: 16px">首次元宝币与太一股（A...</h4>-->

    <!--<p style="font-size: 12px;">测试兑换</p>-->
    <!--</div>-->
    <!--<img src="/home/images/d5b42c5e728e4f80623198daa636fb73.png" style="width: 82px;height: 82px;">-->
    <!--&lt;!&ndash;                                    <img src="&ndash;&gt;&lt;!&ndash;" style="width: 82px;height: 82px;">&ndash;&gt;-->
    <!--</div>-->
    <!--</a>-->

    <!--<div class="sp-step">-->
    <!--<div class="pull-left sp-time">-->
    <!--<p><span>2015-06-23</span></p>-->
    <!--<p><span>2015-06-18</span></p>-->
    <!--<p><span>2015-06-17</span></p>-->
    <!--</div>-->
    <!--<div class="pull-right">-->
    <!--<p>众筹完成</p>-->
    <!--<p>众筹开始</p>-->
    <!--<p>初创项目</p>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--<div style="width: 400px;" class="sp-wrap">-->
    <!--<div class="sp-box">-->
    <!--<a href="https://www.yuanbaohui.com/project_index/?id=191" target="_blank">-->
    <!--<div class="sp-info">-->
    <!--<div class="pull-right">-->
    <!--<h4 style="font-weight: normal;font-size: 16px">OKDICE数学概率二...</h4>-->

    <!--<p style="font-size: 12px;">OKDICE是纯数学随机数的骰子游戏</p>-->
    <!--</div>-->
    <!--<img src="/home/images/b0548d0ea80e93784d36e03a004ba09e.png" style="width: 82px;height: 82px;">-->
    <!--&lt;!&ndash;                                    <img src="&ndash;&gt;&lt;!&ndash;" style="width: 82px;height: 82px;">&ndash;&gt;-->
    <!--</div>-->
    <!--</a>-->

    <!--<div class="sp-step">-->
    <!--<div class="pull-left sp-time">-->
    <!--<p><span>2015-05-18</span></p>-->
    <!--<p><span>2015-04-30</span></p>-->
    <!--<p><span>2015-04-27</span></p>-->
    <!--</div>-->
    <!--<div class="pull-right">-->
    <!--<p>众筹完成</p>-->
    <!--<p>众筹开始</p>-->
    <!--<p>初创项目</p>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--<div style="width: 400px;" class="sp-wrap">-->
    <!--<div class="sp-box">-->
    <!--<a href="https://www.yuanbaohui.com/project_index/?id=9" target="_blank">-->
    <!--<div class="sp-info">-->
    <!--<div class="pull-right">-->
    <!--<h4 style="font-weight: normal;font-size: 16px">元宝汇</h4>-->

    <!--<p style="font-size: 12px;">元宝网，第一数字货币众筹交易平台</p>-->
    <!--</div>-->
    <!--<img src="/home/images/917bbfbcc932a39eca211ec8e89d09d7.png" style="width: 82px;height: 82px;">-->
    <!--&lt;!&ndash;                                    <img src="&ndash;&gt;&lt;!&ndash;" style="width: 82px;height: 82px;">&ndash;&gt;-->
    <!--</div>-->
    <!--</a>-->

    <!--<div class="sp-step">-->
    <!--<div class="pull-left sp-time">-->
    <!--<p><span>2014-06-01</span></p>-->
    <!--<p><span>2014-05-01</span></p>-->
    <!--<p><span>2014-04-13</span></p>-->
    <!--</div>-->
    <!--<div class="pull-right">-->
    <!--<p>众筹完成</p>-->
    <!--<p>众筹开始</p>-->
    <!--<p>初创项目</p>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--<div style="width: 400px;" class="sp-wrap">-->
    <!--<div class="sp-box">-->
    <!--<a href="https://www.yuanbaohui.com/project_index/?id=48" target="_blank">-->
    <!--<div class="sp-info">-->
    <!--<div class="pull-right">-->
    <!--<h4 style="font-weight: normal;font-size: 16px">币涨投资基金</h4>-->

    <!--<p style="font-size: 12px;">实时监测元宝汇、btctrade、比特时代等涉及元宝币交易的正规平台交易行情</p>-->
    <!--</div>-->
    <!--<img src="/home/images/06df42e485da1f44cee4c8886ac2ed95.png" style="width: 82px;height: 82px;">-->
    <!--&lt;!&ndash;                                    <img src="&ndash;&gt;&lt;!&ndash;" style="width: 82px;height: 82px;">&ndash;&gt;-->
    <!--</div>-->
    <!--</a>-->

    <!--<div class="sp-step">-->
    <!--<div class="pull-left sp-time">-->
    <!--<p><span>2014-08-21</span></p>-->
    <!--<p><span>2014-07-21</span></p>-->
    <!--<p><span>2014-04-24</span></p>-->
    <!--</div>-->
    <!--<div class="pull-right">-->
    <!--<p>众筹完成</p>-->
    <!--<p>众筹开始</p>-->
    <!--<p>初创项目</p>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--<div style="width: 400px;" class="sp-wrap">-->
    <!--<div class="sp-box">-->
    <!--<a href="https://www.yuanbaohui.com/project_index/?id=187" target="_blank">-->
    <!--<div class="sp-info">-->
    <!--<div class="pull-right">-->
    <!--<h4 style="font-weight: normal;font-size: 16px">免费VPN币分发投资</h4>-->

    <!--<p style="font-size: 12px;">免费机会仅此一次</p>-->
    <!--</div>-->
    <!--<img src="/home/images/d09a0420c556744cc948a0cbc7cf21bd.png" style="width: 82px;height: 82px;">-->
    <!--&lt;!&ndash;                                    <img src="&ndash;&gt;&lt;!&ndash;" style="width: 82px;height: 82px;">&ndash;&gt;-->
    <!--</div>-->
    <!--</a>-->

    <!--<div class="sp-step">-->
    <!--<div class="pull-left sp-time">-->
    <!--<p><span>2015-04-17</span></p>-->
    <!--<p><span>2015-04-13</span></p>-->
    <!--<p><span>2015-04-12</span></p>-->
    <!--</div>-->
    <!--<div class="pull-right">-->
    <!--<p>众筹完成</p>-->
    <!--<p>众筹开始</p>-->
    <!--<p>初创项目</p>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!-- </div>
 </div>
</div>
</div> -->
    <!-- 客服信息 -->
    <div class="autobox">
        <ul class="web_service clear pl30">
            <li class="w265"><a id="BizQQWPA" href="http://wpa.qq.com/msgrd?v=3&uin={{$config['qq1']}}&site=qq&menu=yes"><div class="web_service_pic service_1"></div><div class="web_service_pic_num"><p>{{!empty($config['qq1'])? $config['qq1']:"暂无"}}</p><div class="qqsecvice">在线QQ客服</div></div></a></li>
            <li class="w245"><div class="web_service_pic service_2"></div><div class="web_service_pic_num"><p>{{!empty($config['tel'])? $config['tel']:"暂无"}}</p><div>工作日:9-19时 节假日:9-18时</div></div></li>
            <li class="w265"><a href="http://weibo.com/{{$config['weibo']}}" target="_blank"><div class="web_service_pic service_3"></div><div class="web_service_pic_num"><p>{{!empty($config['weibo'])? $config['weibo']:"暂无"}}</p><div>新浪官方微博</div></div></a></li>
            <li><div class="web_service_pic service_4"></div><div class="web_service_pic_num"><p>2群：{{!empty($config['qqqun2'])? $config['qqqun2']:"暂无"}}</p><div class="h_underl">交流QQ群<!--  <a href="javascript:;" class="orange">查看更多</a>   --></div></div></li>
        </ul>
    </div>



    <div class="safety_tips">
        <div style="border-top:#d8d8d8 dotted 1px;width: 1000px; margin: 0 auto; margin-bottom: 20px;"></div>
        <h3>专业的安全保障</h3>
        <div class="autobox">
            <ul class="safety_tips_ul clear">
                <li>
                    <img src="/home/images/safe_1.jpg" alt="" height="70" width="70">
                    <h4>系统可靠</h4>
                    <p>银行级用户数据加密、动态身份验证，多级风险识别控制，保障交易安全</p>
                </li>
                <li>
                    <img src="/home/images/safe_2.jpg" alt="" height="70" width="70">
                    <h4>资金安全</h4>
                    <p>钱包多层加密，离线存储于银行保险柜，资金第三方托管，确保安全</p>
                </li>
                <li>
                    <img src="/home/images/safe_3.jpg" alt="" height="70" width="70">
                    <h4>快捷方便</h4>
                    <p>充值即时、提现迅速，每秒万单的高性能交易引擎，保证一切快捷方便</p>
                </li>
                <li>
                    <img src="/home/images/safe_4.jpg" alt="" height="70" width="70">
                    <h4>服务专业</h4>
                    <p>专业的客服团队，400电话和24小时在线QQ，VIP一对一专业服务</p>
                </li>
            </ul>
        </div>
    </div>
    <!--友情链接-->
    <div class="link">
        <div class="linkbox">
            <h4>友情链接</h4>
            <ul>
                @foreach($link_info as $k=>$v)
                    <li><a target="_blank" href="http://{{$v['url']}}" style=" font-size:16px;">{{$v['name']}}</a> </li>
                @endforeach

            </ul>
        </div>
    </div>

    <!--footer start-->
    {{--<include file="App/Home/View/Public/footer.html"/>--}}
    <!--footer end-->
@endsection
@section('footer')
    <script>
        $(function() {
            $(".flexslider").flexslider({
                directionNav: true,
                pauseOnAction: false
            });
        });

    </script>
@endsection
