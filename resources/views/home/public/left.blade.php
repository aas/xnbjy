<div id="my_menu" class="sdmenu left">
		<div>
			<p><i class="iconfont">&#xe61e;</i>&nbsp;我的资产</p>
						<a href="{{urlHome('User','getIndex')}}" class="menu"><b class="ic-uc"></b><i class="iconfont">&#xe6f6;</i>&nbsp;&nbsp;账户资产</a>
                        <a href="{{urlHome('Finance','getIndex')}}" class="menu13"><b class="ic-uc"></b><i class="iconfont">&#xe93e;</i>&nbsp;&nbsp;财务日志</a>
						<a href="{{urlHome('FillByBank','getIndex')}}" class="menu15"><b class="ic-uc"></b><i class="iconfont">&#xe602;</i>&nbsp;&nbsp;在线网银支付</a>
                        <a href="{{urlHome('Fill','getIndex')}}" class="menu138"><b class="ic-uc"></b><i class="iconfont">&#xe93e;</i>&nbsp;&nbsp;网银支付日志</a>
					</div>
		<div style="position:relative;">
			<p><i class="iconfont">&#xe643;</i>&nbsp;我的交易</p>
						<a href="{{urlHome('Entrust','getManage')}}" class="menu2"><b class="ic-uc"></b><i class="iconfont">&#xe616;</i>&nbsp;&nbsp;委托管理</a>
						<a href="{{urlHome('Trade','getIndex')}}" class="menu3"><b class="ic-uc"></b><i class="iconfont">&#xe65b;</i>&nbsp;&nbsp;我的成交</a>
						<a href="{{urlHome('Entrust','getHistory')}}" class="menu4"><b class="ic-uc"></b><i class="iconfont">&#xe62d;</i>&nbsp;&nbsp;委托历史</a>
						
						<a href="{{urlHome('User','getZhongchou')}}" class="menu6"><b class="ic-uc"></b><i class="iconfont">&#xe60b;</i>&nbsp;&nbsp;我的众筹</a>
					</div>
                    <div>
			<p><i class="iconfont">&#xe60e;</i>&nbsp;安全中心</p>
            		
						<!--<a href="{:U('Safe/index')}" class="menu11"><b class="ic-uc"></b><i class="iconfont">&#xe649;</i>&nbsp;&nbsp;安全中心</a>-->
                        <a href="{{urlHome('User','getUpdatePassword')}}" class="menu8"><b class="ic-uc"></b><i class="iconfont">&#xe638;</i>&nbsp;&nbsp;修改密码</a>
                        <a href="{{urlHome('User','getUpdateMessage')}}" class="menu7"><b class="ic-uc"></b><i class="iconfont">&#xe649;</i>&nbsp;&nbsp;个人信息</a>
						<!--<a href="{:U('Safe/mobilebind')}" class="menu12"><b class="ic-uc"></b><i class="iconfont">&#xe609;</i>&nbsp;&nbsp;手机绑定</a>-->
					  </div>
				  <div>
			<p><i class="iconfont">&#xe611;</i>&nbsp;账户中心</p>
			            <a href="{{urlHome('User','getPay')}}" class="menu12"><b class="ic-uc"></b><i class="iconfont">&#xe620;</i>&nbsp;&nbsp;人民币充值</a>
			            <a href="{{urlHome('User','getDraw')}}" class="menu14"><b class="ic-uc"></b><i class="iconfont">&#xe6f0;</i>&nbsp;&nbsp;人民币提现</a>
            		    <a href="{{urlHome('Safe','getIndex')}}" class="menu11"><b class="ic-uc"></b><i class="iconfont">&#xe660;</i>&nbsp;&nbsp;用户中心</a>
						<a href="{{urlHome('User','getInvit')}}" class="menu9"><b class="ic-uc"></b><i class="iconfont">&#xe602;</i>&nbsp;&nbsp;邀请好友</a>
						<a href="{{urlHome('User','getSysmessage')}}" class="menu10"><b class="ic-uc"></b><i class="iconfont">&#xe664;</i>&nbsp;&nbsp;系统消息@if(isset($newMessageCount))<span class="messagenum">{{$newMessageCount}}</span>@endif</a>



					  </div>
		</div>