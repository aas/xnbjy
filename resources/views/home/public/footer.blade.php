<style>
    .rightwidth {
        width: 340px;
    }
</style>
<!--footer start-->

<div class="coin_footer" style="position:relative;">
    <div class="coin_hint">

        <h2>{{!empty($config['title'])? $config['title']:"风险提示"}}</h2>
        <p>{{$config['risk_warning']}}</p>
    </div>
    <div class="coin_footerbar">
        <div class="coin_footer_nav clearfix">
            <div class="coin_nav coin_copy left">
                <p><a href="{{urlHome('index','getIndex')}}"><img style=" height:55px;" src="{{$config['logo']}}"></a>
                </p>
            </div>
            <div class="coin_nav left">
                <h2>{{!empty($config['name'])? $config['name']:"虚拟币"}}团队</h2>
                <ul>
                    <li><a href="#">{{!empty($config['name'])? $config['name']:"虚拟币"}}</a></li>
                    @if(isset($team))
                        @foreach($team as $v)
                            <li><a href="#" target="_blank" class="left">{{$v['title']}}</a></li>
                        @endforeach
                    @endif
                </ul>
            </div>
            <div class="coin_nav left">
                <h2>帮助中心</h2>
                <ul>
                    @if(isset($help))
                        @foreach($help as $v)
                            <li><a href="#" target="_blank" class="left">{{$v['name']}}</a></li>
                        @endforeach
                    @endif
                </ul>
            </div>
            <div class="coin_nav coin_nav02 left">
                <h2 class="clearfix"><span class="left">联系我们</span><a href="http://weibo.com/{{$config['weibo']}}"
                                                                      target="_blank" class="coin_sina left"></a>
                    <!--<a href="#" id="coin_weixin" class="coin_wei left"></a>--></h2>
                <ul>
                    <li>客服电话：{{!empty($config['tel'])? $config['tel']:'暂无'}}</li>
                    <li>客服QQ：{{!empty($config['qq1'])? $config['qq1']:'暂无'}}</li>
                    <li>
                        <a href="mailto:{{$config['email']}}">客服邮箱：{{!empty($config['email'])? $config['email']:'暂无'}}</a>
                    </li>
                    <li>
                        <a href="mailto:{{$config['business_email']}}">业务合作：{{!empty($config['business_email'])? $config['business_email']:'暂无'}}</a>
                    </li>
                </ul>
            </div>
            <div class="coin_nav coin_nav02 left rightwidth" style="position:relative;">
                <div style="float:left; padding-top:25px; padding-left:10px;"><img style=" width:100px;" src=""/></div>
                <div style=" float:left; padding-left:10px;">
                    <p class="coin_phone400">{{!empty($config['tel'])? $config['tel']:'暂无'}}</p>
                    <p class="coin_phoneqq"><a
                                href="http://wpa.qq.com/msgrd?v=3&uin={{$config['qq1']}}&site=qq&menu=yes"
                                target="_blank">在线客服</a></p>
                    <p>工作日:9-19时 节假日:9-18时</p>
                </div>
                <div class="group" style="left:12px;margin-top: 40px">
                    <ul class="qq_all" style="    margin-left: 10px;">
                        <li><a style="flont:left; "
                               href="javascript:void(0)">{{!empty($config['name'])? $config['name']:"虚拟币"}}官方群<img
                                        style="margin-top:5px;" src="/home/images/xiala.png"></a>
                            <ul style="margin-left: 105px;">
                                <li>{{!empty($config['name'])? $config['name']:"虚拟币"}}
                                    官方1群 {{!empty($config['qqqun1'])? $config['qqqun1']:"暂无"}}</li>
                                <li>{{!empty($config['name'])? $config['name']:"虚拟币"}}
                                    官方2群 {{!empty($config['qqqun2'])? $config['qqqun2']:"暂无"}}</li>
                                <li>{{!empty($config['name'])? $config['name']:"虚拟币"}}
                                    官方3群 {{!empty($config['qqqun3'])? $config['qqqun3']:"暂无"}}</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_aq">
        <p>{{!empty($config['copyright'])? $config['copyright']:"暂无"}}</p>
        <p>{{!empty($config['record'])? $config['record']:"暂无"}}</p>
        <ul class="footerSafety clearfix">
            <li class="safety02"><a href="http://net.china.com.cn/" target="_blank"></a></li>
            <li class="safety03"><a href="http://webscan.360.cn/index/checkwebsite/?url={{$config['localhost']}}"
                                    target="_blank"></a></li>
            <li class="safety04"><a href="http://www.cyberpolice.cn/wfjb/" target="_blank"></a></li>
        </ul>
    </div>
    <div id="weixin" style="position:absolute; bottom:88px; left:50%; margin-left:170px; display:block;">
        <!--<img src="{$config.logo}">--></div>
    <script>
        $('#coin_weixin').mouseover(function () {
            $('#weixin').show();
        }).mouseout(function () {
            $('#weixin').hide();
        });
    </script>
    <!--footer end-->
    <script type="text/javascript" src="/home/js/gotop.js"></script>
    <script type="text/javascript" src="/home/js/link.js"></script>
    <script type="text/javascript" src="/home/js/slides.js"></script>


</div>
</body>
</html>