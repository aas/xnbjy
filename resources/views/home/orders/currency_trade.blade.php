@extends('home/mainlayout')
@section('content')
    <style>
        .coin_num td input {
            border: 1px solid #e55600;
            color: #e55600;
            border-radius: 3px;
            width: 80px;
            height: 30px;
            margin: 5px;
            cursor: pointer;
        }

        .coin_num td input:hover {
            border: 1px solid #e55600;
            border-radius: 3px;
            width: 80px;
            height: 30px;
            margin: 5px;
            cursor: pointer;
            color: #FFF;
            background-color: #e55600;
        }
    </style>
    <!--top end-->
    <link rel="shortcut icon" href="http://www.jubi.com/favicon.ico" type="image/x-icon">
    {{--<script src="/home/js/script.js"></script>--}}
    <link rel="stylesheet" type="text/css" href="/home/css/jb_font-awesome.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
    <div id="main">
        <div class="main_box">
            <!--币币交易开始-->
            <div id="tags_coin" class="coinarea left" style="position:relative;">
                <!--<ul id="tags" class="for_coin">
                    <li class="selectTag"><a onmouseover="selectTag('tagContent0',this)">对CNY交易区</a></li>
                    <li><a onmouseover="selectTag('tagContent3',this)">对TRMB交易区</a></li>
                    </ul>
                -->
                <div class="bgcolor" style="display:none;"></div>
                <div style="margin-top:30px;" id="tagContent">
                    <!-- 对CNY交易区 结束-->
                    <div class="tagContent selectTag" id="tagContent0">
                        <p style="color:#f00; font-size:14px; margin-bottom:10px;">{!! $config['friendship_tips'] !!}</p>
                        <table class="coin_list coinarea" border="0" cellpadding="0" cellspacing="0">
                            <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th style="text-align:left;">币名</th>
                                <th class="header">最新价格(CNY)</th>
                                <th class="header">24H成交量</th>
                                <th class="header">24H成交额(CNY)</th>
                                <th class="header">总市值(CNY)</th>
                                <th class="header">24H涨跌</th>
                                <th class="header">7D涨跌</th>
                                <th class="header" style="padding-right: 60px;">去交易</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($currency['data'])
                                @foreach($currency['data'] as $k=>$v)
                                    <tr class="coin_num">
                                        <td><a href="#"><img src="{{$v['currency_logo']}}"
                                                             style="width:20px; height:20px;"></a></td>
                                        <td class="coin_name"><a
                                                    href="#">{{!empty($v['currency_name'])? $v['currency_name']:'虚拟币'}}{{!empty($v['currency_mark'])? $v['currency_mark']:''}}</a>
                                        </td>
                                        <td>@if($v['new_price_status'] == 0)<a href="#" class="buy">@else<a href="#"
                                                                                                            class="sell">@endif
                                                    {{!empty($v['new_price'])? $v['new_price']:'暂无'}}@if($v['new_price_status'] == 0)
                                                        ↓@else↑@endif</a></td>
                                        <td><a href="#">{{!empty($v['24H_done_num'])? $v['24H_done_num']:'0'}}</a></td>
                                        <td><a href="#">{{!empty($v['24H_done_money'])? $v['24H_done_money']:'0'}}</a>
                                        </td>
                                        <td>
                                            <a href="#">{{!empty($v['currency_all_money'])? $v['currency_all_money']:'0'}}</a>
                                        </td>
                                        <td>@if($v['24H_change'] > 0 )<a href="#" class="sell">@else<a href="#"
                                                                                                       class="buy">@endif{{!empty($v['24H_change'])? $v['24H_change']:'0'}}
                                                    %</a></td>
                                        <td>@if($v['7D_change'] > 0)<a href="#" class="sell">+@else<a href="#"
                                                                                                      class="buy">@endif{{!empty( $v['7D_change'])? $v['7D_change']:'0'}}
                                                    %</a></td>
                                        <td style="padding-right: 10px;"><a
                                                    href="{{urlHome('orders','getIndex',['currency'=>$v['currency_mark']])}}"><input
                                                        value="去交易" type="button"></a></td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div style="width: 1200px; clear: both; margin: 0px auto;">
                        <div class="page" style="min-width: 250px !important;margin-left: 500px;">
                            @if($currency['last_page']>1)
                                <div>
                                    @if($currency['prev_page_url'])
                                        <a class="prev" href="{{$currency['prev_page_url']}}">&lt;&lt;</a>
                                    @endif
                                    @if(count($currency['pageNoList']))
                                        @foreach($currency['pageNoList'] as $v)
                                            @if($currency['current_page'] == $v)
                                                <span class="current">{{$v}}</span>
                                            @else
                                                <a class="num" href="{{urlHome('orders','getCurrencyTrade',['page'=>$v],2)}}">{{$v}}</a>
                                            @endif
                                        @endforeach
                                    @endif
                                    @if($currency['next_page_url'])
                                        <a class="next" href="{{$currency['next_page_url']}}">&gt;&gt;</a>
                                    @endif
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <!--币币交易结束-->
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    @endsection
    <!--footer start-->
    <!--footer end-->

