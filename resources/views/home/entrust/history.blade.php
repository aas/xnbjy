@include('home.public.header')
<style>

.filtrate{
border: 1px solid #e55600;
color:#e55600;
border-radius: 3px;
width: 60px;
height: 28px;
margin: 5px;
cursor: pointer;
}
.filtrate:hover{
border: 1px solid #e55600;
border-radius: 3px;
width: 60px;
height: 28px;
margin: 5px;
cursor: pointer;
color:#FFF;
background-color:#e55600;
}

</style>
    <!--top end-->
<div id="main">
	<div class="main_box">
		@include('home.public.left')
		<div class="raise right clearfix">
            <script src="/home/js/coinindex.js"></script>
<div class="ybc_list">
	<div class="ybcoin clearfix">
		<h2 class="left">委托历史</h2> 
		<div class="right">
			<form action="{{urlHome('Entrust','getHistory')}}" method="get">
                <select id="coninname" name="currency">
					@if($currency)
						@foreach($currency as $v)
							<option value="{{$v['currency_id'] or ""}}">{{$v['currency_name']}}
								对{{$v['currency']['currency_name']}}</option>
						@endforeach
					@endif
                </select>
                <select id="status" style="margin-left:0; width:80px;" name="status">
                    
                     <option value="2">已成交</option>
                     <option value="-1">已撤销</option>
                     <option value="2,-1" selected="selected">全部</option>
                </select>
                <input value="提交" class="filtrate" type="submit">
			</form>
        </div>
	</div>
		<table class="raise_list" style="border:1px solid #e1e1df;" align="center" border="0" cellpadding="0" cellspacing="0">
	 		<thead>
				<tr>
					<th>委托时间</th>
					<th>币种</th>
					<th>类型</th>
					<th>委托数量</th>
					<th>委托价格</th>
					<th>成交数量</th>
					<th>尚未成交</th>
					<th>状态</th>
			
				</tr>
				</thead>
				<tbody>
				@if($rows['data'])
					@foreach($rows['data'] as $v)
						<tr>
							<td>{{date('Y-m-d H:i:s',$v['add_time'])}}</td>
							<td>{{$v['currency']['currency_name']}}</td>
							@if($v['type'] == 'sell')
								<td class="red">
							@else
								<td style="color:#039904;">
									@endif
									{{fomatOrdersType($v['type'])}}
								</td>
								<td>{{floatval($v['num'])}}</td>
								<td>{{floatval($v['price'])}}</td>
								<td>{{floatval($v['trade_num'])}}</td>
								<td>{{floatval($v['num']- $v['trade_num'])}}</td>
								<td>{{formatOrdersStatus($v['status'])}}</td>
						</tr>
					@endforeach
				@endif
				</tbody>
              </table>
              <div class="page" style=" min-width:200px !important; float:right;">
				  @if($rows['last_page']>1)
					  <div>
						  @if($rows['prev_page_url'])
							  <a class="prev" href="{{$rows['prev_page_url']}}">&lt;&lt;</a>
						  @endif
						  @if(count($rows['pageNoList']))
							  @foreach($rows['pageNoList'] as $v)
								  @if($rows['current_page'] == $v)
									  <span class="current">{{$v}}</span>
								  @else
									  <a class="num" href="{{urlHome('entrust','getHistory',['page'=>$v],2)}}">{{$v}}</a>
								  @endif
							  @endforeach
						  @endif
						  @if($rows['next_page_url'])
							  <a class="next" href="{{$rows['next_page_url']}}">&gt;&gt;</a>
						  @endif
					  </div>
				  @endif
              </div>
                            
</div>
		</div>
		<div class="clear"></div>
	</div>
</div>  
<script>
$(".menu4").addClass("uc-current");
</script>
@include('home.public.footer')