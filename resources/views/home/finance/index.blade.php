@include('home.public.header')
    <!--top end-->
	<style>
	table tr td{border-bottom:1px solid #e1e1df;}	
	</style>
	<style>
.raise_list tr:hover{
	background-color:#FFE9D2;
	}
	
</style>
<div id="main">
	<div class="main_box">
		@include('home.public.left')
		<div class="raise right clearfix">
            <div class="ybc_list">
	<div class="ybcoin clearfix">
		<h2 class="left">财务日志</h2>
		
	</div>
		<table class="raise_list" style="border:1px solid #e1e1df;" id="Transaction" align="center" border="0" cellpadding="0" cellspacing="0">
			<thead>
			<tr>
				<th>类型</th>
				<th>说明</th>
				<th>变动资金</th>
				<th>支出/收入</th>
				<th>币种</th>
				<th>成交时间</th>
			</tr>
			</thead>
			<tbody>
			@foreach($list['data'] as $v)
			<tr>
				<td>{{$v['typename']}}</td>
				<td>{{$v['content']}}</td>
				<td>{{$v['money']}}</td>
				<td>
					@if($v['money_type'] == 1)
					<span style="color:#393">收入</span>
					@else
					<span style="color:#e55600">支出</span>
					@endif
				</td>
				<td>{{$v['currency_name']}}</td>
				<td>{{date('Y-m-d',$v['add_time'])}}</td>
			</tr>
			@endforeach
			</tbody>
									
		</table>
			 <div class="page" style="min-width:200px !important; float:right;">
					@if($list['last_page']>1)
						<div>
							<a class="prev" href="{{$list['prev_page_url']}}"><<</a>
							@foreach($list['pageNoList'] as $v)
								<a @if($v == $list['current_page'])
								   class="current"
								   @else
								   class="num"
								   @endif
								   href="{{urlHome('Finance','getIndex')}}?page={{$v}}">{{$v}}</a>
							@endforeach
							<a class="next" href="{{$list['next_page_url']}}">>></a>
						</div>
					@endif
              </div>
	</div>
		</div>
		<div class="clear"></div>
	</div>
</div> 
<script>
$(".menu13").addClass("uc-current");
</script>   
<!--footer start-->
@include('home.public.footer')
<!--footer end-->

