
@extends('home.mainlayout')
@section('content')
<link rel="stylesheet" type="text/css" href="/home/css/reset.css">
<link rel="stylesheet" type="text/css" href="/home/css/zcpc.css">
<script src="/home/js/zc_hm.js">
</script><script type="text/javascript" src="/home/js/zc_jquery.js"></script>
<script type="text/javascript" src="/home/js/zc_common.js"></script>
<script type="text/javascript" src="/plugin/Js/layer/layer.min.js"></script>
<style type="text/css">
.jinduline{z-index: 3;}
 .vip_header1 {
        background: #fafafa;
    }
    .vip_user1 {
        position: relative;
        font-size:16px;
        padding:10px 0px 10px 20px;
    }
    .vip_stop1{
        line-height:41px;
    }
    .vip_upgrade ul li{
        line-height:40px;
    }
    .vip_upgrade .left {
        text-align: right;
        width: 405px;
    }
    .vip_upgrade .right {
        width: 575px;
    }
    .vip_upgrade .right a {
        color:#F60;
    }
    .vip_upgrade .right a:hover {
        color:#F90;
    }
    input{ height:25px;}
    .jilu table td{ text-align:center;}
    .pay-m-2 a:hover{ color:#fff !important;}

</style>
<!--top end-->
<div class="acountbd">
    <div class="main clearfix">
        <h1 class="programheadtitle">[认购]{{$list->title or'认购项目'}}</h1>
        <div class="pcdeleft">
            <div class="pcdetpic">
                <img style=" width:830px; height:445px;" src="{{$list->url2 or ''}}" id="sim229784" class="main_img">
            </div>
            <div class="pcdetinfotxt">
                <ul class="pcinfotxtnav">
                    <li data-id="1" class="current">项目详情</li>
                    <li data-id="3">项目进展</li>
                    <li data-id="2">文件下载</li>
                </ul>
                <div id="info_1" class="infodata" style="display:block;">
                    {{$list->info or '此项目暂无详情简介'}}
                </div>
                <div id="info_2" class="infodata" style="display:none;height:100px;">
                 @if(!empty($list->wenjian_url))
                        <a  href="{{$list->wenjian_url}}">
                            <input type="button"  value="文件下载"  style="border:1px solid #999;text-align:center;padding:5px 10px 5px 10px;height: 30px;margin-left:100px; margin-top:30px; background:#f70; color: #fff; border-radius: 5px;
"  />
                        </a>
                    @endif
                </div>
                <div id="info_3" class="infodata">
                    <div style="height: 540px;" class="jindubox">

                        <div class="jinduline"></div>
                        <div class="jindustate"><span>@if($list->status == 0)项目未开始@elseif($list->status == 1)项目进行中...@elseif($list->status == 2)项目完成@endif</span></div>
                        <div class="jinduitem">
                                @if($list->status > 1)
                                <div style="right: 50%; bottom: 305px; margin-right: 1px; padding-right: 50px;" class="jditem jditem4" data-item="4">
                                    <i style="right: 0px; background-image: url(&quot;https://imgm.touchouwang.net/public/p_image/jingdu_2.png&quot;);" class="danshu">04</i>
                                    <h6 style="padding-top:10px;">认购成功</h6>
                                    <p>{{date('Y-m-d',$list->end_time)}}</p>
                                </div>
                                @endif
                                @if($list->status > 1)
                                    <div style="left: 50%; bottom: 245px; margin-left: 1px; padding-left: 50px;" class="jditem jditem3" data-item="3">
                                    <i style="left: 0px;" class="danshu">03</i>
                                    <h6 style="padding-top:10px;">认购结束</h6>
                                    <p>{{date('Y-m-d',$list->end_time)}}</p>
                                    </div>
                                @endif
                                @if($list->status > 0)
                                    <div style="right: 50%; bottom: 185px; margin-right: 1px; padding-right: 50px;" class="jditem jditem2" data-item="2">
                                    <i style="right: 0px; background-image: url(&quot;https://imgm.touchouwang.net/public/p_image/jingdu_2.png&quot;);" class="danshu">02</i>
                                    <h6 style="padding-top:10px;">认购进行中</h6>
                                    <p>{{date('Y-m-d',$list->end_time)}}</p>
                                </div>
                                 @endif
                            <div style="left: 50%; bottom: 125px; margin-left: 1px; padding-left: 50px;" class="jditem jditem1" data-item="1">
                                <i style="left: 0px;" class="danshu">01</i>
                                <h6 style="padding-top:10px;">认购开启</h6>
                                <p>{{date('Y-m-d',$list->add_time)}}</p>
                            </div>
                            <div class="jindustate" style="top: 450px;">
                                <span>初创项目
                                    <p style="text-align:center">
                                        {{!empty($list->ctime)? date('Y-m-d',$list->ctime):''}}
                                    </p>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pcderight">

            <div class="programinfo">
                <h2>项目基本资料<i></i></h2>
                <table class="actab5">
                    <tbody>
                    <tr>
                        <td>总量：<span>{{$list->num or '0.00'}}个</span></td>
                        <td>剩余：<span>{{$list->deal or '0.00'}}个</span></td>
                    </tr>
                    {{--<notempty name="Think.session.USER_KEY_ID">--}}
                    @if(!empty(session('USER_KEY_ID')))
                        <tr>
                            <td>限购：<span>{{$list->limit_count or '0.00'}}次</span></td>
                            <td>已购：<span>{{$list->buy_count or '0'}}次</span></td>
                        </tr>
                        <tr>
                            <td>限购：<span>{{$list->limit or '0.00'}}个</span></td>
                            <td>已购：<span>{{$num_buy or '0.00'}}个</span></td>
                        </tr>
                        @endif
                    {{--</notempty>--}}
                    <tr>
                        <td>购买币种：<span>{{$list->buy_name}}</span></td>
                        <td>价格：<span>{{$list->price or '0.00'}}</span></td>
                    </tr>
                    <tr>
                        <td>成功比例：<span>{{$list->zhongchou_success_bili or '100'}}%</span></td>
                        <td><div class="yuyuebox_1">
                    		<div class="pclastdate2">
                        	<p class="pcfenpost pcfenpost2">
                                @if($list->status == 0)<span class="pctimeico">尚未开始</span>@endif
                                @if($list->status == 1)<span class="pctimeico">认购中</span>@endif
                                @if($list->status == 2)<span class="pctimeico">已结束</span>@endif
                        	</p>
                    		</div>
                			</div>
                          </td>  
                    </tr>
                    </tbody></table>
               
            </div>
            @if($list->status == 0)
                <div class="programinfo programinfo2">
                    <h2>我要投资</h2>
                    <div class="pay-m-2">
                        <button type="button" class="btn-play-1" disabled="disabled">认购未开始</button>
                        <p>该项目已<strong>暂未开放</strong>不支持购买，请关注其他项目</p>
                    </div>
                </div>
           @endif
    
            @if($list->status == 1)
                @if(!empty(session('USER_KEY_ID')))
                <div class="programinfo programinfo2">
                        <h2>我要投资</h2>
                        <form action="{{urlHome('zhongchou','postRun')}}" method="post" id="buy_form">
                            {{csrf_field()}}
                            <div class="pay-m-1">
                                <p>
                                    <ins>账户余额：</ins>
                                    <span id="myMon">{{floatval($buy_num)}}</span>{{$list->buy_mark}}
                                    <a href="{{urlHome('user','getIndex')}}" class="pay-recharge-btn">充值</a>
                                </p>
                                <p>
                                    <ins>认购数量：</ins>
                                      <input value="{{$list->min_limit or '0'}}" class="txt-pay-1" id="num" name="num" type="number" onkeyup="sjmoney();"><span style="font-size:10px;">&nbsp;&nbsp;个(最少购买{{number_format($list->min_limit) or 0}}个)</span>
                                </p>
                         
                                <p>
                                    <ins>实际支付金额：</ins>
                                    <span id="manjianhou">{{number_format($list->min_limit*$list->price) or 0}}</span>{{$list->buy_mark}}
                                </p>
                            </div>
                            <div class="pay-m-2">
                                <input type="button" class="btn-play-1" id="zhongchourun" onclick="zhongchou();" value="众筹">
                            </div>
                        </form>
                    </div>
                    @else
                    <div class="programinfo programinfo2">
                        <h2>我要投资</h2>
                        <div class="pay-m-1">
                            <p>
                                <ins>账户余额：</ins>
                                暂未登录，<a href="{{urlHome('login','getIndex')}}">登录</a> 后查看
                            </p>
                        </div>

                    </div>
                @endif
            @endif
    
            @if($list->status == 2)
                <div class="programinfo programinfo2">
                    <h2>我要投资</h2>
                    <div class="pay-m-2">
                        <button type="button" class="btn-play-1" disabled="disabled">认购结束</button>
                        <p>该项目已<strong>完成</strong>不支持购买，请关注其他项目</p>
                    </div>
                </div>
           @endif
            @if($list->status == 3)
                <div class="programinfo programinfo2">
                    <h2>我要投资</h2>
                    <div class="pay-m-2">
                        <button type="button" class="btn-play-1" disabled="disabled">认购结束</button>
                        <p>该项目已<strong>完成</strong>不支持购买，请关注其他项目</p>
                    </div>
                </div>
          @endif
    
            @if(!empty(session('USER_KEY_ID')))
                <div class="programinfo programinfo2 jilu">
                    <h2>投资记录</h2>
                    <table style=" width:348px; margin-top:10px;">
                        <thead>
                        <tr style=" height:25px; background-color:#f60; color:#FFF;">
                            <th>次数</th>
                            <th>金钱</th>
                            <th>数量</th>
                            <th>总金额</th>
                        </tr>
                        </thead>
                        <tbody>
                       @foreach($log as $k=>$v)
                            <tr>
                                <td>{{$k}}</td>
                                <td>{{$v['price'] or '0.00'}}</td>
                                <td>{{$v['num'] or '0.00'}}</td>
                                <td>{{$v['count'] or '0.00'}}</td>
                            </tr>
                       @endforeach
                        </tbody>
                    </table>
                </div>
               @else
                <div class="programinfo programinfo2">
                    <h2>投资记录</h2>
                    <p class="pay-m-1" style="text-align:center;">暂未登录，<a href="{{urlHome('login','getIndex')}}">登录</a> 后查看</p>
                </div>
           @endif
        </div>
    </div>
</div>
@endsection
@section('footer')
<script>
    $(".menu6").addClass("uc-current");
    $(".pcinfotxtnav").children("li").on("click",function(){
        var n =$(this).index();
        $(this).addClass("current").siblings().removeClass("current");
        if(n==0){
            $("#info_1").show();
            $("#info_2").hide();
            $("#info_3").hide();
        }
        if(n==1){
            $("#info_3").show();
            $("#info_1").hide();
            $("#info_2").hide();
        }
        if(n==2){
            $("#info_2").show();
            $("#info_1").hide();
            $("#info_3").hide();
        }
    });
</script>
<script>
    function zhongchou(){
        var limit = {{$list->limit}};
        var deal = {{$list->deal}};
        var price = {{$list->price}};
        var min_limit = {{$list->min_limit}};
        var id = {{$id}};
        var buy_currency_id = {{$list->buy_currency_id}};
       layer.confirm('确定认筹？', {
  btn: ['确定','取消'], //按钮
  title: '提示'
}, function(){
           if($("#num").val() < min_limit){
               layer.msg("认筹数量不能小于最小认筹数量!");
               $("#num").val("");
               return false;
           }
           if($("#num").val()>deal){
               layer.msg("认筹数量不能超过剩余数量");
               $("#num").val("");
               return false;

           }
           if($("#num").val()>limit){
               layer.msg("认筹数量不能超过限购数量");
               $("#num").val("");
               return false;

           }
  $.post("{{urlHome('zhongchou','postRun')}}",{num:$("#num").val(),id:id,buy_currency_id:buy_currency_id,'_token':"{{csrf_token()}}"},function(data){
            layer.msg(data.info);
            setTimeout(function () {
                window.location.reload();
            }, 2000);
        })
}, function(){
  layer.msg('已取消');
});
    }
    function sjmoney(){
        var price = {{$list->price}};
        $("#manjianhou").text($("#num").val()*price);
    }
</script>
@endsection
<!--footer start-->
<!--footer end-->
<!--footer end-->