
@extends('home/mainlayout')
@section('content')
<link rel="stylesheet" type="text/css" href="/home/css/reset.css">
<script src="/home/js/zc_hm.js">
</script><script type="text/javascript" src="/home/js/zc_jquery.js"></script>
<script type="text/javascript" src="/home/js/zc_common.js"></script>
<style type="text/css">
    .vip_header1 {
        background: #fafafa;
    }
    .vip_user1 {
        position: relative;
        font-size:16px;
        padding:10px 0px 10px 20px;
    }
    .vip_stop1{
        line-height:41px;
    }
    .vip_upgrade ul li{
        line-height:40px;
    }
    .vip_upgrade .left {
        text-align: right;
        width: 405px;
    }
    .vip_upgrade .right {
        width: 575px;
    }
    .vip_upgrade .right a {
        color:#F60;
    }
    .vip_upgrade .right a:hover {
        color:#F90;
    }
    input{ height:25px;}
	.pcfenpost1{
		width:300px;}
</style>
<!--top end-->
<div id="programlist" class="programlistbox pclistnav">
    <!--<h1 class="pcheadertitle">认购项目列表</h1>-->
        @foreach($result as $k=>$v)
        <div class="positiononly clearfix">
           @if($v->status == 0)<h2 class="pctips state1"><span>敬请期待</span></h2>@endif                          @if($v->status == 1)<h2 class="pctips state2"><span>认购中</span></h2>@endif
           @if($v->status == 2)<h2 class="pctips state3"><span>认购成功</span></h2>@endif
           @if($v->status == 3)<h2 class="pctips state3"><span>认购成功</span></h2>@endif
            <div class="pcprpic">
                <a href="{{urlHome('zhongchou','getDetails',['id'=>$v->id],2)}}">
                    <img src="{{!empty($v->url1)? $v->url1:''}}" alt="{{!empty($v->title)? $v->title:'认购项目'}}">
                </a>
            </div>
            <div class="rightbox">
                <h3 class="programtitle"><a href="{{urlHome('zhongchou','getDetails',['id'=>$v->id],2)}}">[认购]{{!empty($v->title)? $v->title:'认购项目'}}</a></h3>
                <div class="pcaddress"><img style=" width:25px; height:25px;vertical-align:middle; margin-top:-5px;" src="{{!empty($v->logo)? $v->logo:''}}" />&nbsp;<span style="font-size:19px;">{{!empty($v->name)? $v->name:''}}</span></div>
                <ul class="pcfendet">
                    <li><span>认购价格：</span>{{!empty($v->price)? $v->price:'0.00'}}{{$v->currency_type}}</li>
                    <li><span>最低数量：</span>{{!empty($v->min_limit)? $v->min_limit:'0.00'}}个</li>
                    <li><span>认购目标：</span>{{!empty($v->num)? $v->num:''}}个</li>
                </ul>
                <div class="pclastdate">
                    <div class="pcfenpost1">
                      <span>已投进度：{{$v->bl}} %</span><br />
                      <span class="pcfenshu1">已投资人数：{{getIssueMemberCountByIssueId($v->id)}}人</span>
                    </div>
                    @if($v->status == 0)<span class="timeste_4"></span><input type="button" style=" width:160px;background-color: #A7A6A5; cursor: pointer;" class="btn-play-1" value="项目尚未开始，敬请期待">@endif
                    @if($v->status == 1)<span class="timeste_4"><a href="{{urlHome('zhongchou','getDetails',['id'=>$v->id],2)}}"><input style=" cursor: pointer;" type="button" class="btn-play-1" value="项目认购中,火速加入"></a></span></span>@endif
                    @if($v->status == 2)<p class="pclastdate"><span class="timeste_2">已结束</span></p>@endif
                    @if($v->status == 3)<p class="pclastdate"><span class="timeste_2">已结束</span></p>@endif
                </div>
            </div>
        </div>
    @endforeach
    <div style="width: 1200px; clear: both; margin: 0px auto;">
        <div class="page" style="min-width: 250px !important;margin-left: 500px;">
           {{ $result->links() }}
        </div>
    </div>

</div>
<script>
    $(".menu6").addClass("uc-current");
</script>
<!--footer start-->
@endsection
<!--footer end-->
<!--footer end-->