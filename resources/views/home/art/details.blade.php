@extends('home.mainlayout')
@section('content')
<div id="main">
  <div class="newsarea" style="padding-top:0;">
    <div class="newsarea_box">
      <div class="product left clearfix">
        <div style="border-bottom:1px solid #e2e2e2; padding-top:20px; position:relative;">
         
         
          <h2 style="line-height:24px; border:none;">{{$art_one['title']}}</h2>
          <p style=" position:absolute; right:40px; bottom:0px; color:#999;">{{date('Y:m:d H:i:s',$art_one['add_time'])}}</p>
        </div>
        <div class="paragraph paragraph_news">
         <p>{{$art_one['content']}}</p>
        </div>
      </div>
     <!--上面为显示的文章-->
      <div class="right">
        <div class="latest">
          <div class="investmentarea">
            <div class="focusnum1" style="border-bottom:1px solid #eee;">

              <h2 class="left">最新动态</h2>

              <p class="right"><a href="{{urlHome('art','getIndex',['position_id'=>$art_one['position_id']],2)}}">查看更多</a></p>
              <div class="clear"></div>
            </div>

            @foreach($art_list as $k=>$v)
            <ul>
              <a href="{{urlHome('art','getDetails',['article_id'=>$v['article_id']],2)}}">
              <li>
                <p class="left" style="width:200px; line-height:18px;">{{$v['title']}}</p>
                <p class=" right recruit" style="width:65px; margin-left:0;">{{date('Y:m:d',$art_one['add_time'])}}</p>
                <div class="clear"></div>
              </li>
              </a>
            </ul>
           @endforeach
          
          </div>
        </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
</div>
<div class="clear"></div>
<!--footer start--> 
@endsection