@extends('home/mainlayout')
@section('content')
<div id="main">
  <div class="main_box">
    <div class="dynamic">
      <h3>最新动态</h3>
      <ul>
        @foreach($result['data'] as $k=>$v)
          <li> <a href="{{urlHome('art','getDetails',['article_id'=>$v['article_id']],2)}}"><span>{{date('Y-m-d H:i:s',$v['add_time'])}}</span> {{$v['title']}}</a> </li>
       @endforeach
      </ul>
      <div class="page" style="min-width: 250px !important;margin-left: 500px;">
        @if($result['last_page']>1)
          <div>
            @if($result['prev_page_url'])
              <a class="prev" href="{{$result['prev_page_url']}}">&lt;&lt;</a>
            @endif
            @if(count($result['pageNoList']))
              @foreach($result['pageNoList'] as $v)
                @if($result['current_page'] == $v)
                  <span class="current">{{$v}}</span>
                @else
                  <a class="num" href="{{urlHome('art','getIndex',['page'=>$v],2)}}">{{$v}}</a>
                @endif
              @endforeach
            @endif
            @if($result['next_page_url'])
              <a class="next" href="{{$result['next_page_url']}}">&gt;&gt;</a>
            @endif
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
<!--footer start--> 
@endsection