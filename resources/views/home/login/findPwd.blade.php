@extends('home.mainlayout')
@section('content')
<style>
    .top{ border-bottom:0; padding:10px 0;}
    #main{ padding:0;}
    .login_tongyi{ width:1000px; margin:0 auto; border:1px solid #ddd;}
    .ybc_footer{ text-align:center; color:#999; margin:30px 0;}
	 .loginBox .login label.error{
         float:inherit;
         padding-left: 10px;
     }
    .error{
        color: #999;
    }
</style>
<script>
    function CheckForm() {
               if (document.myform.email.value == "") {
                   layer.msg('邮箱不能为空！',{time:2000});
                   document.myform.email.focus();
                   return false;
               }

               var email = document.myform.email.value;
               if (!(/^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/).test(email)) {
                   layer.msg('邮箱格式错误！',{time:2000});
                   document.myform.email.focus();
                   return false;
               }

        if (document.myform.captcha.value == "") {
            layer.msg('验证码不能为空！',{time:2000});
            document.myform.captcha.focus();
            return false;
        }
    }
</script>
<div id="main">
    <div class="mainbody login_tongyi">

        <div class="bigcontainer">
            <div class="left630">
                <h2 class="user">忘记密码</h2>

                <div class="loginBox">
                    <form method="post" id="findPwdForm" name="myform" action="{{urlHome('login','postFindPwd')}}" onsubmit="{return CheckForm()}">
                        {{csrf_field()}}
                        <!-- 电子邮箱 -->
                        <ul class="login">
                            <li><label for="email">电子邮箱：</label><input type="text" class="loginValue" value="" name="email">
                            </li>
                            <li><label>验证码：</label><input class="loginValue" name="captcha" id="captcha">
                                <img id="captchaimg" src="{{urlHome('login','getCode')}}">
                                <a href="#" onclick="$('#captchaimg').attr('src', '{{urlHome('login','getCode')}}?'+Math.random())">看不清？</a></li>
                            <li style="margin-bottom:15px;"><label>&nbsp;</label><input type="submit" value="找回密码" class="tijiao" style="border:0;"></li>
                            <li><label>&nbsp;</label>已有账号&nbsp;<a href="{{urlHome('login','getIndex')}}">登录</a><span>没有{{!empty($config['name'])? $config['name']:'本网站'}}账号？&nbsp;<a href="{{urlHome('reg','getIndex')}}" class="zhuce">注册一个</a></span></li>
                        </ul>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $("#findPwdForm").validate({
        rules: {
            email:{
                required:true,
                email:true
            },
            captcha:{
                required:true
            }
        },
        messages: {
            email:{
                required:"邮箱不能为空",
                email:"邮箱格式不正确"
            },
            captcha:{
                required:"验证码不能为空"
            }
        },
        submitHandler:function(form){
            ajax_submit_form(form)
            return false;
        },
        invalidHandler: function() {  //不通过回调
            return false;
        }
    });
</script>

<!--footer start-->
@endsection