<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="renderer" content="webkit">
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta property="wb:webmaster" content="8af72a3a7309f0ee">
    <title><notempty name='article'>{$article.title}-</notempty>{$config.title|default="虚拟币交易网站"}</title>
    <link rel="Shortcut Icon" type="image/x-icon" href="/favicon.ico">
    <link rel="stylesheet" type="text/css" href="/home/css/base.css">
    <link rel="stylesheet" type="text/css" href="/home/css/layout.css">
    <link rel="stylesheet" type="text/css" href="/home/css/subpage.css">
    <link rel="stylesheet" type="text/css" href="/home/css/user.css">
    <link rel="stylesheet" type="text/css" href="/home/css/coin.css">
    <link rel="stylesheet" type="text/css" href="/home/css/zcpc.css">
    <link rel="stylesheet" type="text/css" href="/home/iconfont/demo.css">
    <link rel="stylesheet" type="text/css" href="/home/iconfont/iconfont.css">
    <link rel="stylesheet" type="text/css" href="/home/css/jb_style.css">
    <script src="/home/js/hm.js">
    </script><script type="text/javascript" src="/home/js/jquery-1.js"></script>
    <script type="text/javascript" src="/home/js/downList.js"></script>

    <script type="text/javascript" src="/js/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>
    <script src="/js/bootstrap.min.js?v=3.4.0"></script>
    <script type="text/javascript" src="/js/layer/layer.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/messages_zh.min.js"></script>
    <script src="/js/base.js"></script>

    <script type="text/javascript" src="/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="/js/layer/layer.js"></script>
    <script type="text/javascript" src="/js/laydate/laydate.js"></script>

    <script type="text/javascript">
        var msg = "{{Session::get('message')}}";
        //触发事件
        $(window).ready(function(){
            if(msg){
                layer.msg(msg,{
                    time:2000  //2毫秒
                });
            }
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>

</head>
<body>
<!--<div class="clearfix phone_top" id="phone_top_div" style="display:none;">
	<div class="left">
		<p class="left phone_logo"><img src="/images/phone_logo01.png"/></p>
		<p class="left phone_title">第一数字货币众筹交易平台</p>
	</div>
	<a href="javascript:hidephone();" class="phone_x">X</a>
</div>-->
<!--top start-->
<div style="background:#f9f9f9; height:30px;">
    <div style="width:1000px; margin:0 auto;">
        <ul class="qqkf left" style="line-height:30px; color:#999;">
            <li class="phone"><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin={{$config['qq1']}}&site=qq&menu=yes" class="qq_qq"></a>{{empty($config['qq1'])? '暂无':$config['qq1']}}</li>
            <li class="phone400">{{empty($config['tel'])? '暂无':$config['tel']}}</li>
            <li class="phone_email"><a href="mailto:{{$config['email']}}">{{empty($config['email'])? '暂无':$config['email']}}</a></li>
            <li>&nbsp;&nbsp;工作日:9-19时 节假日:9-18时</li>
        </ul>
        {{--<if  condition="!empty($_SESSION['USER_KEY_ID'])">--}}
        @if(!empty(Session('USER_KEY_ID')))
            <div class="person right">
                {{--<a class="left myhome" href="#" style=" height:30px; line-height:30px; margin-right:5px;">--}}
                    {{--{$username} </a>--}}
                {{--<div style="display: none;" class="mywallet_list"><div class="clear"><ul class="balance_list"><h4>可用余额</h4><li><a href="javascript:void(0)"><em style="margin-top: 5px;" class="deal_list_pic_cny"></em><strong>人民币：</strong><span>{$member.rmb}</span></a></li></ul><ul class="freeze_list"><h4>委托冻结</h4><li><a href="javascript:void(0)"><em style="margin-top: 5px;" class="deal_list_pic_cny"></em><strong>人民币：</strong><span>{$member.forzen_rmb}</span></a></li></ul></div><div class="mywallet_btn_box"><a href="#">充值</a><a href="#">提现</a><a href="#">转入</a><a href="#">转出</a><a href="#">委托管理</a><a href="#">成交查询</a></div></div>--}}
                <span class="left" style="height:30px; line-height:30px; color:#999; margin-right:5px;">(UID: {{Session('USER_KEY_ID')}} )</span>
                <a class="left" href="{{urlHome('login','get_logout_home')}}" style="height:30px; line-height:30px; margin:0 5px;">退出</a>
                <div id="my" class="account left" href="javascript:void(0)" style="z-index:9997; margin-right:5px;">
                    <a class="user_me" href="#">我的账户</a>
                    <ul class="accountList" style="padding: 6px 0px; background: rgb(85, 85, 85) none repeat scroll 0% 0%; border-radius: 5px 0px 5px 5px; display: none;">
                        <!--<li class="accountico no"></li>-->
                        <li><a href="#">我的资产</a></li>
                        <li><a href="#">我的交易</a></li>
                        <li><a href="#">我的众筹</a></li>
                        <li style="border-top:1px solid #666;"><a href="#">人民币充值</a></li>
                        <li><a href="#">人民币提现</a></li>
                        <li style="border-bottom:1px solid #444;"><a href="#">充币提币</a></li>
                        <li><a href="#">修改密码</a></li>
                        <li><a href="#">系统消息
                                {{--<if condition="$newMessageCount">--}}
                                    <span class="messagenum">{$newMessageCount}</span>
                                {{--</if>--}}
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        @else

        {{--</if>--}}
        {{--</if>--}}
        {{--<if condition="empty($_SESSION['USER_KEY_ID'])">--}}
            <div class="loginArea right" style=" margin-right:5px;">
                <a href="{{urlHome('login','get_login_index')}}" style="color:#f60; font-size:14px;">登录</a>
                <span class="sep">&nbsp;|&nbsp;</span>
                <a href="{{urlHome('reg','get_register')}}" style="color:#f60; font-size:14px;">注册</a>
            </div>
        {{--</if>--}}
            @endif
    </div>
</div>
<div class="top">
    <div class="wapper clearfix">
        <h1 class="left"><a href="#"><img style=" width:280px; height:70px;" src="{{$config['logo']}}" alt="虚拟币" title="虚拟币"></a></h1>
        <ul class="nav right" style="z-index:9995;">
            <li><a href="{{urlHome('index','get_index')}}">首页</a></li>
            <li><a href="{{urlHome('orders','get_orders')}}">交易大厅</a></li>
            <li><a href="{{urlHome('zhongchou','get_zhongchou')}}">认购中心<!--hr--></a></li>
            <li><a href="{{empty(Session('USER_KEY_ID'))? urlHome('login','get_login_index'):urlHome('user','getIndex')}}">用户中心</a></li>
            <li><a href="{{urlHome('help','get_help')}}">帮助中心</a></li>
            <li><a href="{{urlHome('art','get_art')}}">最新动态</a></li>
            <li><a href="{{urlHome('market','get_market')}}">行情中心</a></li>
            <li><a href="{{urlHome('dow','get_dow')}}">下载中心</a></li>
        </ul>
    </div>
</div>
<div class="pclxfsbox">
    <ul>
        <li id="opensq">
            <i class="pcicon1 iscion6" ></i>
            <div class="pcicon1box">
                <div class="iscionbox" >
                    <p>在线咨询</p>
                    <p>{{!empty($config['worktime'])? $config['worktime']:'暂无'}}</p>
                </div>
                <i></i>
            </div>
        </li>
        <li>
            <i class="pcicon1 iscion1"></i>
            <div class="pcicon1box">
                <div class="iscionbox">
                    <p><img src="#" alt="投筹网微信公众号" width="108"></p>
                    <p>{{!empty($config['name'])? $config['name']:"虚拟网"}}微信群</p>
                </div>
                <i></i>
            </div>
        </li>
        <li>
            <i class="pcicon1 iscion2"></i>
            <div class="pcicon1box">
                <div class="iscionbox">
                    <p>{{!empty($config['tel'])? $config['tel']:"暂无"}}</p>
                    <p>{{!empty($config['name'])? $config['name']:"虚拟网"}}</p>
                </div>
                <i></i>
            </div>
        </li>
        <li>
            <i class="pcicon1 iscion3"><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin={$config['qq1']}&site=qq&menu=yes"></a></i>
            <div class="pcicon1box">
                <div class="iscionbox">
                    <p>{{!empty($config['qq1'])? $config['qq1']:"暂无"}}</p>
                    <p>{{!empty($config['name'])? $config['name']:"虚拟网"}}QQ在线客服1</p>
                </div>
                <i></i>
            </div>
        </li>
        <li>
            <i class="pcicon1 iscion3" style="background:url(/home/images/kefu2.png) no-repeat #9b9b9b;background-position:-144px 11px;"><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin={$config['qq2']}&site=qq&menu=yes"></a></i>
            <div class="pcicon1box">
                <div class="iscionbox">
                    <p>{{!empty($config['qq2'])? $config['qq2']:"暂无"}}</p>
                    <p>{{!empty($config['name'])? $config['name']:"虚拟网"}}QQ在线客服2</p>
                </div>
                <i></i>
            </div>
        </li>
        <li>
            <i class="pcicon1 iscion3" style="background:url(/home/images/kefu3.png) no-repeat #9b9b9b;background-position:-144px 11px;"><a target="_blank" href="http://wpa.qq.com/msgrd?v=3&uin={$config['qq3']}&site=qq&menu=yes"></a></i>
            <div class="pcicon1box">
                <div class="iscionbox">
                    <p>{{!empty($config['qq3'])? $config['qq2']:"暂无"}}</p>
                    <p>{{!empty($config['name'])? $config['name']:"虚拟网"}}QQ在线客服3</p>
                </div>
                <i></i>
            </div>
        </li>

        <li>
            <i class="pcicon1 iscion4"></i>
            <div class="pcicon1box">
                <div class="iscionbox">
                    <p>返回顶部</p>
                </div>
                <i></i>
            </div>
        </li>
    </ul>
</div>
<script type="text/javascript">
    $(function(){
        $(".pcicon1").on("mouseover",function(){
            $(this).addClass("lbnora").next(".pcicon1box").css({"width":"148px"});
        }).on("mouseout",function(){
            $(this).removeClass("lbnora").next(".pcicon1box").css("width","0px");
        });
        $(".iscion4").on("click",function(){
            $("html, body").animate({
                scrollTop: 0
            })
        });

        var objWin;
        $("#opensq").on("click",function(){
            var top = window.screen.height/2 - 250;
            var left = window.screen.width/2 - 390;
            var target = "http://p.qiao.baidu.com//im/index?siteid=8050707&ucid=18622305";
            var cans = 'width=780,height=550,left='+left+',top='+top+',toolbar=no, status=no, menubar=no, resizable=yes, scrollbars=yes' ;

            if((navigator.userAgent.indexOf('MSIE') >= 0)&&(navigator.userAgent.indexOf('Opera') < 0)){
                //objWin = window.open ('','baidubridge',cans) ; 
                if (objWin === undefined || objWin === null || objWin.closed) {
                    objWin = window.open (target,'baidubridge',cans) ;
                }else {
                    objWin.focus();
                }
            }else{
                var win = window.open('','baidubridge',cans );
                if (win.location.href == "about:blank") {
                    //窗口不存在
                    win = window.open(target,'baidubridge',cans);
                } else {
                    win.focus();
                }
            }
            return false;

        })
    })

</script>
<!--top end-->

<script>
    $(".myhome").hover(function(){
        $(".mywallet_list").show();
    },function(){
        $(".mywallet_list").hover(function(){
            $(".mywallet_list").show();
        },function(){
            $(".mywallet_list").hide();
        });
        $(".mywallet_list").hide();
    });
</script>
@yield('js')
@yield('content')
<style>
    .rightwidth{ width:340px;}
</style>
<!--footer start-->

<div class="coin_footer" style="position:relative;">
    <div class="coin_hint">

        <h2>{{!empty($config['title'])? $config['title']:"风险提示"}}</h2>
        <p>{{$config['risk_warning']}}</p>
    </div>
    <div class="coin_footerbar">
        <div class="coin_footer_nav clearfix">
            <div class="coin_nav coin_copy left">
                <p><a href="#"><img style=" height:55px;" src="{{$config['logo']}}"></a></p>
            </div>
            <div class="coin_nav left">
                <h2>{{!empty($config['name'])? $config['name']:"虚拟币"}}团队</h2>
                <ul>
                    <li><a href="#">{{!empty($config['name'])? $config['name']:"虚拟币"}}</a></li>
                    {{--<volist name="team" id="vo">--}}
                        <li><a href="#" target="_blank" class="left">{$vo.title}</a></li>
                    {{--</volist>--}}
                </ul>
            </div>
            <div class="coin_nav left">
                <h2>帮助中心</h2>
                <ul>
                    {{--<volist name="help" id="vo">--}}
                        <li><a href="#" target="_blank" class="left">{$vo.name}</a></li>
                    {{--</volist>--}}
                </ul>
            </div>
            <div class="coin_nav coin_nav02 left">
                <h2 class="clearfix"><span class="left">联系我们</span><a href="http://weibo.com/{$config.weibo}" target="_blank" class="coin_sina left"></a><!--<a href="#" id="coin_weixin" class="coin_wei left"></a>--></h2>
                <ul>
                    <li>客服电话：{{!empty($config['tel'])? $config['tel']:'暂无'}}</li>
                    <li>客服QQ：{{!empty($config['qq1'])? $config['qq1']:'暂无'}}</li>
                    <li><a href="mailto:{{$config['email']}}">客服邮箱：{{!empty($config['email'])? $config['email']:'暂无'}}</a></li>
                    <li><a href="mailto:{{$config['business_email']}}">业务合作：{{!empty($config['business_email'])? $config['business_email']:'暂无'}}</a></li>
                </ul>
            </div>
            <div class="coin_nav coin_nav02 left rightwidth" style="position:relative;">
                <div style="float:left; padding-top:25px; padding-left:10px;"><img style=" width:100px;" src="{{$config['weixin']}}"/></div>
                <div style=" float:left; padding-left:10px;">
                    <p class="coin_phone400">{{!empty($config['tel'])? $config['tel']:'暂无'}}</p>
                    <p class="coin_phoneqq"><a href="http://wpa.qq.com/msgrd?v=3&uin={{$config['qq1']}}&site=qq&menu=yes" target="_blank">在线客服</a></p>
                    <p>工作日:9-19时 节假日:9-18时</p>
                </div>
                <div class="group" style="left:12px;margin-top: 40px">
                    <ul class="qq_all" style="    margin-left: 10px;">
                        <li><a style="flont:left; " href="javascript:void(0)">{{!empty($config['name'])? $config['name']:"虚拟币"}}官方群<img style="margin-top:5px;" src="/home/images/xiala.png"></a>
                            <ul style="margin-left: 105px;">
                                <li>{{!empty($config['name'])? $config['name']:"虚拟币"}}官方1群 {{!empty($config['qqqun1'])? $config['qqqun1']:"虚拟币"}}</li>
                                <li>{{!empty($config['name'])? $config['name']:"虚拟币"}}官方2群 {{!empty($config['qqqun2'])? $config['qqqun2']:"虚拟币"}}</li>
                                <li>{{!empty($config['name'])? $config['name']:"虚拟币"}}官方3群 {{!empty($config['qqqun3'])? $config['qqqun3']:"虚拟币"}}</li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_aq">
        <p>{{!empty($config['copyright'])? $config['copyright']:"暂无"}}</p>
        <p>{{!empty($config['record'])? $config['record']:"暂无"}}</p>
        <ul class="footerSafety clearfix">
            <li class="safety02"><a href="http://net.china.com.cn/" target="_blank"></a></li>
            <li class="safety03"><a href="http://webscan.360.cn/index/checkwebsite/?url={$config['localhost']}" target="_blank"></a></li>
            <li class="safety04"><a href="http://www.cyberpolice.cn/wfjb/" target="_blank"></a></li>
        </ul>
    </div>
    <div id="weixin" style="position:absolute; bottom:88px; left:50%; margin-left:170px; display:block;"><!--<img src="{$config.logo}">--></div>
    <script>
        $('#coin_weixin').mouseover(function(){
            $('#weixin').show();
        }).mouseout(function(){
            $('#weixin').hide();
        });
    </script>
    <!--footer end-->
    <script type="text/javascript" src="/home/js/gotop.js"></script>
    <script type="text/javascript" src="/home/js/link.js"></script>
    <script type="text/javascript" src="/home/js/slides.js"></script>
    <script>
        var _hmt = _hmt || [];
        (function() {
            var hm = document.createElement("script");
            hm.src = "//hm.baidu.com/hm.js?0ab4db557b96d841137861e0740d1e0a";
            var s = document.getElementsByTagName("script")[0];
            s.parentNode.insertBefore(hm, s);
        })();
    </script>
</div></body></html>
@yield('footer')
