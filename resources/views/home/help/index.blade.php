@extends('home/mainlayout')
@section('content')
    <!--top end-->
    <link rel="shortcut icon" href="http://www.jubi.com/favicon.ico" type="image/x-icon">

	{{--<script src="/home/js/script.js"></script>--}}

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">                                         
</head>
<body>
<div class="autobox">
    <div class="now">

        <a href="{{urlHome('index','getIndex')}}" class="orange">首页</a> &gt;<a href="{{urlHome('help','getIndex')}}" class="orange"> 帮助中心 </a>
		@if(!empty($art_one['title']))&gt;<a class="orange">{{$art_one['title']}}</a>@endif

    </div>
    <div class="assets_center clear po_re zin70">
        <!--左侧菜单-->
        <div class="coin_menu">

            	@foreach($art_list as $k=>$v)
                <dl>
                    <dt >{{$v['name']}}</dt>
                    <div class="dl_menu" style="display:block">
                            <dd class=""><em></em><a href="{{url('help?article_id='.$v['children']['article_id'])}}">{{$v['children']['title']}}</a></dd>
                    </div>
                </dl>
               @endforeach

        </div>

        <!--右侧内容-->
        <div class="assets_content zhinan w753 right bg_w mh1500">

          <h1>{{$art_one['title']}}</h1>
            
            <div class="about_text">
             {{$art_one['content']}}
            </div>
      </div>
    </div>
</div>

<!--footer start-->
@endsection
<!--footer end-->