@extends('home/mainlayout')
@section('content')
    <!--top end-->
    <style>
        .list_con1 td input, .list_con2 td input {
            border: 1px solid #e55600;
            color: #e55600;
            border-radius: 3px;
            width: 90px;
            height: 30px;
            margin: 5px;
            cursor: pointer;
        }

        .list_con1 td input:hover, .list_con2 td input:hover {
            border: 1px solid #e55600;
            border-radius: 3px;
            width: 90px;
            height: 30px;
            margin: 5px;
            cursor: pointer;
            color: #FFF;
            background-color: #e55600;
        }

        .list_con1:hover {
            background-color: #FFE9D2;
        }

        .list_con2:hover {
            background-color: #FFE9D2;
        }

        a {
            text-decoration: none;
        }
    </style>
    <script src="/home/js/index.js"></script>
    <div id="main">
        <div class="main_box">
            @include('home/public/left')
            <div class="raise right clearfix">
                <div class="ybc_list">

                    <div class="ybcoin" id="mycoin">
                        <!-- 旧版：<h2 class="left">账户总资产估算: <span style="color:#f60;">{$allmoneys|default=0.00}</span> CNY</h2>  end-->
                        <!-- <p class="right receive_rules" style="margin-bottom:0px;"><a href="/index/activity"> 存币送利息新规则</a></p> -->

                        <!-- 账户总资产估算新改:-->
                        <div style="font-weight:500;font-size: 18px;">账户资产
                            <span style="font-weight:normal;color:#e55600;font-size:12px;padding-left: 15px;">推荐{{$config['name']}}赚佣金，躺着也挣钱！</span></div>
                        <div class="coin_style_box">
                            <ul>
                                <li><img src="/home/images/coin_rmb.png">　人民币(CNY)</li>
                                <li>可用：<span style="color:green;">￥{{$u_info['rmb']}}</span></li>
                                <li>冻结：<span style="color:orange;">￥{{$u_info['forzen_rmb']}}</span></li>
                                <li>总资产：<b>￥{{$u_info['rmb']+$u_info['forzen_rmb']}}</b></li>
                            </ul>
                        </div>


                        <!-- 账户总资产估算新改:-->


                        <div class="clear" style="margin-bottom:15px;"></div>
                    </div>


                    <!--会员条件不符的时候显示 点击×之后不再显示-->
                    <table class="raise_list" align="center" border="0" cellpadding="0" cellspacing="0">
                        <thead>
                        <tr class="list_head">
                            <th class="assets01">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;名称</th>
                            <th class="assets02">总量</th>
                            <th class="assets02">可用</th>
                            <th class="assets02">冻结<a class="ico_tishi" id="pledge" href=""
                                                      onmouseover="showTips('#pledge','冻结原因：货币交易中、提现/提币处理中、项目众筹中、项目交易中')"
                                                      style="background:#999; color:#fff; padding:2px 5px; border-radius:10px; text-decoration:none; margin-left:5px; font-family:monospace;">i</a>
                            </th>
                            <th class="assets03">操作&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>交易</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr class="list_con1">
                            <td class="usertdtitle assets01">人民币 CNY</td>
                            <td class="assets02">{{$u_info['rmb']+$u_info['forzen_rmb']}}</td>
                            <td class="assets02">{{$u_info['rmb']}}</td>
                            <td class="assets02">{{$u_info['forzen_rmb']}}</td>
                            <td class="cost assets03"><a href="{{urlHome('User','getPay')}}">充值</a> &nbsp;&nbsp;<a
                                        href="{{urlHome('User','getDraw')}}">提现</a></td>
                            <td></td>
                        </tr>
                        @if($currency_user && count($currency_user))
                            @foreach($currency_user as $v)
                                <tr class="list_con2">
                                    <td class="usertdtitle assets01"> {{$v['currency_name'] or '虚拟币'}}{{$v['currency_mark'] or '--'}}</td>
                                    <td class="assets02"> {{$v['count'] or 0.00}}</td>
                                    <td class="assets02">{{$v['num'] or 0.00}}</td>
                                    <td class="assets02">{{$v['forzen_num'] or 0.00}}</td>
                                    <td class="cost interest assets03">
                                        <a href="{{urlHome('pay','getBpay',['currency_id'=>$v['currency_id']],2)}}">充币</a>
                                        &nbsp;&nbsp;<a
                                                href="{{urlHome('pay','getTcoin',['currency_id'=>$v['currency_id']],2)}}">提币</a>
                                    </td>
                                    <td class="cost interest" style="color:#f60;"><a style="text-decoration: none;"
                                                                                     href="{{urlHome('Orders','getIndex',['currency'=>$v['currency_mark']],2)}}"><input
                                                    value="去交易" type="button"></a></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div>
    <script>
        $(".menu").addClass("uc-current");

        function showTips(id, msg) {
            var tips = layer.tips(msg, id, {
                tips: [1, '#fff8db'],
                area: ['400px', '25px'],
            });
            $(id).on('mouseout', function () {
                layer.close(tips);
            });
        }
    </script>

    <!--footer start-->
@endsection
<!--footer end-->