@include('home.public.header')
<div id="main">
    <div class="newsarea" style="padding-top:0;">
        <div class="newsarea_box">
            <div class="product left clearfix">
                <div style="border-bottom:1px solid #e2e2e2; padding-top:20px; position:relative;">
                    <h2 style="line-height:24px; border:none;">【{{$list['category']['name']}}】{{$list['title']}}</h2>
                    <p style=" position:absolute; right:40px; bottom:0px; color:#999;">{{date('Y-m-d H:i:s',$list['add_time'])}}</p>
                </div>
                <div class="paragraph paragraph_news">
                    {{$list['content']}}
                </div>
            </div>

            <div class="right">
                <div class="latest">
                    <div class="investmentarea">
                        <div class="focusnum1" style="border-bottom:1px solid #eee;">
                            <h2 class="left">系统消息</h2>
                            <p class="right"><a href="{{urlHome('user','getSysmessage')}}">查看更多</a></p>
                            <div class="clear"></div>
                        </div>
                        <ul>
                            @if($right['data'])
                                @foreach($right['data'] as $row)
                                    <a href="{{urlHome('User','getShowSystem',['message_id'=>$row['message_id'],'message_all_id'=>$row['message_all_id']],2)}}">
                                        <li>
                                            <p class="left" style="width:200px; line-height:18px;">
                                                【{{$row['category']['name']}}】{{$row['title']}}</p>
                                            <p class=" right recruit"
                                               style="width:65px; margin-left:0;">{{date('Y-m-d',$row['add_time'])}}</p>
                                            <div class="clear"></div>
                                        </li>
                                    </a>
                                @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<!--footer start-->
@include('home.public.footer')