@include('home.public.header')
<!--top end-->
<div id="main">
    <div class="main_box">
        @include('home.public.left')
        <div class="raise right clearfix">
            <div class="ybc_list">
                <div class="ybcoin clearfix">
                    <h2 class="left">我的众筹</h2>
                    <div class="right" style="margin-top:7px;">

                    </div>
                </div>
                <table class="raise_list" style="border:1px solid #e1e1df;" id="Transaction" align="center" border="0"
                       cellpadding="0" cellspacing="0">
                    <thead>
                    <tr>
                        <th>币种</th>
                        <th>成交时间</th>
                        <th>标题</th>
                        <th>成交价格</th>
                        <th>成交数量</th>
                        <th>成交额</th>
                    </tr>
                    @if(count($rows['data']))
                        @foreach($rows['data'] as $v)
                            <tr>
                                <td>{{$v['issue']['currency_name']}}</td>
                                <td>{{date('Y-m-d H:i:s',$v['add_time'])}}</td>
                                <td>{{$v['issue']['title']}}</td>
                                <td>{{$v['price']}}</td>
                                <td>{{$v['num']}}</td>
                                <td>{{$v['price']*$v['num']}}</td>
                            </tr>
                        @endforeach
                    @endif
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div class="page" style="min-width:200px !important; float:right;">
                @if($rows['last_page']>1)
                    <div>
                        @if($rows['prev_page_url'])
                            <a class="prev" href="{{$rows['prev_page_url']}}">&lt;&lt;</a>
                        @endif
                            @if(count($rows['pageNoList']))
                            @foreach($rows['pageNoList'] as $v)
                                @if($rows['current_page'] == $v)
                                    <span class="current">{{$v}}</span>
                                @else
                                    <a class="num" href="{{urlHome('user','getZhongchou',['page'=>$v],2)}}">{{$v}}</a>
                                @endif
                            @endforeach
                        @endif
                        @if($rows['next_page_url'])
                            <a class="next" href="{{$rows['next_page_url']}}">&gt;&gt;</a>
                        @endif
                    </div>
                @endif
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<script>
    $(".menu6").addClass("uc-current");
</script>
<!--footer start-->
@include('home.public.footer')
<!--footer end-->
