@include('home.public.header')
<style>
#main{ min-height:620px;}
</style>
<div id="main" style=" padding:30px 0px !important;">
    <div class="main_box">
		@include('home.public.left')
        <div class="raise right clearfix">
            <div class="message">
                <p class="messagetab"><a href="#" class="current">系统消息
                </a></p>
				@if($rows['data'])
					@foreach($rows['data'] as $row)
					<div class="mynews news01">
						<div class="andfrom yourattention">
							<p class="left news_name">
								<a href="
								{{urlHome('User','getShowSystem',['message_id'=>$row['message_id'],'message_all_id'=>$row['message_all_id']],2)}}
								">
									@if($row['status'] == 0)
										【【{{$row['category']['name']}}】{{$row['title']}}】<span style="color:#FF0000">未读</span>
										@else
										【【{{$row['category']['name']}}】{{$row['title']}}】
									@endif
								</a>
							</p>
							<p class="right news_date">{{date('Y-m-d H:i:s',$row['add_time'])}}</p>
							<div class="clear">
							</div>
						</div>
					</div>
				@endforeach
@endif
            </div>
        </div>
        <div class="page" style=" min-width:200px !important; float:right;">
			@if($rows['last_page']>1)
				<div>
					@if($rows['prev_page_url'])
						<a class="prev" href="{{$rows['prev_page_url']}}">&lt;&lt;</a>
					@endif
					@if(count($rows['pageNoList']))
						@foreach($rows['pageNoList'] as $v)
							@if($rows['current_page'] == $v)
								<span class="current">{{$v}}</span>
							@else
								<a class="num" href="{{urlHome('user','getSysmessage',['page'=>$v],2)}}">{{$v}}</a>
							@endif
						@endforeach
					@endif
					@if($rows['next_page_url'])
						<a class="next" href="{{$rows['next_page_url']}}">&gt;&gt;</a>
					@endif
				</div>
			@endif
        </div>
    </div>
</div>
<div class="clear"></div>
</div>
<script>
$(".menu10").addClass("uc-current");
</script>
<!--footer start-->
@include('home.public.footer')