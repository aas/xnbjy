@extends('admin.mainlayout')
@section('content')
    {{--<style>--}}
        {{--.list-page ul{overflow:hidden;display:inline-block;}--}}
        {{--.list-page ul li{float:left;border:none;height:30px;line-height:30px;border: 1px}--}}
        {{--.list-page ul li a{padding:6px 12px;text-decoration:none}--}}
    {{--</style>--}}
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">提现审核</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
                <form action="{{url('center/pending/index')}}" method="get">
                    {{csrf_field()}}
                    <table class="search-tab">
                        <tr>
                            <th width="120">选择分类:</th>
                            <td>
                                <select name="status" id="">
                                  <option value="0">全部</option>
                                  <option value="1">未通过</option>
                                  <option value="2">通过</option>
                                  <option value="3">审核中</option>
                                </select>
                            </td>
                            <th width="70">提现人:</th>
                            <td><input class="common-text" placeholder="提现人" name="keyname"  id="keyname" type="text"></td>
                  
                            <td><input class="btn btn-primary btn2" value="查询" type="submit"></td>
                            <tr>   
                                    <th><i class="require-red">*</i>开始时间：</th>
                                    <td>
                            
                                    <div class="row"><div class="col-md-6">
                                                    <i class="fa fa-calendar"></i></span>
                                                    <input type="text" name="add_time" class="ui_timepicker" id="aaaaa1" placeholder="选择日期时间" value="{{date('Y-m-d H:i:s',0)}}"  />
                                                </div></div></td>
                                  <script type="text/javascript">
                                    $(function () {
                                        $("#aaaaa1").datetimepicker({
                                            //showOn: "button",
                                            //buttonImage: "./css/images/icon_calendar.gif",
                                            //buttonImageOnly: true,
                                            showSecond: true,
                                            timeFormat: 'hh:mm:ss',
                                            stepHour: 1,
                                            stepMinute: 1,
                                            stepSecond: 1
                                        })
                                            $("#aaaaa2").datetimepicker({
                                            //showOn: "button",
                                            //buttonImage: "./css/images/icon_calendar.gif",
                                            //buttonImageOnly: true,
                                            showSecond: true,
                                            timeFormat: 'hh:mm:ss',
                                            stepHour: 1,
                                            stepMinute: 1,
                                            stepSecond: 1
                                        })
                                   
                                    })
                                </script>
                                    <th><i class="require-red">*</i>截止时间：</th>
                                    <td><div class="row"><div class="col-md-6">
                                                    <i class="fa fa-calendar"></i></span>
                                                    <input type="text" name="end_time" class="ui_timepicker" id="aaaaa2" placeholder="选择日期时间" value="{{date('Y-m-d H:i:s',time())}}" />
                                                </div></div></td>
									<td><input class="btn btn-primary btn2" name="sub" value="下载表格" type="button" onclick="download()"></td>
								</tr>
								
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <div class="result-wrap">
                <div class="result-content">
                    <table class="result-tab" width="100%">
                        <tr>
                            <th>ID</th>               
                            <th>提现人</th>
                            <th>会员ID</th>
                            <th>银行</th>
                            <th>银行账号</th>
                            <th>银行开户地</th>
                            <th>开户支行</th>
                            <th>提现金额</th>
                            <th>手续费（{{$data['fee']}}%）</th>
                            <th>实际金额</th>
                            <th>订单号</th>
                            <th>提交时间</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                        @if(!empty($rows['data']))
                        @foreach($rows['data'] as $k=>$v)
                        <tr>
                            <td>{{$v['withdraw_id']}}</td>
                            <td>{{$v['cardname']}}</td>
                            <td>{{$v['uid']}}</td>
                            <td>{{$v['bankname']}}</td>
                            <td>{{$v['cardnum']}}</td>
                            <td>{{$v['aarea_name']}}&nbsp;&nbsp;{{$v['barea_name']}}</td>
                            <td>{{empty($v['bank_branch'])? '暂未填写':$v['bank_branch']}}</td>
                            <td>{{$v['all_money']}}</td>
                            <td>{{$v['withdraw_fee']}}</td>
                            <td>{{$v['money']}}</td>
                            <td>{{$v['order_num']}}</td>
                            <td>{{date('Y-m-d H:i:s',$v['add_time'])}}</td>
                            <td>@if($v['status'] == 1)未通过@elseif($v['status'] == 2)通过@else审核中@endif</td>
                            <td>	
                            	@if($v['status'] == 3)
                                <a class="link-update" href="javascript:void(0);" onclick="pass({{$v['withdraw_id']}});">通过</a>
                                &nbsp;&nbsp;
                                <a class="link-del" href="javascript:void(0);"onclick="fail({{$v['withdraw_id']}})">不通过</a>
                                @else操作成功@endif
                            </td>                        
                            </tr>
                       @endforeach
                        @else
                            <tr><td style="text-align: center;font-size: 18px;" colspan="14">暂无数据...</td></tr>
                        @endif
                    </table>

                    <div class="list-page">
                        {{--{{ $result->appends(['status'=>$status,'keyname'=>$keyname])->render() }}--}}
                        @if($rows['last_page']>1)
                            <div>
                                {{--@if(!empty($data['prev_page_url']))--}}
                                    {{--<a class="prev" href="{{$data['prev_page_url']}}"><<</a>--}}
                                {{--@endif--}}
                                @foreach($rows['pageNoList'] as $v)
                                    <a @if($v == $rows['page'])
                                       class="current"
                                       @else
                                       class="num"
                                       @endif
                                       href="{{urlAdmin('pending','getIndex')}}?&page={{$v}}">{{$v}}</a>
                                @endforeach
                                {{--@if(!empty($data['next_page_url']))--}}
                                    {{--<a class="next" href="{{$data['next_page_url']}}">>></a>--}}
                                {{--@endif--}}
                            </div>
                        @endif
                    </div>
                  
                </div>
          
        </div>
    </div>
    <!--/main-->
</div>
</body>
</html>
    @endsection
@section('footer')
<script>
$(".sub-menu").eq(6).show();
$(".sub-menu").eq(6).children("li").eq(0).addClass("on");
function pass(withdraw_id){
	layer.confirm('确定通过审核', {
	  btn: ['确定','取消'], //按钮
	}, function(){
		$.post("{{url('center/pending/success_by')}}",{"withdraw_id":withdraw_id,'_token':"{{csrf_token()}}"},function(data){
				if(data.status == 0){
					layer.msg(data['msg']);
					setTimeout("location.reload()",2000);
				}else{
					layer.msg(data['msg']);
					setTimeout("location.reload()",2000);
				}
			});
	  //layer.msg('');
	}, function(){
		layer.msg('已取消');
	});
}
function fail(withdraw_id){
	layer.confirm('确定不通过审核', {
	  btn: ['确定','取消'], //按钮
	}, function(){		
		$.post("{{url('center/pending/error_by')}}",{"withdraw_id":withdraw_id,'_token':"{{csrf_token()}}"},function(data){
				if(data.status == 0){
					layer.msg(data['msg']);
					setTimeout("location.reload()",2000);
				}else if(data.status == 2){
					layer.msg(data['msg']);
					setTimeout("location.reload()",2000);
				}else{
					layer.msg(data['msg']);
					setTimeout("location.reload()",2000);
				}
		});
	  //layer.msg('');
	}, function(){
		layer.msg('已取消');
	});
}
</script>

<script>
function download(){
	var add_time=$('#aaaaa1').val();
	var end_time=$('#aaaaa2').val();
	var url="{{url('center/pending/payExcel')}}";
	window.location.href=url+"?add_time="+add_time+"&&end_time="+end_time;
}
</script>
    @endsection