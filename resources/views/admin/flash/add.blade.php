@extends('admin.mainlayout')
@section('content')
    <script>
        function CheckForm(){


            if(document.myform.pic.value ==''){
                layer.msg('上传图片不能为空!',{
                    time:2000  //2毫秒
                });
                document.myform.pic.focus();
                return false;
            }

            if(document.myform.title.value == ''){
                layer.msg('图片标题不能为空!',{
                    time:2000  //2毫秒
                });
                document.myform.title.focus();
                return false;
            }

            if(document.myform.sort.value == ''){
                layer.msg('排序不能为空!',{
                    time:2000  //2毫秒
                });
                document.myform.sort.focus();
                return false;
            }
            var sort = document.myform.sort.value;
            if ((/[^0-9]/).test(sort)) {
                layer.msg('排序数据格式错误!',{
                    time:2000  //2毫秒
                });
                document.myform.sort.focus();
                return false;
            }

            if(document.myform.jump_url.value == ''){
                layer.msg('跳转地址不能为空!',{
                    time:2000  //2毫秒
                });
                document.myform.jump_url.focus();
                return false;
            }
            var jump_url = document.myform.jump_url.value;
            if(!(/[http|https]:[\/]{2}[a-z]+[.]{1}[a-z\d\-]+[.]{1}[a-z\d]*[\/]*[A-Za-z\d]*[\/]*[A-Za-z\d]*/).test(jump_url)){
                layer.msg('网站地址格式错误!',{
                    time:2000  //2毫秒
                });
                document.myform.jump_url.focus();
                return false;
            }

        }

    </script>
    <script type="text/javascript">
        var msg = "{{Session::get('message')}}";
        //触发事件
        $(window).ready(function(){
            if(msg){
                layer.msg(msg,{
                    time:2000  //2毫秒
                });
            }
        });
    </script>
    <!--/sidebar-->
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">VIP设置</span></div>
        </div>
        <div class="result-wrap">
            <form action="{{url('center/flash/addList')}}" method="post" id="myform" onsubmit="{return CheckForm();}" name="myform" enctype="multipart/form-data" >
                {{csrf_field()}}
                <div class="config-items">
                    <div class="config-title">
                        <h1><i class="icon-font">&#xe00a;</i>VIP详细信息设置</h1>
                    </div>
                    <div class="result-content">
                        <table width="100%" class="insert-tab">
                            <tbody>
                            <tr>
                                <th width="15%"><i class="require-red">*</i>幻灯图片：</th>
                                <td><img id='oldImg' src="" alt="" style="max-width:300px; max-height:80px;"></td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>幻灯图片上传：</th>
                                <td><input type="file" name="pic" id="pic" class="common-text"></td>
                            </tr>
                            <tr>
                                {{--<th><i class="require-red">*</i>幻灯图片上传：</th>--}}
                                {{--<td>--}}
                                    {{--<input type="file" onchange="upload(this)" name="pic" >--}}
                                    {{--<input type="hidden" name="pic" value="" id="res">--}}
                                    {{--<div class="prev" id="prev">--}}
                                        {{--<img src="" alt="" style="max-width: 350px; max-height:100px;">--}}
                                    {{--</div>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                            <tr>
                                <th><i class="require-red">*</i>图片标题：</th>
                                <td><input type="text" id="title"  size="85" name="title" class="common-text"></td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>排序：</th>
                                <td><input type="text" id="sort" size="85" name="sort" class="common-text"><input type="hidden" id=""  size="85" name="flash_id" class="common-text"></td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>跳转地址：</th>
                                <td><input type="text" id="jump_url"  size="85" name="jump_url" class="common-text"></td>
                            </tr>


                            <tr>
                                <th></th>
                                <td>
                                    <input type="submit" value="提交" class="btn btn-primary btn6 mr10">
                                    <a href="javascript:history.go(-1)"><input type="button" value="返回"  class="btn btn6"></a>
                                </td>
                            </tr>
                            </tbody></table>
                    </div>
                </div>

            </form>
        </div>
    </div>
    <!--/main-->
    </div>
    </body>
    </html>
@endsection
@section('footer')
<script>
    $(".sub-menu").eq(2).show();
    $(".sub-menu").eq(2).children("li").eq(0).addClass("on");
</script>
<script>
    window.onload = function () {

        var oFile = document.getElementById('pic');
        var oldImg = document.getElementById('oldImg');


        oFile.onchange = function(){

            var file = this.files[0];

            var reader = new FileReader();

            reader.readAsDataURL(file);   //将文件读取为DataUrl

            //读完了之后就能调取数据了
            reader.onload = function(){
                //console.log(reader.result);
                //在div中添加dom
                //在隐藏标签中添加value
                oldImg.src = reader.result;

            }

        }

    }

</script>
@endsection