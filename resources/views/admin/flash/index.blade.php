@extends('admin.mainlayout')
@section('content')
    <!--/sidebar-->
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">幻灯管理</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
            </div>
        </div>
        <div class="result-wrap">
            <form name="myform" id="myform" method="post">
                <div class="result-title">
                    <div class="result-list">
                        <a href="{{url('center/flash/add')}}"><i class="icon-font"></i>新增幻灯</a>
                    </div>
                </div>
                <div class="result-content">
                    <table class="result-tab" width="100%">
                        <tr>
                            <th>图片</th>
                            <th>网址</th>
                            <th>标题</th>
                            <th>排序</th>
                            <th>更新时间</th>
                            <th>操作</th>
                        </tr>
                        @foreach ($data as $k=>$v)
                        <tr>
                            <td><img style=" width:400px; height: 100px; padding-left:120px;" src="{{asset($v['pic'])}}" /></td>
                            <td>{{$v['jump_url']}}</td>
                            <td>{{$v['title']}}</td>
                            <td>{{$v['sort']}}</td>
                            <td>{{date('Y-m-d H:i:s',$v['add_time'])}}
                            </td>
                            <td>
                                <a class="link-update" href="{{urlAdmin('Flash','getEdit',array('flash_id'=>$v['flash_id']))}}">修改</a>
                                <a class="link-del" href="javascript:;" onclick="del({{$v['flash_id']}})">删除</a>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </form>
        </div>
    </div>

    <!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(2).show();
$(".sub-menu").eq(2).children("li").eq(0).addClass("on");
</script>
<script>
    function del(flash_id){
        layer.confirm('您确定要删除吗？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                //异步处理
                $.get("{{url('center/flash/del')}}?flash_id="+flash_id,function(data){

                    if(data.state==200){
                        location.reload();
                        layer.msg(data.msg, {icon: 6});
                    }else{
                        layer.msg(data.msg, {icon: 5});
                    }
                });
            }
        )}
    </script>
@endsection