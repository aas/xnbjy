@extends('admin.mainlayout')
@section('content')
    <style>
        .list-page ul{overflow:hidden;display:inline-block;}
        .list-page ul li{float:left;border:none;height:30px;line-height:30px;border: 1px}
        .list-page ul li a{padding:6px 12px;text-decoration:none}
    </style>
    <!--/sidebar-->
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">友情链接</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
                
				
            </div>
        </div>
        <div class="result-wrap">

            <div class="result-title">
                <div class="result-list">
                    <a href="{{url('center/link/add')}}"><i class="icon-font"></i>新增链接</a>
                </div>
            </div>
            <div class="result-content">
                <table class="result-tab" width="100%">
                    <tr>
                        <th>ID</th>
                        <th>链接名称</th>
						<th>链接地址</th>
                       
						<th>添加时间</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                        @foreach ($data as $k=>$v)
                        <tr>
                            <td>{{$v['id']}}</td>
                            <td>{{$v['name']}}</td>
							<td>{{$v['url']}}</td>
                            
                       
							<td>{{date('Y-m-d H:i:s'),$v['add_time']}}</td>
                            <td>
                            @if($v['status'] == 1)
									启用
                            @endif
                            @if($v['status'] == 0)
									禁用
                            @endif
							
								</td>
                            <td>
                                <a class="link-update" href="{{url('center/link/edit?id='.$v['id'])}}">修改</a>
                                <a class="link-del" href="javascript:;" onclick="del({{$v['id']}})">删除</a>
                            </td>
                        </tr>
                    @endforeach

                </table>
                <div class="list-page">{!! $data->links() !!}</div>
            </div>

        </div>
    </div>
    <!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(2).show();
$(".sub-menu").eq(2).children("li").eq(1).addClass("on");
</script>
<script>
    function del(id){
        layer.confirm('您确定要删除吗？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                //异步处理
                $.get("{{url('center/link/del')}}?id="+id,function(data){

                    if(data.state==200){
                        location.reload();
                        layer.msg(data.msg, {icon: 6});
                    }else{
                        layer.msg(data.msg, {icon: 5});
                    }
                });
            }
        )}
</script>
@endsection