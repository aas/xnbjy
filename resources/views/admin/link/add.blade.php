@extends('admin.mainlayout')
@section('content')
    <script>
        function CheckForm(){


            if(document.myform.name.value ==''){
                layer.msg('请输入链接名称!',{
                    time:2000  //2毫秒
                });
                document.myform.name.focus();
                return false;
            }

            if(document.myform.url.value == ''){
                layer.msg('请输入链接地址!',{
                    time:2000  //2毫秒
                });
                document.myform.url.focus();
                return false;
            }
            var url = document.myform.url.value;
            if(!(/[http|https]:[\/]{2}[a-z]+[.]{1}[a-z\d\-]+[.]{1}[a-z\d]*[\/]*[A-Za-z\d]*[\/]*[A-Za-z\d]*/).test(url)){
                layer.msg('网站地址格式错误!',{
                    time:2000  //2毫秒
                });
                document.myform.url.focus();
                return false;
            }

            if(document.myform.status.value == ''){
                layer.msg('请选择链接状态!',{
                    time:2000  //2毫秒
                });
                return false;
            }



        }

    </script>
    <script type="text/javascript">
        var msg = "{{Session::get('message')}}";
        //触发事件
        $(window).ready(function(){
            if(msg){
                layer.msg(msg,{
                    time:2000  //2毫秒
                });
            }
        });
    </script>
<!--/sidebar-->
<div class="main-wrap">
    <div class="crumb-wrap">
        <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">添加链接</span></div>
    </div>
    <div class="result-wrap">
        <form action="{{url('center/link/addList')}}" method="post" id="myform" name="myform" enctype="multipart/form-data" onsubmit="{return CheckForm();}">
            {{csrf_field()}}
            <div class="config-items">
                <div class="config-title">
                    <h1><i class="icon-font">&#xe00a;</i>添加链接</h1>
                </div>
                <div class="result-content">
                    <table width="100%" class="insert-tab">
                        <tbody>
                        <tr>
                            <th><i class="require-red"></i>链接名称：</th>
                            <td><input name="name" id="name" type="text"></td>
                        </tr>
						 <tr>
                            <th><i class="require-red"></i>链接地址：</th>
                            <td><input name="url" id="url" type="text" ></td>
                        </tr>
						
                         <tr>
                            <th><i class="require-red"></i>链接状态：</th>
                            <td><input name="status" id="status" type="radio"  value="1" >启用
								<input name="status" id="status" type="radio" value="0"  >禁用
							</td>
                        </tr>
						  {{--<tr>--}}
                            {{--<th><i class="require-red"></i>添加时间：</th>--}}
                            {{--<td></td>--}}
							{{----}}
                        {{--</tr>--}}

            	 	   {{--<input type="hidden" name="id" value="{$list['id']}" />--}}
                        <tr>
                            <th></th>
                            <td>
                                <input type="submit"  value="提交" class="btn btn-primary btn6 mr10">
                                <a href="javascript:history.go(-1)"><input type="button" value="返回"  class="btn btn6"></a>
                            </td>
                        </tr>
						
                        </tbody></table>
                </div>
            </div>
        </form>
    </div>
</div>


<!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(2).show();
$(".sub-menu").eq(2).children("li").eq(2).addClass("on");
</script>
@endsection