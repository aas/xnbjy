@extends('admin.mainlayout')
@section('content')
	<script>
        function CheckForm(){

            if(document.myform.num1.value ==''){
                layer.msg('最少发行量不能为空!',{
                    time:2000  //2毫秒
                });
                document.myform.num1.focus();
                return false;
            }

            var num =document.myform.num1.value;
            if(num !='') {
                    if(!(/^[0-9]*$/).test(num)){
                        layer.msg('最少发行量内容格式错误！',{
                            time:2000  //2毫秒
                        });
                        document.myform.num1.focus();
                        return false;
                    }
            }


            if(document.myform.num2.value ==''){
                layer.msg('发行量(万)不能为空!',{
                    time:2000  //2毫秒
                });
                document.myform.num2.focus();
                return false;
            }

            var num2 =document.myform.num2.value;
            if(num2 != ""){
                if(!(/^[0-9]*$/).test(num2)){
                    layer.msg('发行量(万)格式错误！',{
                        time:2000  //2毫秒
                    });
                    document.myform.num2.focus();
                    return false;
                }
            }


            if(document.myform.money1.value ==''){
                layer.msg('分红条件交易金额不能为空!',{
                    time:2000  //2毫秒
                });
                document.myform.money1.focus();
                return false;
            }

            var money1 =document.myform.money1.value;
            if (money1 != "") {
                if(!(/^[0-9]*$/).test(money1)){
                    layer.msg('分红条件交易金额格式错误！',{
                        time:2000  //2毫秒
                    });
                    document.myform.money1.focus();
                    return false;
                }
            }



          if(document.myform.num3.value ==''){
            layer.msg('(万)---发行量(万)不能为空!',{
                time:2000  //2毫秒
            });
            document.myform.num3.focus();
            return false;
        }

        var num3 =document.myform.num3.value;
        if(num3 != "") {
            if(!(/^[0-9]*$/).test(num3)){
                layer.msg('(万)---发行量(万)格式错误！',{
                    time:2000  //2毫秒
                });
                document.myform.num3.focus();
                return false;
            }
        }


        if(document.myform.money2.value ==''){
            layer.msg('达到分红条件交易金额不能为空!',{
                time:2000  //2毫秒
            });
            document.myform.money2.focus();
            return false;
        }

        var money2 =document.myform.money2.value;
        if(money2 != "") {
            if(!(/^[0-9]*$/).test(money2)){
                layer.msg('达到分红条件交易金额格式错误！',{
                    time:2000  //2毫秒
                });
                document.myform.money2.focus();
                return false;
            }
        }


            if(document.myform.num4.value ==''){
                layer.msg('(万)---发行量(万)不能为空!',{
                    time:2000  //2毫秒
                });
                document.myform.num4.focus();
                return false;
            }

            var num4 =document.myform.num4.value;
            if(num4 != "") {
                if(!(/^[0-9]*$/).test(num4)){
                    layer.msg('(万)---发行量(万)格式错误！',{
                        time:2000  //2毫秒
                    });
                    document.myform.num4.focus();
                    return false;
                }
            }


            if(document.myform.money3.value ==''){
                layer.msg('达到分红条件交易金额不能为空!',{
                    time:2000  //2毫秒
                });
                document.myform.money3.focus();
                return false;
            }

            var money3 =document.myform.money3.value;
            if(money3 != "") {
                if(!(/^[0-9]*$/).test(money3)){
                    layer.msg('达到分红条件交易金额格式错误！',{
                        time:2000  //2毫秒
                    });
                    document.myform.money3.focus();
                    return false;
                }
            }


        }
	</script>
    <script type="text/javascript">
    var msg = "{{Session::get('message')}}";
    //触发事件
    $(window).ready(function(){
        if(msg){
            layer.msg(msg,{
                time:2000  //2毫秒
            });
        }
    });
	</script>
<!--/sidebar-->
<div class="main-wrap">
	<div class="crumb-wrap">
		<div class="crumb-list">
			<i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span
				class="crumb-step">&gt;</span><span class="crumb-name">系统设置</span>
		</div>
	</div>
	<div class="result-wrap">
		<form action="{{url('center/dividend/indexEdit')}}" method="post" id="myform"
			name="myform"  enctype="multipart/form-data" onsubmit="{return CheckForm();}">
			{{csrf_field()}}
			<div class="config-items">
				<div class="config-title">
					<h1>
						<i class="icon-font">&#xe00a;</i>网站信息设置
					</h1>
				</div>
				<div class="result-content">
					<table width="100%" class="insert-tab">
						<tbody>
							<tr>
								<th><i class="require-red">*</i>分红股名称：
								</th>
								<td>
									<select name="dividend_id">
										<option value="0">无分红</option>
										@foreach ($currency as $k=>$v)
											<option value="{{$v->currency_id}}" @if($list['dividend_id'] == $v->currency_id) selected @endif >{{$v->currency_name}}</option>
										@endforeach
									</select>
									<span style="color:red;">分红股设置的币种是用户达到条件获得分红的币种,如不需要分红奖励设置分红股名称为无分红</span>
								</td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>最少发行量：</th>
								<td><input type="text" id="num1" value="{{$list['num1']}}"
									size="85" name="num1" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>{{$list['num1']}}---发行量(万)：<br/>
									<i class="require-red">*</i>达到分红条件交易金额：<br/>
									<i class="require-red">*</i>每日做多获得奖励数：<br/>
									<i class="require-red">*</i>达到条件获得分红股数量（个）
								</th>
								<td><input type="text" id="num2" value="{{$list['num2']}}"
									size="85" name="num2" class="common-text"><br/>
									<input type="text" id="money1" value="{{$list['money1']}}"
									size="85" name="money1" class="common-text"><br/>
									<input type="text" id="" value=""
									size="85" name="max1" class="common-text"><span style="color:red;">如果用户当日获得币数大于最大获得数量业绩也不会增加</span>
									<br/>
									<input type="text" id="" value=""
									size="85" name="get1" class="common-text">
								</td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>{{$list['num2']}}(万)---发行量(万)：<br/>
									<i class="require-red">*</i>达到分红条件交易金额：<br/>
									<i class="require-red">*</i>每日做多获得奖励数：<br/>
									<i class="require-red">*</i>达到条件获得分红股数量（个）
								</th>
								<td><input type="text" id="num3" value="{{$list['num3']}}"
										   size="85" name="num3" class="common-text"><br/>
									<input type="text" id="money2" value="{{$list['money2']}}"
										   size="85" name="money2" class="common-text"><br/>
									<input type="text" id="" value=""
										   size="85" name="max2" class="common-text"><br/>
									<input type="text" id="" value=""
										   size="85" name="get2" class="common-text">
								</td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>{{$list['num3']}}(万)---发行量(万)：<br/>
									<i class="require-red">*</i>达到分红条件交易金额：<br/>
									<i class="require-red">*</i>每日做多获得奖励数：<br/>
									<i class="require-red">*</i>达到条件获得分红股数量（个）
								</th>
								<td><input type="text" id="num4" value="{{$list['num4']}}"
										   size="85" name="num4" class="common-text">
									<span style="color:red;">如果发行量超过系统设置发行量不会获得任何分红</span>
									<br/>
									<input type="text" id="money3" value="{{$list['money3']}}"
										   size="85" name="money3" class="common-text"><br/>
									<input type="text" id="" value=""
										   size="85" name="max3" class="common-text"><br/>
									<input type="text" id="" value=""
										   size="85" name="get3" class="common-text">
								</td>
							</tr>
							<tr>
								<th></th>
								<td>
									<input type="submit" value="提交" class="btn btn-primary btn6 mr10">
									<input type="button" value="返回" onclick="history.go(-1)" class="btn btn6">
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</form>
	</div>
</div>
<!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(4).show();
$(".sub-menu").eq(4).children("li").eq(2).addClass("on");
</script>
@endsection