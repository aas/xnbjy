@extends('admin.mainlayout')
@section('js')

@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">审核管理</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
                <form action="{{urlAdmin('Examine','getIndex')}}" method="get">
                    <table class="search-tab">
                        <tr>
                            <th width="70">用户ID:</th>
                            <td><input class="common-text" placeholder="会员id" name="u_id" type="text"></td>
                            <td><input class="btn btn-primary btn2"  value="查询" type="submit"></td>
                        </tr>
                    </table>
                </form>

            </div>
        </div>
        <div class="result-wrap">

            <div class="result-title">
            </div>
            <div class="result-content">
                <table class="result-tab" width="100%">
                    <tr>
                        <th>ID</th>
                        <th>会员ID</th>
                        <th>证件号码</th>
                        <th>身份证正面</th>
                        <th>身份证反面</th>
                        <th>手持身份证</th>
                        <th>申请时间</th>
                        <th>审核通过时间</th>
                        <th>状态</th>
                        <th>审核人员</th>
                        <th>操作</th>
                    </tr>
                    @if(!empty($list['data']))
                        @foreach($list['data'] as $v)
                        <tr>
                            <td>{{$v['id']}}</td>
                            <td>{{$v['u_id']}}</td>
                            <td>{{$v['idcard']}}</td>
                            <td><img height="70px;" width="150px;" src="{{$v['idcardPositive']}}" /></td>
                            <td><img height="70px;" width="150px;" src="{{$v['idcardSide']}}" /></td>
                            <td><img height="70px;" width="150px;" src="{{$v['idcardHold']}}" /></td>
                            <td>{{$v['add_time']}}</td>
                            <td>{{$v['examine_time']?$v['examine_time']:"未审核"}}</td>
                            <td>
                            @if($v['status']==0)
                            <span>审核中</span>
                            @endif
                            @if($v['status']==1)
                            <span style="color:#00FF00">审核通过</span>
                            @endif
                            @if($v['status']==2)
                            <span style="color:#FF0000">审核未通过</span>
                            @endif
                            </td>
                            <td>{{$v['examine_name']}}</td>
                            <td>
                <a class="link-update" href="{{urlAdmin('Examine','getEdit',array('id'=>$v['id']))}}">查看</a>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="11" align="center">暂无数据...</td>
                        </tr>
                    @endif
                </table>
                <div class="list-page">
                    <ul>
                        @if($list['last_page']>1)
                        <div>
                            @if(!empty($list['prev_page_url']))
                                <a class="prev" href="{{$list['prev_page_url']}}"><<</a>
                            @endif
                            @foreach($list['pageNoList'] as $v)
                                <a @if($v == $list['current_page'])
                                   class="current"
                                   @else
                                   class="num"
                                   @endif
                                   href="{{urlAdmin('Member','getShow')}}?page={{$v}}">{{$v}}</a>
                            @endforeach
                            @if(!empty($list['next_page_url']))
                                <a class="next" href="{{$list['next_page_url']}}">>></a>
                            @endif
                        </div>
                         @endif
                    </ul>
                </div>
            </div>

        </div>
    </div>

@endsection
@section('footer')
    <script>
        $(".sub-menu").eq(3).show();
        $(".sub-menu").eq(3).children("li").eq(3).addClass("on");
    </script>
@endsection