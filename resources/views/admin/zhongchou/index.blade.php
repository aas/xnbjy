@extends('admin.mainlayout')
@section('js')
<script>
    /********所有ajax加载效果**********/
    $(document).ajaxStart(function(){

        //加载效果
        layer.load(2, {shade: false});

    }).ajaxStop(function(){

        //关闭加载效果
        layer.closeAll("loading");
    });
    /********所有ajax加载效果**********/
    //单个删除
    function del(id){


        layer.confirm('确定要删除该项吗?',{btn:['确定','取消']},function(){
            layer.closeAll();

            $.ajax({
                type:'post',
                url:"{{urlAdmin('Zhongchou','postDel')}}",
                data:{'id':id,'_token':'{{csrf_token()}}'},
                dataType:'json',
                success: function (res) {

                    // alert(res);
                    if(res['status']==1){

                        layer.msg(res.msg,{icon:6});
                        setTimeout("location.reload()",1500);


                    }else{

                        layer.msg(res.msg,{icon:5});

                    }

                }

            });

        },function(){

            layer.msg('已取消!');

        });
    }

    function start(id){


        layer.confirm('确定要开启众筹吗?',{btn:['确定','取消']},function(){

            layer.closeAll();
            $.ajax({
                type:'post',
                url:"{{urlAdmin('Zhongchou','postZhongchouStart')}}",
                data:{'id':id,'_token':'{{csrf_token()}}'},
                dataType:'json',
                success: function (res) {

                    // alert(res);
                    if(res['status']==1){

                        layer.msg(res.msg,{icon:6});
                        setTimeout("location.reload()",1500);


                    }else{

                        layer.msg(res.msg,{icon:5});

                    }

                }

            });

        },function(){

            layer.msg('已取消!');

        });
    }



    function end(id){


        layer.confirm('确定要结束众筹吗?',{btn:['确定','取消']},function(){

            layer.closeAll();
            $.ajax({
                type:'post',
                url:"{{urlAdmin('Zhongchou','postZhongchouEnd')}}",
                data:{'id':id,'_token':'{{csrf_token()}}'},
                dataType:'json',
                success: function (res) {

                    // alert(res);
                    if(res['status']==1){

                        layer.msg(res.msg,{icon:6});
                        setTimeout("location.reload()",1500);


                    }else{

                        layer.msg(res.msg,{icon:5});

                    }

                }

            });

        },function(){

            layer.msg('已取消!');

        });
    }
</script>
@endsection
@section('content')
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{:U('Index/index')}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">众筹管理</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
            </div>
        </div>
        <div class="result-wrap">
            <form name="myform" id="myform" method="post">
                <div class="result-title">
                    <div class="result-list">
                        <a href="{{urlAdmin('Zhongchou','getAdd')}}"><i class="icon-font"></i>添加众筹</a>
                    </div>
                </div>
                <div class="result-content">
                    <table class="result-tab" width="100%">
                        <tr>
                            <th >币种编号</th>
                            <th >众筹名称</th>
                            <th >众筹币种</th>
                            <th >购买币种</th>
                            <th >众筹保留量</th>
                            <th >众筹发售量</th>
                            <th >众筹剩余量</th>
                            <th >众筹价格</th>
                            <th >每人限购</th>
                            <th >成功比例</th>
                            <th>开始时间</th>
                            <th>结束时间</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                        @if(!empty($list['data']))
                            @foreach($list['data'] as $v)
                            <tr>
                                <td>{{$v['id']}}</td>
                                <td>{{$v['title']}}</td>
                                <td>{{$v['name']}}</td>
                                <td>
                                    @if($v['buy_currency_id']==0)
                                        人民币
                                    @endif
                                    @if($v['buy_currency_id']==29)
                                        丘特币
                                    @endif
                                    @if($v['buy_currency_id']==30)
                                        教育币
                                    @endif
                                </td>
                                <td>{{$v['num_nosell']}}</td>
                                <td>{{$v['num']}}</td>
                                <td>{{$v['deal']}}</td>
                                <td>{{$v['price']}}</td>
                                <td>{{$v['limit']}}</td>
                                <td>{{$v['zhongchou_success_bili']*100}}%</td>
                                <td>{{date('Y-m-d H:i:s',$v['add_time'])}}</td>
                                <td>{{date('Y-m-d H:i:s',$v['end_time'])}}</td>
                                <td>
                                    @if($v['status']==0)
                                    众筹开始
                                    @elseif($v['status']==1)
                                    众筹冻结
                                    @elseif($v['status']==3)
                                    众筹结束
                                    @else
                                    未知状态
                                    @endif
                                </td>
                                <td><a href="{{urlAdmin('Zhongchou','getEdit',array('id'=>$v['id']))}}">修改</a>
                                    <a href="javascript:void(0)" onclick="del({{$v['id']}})">删除</a>
                                    <a href="javascript:void(0)" onclick="end({{$v['id']}})">结束众筹</a>&nbsp;
                                    <a href="javascript:void(0)" onclick="start({{$v['id']}})">开启众筹</a>
                                    <a href="{{urlAdmin('Zhongchou','getAwardsAdd',array('id'=>$v['id']))}}">添加众筹推荐奖励</a>
                                </td>
                            </tr>
                             @endforeach
                        @else
                            <tr>
                                <td colspan="14" align="center">暂无数据...</td>
                            </tr>
                        @endif
                        {{--<tr>--}}
                            {{--<td colspan=14 class="page">--}}
                            {{--</td>--}}
                        {{--</tr>--}}
                    </table>
                    <div class="list-page">
                        <ul>
                            @if($list['last_page']>1)
                            <div>
                                <a class="prev" href="{{$list['prev_page_url']}}"><<</a>
                                @foreach($list['pageNoList'] as $v)
                                    <a @if($v == $list['current_page'])
                                       class="current"
                                       @else
                                       class="num"
                                       @endif
                                       href="{{urlAdmin('Zhongchou','getIndex')}}?page={{$v}}">{{$v}}</a>
                                @endforeach
                                <a class="next" href="{{$list['next_page_url']}}">>></a>
                            </div>
                            @endif
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('footer')
    <script>
        $(".sub-menu").eq(5).show();
        $(".sub-menu").eq(5).children("li").eq(0).addClass("on");
    </script>
@endsection