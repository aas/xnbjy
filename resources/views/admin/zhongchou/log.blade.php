@extends('admin.mainlayout')
@section('js')

@endsection
@section('content')
	<div class="main-wrap">


		<div class="crumb-wrap">
			<div class="crumb-list"><i class="icon-font"></i><a href="{:U('Index/index')}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">众筹个人记录</span></div>
		</div>
		<div class="search-wrap">
			<div class="search-content">
				<div class="search-wrap">
					<div class="search-content">
						<form action="{{urlAdmin('Zhongchou','getLog')}}" method="get">
							<table class="search-tab">
								<tr>
									<th width="120">选择分类:</th>
									<td>
										<select name="iid" >
											<option value="0">全部</option>
												@foreach($issue as $v)
												<option value="{{$v['id']}}">{{$v['title']}}</option>
												@endforeach
										</select>
									</td>
									<th width="70">用户姓名:</th>
									<td><input class="common-text" placeholder="用户姓名" name="name" value="" id="" type="text"></td>
									<th width="70">用户ID:</th>
									<td><input class="common-text" placeholder="用户ID" name="uid" value="" id="" type="text"></td>
									<th width="70">用户邮箱:</th>
									<td><input class="common-text" placeholder="用户邮箱" name="email" value="" id="" type="text"></td>
									<td><input class="btn btn-primary btn2" name="sub" value="查询" type="submit"></td>
								</tr>
							</table>
						</form>
						<form action="" method="post">
							<table class="search-tab">
								<tr>
									<th width="120">选择分类:</th>
									<td>
										<select name="iid" id="zhongchouid">
											<option value="0">全部</option>
											@foreach($issue as $v)
												<option value="{{$v['id']}}">{{$v['title']}}</option>
											@endforeach
										</select>
									</td>
									<td><input class="btn btn-primary btn2" name="sub" value="开始解冻" type="button" onclick="kaishijiedong()">（按照比例开始第一次解冻）</td>
									<td><input class="btn btn-primary btn2" name="sub" value="解冻该众筹" type="button" onclick="jiedongquanbu()">（解冻当前选择众筹全部币种）</td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="result-wrap">
			<form name="myform" id="myform" method="post">
				<div class="result-content">
					<table class="result-tab" width="100%">
						<tr>
							<th>众筹编号</th>
							<th>众筹名称</th>
							<th>购买人</th>
							<th>购买数量</th>
							<th>冻结数量</th>
							<th>单价</th>
							<th>购买总额</th>
							<th>时间</th>
							<th>购买花费币种类型</th>
							<th>操作</th>
						</tr>

							@if(!empty($data['data']))
								@foreach($data['data'] as $v)
							<tr>
								<td>{{$v['iid']}}</td>
								<td>{{$v['title']}}</td>
								<td>{{$v['name']}}</td>
								<td>{{$v['num']}}</td>
								<td>{{$v['deal']}}</td>
								<td>{{$v['price']}}</td>
								<td>{{$v['count']}}</td>
								<td>{{date('Y-m-d H:i:s',$v['add_time'])}}</td>
								<td>[{{$v['buy_name']}}]</td>
								<td><a href="#" onclick="jiedongById({{$v['id']}});">解冻</a></td>
							</tr>
								@endforeach
							@else
							<tr>
								<td colspan="10" align="center">暂无数据...</td>
							</tr>
							@endif

					</table>
					<div class="list-page">
						<ul>
							@if($data['last_page']>1)
							<div>
								@if(!empty($data['prev_page_url']))
									<a class="prev" href="{{$data['prev_page_url']}}"><<</a>
								@endif
								@foreach($data['pageNoList'] as $v)
									<a @if($v == $data['current_page'])
									   class="current"
									   @else
									   class="num"
									   @endif
									   href="{{urlAdmin('Zhongchou','getLog')}}?page={{$v}}">{{$v}}</a>
								@endforeach
								@if(!empty($data['next_page_url']))
									<a class="next" href="{{$data['next_page_url']}}">>></a>
								@endif
							</div>
							@endif
						</ul>
					</div>
				</div>
			</form>
		</div>
	</div>

@endsection
@section('footer')
	<script>
		//解冻单个
        function jiedongById(id){

            layer.confirm('确定要解冻该项吗?',{btn:['确定','取消']},function(){

                layer.closeAll();
                layer.load(2, {shade: false});
                $.ajax({
                    type:'post',
                    url:"{{urlAdmin('Zhongchou','postJiedongById')}}",
                    data:{'id':id,'_token':'{{csrf_token()}}'},
                    dataType:'json',
                    success: function (res) {

                        layer.closeAll();
                        if(res['status']==1){

                            layer.msg(res.msg,{icon:6});
                            setTimeout("location.reload()",1500);

                        }else{

                            layer.msg(res.msg,{icon:5});

                        }

                    }

                });

            },function(){

                layer.msg('已取消了!');

            });
        }

        //解冻全部一类
        function jiedongquanbu(){

            layer.confirm('确定要解冻该类所有众筹吗?',{btn:['确定','取消']},function(){

				var iid = $('#zhongchouid').val();

                layer.closeAll();
                layer.load(2, {shade: false});
                $.ajax({
                    type:'post',
                    url:"{{urlAdmin('Zhongchou','postJiedongByIid')}}",
                    data:{'iid':iid,'_token':'{{csrf_token()}}'},
                    dataType:'json',
                    success: function (res) {

                        layer.closeAll();
                        if(res['status']==1){

                            layer.msg(res.msg,{icon:6});
                            setTimeout("location.reload()",1500);

                        }else{

                            layer.msg(res.msg,{icon:5});

                        }

                    }

                });

            },function(){

                layer.msg('已取消了!');

            });
        }




		function kaishijiedong(){
			layer.confirm('确定开始解冻？', {
				btn: ['确定','取消'], //按钮
				title: '开始解冻'
			}, function(){
                layer.closeAll();
                layer.load(2, {shade: false});
				$.post("{{urlAdmin('Zhongchou','postJiedong_start')}}",
					{iid:$('#zhongchouid').val(),'_token':'{{csrf_token()}}'},
					function(res){
                        layer.closeAll();
					if(res['status'] == 1){
                        layer.msg(res.msg,{icon:6});
						setTimeout(function(){location.reload();},1000);
					}else{
                        layer.msg(res.msg,{icon:5});
					}
				})
			}, function(){
				layer.msg('已取消');
			});

		}



	</script>
	<script>
		$(".sub-menu").eq(5).show();
		$(".sub-menu").eq(5).children("li").eq(1).addClass("on");
	</script>
@endsection