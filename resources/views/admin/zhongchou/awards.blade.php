@extends('admin.mainlayout')
@section('js')
<script>
    window.onload = function(){
        /********快闪重定向*********/
        var message = "{{Session::get('message')}}";
        $(window).ready(function () {
            if (message) {
                layer.msg(message, {
                    time: 3000,

                });
            }
        });
        /********快闪重定向*********/
    }

</script>
@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">推荐奖励管理</span></div>
        </div>
        <div class="result-wrap">
            <form action="{{urlAdmin('Zhongchou','postAwardsAdd')}}" method="post" id="myform" name="myform">
                {{csrf_field()}}
                <div class="config-items">
                    <div class="config-title">
                        <h1><i class="icon-font">&#xe00a;</i>推荐奖励管理</h1>
                    </div>
                    <div class="result-content">
                        <table width="100%" class="insert-tab">
                            <tbody>
                            <input type="hidden" name="id" value="{{$list['id']}}" />
                            <tr>
                                <th width="15%"><i class="require-red">*</i>奖励币种：</th>
                                <td>
                                    <select name="zc_awards_currency_id">

                                            @foreach($currency as $v)
                                            <option value="{{$v['currency_id']}}"
                                                    @if($v['currency_id']==$list['zc_awards_currency_id'])
                                                    selected
                                                    @endif
                                                >{{$v['currency_name']}}
                                            </option>
                                            @endforeach

                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>一级奖励比例：</th>
                                <td><input name="zc_awards_one_ratio" value="{{$list['zc_awards_one_ratio']}}" type="text">%&nbsp;&nbsp;无比例请填写 0</td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>二级奖励比例：</th>
                                <td><input name="zc_awards_two_ratio" value="{{$list['zc_awards_two_ratio']}}" type="text">%&nbsp;&nbsp;无比例请填写 0</td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>是否开启奖励：</th>
                                <td><input type="radio" value="1"
                                        @if($list['zc_awards_status'] ==1)
                                        checked
                                        @endif
                                    name="zc_awards_status" >开启
                                    <input type="radio" value="0"
                                        @if($list['zc_awards_status']==0)
                                        checked
                                        @endif
                                    name="zc_awards_status" >关闭</td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <input type="submit" value="提交" class="btn btn-primary btn6 mr10">
                                    <a href="#" onclick="history.go(-1)"><input type="button" value="返回"  class="btn btn6"></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('footer')
    <script>
        $(".sub-menu").eq(5).show();
        $(".sub-menu").eq(5).children("li").eq(0).addClass("on");
    </script>
@endsection