@extends('admin.mainlayout')
@section('js')

@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{:U('Index/index')}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">众筹个人记录</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
            </div>
            <div class="result-wrap">
                <div class="result-content">
                    <table class="result-tab" width="100%">
                        <tr>
                            <th>编号</th>
                            <th>奖励人员</th>
                            <th>内容</th>
                            <th>奖励币种</th>
                            <th>奖励钱数</th>
                            <th>时间</th>
                        </tr>
                            @if(!empty($data['data']))
                            @foreach($data['data'] as $v)
                            <tr>
                                <td>{{$v['finance_id']}}</td>
                                <td>{{$v['email']}}</td>
                                <td>{{$v['content']}}</td>
                                <td>
                                    @if($v['currency_id']==0)
                                        人民币
                                    @else
                                        {{$v['currency_name']}}
                                    @endif
                                </td>
                                <td>{{$v['money']}}</td>
                                <td>{{date('Y-m-d H:i:s',$v['add_time'])}}</td>
                            </tr>
                            @endforeach
                            @else
                            <tr>
                                <td colspan="6" align="center">暂无数据...</td>
                            </tr>
                            @endif
                    </table>
                    <div class="list-page">
                        <ul>
                            @if($data['last_page']>1)
                            <div>
                                @if(!empty($data['prev_page_url']))
                                    <a class="prev" href="{{$data['prev_page_url']}}"><<</a>
                                @endif
                                @foreach($data['pageNoList'] as $v)
                                    <a @if($v == $data['current_page'])
                                       class="current"
                                       @else
                                       class="num"
                                       @endif
                                       href="{{urlAdmin('Zhongchou','getAwardsList')}}?page={{$v}}">{{$v}}</a>
                                @endforeach
                                @if(!empty($data['next_page_url']))
                                    <a class="next" href="{{$data['next_page_url']}}">>></a>
                                @endif
                            </div>
                            @endif

                        </ul>
                    </div>
                </div>
            </div>
        </div>

@endsection
@section('footer')
<script>
    $(".sub-menu").eq(5).show();
    $(".sub-menu").eq(5).children("li").eq(2).addClass("on");
</script>
@endsection