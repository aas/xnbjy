@extends('admin.mainlayout')
@section('content')
    <style>
        .list-page ul{overflow:hidden;display:inline-block;}
        .list-page ul li{float:left;border:none;height:30px;line-height:30px;border: 1px}
        .list-page ul li a{padding:6px 12px;text-decoration:none}
    </style>
    <!--/sidebar-->
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">财务日志</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
				<div class="search-wrap">
					<div class="search-content">
						<form action="{{url('center/finance/index')}}" method="get">
                            {{csrf_field()}}
							<table class="search-tab">
								<tr>
									<th width="120">选择分类:</th>
									<td>
										<select name="type_id" id="">
											<option value="0">全部</option>
                                            @foreach($type as $k=>$v)
											<option value="{{$v->id}}">{{$v->name}}</option>
                                            @endforeach
										</select>
									</td>
									<th width="70">用户:</th>
									<td><input class="common-text" placeholder="用户" name="name"  id="" type="text"></td>
                                    <th width="70">用户ID:</th>
									<td><input class="common-text" placeholder="用户ID" name="member_id"  id="" type="text"></td>
									<td><input class="btn btn-primary btn2" name="sub" value="查询" type="submit"></td>
                                 </tr>
                                 <tr>   
                                    <th><i class="require-red">*</i>开始时间：</th>
                                    <td>
                            
                                    <div class="row"><div class="col-md-6">
                                                    <i class="fa fa-calendar"></i></span>
                                                    <input type="text" name="add_time" class="ui_timepicker" id="aaaaa1" placeholder="选择日期时间" value="{{date('Y-m-d H:i:s',0)}}"  />
                                                </div></div></td>
                                  <script type="text/javascript">
                                    $(function () {
                                        $("#aaaaa1").datetimepicker({
                                            //showOn: "button",
                                            //buttonImage: "./css/images/icon_calendar.gif",
                                            //buttonImageOnly: true,
                                            showSecond: true,
                                            timeFormat: 'hh:mm:ss',
                                            stepHour: 1,
                                            stepMinute: 1,
                                            stepSecond: 1
                                        })
                                            $("#aaaaa2").datetimepicker({
                                            //showOn: "button",
                                            //buttonImage: "./css/images/icon_calendar.gif",
                                            //buttonImageOnly: true,
                                            showSecond: true,
                                            timeFormat: 'hh:mm:ss',
                                            stepHour: 1,
                                            stepMinute: 1,
                                            stepSecond: 1
                                        })
                                   
                                    })
                                </script>
                                    <th><i class="require-red">*</i>截止时间：</th>
                                    <td><div class="row"><div class="col-md-6">
                                                    <i class="fa fa-calendar"></i></span>
                                                    <input type="text" name="end_time" class="ui_timepicker" id="aaaaa2" placeholder="选择日期时间" value="{{date('Y-m-d H:i:s',time())}}" />
                                                </div></div></td>
									<td><input class="btn btn-primary btn2" name="sub" value="下载表格" type="button" onclick="download()"></td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
        </div>
        <div class="result-wrap">
            <form name="myform" id="myform" method="post">
                <div class="result-content">
                    <table class="result-tab" width="100%">
                        <tr>
                            <th>日志编号</th>
                            <th>所属</th>
                            <th>财务类型</th>
                            <th>内容</th>
                            <th>金额</th>
                            <th>币种</th>
							<th>收入/支出</th>
                            <th>时间</th>
                            <th>操作</th>
                        </tr>
                        @if($result != "")
                            @foreach($result as $k=>$v)
                        <tr>
                            <td>{{$v->finance_id}}</td>
                            <td>{{$v->username}}</td>
                            <td>{{$v->typename}}</td>
                            <td>{{$v->content}}</td>
                            <td>{{$v->money}}</td>
                            <td>{{$v->currency_name}}</td>
							<td>@if($v->money_type == 1) 收入 @else 支出 @endif</td>
                            <td>{{date('Y-m-d H:i:s',$v->add_time)}}</td>
                            <td>
							</td>
                        </tr>
                            @endforeach
                        @else
                            <tr><td style="text-align: center;font-size: 18px;" colspan="9">暂无数据...</td></tr>
                        @endif
                    </table>
					<div class="list-page">{{ $result->appends(['type_id'=>$type_id,'name'=>$name,'member_id'=>$member_id])->render() }}</div>
                </div>
            </form>
        </div>
    </div>
    <!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(6).show();
$(".sub-menu").eq(6).children("li").eq(2).addClass("on");
</script>
<script>
function download(){
	var add_time=$('#aaaaa1').val();
	var end_time=$('#aaaaa2').val();
	var url="{{url('center/finance/payExcel')}}";
	window.location.href=url+"?add_time="+add_time+"&&end_time="+end_time;
}
</script>
@endsection