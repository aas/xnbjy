@extends('admin.mainlayout')
@section('content')
    <!--/sidebar-->
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">财务明细</span></div>
        </div>
        <div class="search-wrap">
        </div>
        <div class="result-wrap">
             <table class="result-tab" width="100%">
                <tr>
                    <td>人工充值人民币总额</td>
                    <td>{{$pay}}</td>
                    <td>管理员充值人民币总额</td>
                    <td>{{$pay_admin}}</td>
                </tr>
                <tr>
                    <td>提现成功总额</td>
                    <td>{{empty($draw)? 0:$draw}}</td>
                    <td>会员持有人民币总额</td>
                    <td>{{$rmb}}</td>
                </tr>
                 @foreach($currency_user as $k=>$v)
                <tr>
                    <td>会员持{{$v['name']}}币可用总额</td>
                    <td>{{$v['num']}}</td>
                    <td>会员持{{$v['name']}}币冻结总额</td>
                    <td>{{$v['forzen_num']}}</td>
                </tr>
                 @endforeach
            </table>
        </div>
    </div>
    <!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(6).show();
$(".sub-menu").eq(6).children("li").eq(2).addClass("on");
</script>
@endsection