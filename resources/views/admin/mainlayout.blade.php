<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" >
    <meta name="renderer" content="webkit">
    <title>网站后台管理</title>
    <link rel="stylesheet" type="text/css" href="/admin/css/common.css"/>
    <link rel="stylesheet" type="text/css" href="/admin/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="/admin/iconfont/demo.css">
    <link rel="stylesheet" type="text/css" href="/admin/iconfont/iconfont.css"/>
    <script type="text/javascript" src="/admin/js/libs/modernizr.min.js"></script>
    <script type="text/javascript" src="/admin/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="/js/layer/layer.js"></script>
    <script type="text/javascript" src="/js/laydate/laydate.js"></script>
    <link type="text/css" href="/admin/css/jquery-ui-1.8.17.custom.css" rel="stylesheet" />
    <link type="text/css" href="/admin/css/jquery-ui-timepicker-addon.css" rel="stylesheet" />
    <script type="text/javascript" src="/admin/js/jquery-ui-1.8.17.custom.min.js"></script>
    <script type="text/javascript" src="/admin/js/jquery-ui-timepicker-addon.js"></script>
    <script type="text/javascript" src="/admin/js/jquery-ui-timepicker-zh-CN.js"></script>
    <script>
        (function ($) {
            // 汉化 Datepicker
            $.datepicker.regional['zh-CN'] =
            {
                clearText: '清除', clearStatus: '清除已选日期',
                closeText: '关闭', closeStatus: '不改变当前选择',
                prevText: '<上月', prevStatus: '显示上月',
                nextText: '下月>', nextStatus: '显示下月',
                currentText: '今天', currentStatus: '显示本月',
                monthNames: ['一月', '二月', '三月', '四月', '五月', '六月',
                    '七月', '八月', '九月', '十月', '十一月', '十二月'],
                monthNamesShort: ['一', '二', '三', '四', '五', '六',
                    '七', '八', '九', '十', '十一', '十二'],
                monthStatus: '选择月份', yearStatus: '选择年份',
                weekHeader: '周', weekStatus: '年内周次',
                dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
                dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
                dayNamesMin: ['日', '一', '二', '三', '四', '五', '六'],
                dayStatus: '设置 DD 为一周起始', dateStatus: '选择 m月 d日, DD',
                dateFormat: 'yy-mm-dd', firstDay: 1,
                initStatus: '请选择日期', isRTL: false
            };
            $.datepicker.setDefaults($.datepicker.regional['zh-CN']);

            //汉化 Timepicker
            $.timepicker.regional['zh-CN'] = {
                timeOnlyTitle: '选择时间',
                timeText: '时间',
                hourText: '小时',
                minuteText: '分钟',
                secondText: '秒钟',
                millisecText: '微秒',
                timezoneText: '时区',
                currentText: '现在时间',
                closeText: '关闭',
                timeFormat: 'hh:mm',
                amNames: ['AM', 'A'],
                pmNames: ['PM', 'P'],
                ampm: false
            };
            $.timepicker.setDefaults($.timepicker.regional['zh-CN']);
        })(jQuery);
    </script>
    @yield('js')
    <style>
        .iconfont{ padding-right:5px;}
        .fsize{ font-size:15px;}
    </style>
</head>
<body>
<div class="topbar-wrap white">
    <div class="topbar-inner clearfix">
        <div class="topbar-logo-wrap clearfix">

            <h1 class="topbar-logo none"><a href="#" class="navbar-brand">后台管理</a></h1>
            <ul class="navbar-list clearfix">
                <li><a class="on" href="{{url('center/index')}}">首页</a></li>
                <li><a href="{{url('center/index')}}" target="_blank">网站首页</a></li>
                <li><a href="{{url('center/index/info')}}" target="_blank">全站统计信息</a></li>
            </ul>
        </div>
        <div class="top-info-wrap">
            <ul class="top-info-list clearfix">
                <li><a href="{{url('center/manage/index')}}">管理员</a></li>
                <li><a href="{{url('center/manage/edit_pwd')}}">修改密码</a></li>
                <li><a href="{{urlAdmin('login','getLogout')}}">退出</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="container clearfix">
    <div class="sidebar-wrap">
        <div class="sidebar-title">
            <h1>菜单</h1>
        </div>
        <div class="sidebar-content">
            <ul class="sidebar-list">
                @if(!empty(session('menus')['sys_nav']))
                <li>
                        <a href="#"><i class="iconfont">&#xe614;</i><span class="fsize">系统管理</span></a>
                        <ul class="sub-menu">
                            @foreach(session('menus')['sys_nav'] as $k=>$v)
                        <li><a href="{{url($v['nav_url'])}}"><i class="iconfont">{{$v["nav_e"]}}</i>&nbsp;{{$v['nav_name']}}</a></li>
                             @endforeach
                        </ul>
                    </li>
                @endif

            @if(!empty(session('menus')['bank_nav']))
    
                <li>
                        <a href="#"><i class="iconfont">&#xe635;</i><span class="fsize">银行管理</span></a>
                        <ul class="sub-menu">
                            @foreach(session('menus')['bank_nav'] as $k=>$v)
                                <li><a href="{{url($v['nav_url'])}}"><i class="iconfont">{{$v["nav_e"]}}</i>&nbsp;{{$v['nav_name']}}</a></li>
                            @endforeach
                        
                        </ul>
                    </li>
    
                @endif

            @if(!empty(session('menus')['common_nav']))
    
                <li>
                        <a href="#"><i class="iconfont">&#xe635;</i><span class="fsize">常用操作</span></a>
                        <ul class="sub-menu">
                            @foreach(session('menus')['common_nav'] as $k=>$v)
                                <li><a href="{{url($v['nav_url'])}}"><i class="iconfont">{{$v["nav_e"]}}</i>&nbsp;{{$v['nav_name']}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                @endif


            @if(!empty(session('menus')['user_nav']))
    
                <li>
                        <a href="#"><i class="iconfont">&#xe64d;</i><span class="fsize">会员管理</span></a>
                        <ul class="sub-menu">
                            @foreach(session('menus')['user_nav'] as $k=>$v)
                                <li><a href="{{url($v['nav_url'])}}"><i class="iconfont">{{$v["nav_e"]}}</i>&nbsp;{{$v['nav_name']}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                @endif

            @if(!empty(session('menus')['bonus_nav']))
                <li>
                        <a href="#"><i class="icon-font">&#xe018;</i><span class="fsize">分红管理</span></a>
                        <ul class="sub-menu">
                            @foreach(session('menus')['bonus_nav'] as $k=>$v)
                                <li><a href="{{url($v['nav_url'])}}"><i class="iconfont">{{$v["nav_e"]}}</i>&nbsp;{{$v['nav_name']}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                @endif

            @if(!empty(session('menus')['zhongchou_nav']))
                <li>
                        <a href="#"><i class="iconfont">&#xe650;</i><span class="fsize">众筹管理</span></a>
                        <ul class="sub-menu">
                            @foreach(session('menus')['zhongchou_nav'] as $k=>$v)
                                <li><a href="{{url($v['nav_url'])}}"><i class="iconfont">{{$v["nav_e"]}}</i>&nbsp;{{$v['nav_name']}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                @endif

            @if(!empty(session('menus')['finance_nav']))
                <li>
                        <a href="#"><i class="iconfont">&#xe6c8;</i><span class="fsize">财务管理</span></a>
                        <ul class="sub-menu">
                            @foreach(session('menus')['finance_nav'] as $k=>$v)
                                <li><a href="{{url($v['nav_url'])}}"><i class="iconfont">{{$v["nav_e"]}}</i>&nbsp;{{$v['nav_name']}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                @endif
                @if(!empty(session('menus')['trade_nav']))
                <li>
                        <a href="#"><i class="iconfont">&#xe631;</i><span class="fsize">交易管理</span></a>
                        <ul class="sub-menu">
                            @foreach(session('menus')['trade_nav'] as $k=>$v)
                                <li><a href="{{url($v['nav_url'])}}"><i class="iconfont">{{$v["nav_e"]}}</i>&nbsp;{{$v['nav_name']}}</a></li>
                            @endforeach
                            
                        </ul>
                    </li>
               @endif
                @if(!empty(session('menus')['wallet_nav']))
                <li>
                        <a href="#"><i class="iconfont">&#xe631;</i><span class="fsize">钱包币种管理</span></a>
                        <ul class="sub-menu">
                            @foreach(session('menus')['wallet_nav'] as $k=>$v)
                                <li><a href="{{url($v['nav_url'])}}"><i class="iconfont">{{$v["nav_e"]}}</i>&nbsp;{{$v['nav_name']}}</a></li>
                            @endforeach
                            

                        </ul>
                    </li>
                @endif
                @if(!empty(session('menus')['article_nav']))
                <li>
                        <a href="#"><i class="iconfont">&#xe6f7;</i><span class="fsize">文章管理</span></a>
                        <ul class="sub-menu">
                            @foreach(session('menus')['article_nav'] as $k=>$v)
                                <li><a href="{{url($v['nav_url'])}}"><i class="iconfont">{{$v["nav_e"]}}</i>&nbsp;{{$v['nav_name']}}</a></li>
                            @endforeach
                            
                        </ul>
                    </li>
                   @endif
                @if(!empty(session('menus')['admin_nav']))
                <li>
                        <a href="#"><i class="iconfont">&#xe64d;</i><span class="fsize">管理员管理</span></a>
                        <ul class="sub-menu">
                            @foreach(session('menus')['admin_nav'] as $k=>$v)
                                <li><a href="{{url($v['nav_url'])}}"><i class="iconfont">{{$v["nav_e"]}}</i>&nbsp;{{$v['nav_name']}}</a></li>
                            @endforeach
                            
                        </ul>
                    </li>
                @endif
                @if(!empty(session('menus')['tongji_nav']))
                <li>
                        <a href="#"><i class="iconfont">&#xe64d;</i><span class="fsize">统计</span></a>
                        <ul class="sub-menu">
    
                            @foreach(session('menus')['tongji_nav'] as $k=>$v)
                                <li><a href="{{url($v['nav_url'])}}"><i class="iconfont">{{$v["nav_e"]}}</i>&nbsp;{{$v['nav_name']}}</a></li>
                            @endforeach
    
                            
                        </ul>
                    </li>
                    @endif
            </ul>
        </div>
    </div>
    @yield('content')
</div>
</body>
</html>
<script type="text/javascript">
    $(".sidebar-list li").children("a").on("click",function(){
        $(this).next(".sub-menu").toggle();
    });
</script>
@yield('footer')