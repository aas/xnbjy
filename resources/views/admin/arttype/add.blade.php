@extends('admin.mainlayout')
@section('js')
    <script>
        window.onload = function(){

            /********快闪重定向*********/
            var message = "{{Session::get('message')}}";
            var status = "{{Session::get('status')}}";
            $(window).ready(function () {
                if (message) {
                    layer.msg(message, {
                        time: 3000,

                    });
                }
                if(status==1){

                    location.href="{{url('center/art/help?article_category_id=6')}}";

                }

            });
            /********快闪重定向*********/

        }


        function checkForm(){
            var name = document.getElementById('name').value;
            var sort = document.getElementById('sort').value;
            if(name != ""){
                if(!isNaN(sort)){
                    document.getElementById('myform').submit();
                }else{
                    alert("排序请输入数字");
                }
            }else{
                alert('请填写分类名称');
            }
        }
    </script>
    <style>
        .newtable {width:60%; float:left; border:#eee 1px  solid; text-align:center;}
        .newtable  tr  td{line-height: 40px;  border:#eee 1px  solid;}
    </style>
@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap" >
            <div class="crumb-list"><i class="icon-font"></i><a href="">首页</a><span class="crumb-step">&gt;</span><a class="crumb-name" href="">文章分类管理</a><span class="crumb-step">&gt;</span><span>新增文章分类</span></div>
        </div>
        <div class="result-wrap" style="border:none;">
            <div class="result-content" style="float:left; width:40%;">
                <form action="{{urlAdmin('Arttype','postAdd')}}" method="post" id="myform" name="myform" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <table class="insert-tab">
                        <tbody>
                        <tr>
                            <th><i class="require-red">*</i>分类名称：</th>
                            <td><input class="common-text required" id="name" name="name" size="50" value="" type="text"></td>
                        </tr>
                        <tr>
                            <th>关键字：</th>
                            <td><input class="common-text required" id="keyword" name="keyword" size="50" value="" type="text"></td>
                        </tr>
                        <tr>
                            <th>排序(输入数字)：</th>
                            <td><input class="common-text required" id="sort" name="sort" size="50" value="" type="text"></td>
                        </tr>
                        <input type="hidden" name="fenlei_id" value="">
                        <tr><i class="require-red">*</i>为必填，其他选填</tr>
                        <tr>
                            <th></th>
                            <td><input class="btn btn-primary btn6 mr10" value="提交"  type="button" onclick="checkForm()" >
                                <input class="btn btn6" onclick="history.go(-1)" value="返回" type="button"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>

            </div>
            <table class="newtable" >
                <tr>
                    <td>ID</td>
                    <td>分类名：</td>
                    <td>关键字：</td>
                    <td>排序：</td>
                    <td>操作</td>
                </tr>
                    @foreach($list as $v)
                    <tr>
                        <td>{{$v['id']}}</td>
                        <td>{{$v['name']}}</td>
                        <td>{{$v['keywords']}}</td>
                        <td>{{$v['sort']}}</td>
                        <td>
                            <a class="link-update" href="{{urlAdmin('Arttype','getEdit',array('id'=>$v['id']))}}">修改</a>
                            &nbsp;&nbsp;
                            <a class="link-del" href="{{urlAdmin('Arttype','getDel',array('id'=>$v['id']))}}" onclick="return confirm('确认删除吗？')">删除</a></td>
                    </tr>
                    @endforeach
            </table>
        </div>
    </div>

@endsection
@section('footer')
    <script>
        $(".sub-menu").eq(9).show();
        $(".sub-menu").eq(9).children("li").eq(2).addClass("on");
    </script>
@endsection