<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>网站后台管理</title>
    <link href="/Admin/css/admin_login.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="/admin/js/jquery-1.7.2.min.js"></script>
    <script type="text/javascript" src="/js/layer/layer.js"></script>
<style>
body{background-image:url(/admin/images/adminbg.jpg);background-size:cover;background-repeat: repeat;}
</style>
    <script>
        window.onload = function(){

            /********快闪重定向*********/
            var message = "{{Session::get('message')}}";
            $(window).ready(function () {
                if (message) {
                    layer.msg(message, {
                        time: 3000,

                    });
                }
            });
            /********快闪重定向*********/
        }
    </script>
</head>
<body>
<div class="admin_login_wrap">
    <h1>后台登录</h1>
    <div class="adming_login_border">
        <div class="admin_input">
            <form action="{{urlAdmin('Login','postLogin')}}" method="post">
                {{csrf_field()}}
                <ul class="admin_items">
                    <li>
                        <label for="user">用户名：</label>
                        <input type="text" name="username" value="admin" id="user" size="40" class="admin_input_style" />
                    </li>
                    <li>
                        <label for="pwd">密码：</label>
                        <input type="password" name="pwd" value="123456" id="pwd" size="40" class="admin_input_style" />
                    </li>
                    <li>
                        <input type="submit" tabindex="3" value="提交" class="btn btn-primary" />
                    </li>
                </ul>
            </form>
        </div>
    </div>
    
</div>
</body>
</html>