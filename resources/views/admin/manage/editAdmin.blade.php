@extends('admin.mainlayout')
@section('content')
    <script type="text/javascript">
        var msg = "{{Session::get('message')}}";
        //触发事件
        $(window).ready(function(){
            if(msg){
                layer.msg(msg,{
                    time:2000  //2毫秒
                });
            }
        });
    </script>
    {{--<script>--}}
        {{--function CheckForm() {--}}

            {{--if(document.myform.pwd_show.value == ""){--}}
                {{--layer.msg('管理员密码不能为空！',{time:2000});--}}
                {{--document.myform.pwd_show.focus();--}}
                {{--return false;--}}
            {{--}--}}

            {{--if(document.myform.password.value == ""){--}}
                {{--layer.msg('管理员名称不能为空！',{time:2000});--}}
                {{--document.myform.password.focus();--}}
                {{--return false;--}}
            {{--}--}}
        {{--}--}}
    {{--</script>--}}
    <!--/sidebar-->
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">管理员管理</span></div>
        </div>
        <div class="result-wrap">
            <form action="{{url('center/manage/edit_admin')}}" method="post" id="myform" name="myform" enctype="multipart/form-data" onsubmit="{return CheckForm()}">
                {{csrf_field()}}
                <div class="config-items">
                    <div class="config-title">
                        <h1><i class="icon-font">&#xe00a;</i>管理员信息设置</h1>
                    </div>
                    <div class="result-content">
                        <table width="100%" class="insert-tab">
                            <tbody>
                            <tr>
                                <th width="15%"><i class="require-red">*</i>管理员名称：</th>
                                <td><input name="username" id="" value="{{$list['username']}}" type="text">
                                    <input name="admin_id" id="" value="{{$list['admin_id']}}" type="hidden"><span id="showBug" style=" margin-left:10px;color:#FF0000"></span></td>
                            </tr>
                            <tr>
                                <th width="15%"><i class="require-red">*</i>管理员密码：</th>
                                <td><input name="pwd_show" id=""  value="{{$list['pwd_show']}}"  type="text" readonly><span id="showBug" style=" margin-left:10px;color:#FF0000"></span></td>
                            </tr>
                            <tr>
                                <th width="15%"><i class="require-red">*</i>管理员密码修改：</th>
                                <td><input name="password" id="" type="password"><span id="showBug" style=" margin-left:10px;color:#FF0000"></span></td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <input type="submit" value="提交" class="btn btn-primary btn6 mr10">
                                    <a href="javascript:history.go(-1)"><input type="button" value="返回"  class="btn btn6"></a>
                                </td>
                            </tr>
                            </tbody></table>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!--/main-->
    </div>
    </body>
    </html>
@endsection