@extends('admin.mainlayout')
@section('content')
    <style>
        .list-page ul{overflow:hidden;display:inline-block;}
        .list-page ul li{float:left;border:none;height:30px;line-height:30px;border: 1px}
        .list-page ul li a{padding:6px 12px;text-decoration:none}
    </style>

    <!--/sidebar-->
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">管理员管理</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
                <form action="{{url('center/manage/index')}}" method="get">
                    {{csrf_field()}}
                    <table class="search-tab">
                        <tr>
                            <th width="70">管理员:</th>
                            <td><input class="common-text" placeholder="管理员" name="username" type="text"></td>
                            <td><input class="btn btn-primary btn2"  value="查询" type="submit"></td>
                        </tr>
                    </table>
                </form>
				
            </div>
        </div>
        <div class="result-wrap">

            <div class="result-title">
                <div class="result-list">
                    <a href="{{url('center/manage/add_admin')}}"><i class="icon-font"></i>新增管理员</a>
                </div>
            </div>
            <div class="result-content">
                <table class="result-tab" width="100%">
                    <tr>
                        <th>ID</th>
                        <th>管理员名称</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    @if(!$data->isEmpty())
                    @foreach($data as $k=>$v)
                        <tr>
                            <td>{{$v['admin_id']}}</td>
                            <td>{{$v['username']}}@if($v['nav'] == '')&nbsp;（此账号尚未分配权限）@endif</td>
                            <td>@if($v['status'] == 0)可用 @else 停用 @endif</td>
                            <td>
                                <a class="link-update" href="{{url('center/manage/edit_admin?admin_id=').$v['admin_id']}}">修改信息</a>
                                <a class="link-update" href="{{url('center/manage/auth_admin').'?admin_id='.$v['admin_id']}}">修改权限</a>
                                @if($v['admin_id'] == 1) @else<a class="link-del" href="javascript:;" onclick="del({{$v['admin_id']}})">删除 </a>@endif
                            </td>
                        </tr>
                    @endforeach
                    @else
                        <tr><td colspan="4" style="text-align: center;font-size: 18px;">暂无数据...</td></tr>
                    @endif
                </table>
                <div class="list-page">{{ $data->appends(['username'=>$name])->links() }}</div>
            </div>

        </div>
    </div>
    <!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
    <script>
        function del(admin_id) {
            layer.confirm('您确定要删除吗？',{btn:['确定','取消']},function () {
                $.post("{{url('center/manage/del_admin')}}?admin_id="+admin_id,{'_token':"{{csrf_token()}}"},function (data) {
                    if (data.state == 200) {
                    layer.msg(data.msg,{icon: 6});
                    location.reload();
                    }else{
                        layer.msg(data.msg,{icon: 6});
                    }
                });
            });

        }
    </script>
<script>
$(".sub-menu").eq(10).show();
$(".sub-menu").eq(10).children("li").eq(0).addClass("on");
</script>
@endsection