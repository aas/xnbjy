@extends('admin.mainlayout')
@section('content')
  <script type="text/javascript">
      var msg = "{{Session::get('message')}}";
      //触发事件
      $(window).ready(function(){
          if(msg){
              layer.msg(msg,{
                  time:2000  //2毫秒
              });
          }
      });
  </script>
<!--/sidebar-->
<div class="main-wrap">
  <div class="crumb-wrap">
    <div class="crumb-list"><i class="icon-font"></i><a href="{:U('Index/index')}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">权限管理</span></div>
  </div>
  <div class="search-wrap">
      <div class="config-title">
          <h1>&nbsp;&nbsp;&nbsp;<i class="icon-font">&#xe00a;</i>管理员信息设置</h1>
      </div>
  </div>
  <div class="result-wrap">
    <form action="{{url('center/manage/auth_admin')}}" method="post">
      {{csrf_field()}}
      <div class="config-items">
        <div class="result-content">
          权限列表：
          <table width="100%" class="result-tab">
            <input type="hidden" value="{{$admin_id}}" name="admin_id" />
            <tbody>
              <tr>
                @foreach($nav as $k=>$v)
                  <td><input name="nav[]" type="checkbox" value="{{$v['nav_id']}}" class="xuanzhong" @if($v['status'] == 1)checked="checked"@endif />{{$v['nav_name']}}
                  </td>
              @if($k%4 == 0)
            	</tr><tr>
           	   @endif
              @endforeach
              </tr>
                <tr><td colspan="4" style="text-align:center;">
                <input type="button" value="全选" onclick="quanxuan()">
                <input type="button" value="全不选" onclick="buxuan()">
                <input type="submit" value="确认"></td></tr>
            </tbody>
          </table>
        </div>
      </div>
    </form>
  </div>
</div>
<script>
	function quanxuan(){
		$(".xuanzhong").prop("checked",true);
	}
	function buxuan(){
		$(".xuanzhong").prop("checked",false);
	}
</script>
<!--/main-->
</div>
</body></html>
  @endsection