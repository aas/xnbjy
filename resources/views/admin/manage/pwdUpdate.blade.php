@extends('admin.mainlayout')
@section('content')
    <script type="text/javascript">
        var msg = "{{Session::get('message')}}";
        //触发事件
        $(window).ready(function(){
            if(msg){
                layer.msg(msg,{
                    time:2000  //2毫秒
                });
            }
        });
    </script>
    <script>
    function CheckForm() {

    if(document.myform.old_pwd.value == ""){
    layer.msg('原始密码不能为空！',{time:2000});
    document.myform.old_pwd.focus();
    return false;
    }

    if(document.myform.password.value == ""){
    layer.msg('新密码不能为空！',{time:2000});
    document.myform.password.focus();
    return false;
    }

        if(document.myform.password.value.length<6){
            layer.msg('密码长度不少于6位！',{time:2000});
            document.myform.password.focus();
            return false;
        }

    var password = document.myform.password.value;
    if ((/[^A-Za-z0-9]/).test(password)) {
        layer.msg('密码含有特殊字符！',{time:2000});
        document.myform.password.focus();
        return false;
    }
    }
    </script>
<!--/sidebar-->
<div class="main-wrap">
    <div class="crumb-wrap">
        <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">管理员管理</span></div>
    </div>
    <div class="result-wrap">
        <form action="{{url('center/manage/edit_pwd')}}" method="post" id="myform" name="myform" enctype="multipart/form-data" onsubmit="{return CheckForm()}">
            {{csrf_field()}}
            <div class="config-items">
                <div class="config-title">
                    <h1><i class="icon-font">&#xe00a;</i>管理员信息设置</h1>
                </div>
                <div class="result-content">
                    <table width="100%" class="insert-tab">
                        <tbody>
                        <tr>
                            <th width="15%"><i class="require-red">*</i>管理员名称：</th>
                            <td>
                                {{$data['username']}}
                            </td>
                        </tr>
                      	<tr>
                            <th width="15%"><i class="require-red">*</i>原始密码：</th>
                            <td><input name="old_pwd" id="" value="" type="password" ><span id="showBug" style=" margin-left:10px;color:#FF0000"></span></td>
                        </tr>
                        <tr>
                            <th width="15%"><i class="require-red">*</i>新密码：</th>
                            <td><input name="password" id="" value="" type="password"><span id="showBug" style=" margin-left:10px;color:#FF0000"></span></td>
                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <input type="submit" value="提交" class="btn btn-primary btn6 mr10">
                                <a href="javascript:history.go(-1)"><input type="button" value="返回"  class="btn btn6"></a>
                            </td>
                        </tr>
                        </tbody></table>
                </div>
            </div>
        </form>
    </div>
</div>

<!--/main-->
</div>
</body>
</html>
    @endsection