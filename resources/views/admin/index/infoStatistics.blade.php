@extends('admin.mainlayout')
@section('content')
<!--/sidebar-->
<div class="main-wrap">
    <div class="crumb-wrap">
        <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">全站统计信息</span></div>
    </div>
    <div class="result-wrap">
        <div class="result-content">
        </div>
    </div>
    <div class="result-wrap">
        <div class="result-title">
            <h1>全站统计信息</h1>
        </div>
        <div class="result-content">
            <table class="result-tab" width="100%">
                <tr>
                    <td>会员总人数</td>
                    <td>{{$count}}人</td>
                    <td>众筹总数量</td>
                    <td>{{$issue_count}}</td>
                </tr>
                <tr>
                    <td>人民币收入</td>
                    <td>￥{{$pay_money_count}}</td>
                    <td>人民币支出</td>
                    <td>￥{{$withdraw_money_count}}</td>
                </tr>
                <tr>
                    <td>充值单数</td>
                    <td>{{$pay_count}}单</td>
                    <td>提现单数</td>
                    <td>{{$withdraw_count}}单</td>
                </tr>
            </table>
            <div class="result-title">
                <h1>全站币种统计</h1>
            </div>
            <table class="result-tab" width="70%" style="text-align: center;margin-left: 130px;">
                <tr>
                    <td>币种名</td>
                    <td>可用</td>
                    <td>冻结</td>
                    <td>总数</td>
                </tr>
                   @foreach($currency_u_info as $k=>$v)
                    <tr>
                        <td>{{$v->currency_name}}</td>
                        <td>{{$v->num}}</td>
                        <td>{{$v->forzen_num}}</td>
                        <td>{{$v->num + $v->forzen_num}}</td>
                    </tr>
                @endforeach

            </table>
        </div>
    </div>
    <script>
        $(function(){
            $('#button').click(function(){
                if(confirm("确认要清除缓存？")){
                    var $type=$('#type').val();
                    $.post("{:U('Index/cache')}",{type:$type},function(data){
                        alert("缓存清理成功");
                    });
                }else{
                    return false;
                }});
        });
    </script>
</div>
<!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(2).show();
$(".sub-menu").eq(2).children("li").eq(2).addClass("on");
</script>
@endsection
