@extends('admin.mainlayout')
@section('content')
<!--/sidebar-->
    <script>
        function CheckForm() {
            if (document.myform.name.value == '') {
                layer.msg('用户名不能为空!', {
                    time: 2000  //2毫秒
                });
                document.myform.name.focus();
                return false;
            }

            if (document.myform.num.value == '') {
                layer.msg('数量不能为空!', {
                    time: 2000  //2毫秒
                });
                document.myform.num.focus();
                return false;
            }

            var num = document.myform.num.value;
            if ((/[^0-9\.]/).test(num)) {
                layer.msg('数量格式错误!', {
                    time: 2000  //2毫秒
                });
                document.myform.num.focus();
                return false;
            }

            if (document.myform.password.value == '') {
                layer.msg('密码不能为空!', {
                    time: 2000  //2毫秒
                });
                document.myform.password.focus();
                return false;
            }
        }

    </script>
    <script>
        var msg = "{{Session::get('message')}}";
        $(window).ready(function() {
            if (msg) {
                layer.msg(msg,{time:2000});
            }
        });
    </script>
<div class="main-wrap">
    <div class="crumb-wrap">
        <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">钱包转账</span></div>
    </div>
    <div class="result-wrap">
        <form action="{{url('center/currency/set_member_qianbao')}}" method="post" id="myform" name="myform" onsubmit="{return CheckForm();}" >
            {{csrf_field()}}
            <div class="config-items">
                <div class="config-title">
                    <h1><i class="icon-font">&#xe00a;</i>{{$data['currency_name']}}</h1>
                </div>
                <div class="result-content">
                    <table width="100%" class="insert-tab">
                        <tbody>
						<input type="hidden" name="cuid" value="{{$data['currency_id']}}">
						  <tr>
                            <th><i class="require-red"></i>钱包余额：</th>
                            <td><span>@if($data['qianbao_blance'] == null) 0 @else {{$data['qianbao_blance']}} @endif</span></td>
                        </tr>
                        <tr>
                            <th><i class="require-red"></i>用户名：</th>
                            <td><input name="name" id="name" type="text" placeholder="用户邮箱" ></td>
                        </tr>
						 <tr>
                            <th><i class="require-red"></i>数量：</th>
                            <td><input name="num" type="text" ></td>
                        </tr>
						
                         	 <tr>
                            <th><i class="require-red"></i>管理员密码：</th>
                            <td><input name="password" type="password" /></td>
                        </tr>
					
                        <tr>
                            <th></th>
                            <td>
                                <input type="submit"  value="提交" class="btn btn-primary btn6 mr10">
                                <a href="javascript:history.go(-1)"><input type="button" value="返回"  class="btn btn6"></a>
                            </td>
                        </tr>
						
                        </tbody></table>
                </div>
            </div>
        </form>
    </div>
</div>


<!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(1).show();
$(".sub-menu").eq(1).children("li").eq(1).addClass("on");
</script>
    @endsection