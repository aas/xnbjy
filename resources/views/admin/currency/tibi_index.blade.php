@extends('admin.mainlayout')
@section('content')
    <style>
        .list-page ul{display:inline-block;}
        .list-page ul li{float:left;border:none;height:30px;line-height:30px;border: 1px}
        .list-page ul li a{padding:6px 12px;text-decoration:none}
    </style>
    <!--/sidebar-->
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">提币记录</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
                <form action="{{url('center/currency/tibi')}}" method="get">
                    {{csrf_field()}}
                    <table class="search-tab">
                        <tr>
                            <th width="70">会员账号:</th>
                            <td><input class="common-text" placeholder="关键字" name="email" type="text"></td>
							     <td><select name="cuid">
								@foreach($curr as $k=>$v)
								 	<option value="{{$v['currency_id']}}">{{$v['currency_name']}}</option>
								@endforeach
								 
								 
								 </select></td>
                            <td><input class="btn btn-primary btn2"  value="查询" type="submit"></td>
                        </tr>
                    </table>
                </form>
				
            </div>
        </div>
        <div class="result-wrap">

            <div class="result-title">
                <div class="result-list">
                </div>
            </div>
            <div class="result-content">
                <table class="result-tab" width="100%">
                    <tr>
                      
                        <th>会员邮箱</th>
						<th>币种名称</th>
						<th>转入钱包地址</th>
                        <th>转出数量</th>
                        <th>实际数量</th>
                        <th>操作时间</th>
                     
                        <th>状态</th>
              
                    </tr>
                    @if(!$result->isEmpty())
                    @foreach($result as $k=>$v)
                        <tr>
                         
                            <td>{{$v->email}}</td>
							<td>{{$v->currency_name}}</td>
							<td>{{$v->url}}</td>
                            <td>{{$v->num}}</td>
                            <td>{{$v->actual}}</td>
							<td>{{date('Y-m-d H:i:s',$v['add_time'])}}</td>
                         
						
                            <td>
                               @if($v->status == 0)
									等待转出...
								@endif
                                @if($v->status == 1)
									已完成
								@endif
								
								</td>
                          
                        </tr>
                    @endforeach
                    @else
                        <tr><td colspan="7" style="text-align: center;font-size: 18px;">暂无数据...</td></tr>
                    @endif
                </table>

                <div class="list-page"> {{ $result->appends(['email'=>$email,'cuid'=>$cuid])->links() }}</div>
            </div>

        </div>
    </div>
    <!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(8).show();
$(".sub-menu").eq(8).children("li").eq(2).addClass("on");
</script>
@endsection