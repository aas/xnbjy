@extends('admin.mainlayout')
@section('content')
    <script>
    function CheckForm() {

        if (document.myform.currency_logo.value == '') {
            layer.msg('上传图片不能为空！', {
                time: 2000
            });
        document.myform.currency_logo.focus();
        return false;
        }

        if (document.myform.currency_name.value == '') {
            layer.msg('货币名称不能为空！', {
                time: 2000
            });
        document.myform.currency_name.focus();
        return false;
        }

        if (document.myform.currency_mark.value == '') {
            layer.msg('英文标识不能为空！', {
                time: 2000
            });
        document.myform.currency_mark.focus();
        return false;
        }

        var currency_mark = document.myform.currency_mark.value;
        if ((/[^A-Za-z_]/).test(currency_mark)) {
            layer.msg('英文标识格式错误！', {
                time: 2000
            });
            document.myform.currency_mark.focus();
            return false;
        }

        if (document.myform.sort.value == '') {
            layer.msg('排序不能为空！', {
                time: 2000
            });
        document.myform.sort.focus();
        return false;
        }

        var sort = document.myform.sort.value;
        if ((/[^0-9]/).test(sort)) {
            layer.msg('排序格式错误！', {
                time: 2000
            });
            document.myform.sort.focus();
            return false;
        }

        if (document.myform.currency_all_tibi.value == '') {
            layer.msg('最大(单次提币额度)不能为空！', {
                time: 2000
            });
            document.myform.currency_all_tibi.focus();
            return false;
        }

        var currency_all_tibi = document.myform.currency_all_tibi.value;
            if ((/[^0-9]/).test(currency_all_tibi)) {
                layer.msg('最大(单次提币额度)格式错误！', {
                    time: 2000
                });
                document.myform.currency_all_tibi.focus();
                return false;
            }

        if (document.myform.currency_all_num.value == '') {
            layer.msg('发行总量不能为空！', {
                time: 2000
            });
            document.myform.currency_all_num.focus();
            return false;
        }

        var currency_all_num = document.myform.currency_all_num.value;
        if(currency_all_num != "") {
            if ((/[^0-9]/).test(currency_all_num)) {
                layer.msg('发行总量格式错误！', {
                    time: 2000
                });
                document.myform.currency_all_num.focus();
                return false;
            }
        }


        if (document.myform.currency_buy_fee.value == '') {
            layer.msg('买入手续费不能为空！', {
                time: 2000
            });
            document.myform.currency_buy_fee.focus();
            return false;
        }

        var currency_buy_fee = document.myform.currency_buy_fee.value;
        if ((/[^0-9]/).test(currency_buy_fee)) {
            layer.msg('买入手续费格式错误！', {
                time: 2000
            });
            document.myform.currency_buy_fee.focus();
            return false;
        }

        if (document.myform.currency_sell_fee.value == '') {
            layer.msg('卖出手续费不能为空！', {
                time: 2000
            });
            document.myform.currency_sell_fee.focus();
            return false;
        }

        var currency_sell_fee = document.myform.currency_sell_fee.value;
        if ((/[^0-9]/).test(currency_sell_fee)) {
            layer.msg('卖出手续费格式错误！', {
                time: 2000
            });
            document.myform.currency_sell_fee.focus();
            return false;
        }

        if (document.myform.currency_digit_num.value == '') {
            layer.msg('限制位数不能为空！', {
                time: 2000
            });
            document.myform.currency_digit_num.focus();
            return false;
        }

        var currency_digit_num = document.myform.currency_digit_num.value;
            if ((/[^0-9]/).test(currency_digit_num)) {
                layer.msg('限制位数格式错误！', {
                    time: 2000
                });
                document.myform.currency_digit_num.focus();
                return false;
            }

        if (document.myform.currency_url.value == '') {
            layer.msg('链接地址不能为空！', {
                time: 2000
            });
            document.myform.currency_url.focus();
            return false;
        }

        var currency_url = document.myform.currency_url.value;
            if (!(/[http|https]:[\/]{2}[a-z]+[.]{1}[a-z\d\-]+[.]{1}[a-z\d]*[\/]*[A-Za-z\d]*[\/]*[A-Za-z\d]*/).test(currency_url)) {
                layer.msg('链接地址格式错误！', {time: 2000});
                document.myform.currency_url.focus();
                return false;
            }


        if (document.myform.rpc_user.value == '') {
            layer.msg('用户名不能为空！', {
                time: 2000
            });
        document.myform.rpc_user.focus();
        return false;
        }

        if (document.myform.rpc_pwd.value == '') {
            layer.msg('密码不能为空！', {
                time: 2000
            });
        document.myform.rpc_pwd.focus();
        return false;
        }

        if (document.myform.rpc_pwd.value.length<= 5) {
            layer.msg('密码长度不少于6位！', {
                time: 2000
            });
        document.myform.rpc_pwd.focus();
        return false;
        }

        var rpc_pwd = document.myform.rpc_pwd.value;
        if ((/[^0-9A-Za-z]/).test(rpc_pwd)) {
            layer.msg('密码含有特殊字符！', {
                time: 2000
            });
            document.myform.rpc_pwd.focus();
            return false;
        }

        if (document.myform.rpc_url.value == '') {
            layer.msg('地址不能为空！', {
                time: 2000
            });
            document.myform.rpc_url.focus();
            return false;
        }

        if (document.myform.port_number.value == '') {
            layer.msg('端口号不能为空！', {
                time: 2000
            });
            document.myform.port_number.focus();
            return false;
        }

        if (document.myform.qianbao_key.value == "") {
            layer.msg('钱包秘钥不能为空！', {
                time: 2000
            });
            document.myform.qianbao_key.focus();
            return false;
        }
        var qianbao_key = document.myform.qianbao_key.value;
        if ((/[^0-9A-Za-z]/).test(qianbao_key)) {
            layer.msg('钱包秘钥含有特殊字符！', {
                time: 2000
            });
            document.myform.qianbao_key.focus();
            return false;
        }

        if (document.myform.is_line.value == '') {
            layer.msg('请选填上线状态！', {
                time: 2000
            });
            return false;
        }

        if (document.myform.is_lock.value == '') {
            layer.msg('请选填交易状态！', {
                time: 2000
            });
            return false;
        }

        if (document.myform.detail_url.value == "") {
            layer.msg('货币详情链接不能为空！', {
                time: 2000
            });
            document.myform.detail_url.focus();
            return false;
        }

        var detail_url = document.myform.detail_url.value;
        if(detail_url != "") {
            if (!(/[http|https]:[\/]{2}[a-z]+[.]{1}[a-z\d\-]+[.]{1}[a-z\d]*[\/]*[A-Za-z\d]*[\/]*[A-Za-z\d]*/).test(detail_url)) {
                layer.msg('货币详情链接格式错误！', {time: 2000});
                document.myform.detail_url.focus();
                return false;
            }
        }

    }
    </script>
    <script>
        var msg = "{{Session::get('message')}}";
        $(window).ready(function () {
           if(msg) {
               layer.msg(msg,{
                   time:2000
               });
           }
        });


    </script>
    <!--/sidebar-->
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">币种设置</span></div>
        </div>
        <div class="result-wrap">
            <form action="{{url('center/currency/addList')}}" method="post" id="myform" name="myform" enctype="multipart/form-data" onsubmit="{return CheckForm()}">
                {{csrf_field()}}
                <div class="config-items">
                    <div class="config-title">
                        <h1><i class="icon-font">&#xe00a;</i>币种详细信息设置</h1>
                    </div>
                    <div class="result-content">
                        <table width="100%" class="insert-tab">
                            <tbody>
                            <tr>
                                <th width="15%"><i class="require-red">*</i>货币LOGO</th>
                                <td><img  id="oldImg" src="" style="max-width:300px; max-height:80px;"></td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>货币LOGO上传</th>
                                <td><input type="file" name="currency_logo" id="currency_logo" class="common-text"></td>
                            </tr>
                                <tr>
                                    <th><i class="require-red">*</i>货币名称：</th>
                                    <td><input type="text" id=""  size="85" name="currency_name" class="common-text"></td>
                                </tr>
                                 <tr>
                                    <th><i class="require-red">*</i>英文标识</th>
                                    <td><input type="text" id=""  size="85" name="currency_mark" class="common-text"></td>
                                </tr>
                                <tr>
                                    <th><i class="require-red">*</i>显示排序</th>
                                    <td><input type="text" id=""  size="85" name="sort" class="common-text"><i class="require-red">（在前端显示的排序，数值越小，排序越靠前）</i></td>
                                </tr>
                               <tr>
                                    <th><i class="require-red">*</i>最大(单次提币额度)</th>
                                    <td><input type="text" id=""  size="85" name="currency_all_tibi" class="common-text"></td>
                                </tr>
								
								<tr>
                                    <th><i class="require-red">*</i>发行总量</th>
                                    <td><input type="text" id=""  size="85" name="currency_all_num" class="common-text"><i class="require-red">（与最新价格乘积为总市值）</i></td>
                                </tr>
                                <tr>
                                    <th><i class="require-red">*</i>买入手续费</th>
                                    <td><input type="text" id=""  size="85" name="currency_buy_fee" class="common-text">%</td>
                                </tr>
                                <tr>
                                    <th><i class="require-red">*</i>卖出手续费</th>
                                    <td><input type="text" id="" size="85" name="currency_sell_fee" class="common-text">%</td>
                                </tr>
						     	 <tr>
                                    <th><i class="require-red">*</i>限制位数</th>
                                    <td><input type="text" id="" size="85" name="currency_digit_num" class="common-text"></td>
                                </tr>
								<tr>
                                    <th><i class="require-red">*</i>链接地址</th>
                                    <td><input type="text" id=""  size="85" name="currency_url" class="common-text"></td>
                                </tr>
								<tr>
                                    <th><i class="require-red">*</i>用户名</th>
                                    <td><input type="text" id=""  size="85" name="rpc_user" class="common-text"></td>
                                </tr>
								<tr>
                                    <th><i class="require-red">*</i>密码</th>
                                    <td><input type="text" id="" size="85" name="rpc_pwd" class="common-text"></td>
                                </tr>
								<tr>
                                    <th><i class="require-red">*</i>地址</th>
                                    <td><input type="text" id="" size="85" name="rpc_url" class="common-text"></td>
                                </tr>
								<tr>
                                    <th><i class="require-red">*</i>端口号</th>
                                    <td><input type="text" id=""  size="85" name="port_number" class="common-text"></td>
                                </tr>

									<tr>
                                    <th><i class="require-red">*</i>钱包密钥</th>
                                    <td><input type="text" id=""  size="85" name="qianbao_key" class="common-text"><i class="require-red">*密钥妥善保存，一旦丢失，后果严重且找不回</i></td>
									
                                </tr>
								

								<tr>
                                    <th><i class="require-red">*</i>上线开关</th>
                                    <td>关闭上线：<input type="radio" name="is_line" value="0" />
										开启上线：<input type="radio" name="is_line" value="1" />
									
									</td>
                                </tr>

                             	<tr>
                                    <th><i class="require-red">*</i>交易开关</th>
                                    <td>开启交易：<input type="radio" name="is_lock" value="0" />
										关闭交易：<input type="radio" name="is_lock" value="1" />
									
									</td>
                                </tr>
									<tr>
                                    <th><i class="require-red">*</i>买入币种</th>
                                    <td><select name="trade_currency_id">
										 <option value="0">人民币</option>
                                             @foreach($data as $k=>$v)
										 	 <option value="{{$v['currency_id']}}">{{$v['currency_name']}}</option>
										 @endforeach
										</select>
									</td>
                                </tr>
                               	<tr>
                                    <th><i class="require-red">*</i>货币详情链接</th>
                                    <td>
                                    	<input type="text" id="" size="85" name="detail_url" class="common-text">
									</td>
                                </tr>
                                <!--  <tr>
                                    <th><i class="require-red">*</i>币种说明</th>
                                    <td>	
                                    <textarea id="content3" value="" name="currency_content" class="common-text" style="width:610px;height:100px;">					{$list.currency_content}
                                    </textarea>							
									</td>
                                </tr>
                                 <tr>
                               		 <th><i class="require-red">*</i>货币详情图片上传</th>
                               		 <td><input type="file" name="pic" class="common-text"></td>
                           		 </tr>
                                 <volist name='pic' id='vo'>
                                 <tr>
                               		 <th><i class="require-red">*</i>货币详情图片展示<br>(<a href="{:U('Currency/delCurrencyPic',array('id'=>$vo['id']))}">点此处删除此详情图片</a>)</th>
                               		 <td><img src='{$vo.pic}' style="height:200px;"></td>
                           		 </tr>
                                 </volist>-->
                                <tr>
                                    <th></th>
                                    <td>
                                        <input type="submit" value="提交" class="btn btn-primary btn6 mr10">
                                        <a href="{:U('Currency/index')}"><input type="button" value="返回"  class="btn btn6"></a>
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
    <!--/main-->
</div>
</body>
</html>
    @endsection
@section('footer')
<script>
$(".sub-menu").eq(2).show();
$(".sub-menu").eq(2).children("li").eq(1).addClass("on");
</script>
<script>									
	KindEditor.ready(function(K) {
		window.editor1 = K.create('#content3');	
	});
</script>
<script>
    window.onload = function () {

        var oFile = document.getElementById('currency_logo');
        var oldImg = document.getElementById('oldImg');


        oFile.onchange = function(){

            var file = this.files[0];

            var reader = new FileReader();

            reader.readAsDataURL(file);   //将文件读取为DataUrl

            //读完了之后就能调取数据了
            reader.onload = function(){
                //console.log(reader.result);
                //在div中添加dom
                //在隐藏标签中添加value
                oldImg.src = reader.result;

            }

        }

    }

</script>
    @endsection