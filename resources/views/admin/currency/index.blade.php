@extends('admin.mainlayout')
@section('content')
    <style>
        .list-page ul{overflow:hidden;display:inline-block;}
        .list-page ul li{float:left;border:none;height:30px;line-height:30px;border: 1px}
        .list-page ul li a{padding:6px 12px;text-decoration:none}
    </style>
    <!--/sidebar-->
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">币种管理</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
            </div>
        </div>
        <div class="result-wrap">
          
                <div class="result-title">
                    <div class="result-list">
                        <a href="{{url('center/currency/add')}}"><i class="icon-font"></i>新增币种</a>
                    </div>
                </div>
                <div class="result-content">
                    <table class="result-tab" width="100%">
                        <tr>
                        	<th>排序</th>
                            <th>货币LOGO</th>
                            <th>上线状态</th>
                            <th>交易状态</th>
                            <th>货币名称</th>
                            <th>英文标识</th>
							<th>钱包余额</th>
                            <th>买入手续费</th>
                            <th>卖出手续费</th>
                            <th>最新价</th>
                            <th>涨停价格</th>
                            <th>跌停价格</th>
                            <th>修改时间</th>
                            <th>操作</th>
                        </tr>
                        @foreach ($result as $k=>$v)
                        <tr> 
                        	<td>{{$v['sort']}}</td>
                            <td> @if(!empty($v['currency_logo'])) <img  style="height:100px;"src='{{$v['currency_logo']}}' /> @else 暂无图片数据 @endif</td>
                            <td>@if($v['is_line'] == 0 )未上线 @else 已上线 @endif </td>
                            <td>@if($v['is_lock'] == 0 )已开启 @else 未开启 @endif </td>
                            <td>{{$v['currency_name']}}</td>
                            <td>{{$v['currency_mark']}}</td>
							<td>{{$v['qianbao_balance']}}</td>
                            <td>{{$v['currency_buy_fee']}}%</td>
                            <td>{{$v['currency_sell_fee']}}%</td>
                            <td>{{$v['new_price']}}</td>
                            <td>{{$v['price_up']}}</td>
                            <td>{{$v['price_down']}}</td>
                            <td>{{date('Y-m-d',$v['add_time'])}}</td>
                            <td>
                                <a class="link-update" href="{{url('center/trade/trade?currency_id=').$v['currency_id']}}">查看交易记录</a></br> <a class="link-limit" href="javascript:void(0);" onclick="limit({{$v['currency_id']}},{{$v['price_up']}},{{$v['price_down']}})">设置交易限额</a><br>
                                <a class="link-update" href="{{url('center/trade/order?currency_id=').$v['currency_id']}}">查看委托记录</a><br>
                                <a class="link-del" href="{{url('center/currency/set_member?cuid=').$v['currency_id']}}">向会员钱包转账</a><br>
                                <a class="link-update" href="{{url('center/currency/edit?currency_id=').$v['currency_id']}}">修改</a><br>
                                <a class="link-del" href="javascript:;" onclick="del({{$v['currency_id']}})">删除</a><br>
								 
                               
                            </td>
                        </tr>
                            @endforeach
                    </table>
                    <div class="list-page">{!! $result->links() !!}</div>

                </div>
         
        </div>
    </div>
    <!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(8).show();
$(".sub-menu").eq(8).children("li").eq(0).addClass("on");

function limit(id,price_up,price_down){
	var lid = id;
	var price_up = price_up;
	var price_down = price_down;
	layer.open({
	  title: '设置交易限额',
	  type: 1,
	  skin: 'layui-layer-demo', //样式类名
	  closeBtn: 1, //不显示关闭按钮
	  shift: 2,
	  area: ['300px', '170px'], 
	  shadeClose: true, //开启遮罩关闭
	  content: '<div style="margin-top:15px;text-align:center;"><span>设置交易上限</span><input type="text" name="price_up" value="'+price_up+'" id="price_up"/></div><div style="margin-top:15px;text-align:center;"><span>设置交易下限</span><input type="text" value='+price_down+' name="price_down" id="price_down"/></div><div style="text-align:center;margin-top:15px;"><input type="button" value="确定" onclick="setlimit('+lid+');" style="padding:3px 10px;" /></div>'
	});

};
function setlimit(id){
	$.post("{{url('center/currency/savePrice')}}",{id:id,price_up:$("#price_up").val(),price_down:$("#price_down").val(),'_token':"{{csrf_token()}}"},function(data){
		 if(data<0){
			layer.msg(data.msg);
		 }else{
			layer.msg(data.msg);
			setTimeout("location.reload()",2000);
		 }
	});
}
</script>
<script>
    function del(currency_id){
        layer.confirm('您确定要删除吗？', {
                btn: ['确定','取消'] //按钮
            }, function(){
                //异步处理
                $.get("{{url('center/currency/del')}}?currency_id="+currency_id,function(data){

                    if(data.state==200){
                        location.reload();
                        layer.msg(data.msg, {icon: 6});
                    }else{
                        layer.msg(data.msg, {icon: 5});
                    }
                });
            }
        )}

</script>
@endsection