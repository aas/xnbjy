@extends('admin.mainlayout')
@section('js')

@endsection
@section('content')
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="#">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">银行卡管理</span></div>
        </div>
        <div class="search-wrap">
        </div>
        <div class="result-wrap">

            <div class="result-title">
                <div class="result-list">
                    <a href="{{urlAdmin('Websitebank','getAdd')}}"><i class="icon-font"></i>新增银行卡</a>
                </div>
            </div>
            <div class="result-content">
                <table class="result-tab" width="100%">
                    <tr>
                        <th>收款人</th>
                        <th>收款人账号</th>
                        <th>收款人开户行</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    @if(!empty($data['data']))
                    @foreach($data['data'] as $v)
                        <tr>
                            <td>{{$v['bank_name']}}</td>
                            <td>{{$v['bank_no']}}</td>
                            <td>{{$v['bank_adddress']}}</td>
                            <td>
                                @if(isset($v['status'])&&$v['status']==1)
                                    开通
                                @else
                                    关闭
                                @endif
                            </td>
                            <td>
                                <a class="link-update" href="{{urlAdmin('Websitebank','getEdit',array('bank_id'=>$v['bank_id']))}}">修改|</a>
                                <a href="javascript:void(0)" class="link-del" onclick="del({{$v['bank_id']}},this)">删除</a>
                            </td>
                        </tr>
                    @endforeach
                    @else
                        <tr>
                            <td colspan="5" align="center">暂无数据...</td>
                        </tr>
                    @endif


                </table>
                <div class="list-page">
                    <ul>
                    <div>
                        @if($data['last_page']>1)
                        <a class="prev" href="{{$data['prev_page_url']}}"><<</a>
                        @foreach($data['pageNoList'] as $v)
                        <a @if($v == $data['current_page'])
                           class="current"
                           @else
                           class="num"
                           @endif
                           href="{{urlAdmin('Websitebank','getIndex')}}?page={{$v}}">{{$v}}</a>
                        @endforeach
                        <a class="next" href="{{$data['next_page_url']}}">>></a>
                        @endif
                    </div>
                    </ul>
                </div>

            </div>

        </div>
    </div>
    <!--/main-->
@endsection

@section('footer')
    <script>
        {{--function cexiao(_this){--}}
            {{--layer.confirm('确定删除银行卡？', {--}}
                {{--btn: ['确定','取消'], //按钮--}}
                {{--title: '撤销删除银行卡'--}}
            {{--}, function(){--}}
                {{--$.post("{{urlAdmin('Websitebank','postDel')}}",{bank_id:_this},function(data){--}}
                    {{--if(data['status'] == 1){--}}
                        {{--layer.msg(data['info']);--}}                        {{--setTimeout(function(){location.reload();},1000);--}}
                    {{--}else{--}}
                        {{--layer.msg(data['info']);--}}
                    {{--}--}}
                {{--})--}}
            {{--}, function(){--}}
                {{--layer.msg('已取消');--}}
            {{--});--}}

        {{--}--}}
//function ab(a){
//    alert($(a).index())
//}

        /********所有ajax加载效果**********/
        $(document).ajaxStart(function(){

            //加载效果
            layer.load(2, {shade: false});

        }).ajaxStop(function(){

            //关闭加载效果
            layer.closeAll("loading");
        });
        /********所有ajax加载效果**********/
        //单个删除
        function del(id,a){


            layer.confirm('确定要删除该项吗?',{btn:['确定','取消']},function(){

                layer.closeAll();
                $.ajax({
                    type:'post',
                    url:"{{urlAdmin('Websitebank','postDel')}}",
                    data:{'id':id,'_token':'{{csrf_token()}}'},
                    dataType:'json',
                    success: function (res) {

                        // alert(res);
                        if(res['status']==1){

                            layer.msg(res.msg,{icon:6});
                            //setTimeout("location.reload()",1500);
                            var pindex=$(a).parents('tr').index();
                            setTimeout(function (){$('table tr').eq(pindex).remove();},1000);

                        }else{

                            layer.msg(res.msg,{icon:5});

                        }

                    }

                });

            },function(){

                layer.msg('已取消!');

            });
        }
    </script>
    <script>
        $(".sub-menu").eq(1).show();
        $(".sub-menu").eq(1).children("li").eq(0).addClass("on");
    </script>
@endsection