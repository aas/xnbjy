@extends('admin.mainlayout')
@section('js')
    <script>
        window.onload = function(){

            /********快闪重定向*********/
            var message = "{{Session::get('message')}}";
            $(window).ready(function () {
                if (message) {
                    layer.msg(message, {
                        time: 3000,

                    });
                }
            });
            /********快闪重定向*********/



        }
    </script>
@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{:U('Index/index')}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">银行管理</span></div>
        </div>
        <div class="result-wrap">

            <form action="{{urlAdmin('Websitebank','postEdit')}}" method="post"  enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="config-items">
                    <div class="result-content">
                        <table width="100%" class="insert-tab">
                            <input type="hidden" value="{$data['member_id']}" name="member_id" />
                            <tbody>
                            <tr>
                                <th>收款人：</th>
                                <td><input name="bank_name" value="{{$data['bank_name']}}" type="text"></td>
                            </tr>

                                @if($data['status']!=0)
                                <tr>
                                    <th>账号：</th>
                                    <td><input name="bank_no" id="nick" value="{{$data['bank_no']}}" type="text"></td>
                                </tr>
                                <input name="bank_id"  value="{{$data['bank_id']}}" type="hidden">
                                <tr>
                                    <th>开户行：</th>
                                    <td><input name="bank_adddress" id="idcard" value="{{$data['bank_adddress']}}" type="text"></td>
                                </tr>
                                @endif

                            <tr>
                            <tr>
                                <th>状态</th>
                                <td>
                                    <input type="radio" value="1"
                                    @if($data['status']==1) checked @endif
                                    name="status" />开通&nbsp;
                                    <input type="radio" value="2"
                                    @if($data['status']==2) checked @endif
                                    name="status" />关闭</td>
                            </tr>
                            <th></th>
                            <td>
                                <input type="submit" value="提交" class="btn btn-primary btn6 mr10">
                                <input type="button" value="返回" onclick="history.go(-1)" class="btn btn6">
                            </td>
                            </tr>
                            </tbody></table>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('footer')
    <script>
        $(".sub-menu").eq(1).show();
        $(".sub-menu").eq(1).children("li").eq(0).addClass("on");
    </script>
@endsection