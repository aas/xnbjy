@extends('admin.mainlayout')
@section('content')
    <script>
        function CheckForm() {
            if(document.myform.jiaoyi_start_hour.value >24 || document.myform.jiaoyi_start_minute.value >60 ){
                layer.msg('开始时间请规范填写!',{
                    time:2000  //2毫秒
                });
                return false;
            }
            if(document.myform.jiaoyi_over_hour.value >24 || document.myform.jiaoyi_over_minute.value >60 ){
                layer.msg('结束时间请规范填写!',{
                    time:2000  //2毫秒
                });
                return false;
            }
            var pay_min_money = document.myform.pay_min_money.value;
            if(pay_min_money != "") {
                if(!(/[0-9+]/).test(pay_min_money)){
                    layer.msg('最低限额必须为数值!',{
                        time:2000  //2毫秒
                    });
                    document.myform.pay_min_money.focus();
                    return false;
                }
            }

            var pay_max_money = document.myform.pay_max_money.value;
            if(pay_max_money != "") {
                if(!(/[0-9+]/).test(pay_max_money)){
                    layer.msg('最高限额必须为数值!',{
                        time:2000  //2毫秒
                    });
                    document.myform.pay_max_money.focus();
                    return false;
                }
            }

            var time_limit = document.myform.time_limit.value;
            if(time_limit != "") {
                if(!(/[0-9+]/).test(time_limit)){
                    layer.msg('登录超时退出时间必须为数值!',{
                        time:2000  //2毫秒
                    });
                    document.myform.time_limit.focus();
                    return false;
                }
            }

            var pay_fee = document.myform.pay_fee.value;
            if(pay_fee != "") {
                if(!(/[0-9+]/).test(pay_fee)){
                    layer.msg('人工充值手续费必须为数值!',{
                        time:2000  //2毫秒
                    });
                    document.myform.pay_fee.focus();
                    return false;
                }
            }

            var fee = document.myform.fee.value;
            if(fee != "") {
                if(!(/[0-9+]/).test(fee)){
                    layer.msg('人工体现手续费必须为数值!',{
                        time:2000  //2毫秒
                    });
                    document.myform.fee.focus();
                    return false;
                }
            }

            var tcoin_fee = document.myform.tcoin_fee.value;
            if(tcoin_fee != "") {
                if(!(/[0-9+]/).test(tcoin_fee)){
                    layer.msg('币种手续费必须为数值!',{
                        time:2000  //2毫秒
                    });
                    document.myform.tcoin_fee.focus();
                    return false;
                }
            }

            var transaction_false = document.myform.transaction_false.value;
            if(transaction_false != "") {
                if(!(/[0-9+]/).test(transaction_false)){
                    layer.msg('网站总交易额必须为数值!',{
                        time:2000  //2毫秒
                    });
                    document.myform.transaction_false.focus();
                    return false;
                }
            }

            var jiedong_bili = document.myform.jiedong_bili.value;
            if(jiedong_bili != "") {
                if(!(/[0-9+]/).test(jiedong_bili)){
                    layer.msg('解冻比例必须为数值!',{
                        time:2000  //2毫秒
                    });
                    document.myform.jiedong_bili.focus();
                    return false;
                }
            }
        }
    </script>
    <script>
        var msg = "{{Session::get('message')}}";
        $(window).ready(function () {
           if(msg) {
               layer.msg(msg,{time:2000});
           }
        })
    </script>
    <!--/sidebar-->
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">系统设置</span></div>
        </div>
        <div class="result-wrap">
            <form action="{{url('center/config/finance_edit')}}" method="post" id="myform" name="myform" enctype="multipart/form-data" onsubmit="{return CheckForm()}">
                {{csrf_field()}}
                <div class="config-items">
                    <div class="config-title">
                        <h1><i class="icon-font">&#xe00a;</i>网站财务设置</h1>
                    </div>
                    <div class="result-content">
                        <table width="100%" class="insert-tab">
                            <tbody>
                              <tr>
                                    <th><i class="require-red">*</i>交易时间段：开始时间</th>
                                    <td>时：<input type="text" id="" value="{{$data['jiaoyi_start_hour']}}" size="30" name="jiaoyi_start_hour" class="common-text">&nbsp;&nbsp;&nbsp;分：<input type="text" id="" value="{{$data['jiaoyi_start_minute']}}" size="30" name="jiaoyi_start_minute" class="common-text"></td>
                                </tr>
                                <tr>
                                    <th><i class="require-red">*</i>交易时间段：结束时间</th>
                                    <td>时：<input type="text" id="" value="{{$data['jiaoyi_over_hour']}}" size="30" name="jiaoyi_over_hour" class="common-text">&nbsp;&nbsp;&nbsp;分：<input type="text" id="" value="{{$data['jiaoyi_over_minute']}}" size="30" name="jiaoyi_over_minute" class="common-text"></td>
                                </tr>
                                <tr>
                                    <th><i class="require-red">*</i>人工充值最低限额：</th>
                                    <td><input type="text" id="" value="{{$data['pay_min_money']}}" size="85" name="pay_min_money" class="common-text"></td>
                                </tr>
                                <tr>
                                    <th><i class="require-red">*</i>人工充值最高限额：</th>
                                    <td><input type="text" id="" value="{{$data['pay_max_money']}}" size="85" name="pay_max_money" class="common-text"></td>
                                </tr>
                                <tr>
                                    <th><i class="require-red">*</i>登录超时退出（分钟）：</th>
                                    <td><input type="num" id="" value="{{$data['time_limit']}}" size="85" name="time_limit" class="common-text">为0则此项无配置</td>
                                </tr>
                                <tr>
                                    <th><i class="require-red">*</i>人工充值手续费：</th>
                                    <td><input type="text" id="" value="{{$data['pay_fee']}}" size="85" name="pay_fee" class="common-text">元&nbsp;&nbsp;&nbsp;固定费用，无请填0</td>
                                </tr>
                                <tr>
                                    <th><i class="require-red">*</i>人工提现手续费：</th>
                                    <td><input type="text" id="" value="{{$data['fee']}}" size="85" name="fee" class="common-text">%&nbsp;&nbsp;&nbsp;比例费用，无请填0</td>
                                </tr>
                                 <tr>
                                    <th><i class="require-red">*</i>提币手续费：</th>
                                    <td><input type="text" id="" value="{{$data['tcoin_fee']}}" size="85" name="tcoin_fee" class="common-text">%&nbsp;&nbsp;&nbsp;比例费用，无请填0</td>
                                </tr>
                                 <tr>
                                    <th><i class="require-red">*</i>网站总交易额增加：</th>
                                    <td><input type="text" id="" value="{{$data['transaction_false']}}" size="85" name="transaction_false" class="common-text">用于首页显示</td>
                                </tr>
                                 <tr>
                                    <th><i class="require-red">*</i>解冻比例：</th>
                                    <td><input type="text" id="" value="{{$data['jiedong_bili']}}" size="85" name="jiedong_bili" class="common-text">%&nbsp;&nbsp;&nbsp;每日按照设置比例解冻</td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>
                                        <input type="submit" value="提交" class="btn btn-primary btn6 mr10">
                                        <input type="button" value="返回" onclick="history.go(-1)" class="btn btn6">
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(0).show();
$(".sub-menu").eq(0).children("li").eq(2).addClass("on");
</script>
@endsection