@extends('admin.mainlayout')
@section('content')
    <script>
        var msg = "{{Session::get('message')}}"
        $(window).ready(function () {
            if(msg) {
                layer.msg(msg,{
                    time:2000
                });
            }
        })
    </script>
<!--/sidebar-->
<script type="text/javascript" charset="utf-8"
	src="/admin/kindeditor/kindeditor.js"></script>
<script type="text/javascript" charset="utf-8"
	src="/admin/kindeditor/lang/zh_CN.js"></script>
<script type="text/javascript" charset="utf-8"
	src="/admin/kindeditor/plugins/prettify.js"></script>

<div class="main-wrap">
  <div class="crumb-wrap">
    <div class="crumb-list"> <i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span
				class="crumb-step">&gt;</span><span class="crumb-name">系统设置</span> </div>
  </div>
  <script>
			function checkForm(){
				editor.sync();
				editor1.sync();
				editor2.sync();
				editor3.sync();
				editor4.sync();
				editor7.sync();
				editor8.sync();
				editor9.sync();
				editor10.sync();
				document.getElementById('myform').submit();
		}
</script>
  <style>
.insert-tab td {
	padding: 10px;
}
</style>
  <div class="result-wrap">
    <form action="{{url('center/config/information_add')}}" method="post" id="myform"
			name="myform" enctype="multipart/form-data">
        {{csrf_field()}}
      <div class="config-items">
        <div class="config-title">
          <h1> <i class="icon-font">&#xe00a;</i>网站信息设置 </h1>
        </div>
        <div class="result-content">
          <table width="100%" class="insert-tab">
              <tbody>
              <tr>
                  <th><i class="require-red"></i>友情提示：</th>
                  <td><textarea id="content" value="" name="friendship_tips"
                                class="common-text" style="width: 610px; height: 100px;">{{$data['friendship_tips']}}</textarea></td>
              </tr>
              <script>
                  KindEditor.ready(function(K) {
                      window.editor  = K.create('#content');

                  });
              </script>
              <tr>
                  <th><i class="require-red"></i>风险提示：</th>
                  <td><textarea id="content1" value="" name="risk_warning"
                                class="common-text" style="width: 610px; height: 100px;">{{$data['risk_warning']}}</textarea></td>
              </tr>
              <script>
                  KindEditor.ready(function(K) {
                      window.editor1 = K.create('#content1');
                  });
              </script>
              <tr>
                  <th><i class="require-red"></i>提现提示：</th>
                  <td><textarea id="content2" value=""
                                name="withdraw_warning" class="common-text"
                                style="width: 610px; height: 100px;">{{$data['withdraw_warning']}}</textarea></td>
              </tr>
              <script>
                  KindEditor.ready(function(K) {
                      window.editor2 = K.create('#content2');
                  });
              </script>
              <tr>
                  <th><i class="require-red"></i>邀请规则：</th>
                  <td><textarea id="content3" value="" name="invite_rule"
                                class="common-text" style="width: 610px; height: 100px;">{{$data['invite_rule']}}</textarea></td>
              </tr>
              <script>
                  KindEditor.ready(function(K) {
                      window.editor3 = K.create('#content3');
                  });
              </script>
              <tr>
                  <th><i class="require-red"></i>交易规则：</th>
                  <td><textarea id="content4" value="" name="VAP_rule"
                                class="common-text" style="width: 610px; height: 100px;">{{$data['VAP_rule']}}</textarea></td>
              </tr>
              <script>
                  KindEditor.ready(function(K) {
                      window.editor4 = K.create('#content4');
                  });
              </script>
              <tr>
                  <th><i class="require-red"></i>免责声明：</th>
                  <td><textarea id="content7" value="" name="disclaimer"
                                class="common-text" style="width: 610px; height: 100px;">{{$data['disclaimer']}}</textarea></td>
              </tr>
              <script>
                  KindEditor.ready(function(K) {
                      window.editor7 = K.create('#content7');
                  });
              </script>
              <tr>
                  <th><i class="require-red"></i>服务条款：</th>
                  <td ><textarea id="content8" value="" name="FWTK" class="common-text" style="width:610px;height:100px;">{{$data['FWTK']}}</textarea></td>
              </tr>
              <script>
                  KindEditor.ready(function(K) {
                      window.editor8 = K.create('#content8');
                  });
              </script>
              <tr>
                  <th><i class="require-red"></i>新币上线申请：</th>
                  <td ><textarea id="content9" value="" name="new_coin_up" class="common-text" style="width:610px;height:100px;">{{$data['new_coin_up']}}</textarea></td>
              </tr>
              <script>
                  KindEditor.ready(function(K) {
                      window.editor9 = K.create('#content9');
                  });
              </script>
              <tr>
                  <th><i class="require-red"></i>注册风险警示：</th>
                  <td ><textarea id="content10" value="" name="reg_risk_warning" class="common-text" style="width:610px;height:100px;">{{$data['reg_risk_warning']}}</textarea></td>
              </tr>
              <script>
                  KindEditor.ready(function(K) {
                      window.editor10 = K.create('#content10');
                  });
              </script>
              <th></th>
              <td><input type="button" value="提交" onclick="checkForm()"
                         class="btn btn-primary btn6 mr10">
                  <input type="button"
                         value="返回" onclick="history.go(-1)" class="btn btn6"></td>
              </tr>
              </tbody>
            
          </table>
        </div>
      </div>
    </form>
  </div>
</div>
<!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(0).show();
$(".sub-menu").eq(0).children("li").eq(1).addClass("on");
</script>
@endsection