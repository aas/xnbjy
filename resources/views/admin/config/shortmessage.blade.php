
@extends('admin.mainlayout')
@section('content')
	<script>
		function CheForm() {
          var CODE_NAME = document.myform.CODE_NAME.value;
          if(CODE_NAME != ""){
              if(!(/[\u4e00-\u9fa5]/).test(CODE_NAME)) {
                  layer.msg('发送标题格式错误！',{time:2000});
                  document.myform.CODE_NAME.focus();
                  return false;
			  }
		  }

            var EMAIL_HOST = document.myform.EMAIL_HOST.value;
            if(EMAIL_HOST != "") {
                if (!(/^[A-Za-z\d]+([-_.][A-Za-z\d]+)*([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/).test(EMAIL_HOST)) {
                    layer.msg('请输入正确的企业邮局域名！', {time: 2000});
                    document.myform.EMAIL_HOST.focus();
                    return false;
                }
            }

            var EMAIL_USERNAME = document.myform.EMAIL_USERNAME.value;
            if(EMAIL_USERNAME != "") {
                if (!(/^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/).test(EMAIL_USERNAME)) {
                    layer.msg('请输入正确的邮局用户名！', {time: 2000});
                    document.myform.EMAIL_USERNAME.focus();
                    return false;
                }
            }

            var EMAIL_PASSWORD = document.myform.EMAIL_PASSWORD.value;
            if(EMAIL_PASSWORD != "") {
                if ((/[^0-9A-Za-z]/).test(EMAIL_PASSWORD)) {
                    layer.msg('密码中含有特殊字符！', {time: 2000});
                    document.myform.EMAIL_PASSWORD.focus();
                    return false;
                }
            }

        }
	</script>
	<script>
		var msg = "{{Session::get('message')}}";
		$(window).ready(function () {
			if(msg){
              layer.msg(msg,{time:2000});
			}
        })
	</script>
<!--/sidebar-->
<div class="main-wrap">
	<div class="crumb-wrap">
		<div class="crumb-list">
			<i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span
				class="crumb-step">&gt;</span><span class="crumb-name">系统设置</span>
		</div>
	</div>
	<div class="result-wrap">
		<form action="{{url('center/config/short_edit')}}" method="post" id="myform"
			name="myform" enctype="multipart/form-data" onsubmit="{return CheForm()}">
			{{csrf_field()}}
			<div class="config-items">
				<div class="config-title">
					<h1>
						<i class="icon-font">&#xe00a;</i>网站短信设置
					</h1>
				</div>
				<div class="result-content">
					<table width="100%" class="insert-tab">
						<tbody>
							<tr>
								<th><i class="require-red">*</i>短信接口用户名：</th>
								<td><input type="text" id=""
									value="{{$data['CODE_USER_NAME']}}" size="85"
									name="CODE_USER_NAME" class="common-text"></td>
							</tr>
							{{--<tr>--}}
								{{--<th><i class="require-red">*</i>短信接口密码：</th>--}}
								{{--<td><input type="text" id=""--}}
									{{--value="{{$data['CODE_USER_PASS']}}" size="85"--}}
									{{--name="CODE_USER_PASS" class="common-text"></td>--}}
							{{--</tr>--}}
							<tr>
								<th><i class="require-red">*</i>短信接口发送标题：</th>
								<td><input type="text" id="" value="{{$data['CODE_NAME']}}"
									size="85" name="CODE_NAME" class="common-text"></td>
							</tr>
                              <tr>
								<th><i class="require-red">*</i>购买短信地址：</th>
								<td><a href="http://www.smsbao.com/reg?r=10217" style="font-size:24px">点击进入</a></td>
							</tr>
							<tr>
								<td colspan="2"><div class="config-title">
										<h1>
											<i class="icon-font">&#xe00a;</i>网站邮箱设置
										</h1>
									</div></td>
							</tr>

							<tr>
								<th><i class="require-red">*</i>您的企业邮局域名：</th>
								<td><input type="text" id="" value="{{$data['EMAIL_HOST']}}"
									size="85" name="EMAIL_HOST" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>邮局用户名(请填写完整的email地址)：</th>
								<td><input type="text" id=""
									value="{{$data['EMAIL_USERNAME']}}" size="85"
									name="EMAIL_USERNAME" class="common-text"></td>
							</tr>

							<tr>
								<th><i class="require-red">*</i>邮局密码：</th>
								<td><input type="text" id=""
									value="{{$data['EMAIL_PASSWORD']}}" size="85"
									name="EMAIL_PASSWORD" class="common-text"></td>
							</tr>
                          
							<tr>
								<th></th>
								<td><input type="submit" value="提交"
									class="btn btn-primary btn6 mr10"> <input type="button"
									value="返回" onclick="history.go(-1)" class="btn btn6"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</form>
	</div>
</div>
<!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
	$(".sub-menu").eq(0).show();
	$(".sub-menu").eq(0).children("li").eq(4).addClass("on");
</script>
@endsection