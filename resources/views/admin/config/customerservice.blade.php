@extends('admin.mainlayout')
@section('content')
    <script>
        function CheckForm() {
         var tel = document.myform.tel.value;
         if(tel != ""){
             if(!(/^1[3|4|5|8][0-9]{9}$/).test(tel)){
                 layer.msg('请输入正确的手机号码！',{time:2000});
                 document.myform.tel.focus();
                 return false;

             }
         }
            var weibo = document.myform.weibo.value;
            if(weibo != ""){
                if(!((/^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/).test(weibo) || (/^1[3|4|5|8][0-9]{9}$/).test(weibo))){
                    layer.msg('请输入正确的微博账号！',{time:2000});
                    document.myform.weibo.focus();
                    return false;

                }
            }
            var qq1 = document.myform.qq1.value;
            if(qq1 != "") {
                if (!(/^[1-9][0-9]{4,9}$/).test(qq1)) {
                    layer.msg('请输入正确的QQ账号！', {time: 2000});
                    document.myform.qq1.focus();
                    return false;
                }
            }

            var qq2 = document.myform.qq2.value;
            if(qq2 != "") {
                if (!(/^[1-9][0-9]{4,9}$/).test(qq2)) {
                    layer.msg('请输入正确的QQ账号！', {time: 2000});
                    document.myform.qq2.focus();
                    return false;
                }
            }

            var qq3 = document.myform.qq3.value;
            if(qq3 != "") {
                if (!(/^[1-9][0-9]{4,9}$/).test(qq3)) {
                    layer.msg('请输入正确的QQ账号！', {time: 2000});
                    document.myform.qq3.focus();
                    return false;
                }
            }

            var qqqun1 = document.myform.qqqun1.value;
            if(qqqun1 != "") {
                if (!(/^[1-9][0-9]{4,9}$/).test(qqqun1)) {
                    layer.msg('请输入正确的QQ账号！', {time: 2000});
                    document.myform.qqqun1.focus();
                    return false;
                }
            }

            var qqqun2 = document.myform.qqqun2.value;
            if(qqqun2 != "") {
                if (!(/^[1-9][0-9]{4,9}$/).test(qqqun2)) {
                    layer.msg('请输入正确的QQ账号！', {time: 2000});
                    document.myform.qqqun2.focus();
                    return false;
                }
            }

            var qqqun3 = document.myform.qqqun3.value;
            if(qqqun3 != "") {
                if (!(/^[1-9][0-9]{4,9}$/).test(qqqun3)) {
                    layer.msg('请输入正确的QQ账号！', {time: 2000});
                    document.myform.qqqun3.focus();
                    return false;
                }
            }

            var email = document.myform.email.value;
            if(email != "") {
                if (!(/^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/).test(email)) {
                    layer.msg('请输入正确的邮箱账号！', {time: 2000});
                    document.myform.email.focus();
                    return false;
                }
            }
            var business_email = document.myform.business_email.value;
            if(business_email != "") {
                if (!(/^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,4}$/).test(business_email)) {
                    layer.msg('请输入正确的邮箱账号！', {time: 2000});
                    document.myform.business_email.focus();
                    return false;
                }
            }

        }
    </script>
    <script>
        var msg = "{{Session::get('message')}}";
        $(window).ready(function () {
            if(msg) {
                layer.msg(msg,{time:2000});
            }
        })
    </script>
    <!--/sidebar-->
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">系统设置</span></div>
        </div>
        <div class="result-wrap">
            <form action="{{url('center/config/customer_edit')}}" method="post" id="myform" name="myform" enctype="multipart/form-data" onsubmit="{return CheckForm()}">
                {{csrf_field()}}
                <div class="config-items">
                    <div class="config-title">
                        <h1><i class="icon-font">&#xe00a;</i>网站客服设置</h1>
                    </div>
                    <div class="result-content">
                        <table width="100%" class="insert-tab">
                            <tbody>
                                 <tr>
                                    <th><i class="require-red">*</i>客服电话：</th>
                                    <td><input type="text" id="" value="{{$data['tel']}}" size="85" name="tel" class="common-text"></td>
                                </tr>
                                <tr>
                                    <th><i class="require-red">*</i>官方微博：</th>
                                    <td><input type="text" id="" value="{{$data['weibo']}}" size="85" name="weibo" class="common-text"></td>
                                </tr>
                                <tr>
                                    <th><i class="require-red">*</i>微信二维码图片上传：</th>
                                    <td><input type="file" id="" value="{{$data['weixin']}}" size="85" name="weixin" class="common-text"><input type="hidden" id="" value="{{$data['weixin']}}" size="85" name="weixin" class="common-text"></td>
                                </tr>
                                <tr>
                                    <th><i class="require-red">*</i>客服QQ1：</th>
                                    <td><input type="text" id="" value="{{$data['qq1']}}" size="85" name="qq1" class="common-text"></td>
                                </tr>
                                <tr>
                                    <th><i class="require-red">*</i>客服QQ2：</th>
                                    <td><input type="text" id="" value="{{$data['qq2']}}" size="85" name="qq2" class="common-text"></td>
                                </tr>
                                <tr>
                                    <th><i class="require-red">*</i>客服QQ3：</th>
                                    <td><input type="text" id="" value="{{$data['qq3']}}" size="85" name="qq3" class="common-text"></td>
                                </tr>
                                 <tr>
                                    <th><i class="require-red">*</i>QQ群1：</th>
                                    <td><input type="text" id="" value="{{$data['qqqun1']}}" size="85" name="qqqun1" class="common-text"></td>
                                </tr>
                                 <tr>
                                    <th><i class="require-red">*</i>QQ群2：</th>
                                    <td><input type="text" id="" value="{{$data['qqqun2']}}" size="85" name="qqqun2" class="common-text"></td>
                                </tr>
                                 <tr>
                                    <th><i class="require-red">*</i>QQ群3：</th>
                                    <td><input type="text" id="" value="{{$data['qqqun3']}}" size="85" name="qqqun3" class="common-text"></td>
                                </tr>
                                 <tr>
                                    <th><i class="require-red">*</i>客服邮箱：</th>
                                    <td><input type="text" id="" value="{{$data['email']}}" size="85" name="email" class="common-text"></td>
                                </tr>
                                 <tr>
                                    <th><i class="require-red">*</i>业务合作邮箱：</th>
                                    <td><input type="text" id="" value="{{$data['business_email']}}" size="85" name="business_email" class="common-text"></td>
                                </tr>
                              	 <tr >
                                    <th><i class="require-red"></i>QQ群链接代码：</th>
                                    <td ><textarea id="" value="{{$data['qqqun_url']}}" name="qqqun_url" class="common-text" style="width:610px;height:100px;">{{$data['qqqun_url']}}</textarea></td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>
                                        <input type="submit" value="提交" class="btn btn-primary btn6 mr10">
                                        <input type="button" value="返回" onclick="history.go(-1)" class="btn btn6">
                                    </td>
                                </tr>
                            </tbody></table>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
    <!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(0).show();
$(".sub-menu").eq(0).children("li").eq(3).addClass("on");
</script>
@endsection