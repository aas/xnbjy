@extends('admin.mainlayout')
@section('content')
<!--/sidebar-->
<script type="text/javascript">
    var msg = "{{Session::get('message')}}";
    //触发事件
    $(window).ready(function(){
        if(msg){
            layer.msg(msg,{
                time:2000  //2毫秒
            });
        }
    });
</script>
<div class="main-wrap">
	<div class="crumb-wrap">
		<div class="crumb-list">
			<i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span
				class="crumb-step">&gt;</span><span class="crumb-name">系统设置</span>
		</div>
	</div>
	<div class="result-wrap">
		<form action="{{url('center/config/indexAdd')}}" method="post" id="myform"
			name="myform" enctype="multipart/form-data">
			{{csrf_field()}}
			<div class="config-items">
				<div class="config-title">
					<h1>
						<i class="icon-font">&#xe00a;</i>网站信息设置
					</h1>
				</div>
				<div class="result-content">
					<table width="100%" class="insert-tab">
						<tbody>
							<tr>
								<th width="15%"><i class="require-red">*</i>网站LOGO：</th>
								<td><img style="width: 300px;height: 150px;border-radius: 15px;border: 1px solid;" src="{{$data['logo']}}"><input type="hidden"
									name="logo" value="{{$data['logo']}}" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>网站LOGO上传：</th>
								<td><input type="file" name="logo" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>网站网址：</th>
								<td><input type="text" id="" value="{{$data['localhost']}}"
									size="85" name="localhost" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>网站标题：</th>
								<td><input type="text" id="" value="{{$data['title']}}"
									size="85" name="title" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>网站名称：</th>
								<td><input type="text" id="" value="{{$data['name']}}"
									size="85" name="name" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>网站关键字：</th>
								<td><input type="text" id="" value="{{$data['keywords']}}"
									size="85" name="keywords" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>网站虚拟币英文标识：</th>
								<td><input type="text" id="" value="{{$data['xnb']}}"
									size="85" name="xnb" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>网站虚拟币代称：</th>
								<td><input type="text" id="" value="{{$data['xnb_name']}}"
									size="85" name="xnb_name" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>虚拟币对人民币兑换：</th>
								<td><input type="text" id="" value="{{$data['bili']}}"
									size="85" name="bili" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>网站描述：</th>
								<td><input type="text" id="" value="{{$data['desc']}}"
									size="85" name="desc" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>版权信息：</th>
								<td><input type="text" id="" value="{{$data['copyright']}}"
									size="85" name="copyright" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>备案信息：</th>
								<td><input type="text" id="" value="{{$data['record']}}"
									size="85" name="record" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>地址：</th>
								<td><input type="text" id="" value="{{$data['address']}}"
									size="85" name="address" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>新币申请表上传：</th>
								<td><input type="file" name="biaoge_url" class="common-text"></td>
							</tr>
							<tr>
								<th></th>
								<td><input type="submit" value="提交"
									class="btn btn-primary btn6 mr10"> <input type="button"
									value="返回" onclick="history.go(-1)" class="btn btn6"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

		</form>
	</div>
</div>
<!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(0).show();
$(".sub-menu").eq(0).children("li").eq(0).addClass("on");
</script>
@endsection