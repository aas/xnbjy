@extends('admin.mainlayout')
@section('content')
	<style>
		.list-page ul{overflow:hidden;display:inline-block;}
		.list-page ul li{float:left;border:none;height:30px;line-height:30px;border: 1px}
		.list-page ul li a{padding:6px 12px;text-decoration:none}
	</style>
    <!--/sidebar-->
    <div class="main-wrap">
		

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">第三方充值记录</span></div>
        </div>
        <!-- <div class="search-wrap">
            <div class="search-content">
				<div class="search-wrap">
					<div class="search-content">
						<form action="{:U('Pay/payByMan')}" method="post">
							<table class="search-tab">
								<tr>
									<th width="120">选择分类:</th>
									<td>
										<select name="status" id="">
											<option value="">全部</option>
											<option value="0">未付款</option>
											<option value="1">充值成功</option>
											<option value="2">充值失败</option>
											<option value="3">已失效</option>
										</select>
									</td>
									<th width="70">汇款人:</th>
									<td><input class="common-text" placeholder="汇款人" name="member_name" value="" id="" type="text"></td>
									<td><input class="btn btn-primary btn2" name="sub" value="查询" type="submit"></td>
								</tr>
							</table>
						</form> 
					</div>
				</div>
			</div>
        </div> -->
        <div class="result-wrap">
            <form name="myform" id="myform" method="post">
                <div class="result-content">
                    <table class="result-tab" width="100%">
                        <tr>
                            <th>订单号</th>
                            <th>汇款人</th>
                            <th>充值钱数</th>
                            <th>状态</th>
                            <th>时间</th>
                        </tr>
						@foreach($result as $k=>$v)
                        <tr>
                            <td>{{$v->tradeno}}</td>
                     		<td>{{$v->uname}}</td>
                            <td>{{$v->num}}</td>
                            <td>@if($v->status == 0) 未成功 @else 汇款成功 @endif </td>
                            <td>{{date('Y-m-d H:i:s',$v->ctime)}}</td>
                        </tr>
						@endforeach
                    </table>
                     <div class="list-page">{{ $result->links() }}</div>
                </div>
            </form>
        </div>
    </div>
    <!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
function pass(id){
	layer.confirm(
	'确定通过审核', 
	{btn:['确定','取消']},
	function(){
	  $.post("{:U('Pay/payUpdate')}",{"pay_id":id,"status":1},function(data){
		  if(data.status==0){
					layer.msg(data['info']);
					setTimeout("location.reload()",2000);
				}else{
					layer.msg(data['info']);
					setTimeout("location.reload()",2000);
				}
		})
	}
	),
	function(){
		layer.msg('已取消');
	}
}

function fail(id){
	layer.confirm(
	'确定不通过审核', 
	{btn: ['确定','取消'],},
	function(){
		//href="{:U('Pay/payUpdate',array('pay_id'=>$vo['pay_id'],'status'=>1))}			
		$.post("{:U('Pay/payUpdate')}",{"pay_id":id,"status":2},function(data){
				if(data.status == 0){
					layer.msg(data['info']);
					setTimeout("location.reload()",2000);
					
				}else if(data.status == 2){
					layer.msg(data['info']);
					setTimeout("location.reload()",2000);
				}else{
					layer.msg(data['info']);
					setTimeout("location.reload()",2000);
				}
		})
		}
		),
	 function(){
		layer.msg('已取消');
	}
}
</script>
<script>
$(".sub-menu").eq(6).show();
$(".sub-menu").eq(6).children("li").eq(4).addClass("on");
</script>
@endsection