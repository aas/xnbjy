@extends('admin.mainlayout')
@section('content')
	<style>
		.list-page ul{overflow:hidden;display:inline-block;}
		.list-page ul li{float:left;border:none;height:30px;line-height:30px;border: 1px}
		.list-page ul li a{padding:6px 12px;text-decoration:none}
	</style>
    <script>
        function CheckForm(){
           if(document.myform.member_id.value == "" || document.myform.member_id.value == 0){
               layer.msg('请输入充值用户ID!');
               document.myform.member_id.focus();
               return false;
		   }

            var member_id = document.myform.member_id.value;
            if( member_id != ""){
                if(!(/^[0-9]*$/).test(member_id)){
                    layer.msg('用户ID格式错误!');
                    document.myform.member_id.focus();
                    return false;
                }
            }

            if(document.myform.money.value == "" || document.myform.money.value == 0){
                layer.msg('请输入充值金额!');
                document.myform.money.focus();
                return false;
            }
            var money = document.myform.money.value;
            if( money != ""){
                if(!(/^[0-9]*$/).test(money)){
                    layer.msg('充值金额格式错误!');
                    document.myform.money.focus();
                    return false;
				}
            }

            if(document.myform.password.value == ""){
                layer.msg('请输入管理员密码!');
                document.myform.password.focus();
                return false;
            }
		}
    </script>
	<script type="text/javascript">
        var msg = "{{Session::get('message')}}";
        //触发事件
        $(window).ready(function(){
            if(msg){
                layer.msg(msg,{
                    time:2000  //2毫秒
                });
            }
        });
	</script>
<!--/sidebar-->
<div class="main-wrap">
	<div class="crumb-wrap">
		<div class="crumb-list">
			<i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span
				class="crumb-step">&gt;</span><span class="crumb-name">添加分红奖励</span>
		</div>
	</div>

	<form class="result-wrap">



		<div style="float: left; width: 30%; margin-right: 20px;">
			<div class="config-title">

				<h1>
					<i class="icon-font">&#xe00a;</i>添加管理员充值
				</h1>
			</div>
			<form action="{{url('center/pay/admAdd')}}" method="post" id="myform"
				name="myform" onsubmit="{return CheckForm()}" >
				{{csrf_field()}}
				<div class="config-items">
					<div class="result-content">
						<table width="100%" class="insert-tab">
							<tbody>
								<tr>
									<th style="width: 35%;"><i class="require-red">*</i>充值用户ID：</th>
									<td><input name="member_id" type="text"><span
										id="showBug" style="margin-left: 10px; color: #FF0000"></span></td>
								</tr>
								<tr>
									<th><i class="require-red">*</i>充值币种：</th>
									<td><select name="currency_id" id="category">
											<option value="0">人民币</option>
												@foreach($data as $k=>$v)
											<option value="{{$v->currency_id}}">{{$v->currency_name}}</option>
													@endforeach
									</select></td>

								</tr>
								<tr>
									<th><i class="require-red">*</i>充值金额：</th>
									<td><input name="money" type="text"></td>
								</tr>
									<tr>
									<th><i class="require-red">*</i>管理员密码：</th>
									<td><input name="password" type="password" /></td>
								</tr>

								<th></th>
								<td><input type="submit" value="提交"
									class="btn btn-primary btn6 mr10"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</form>
		</div>
		<div style="float: left; width: 65%;">
			<div class="config-title">
				<h1>
					<i class="icon-font">&#xe00a;</i>管理员充值列表
				</h1>
			</div>
            <div class="search-content">
						<form action="{{url('center/pay/adm')}}" method="get">
							{{csrf_field()}}
							<table class="search-tab">
								<tr>
									<th width="120">选择分类:</th>
									<td>
										<select name="type_id" id="">
											<option value="">全部</option>
											@foreach($data as $k=>$v)
											<option value="{{$v->currency_id}}">{{$v->currency_name}}</option>
											@endforeach
										</select>
									</td>
									<th width="70">用户邮箱:</th>
									<td><input class="common-text" placeholder="用户邮箱" name="email" id="" type="text"></td>
                                    <th width="70">用户ID:</th>
									<td><input class="common-text" placeholder="用户ID" name="member_id" id="" type="text"></td>
									<td><input class="btn btn-primary btn2" name="sub" value="查询" type="submit"></td>
								</tr>
							</table>
						</form>
					</div>
                    <br>
			<table class="result-tab" width="100%">
				<tr>
					<th>充值用户ID</th>
					<th>充值用户</th>
					<th>充值币种</th>
					<th>充值金额</th>
				</tr>
				@if(!$result->isEmpty())
				@foreach($result as $k=>$v)
				<tr>
					<td>{{$v['member_id']}}</td>
					<td>{{$v['email']}}</td>
					<td>@if($v['currency_name'] == null ) 人民币 @else {{$v['currency_name']}} @endif</td>
					<td>{{$v['money']}}</td>
				</tr>
				@endforeach
				@else
				<tr>
					<td style="font-size: 18px;text-align: center;" colspan="4">暂无数据...</td>
				</tr>
					@endif
			</table>
			  <div class="list-page">{{ $result->appends(['type_id'=>$type_id,'email'=>$email,'member_id'=>$member_id])->links() }}</div>
		</div>

		<div style="clear: both;"></div>
	</div>
</div>
<!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(6).show();
$(".sub-menu").eq(6).children("li").eq(3).addClass("on");
</script>
@endsection