@extends('admin.mainlayout')
@section('content')
    <style>
        .list-page ul{overflow:hidden;display:inline-block;}
        .list-page ul li{float:left;border:none;height:30px;line-height:30px;border: 1px}
        .list-page ul li a{padding:6px 12px;text-decoration:none}
    </style>
    <!--/sidebar-->
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">分红列表</span></div>
        </div>
        <div class="search-wrap">
           
        </div>
        <div class="result-wrap">

            <div class="result-content">
                <table class="result-tab" width="100%">
                    <tr>
                        <th>用户</th>
                        <th>分红金额</th>
                        <th>分红币种</th>
						<th>分红时间</th>
                    </tr>
                    @foreach ($data as $v)
                        <tr>
                            <td>{{$v->name}}</td>
							<td>{{$v->money}}</td>
                            <td>{{$v->currency_id}}</td>
							 <td>{{date('Y-m-d H:i:s',$v->add_time)}}</td>
                        </tr>
                   @endforeach
                </table>
                <div class="list-page"> {{ $data->links() }}</div>
            </div>
        </div>
    </div>
    <!--/main-->
</div>
</body>
</html>
    @endsection
@section('footer')
<script>
$(".sub-menu").eq(4).show();
$(".sub-menu").eq(4).children("li").eq(1).addClass("on");
</script>
    @endsection