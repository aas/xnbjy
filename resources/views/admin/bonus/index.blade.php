@extends('admin.mainlayout')
@section('content')
    <script type="text/javascript">
        var msg = "{{Session::get('message')}}";
        //触发事件
        $(window).ready(function(){
            if(msg){
                layer.msg(msg,{
                    time:2000  //2毫秒
                });
            }
        });
    </script>
    <script>
        function CheckForm(){
            if(document.myform.num.value ==''){
                layer.msg('分红数值不能为空!',{
                    time:2000  //2毫秒
                });
                document.myform.num.focus();
                return false;
            }

            if(document.myform.text.value == ''){
                layer.msg('消息提醒不能为空!',{
                    time:2000  //2毫秒
                });
                document.myform.text.focus();
                return false;
            }

        }

    </script>
<!--/sidebar-->
<div class="main-wrap">
    <div class="crumb-wrap">
        <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">添加分红奖励</span></div>
    </div>

    <div class="result-wrap">
    <div style="float:left; width:30%; margin-right:20px;">
             <div class="config-title">

				<h1><i class="icon-font">&#xe00a;</i>添加分红</h1>
			</div>
        <form action="{{url('center/bonus/addList')}}" method="post" id="myform" name="myform" enctype="multipart/form-data" onsubmit="{return CheckForm();}">
            {{csrf_field()}}
            <div class="config-items">
                <div class="result-content">
                    <table width="100%" class="insert-tab">
                        <tbody>
                        
                        
                        <tr>
                            <th style="width:35%;"><i class="require-red">*</i>分红金额：</th>
                            <td><input name="num" id="num"  type="text"><span id="showBug" style=" margin-left:10px;color:#FF0000"></span></td>
                        </tr>
                        
                        <tr>
                            <th><i class="require-red">*</i>分红运算基数：</th>
							 <td>
								<select name="currency_id" id="category">
								@foreach($result as $k=>$v)
								<option  value="{{$v['currency_id']}}">{{$v['currency_name']}}</option>
								@endforeach
								</select>
							</td>
                        </tr>
                        <tr>
                            <th><i class="require-red">*</i>用户得到币种：</th>
							 <td>
								<select name="set_currency_id" id="category">
								<option value="0">人民币</option>
                                    @foreach($result as $k=>$v)
								<option  value="{{$v['currency_id']}}">{{$v['currency_name']}}</option>
                                        @endforeach
								</select>
							</td>
                        </tr>
                        <tr>
                            <th><i class="require-red">*</i>消息提醒：</th>
                            <td><input name="text" id="text" type="text"></td>
                        </tr>
                       
                            <th></th>
                            <td>
                                <input type="submit" value="提交" class="btn btn-primary btn6 mr10">
                            </td>
                        </tr>
                        </tbody></table>
                </div>
            </div>
        </form>
    </div>
    <div style="float:left; width:65%;">
	 <div class="config-title">
         <h1><i class="icon-font">&#xe00a;</i>分红管理</h1>
     </div>
	<table class="result-tab" width="100%">
                    <tr>
                        <th>分红手续费币种</th>
                        <th>分红手续费总数</th>
						<th>已分手续费</th>
                        <th>剩余分红手续费</th>
                    </tr>
                    @foreach($data as $k=>$v)
                        <tr>
                           <td>{{$v['currency_name']}}</td>
							<td>
                                @if($v['sumMoney'] == '')
                                0
                                @else
                                {{$v['sumMoney']}}
                                @endif
                            </td>
                            <td>
                                @if($v['money'] == '')
                                    0
                            @else
                                {{$v['money']}}
                            @endif
                           <td>
                               @if ($v['sumMoney'] - $v['money'] < 0) 0 @else {{$v['sumMoney'] - $v['money']}} @endif  </td>
                        </tr>
                    @endforeach
                </table>
    </div>

	
		<div style="clear:both;"></div>
    </div>
</div>
<script>
ckemail = 0;

function subform(){
	$('#myform').submit();

}
</script>

<!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
<script>
$(".sub-menu").eq(4).show();
$(".sub-menu").eq(4).children("li").eq(0).addClass("on");
</script>
@endsection