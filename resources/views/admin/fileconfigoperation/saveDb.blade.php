
@extends('admin.mainlayout')
@section('content')
	<script>
        var msg = "{{Session::get('message')}}";
        $(window).ready(function () {
            if(msg) {
                layer.msg(msg,{time:2000});
            }
        });
	</script>
<!--/sidebar-->
<style>
.tab1 th{ width:225px;}
</style>
<div class="main-wrap">
	<div class="crumb-wrap">
		<div class="crumb-list">
			<i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span
				class="crumb-step">&gt;</span><span class="crumb-name">系统设置</span>
		</div>
	</div>
	<div class="result-wrap">
		<form action="{{url('center/fileconfig/saveDb_edit')}}" method="post" id="myform"
			name="myform" enctype="multipart/form-data">
			{{csrf_field()}}
			<div class="config-items">
				<div class="config-title">
					<h1>
						<i class="icon-font">&#xe00a;</i>数据库表前缀配置管理
					</h1>
				</div>
				<div class="result-content">
					<table width="100%" class="insert-tab">
						<tbody>
							<tr>
								<th><i class="require-red">*</i>数据库类型：</th>
								<td>mysql</td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>现数据库表前缀：</th>
								<td>{{$data['prefix']}}</td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>数据表前缀：</th>
								<td><input type="text" id="" value="" size="85"
									name="DB_PREFIX" class="common-text"></td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>注意：</th>
								<td><span style="font-size: 15px;color: red;">数据库配置请谨慎修改,修改后自动更改为最新口令(自动修改MySql数据库表前缀)</span></td>
							</tr>
							<tr>
								<th></th>
								<td><input type="button" onclick="check()"  value="提交"
									class="btn btn-primary btn6 mr10"> <input type="button"
									value="返回" onclick="history.go(-1)" class="btn btn6"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</form>
		<form action="{{url('center/fileconfig/saveDb_edit')}}" method="post" id="myform"
			name="myform" enctype="multipart/form-data">
			{{csrf_field()}}
			<div class="config-items">
				<div class="config-title">
					<h1>
						<i class="icon-font">&#xe00a;</i>数据密码管理
					</h1>
				</div>
				<div class="result-content">
					<table width="100%" class="insert-tab tab1">
						<tbody>
							<tr>
								<th><i class="require-red">*</i>数据库类型：</th>
								<td>mysql</td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>数据库账号：</th>
								<td>{{$data['username']}}</td>
							</tr>
							<tr>
								<th><i class="require-red">*</i>数据库密码：</th>
								<td><input type="password" id="" value="" size="85"
									name="DB_PWD" class="common-text"></td>
							</tr>
							
							<tr>
								<th><i class="require-red">*</i>注意：</th>
								<td><span style="font-size: 15px;color: red;">数据库配置请谨慎修改,修改后自动更改为最新口令(先在后台配置,在手动修改MySql数据库密码)</span></td>
							</tr>
							<tr>
								<th></th>
								<td><input type="button"  onclick="check()" value="提交"
									class="btn btn-primary btn6 mr10"> <input type="button"
									value="返回" onclick="history.go(-1)" class="btn btn6"></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</form>
	</div>
</div>
<!--/main-->
</div>
</body>
</html>
@endsection
@section('footer')
	<script>
		function check() {
            layer.confirm('您确定要执行此操作吗？', {
                btn: ['确定','取消'] //按钮
            },function () {
				$('#myform').submit();
            });
        }
	</script>
<script>
$(".sub-menu").eq(0).show();
$(".sub-menu").eq(0).children("li").eq(6).addClass("on");
</script>
	@endsection