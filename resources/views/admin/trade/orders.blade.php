@extends('admin.mainlayout')
@section('js')

@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{:U('Index/index')}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">委托记录</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
                <div class="search-wrap">
                    <div class="search-content">
                        <form action="{{urlAdmin('Trade','getOrderIndex')}}" method="get">
                            <table class="search-tab">
                                <tr>
                                    <th width="120">选择币种:</th>
                                    <td>
                                        <select name="currency_id" id="">
                                            <option value="0">全部</option>
                                            @foreach($currency_type as $v)
                                            <option value="{{$v['currency_id']}}" >{{$v['currency_name']}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <th width="120">选择分类:</th>
                                    <td>
                                        <select name="status" id="">
                                            <option value="">全部</option>
                                            <option value="0">挂单</option>
                                            <option value="1">部分成交</option>
                                            <option value="2">成交</option>
                                            <option value="-1">已撤销</option>
                                        </select>
                                    </td>
                                    <th width="70">购买人:</th>
                                    <td><input class="common-text" placeholder="购买人email" name="email" value="" id="" type="text"></td>
                                    <td><input class="btn btn-primary btn2" value="查询" type="submit"></td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="result-wrap">
            <form name="myform" id="myform" method="post">
                <div class="result-content">
                    <table class="result-tab" width="100%">
                        <tr>
                            <th>委托编号</th>
                            <th>用户邮箱</th>
                            <th>币种</th>
                            <th>价格</th>
                            <th>挂单数量</th>
                            <th>成交数量</th>
                            <th>手续费</th>
                            <th>类型</th>
                            <th>挂单时间</th>
                            <th>成交时间</th>
                            <th>状态</th>
                            <th>操作</th>
                        </tr>
                            @foreach($list['data'] as $v)
                            <tr>
                                <td>{{$v['orders_id']}}</td>
                                <td>{{$v['member']['email']}}</td>
                                <td>{{$v['currency']['currency_name']}}</td>
                                <td>{{$v['price']}}</td>
                                <td>{{$v['num']}}</td>
                                <td>{{$v['trade_num']}}</td>
                                <td>{{number_format($v['num']*$v['fee']*$v['price'],4)}}</td>
                                <td>{{getOrdersType($v['type'])}}</td>
                                <td>{{date('Y-m-d H:i:s',$v['add_time'])}}</td>
                                <td>
                                    @if(empty($v['trade_time']))
                                    ###
                                    @else
                                    {{date('Y-m-d H:i:s',$v['trade_time'])}}
                                    @endif
                                </td>
                                <td>
                                    {{getOrdersStatus($v['status'])}}    {{--//'0是挂单，1是部分成交,2成交， -1撤销'--}}
                                </td>
                                <td>
                                    @if($v['status']==2||$v['status']==-1)
                                    {{getOrdersStatus($v['status'])}}
                                    @else
                                    <a href="javascript:void(0)"  onclick="cexiao({{$v['orders_id']}})">撤销</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                    </table>
                    <div class="list-page">
                        <ul>
                            @if($list['last_page']>1)
                            <div>
                                @if(!empty($list['prev_page_url']))
                                <a class="prev" href="{{$list['prev_page_url']}}"><<</a>
                                @endif
                                @foreach($list['pageNoList'] as $v)
                                    <a @if($v == $list['current_page'])
                                       class="current"
                                       @else
                                       class="num"
                                       @endif
                                       href="{{urlAdmin('Trade','getOrderIndex')}}?page={{$v}}">{{$v}}</a>
                                @endforeach
                                @if(!empty($list['next_page_url']))
                                <a class="next" href="{{$list['next_page_url']}}">>></a>
                                @endif
                            </div>
                            @endif
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!--/main-->

@endsection
@section('footer')
    <script>
        function cexiao(_this){
            layer.confirm('确定撤销委托？', {
                btn: ['确定','取消'], //按钮
                title: '撤销委托'
            }, function(){
                $.post(
                    '{{urlAdmin('Trade','postCancel')}}',
                     {
                      status:-1,
                      orders_id:_this,
                     '_token':'{{csrf_token()}}'
                     },
                    function(data){
                    if(data['status'] == 1){
                        layer.msg(data['info']);
                        setTimeout(function(){location.reload();},1000);
                    }else{
                        layer.msg(data['info']);
                    }
                    });
            }, function(){
                layer.msg('已取消');
            });

        }
    </script>
    <script>
        $(".sub-menu").eq(7).show();
        $(".sub-menu").eq(7).children("li").eq(0).addClass("on");
    </script>
@endsection