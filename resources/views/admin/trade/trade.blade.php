@extends('admin.mainlayout')
@section('js')

@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{:U('Index/index')}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">交易记录</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
                <div class="search-wrap">
                    <div class="search-content">
                        <form action="{{urlAdmin('Trade','getTradeIndex')}}" method="get">
                            <table class="search-tab">
                                <tr>
                                    <th width="120">选择币种:</th>
                                    <td>
                                        <select name="currency_id" id="">
                                            <option value="0">全部</option>

                                                @foreach($currency_type as $v)
                                                <option value="{{$v['currency_id']}}" >{{$v['currency_name']}}</option>
                                                @endforeach

                                        </select>
                                    </td>
                                    <th width="120">选择类型:</th>
                                    <td>
                                        <select name="type" id="">
                                            <option value="0">全部</option>
                                            <option value="buy" >买入</option>
                                            <option value="sell" >卖出</option>
                                        </select>
                                    </td>

                                    <th width="70">购买人:</th>
                                    <td><input class="common-text" placeholder="购买人email" name="email" value="" id="" type="text"></td>
                                    <td><input class="btn btn-primary btn2" value="查询" type="submit"></td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="result-wrap">
            <form name="myform" id="myform" method="post">
                <div class="result-content">
                    <table class="result-tab" width="100%">
                        <tr>
                            <th>成交编号</th>
                            <th>订单号</th>
                            <th>买家email</th>
                            <th>币种</th>
                            <th>数量</th>
                            <th>单价</th>
                            <th>总量</th>
                            <th>手续费</th>
                            <th>类型</th>
                            <th>成交时间</th>
                        </tr>

                            @foreach($list['data'] as $v)
                            <tr>
                                <td>{{$v['trade_id']}}</td>
                                <td>{{$v['trade_no']}}</td>
                                <td>{{$v['member']['email']}}</td>
                                <td>{{$v['currency']['currency_name']}}</td>
                                <td>{{$v['num']}}</td>
                                <td>{{$v['price']}}</td>
                                <td>{{$v['money']}}</td>
                                <td>{{number_format($v['fee'],4)}}</td>
                                <td>{{$v['type']}}</td>
                                <td>{{date('Y-m-d H:i:s',$v['add_time'])}}</td>
                            </tr>
                            @endforeach

                    </table>
                    <div class="list-page">
                        <ul>
                            @if($list['last_page']>1)
                            <div>
                                <a class="prev" href="{{$list['prev_page_url']}}"><<</a>
                                @foreach($list['pageNoList'] as $v)
                                    <a @if($v == $list['current_page'])
                                       class="current"
                                       @else
                                       class="num"
                                       @endif
                                       href="{{urlAdmin('Trade','getTradeIndex')}}?page={{$v}}">{{$v}}</a>
                                @endforeach
                                <a class="next" href="{{$list['next_page_url']}}">>></a>
                            </div>
                            @endif
                        </ul>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('footer')
    <script>
        $(".sub-menu").eq(7).show();
        $(".sub-menu").eq(7).children("li").eq(1).addClass("on");
    </script>
@endsection