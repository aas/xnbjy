@extends('admin.mainlayout')
@section('content')
    <style>
        .result-wrap{ font-size:14px;}
        .wid1{ width:180px;}
        .wid2{ width:270px !important;}
        .tablesty tr{ height:30px; line-height:30px;}
        .ceshi{clear:both; padding-left:14px;}
    </style>
    <script>
        var msg = "{{Session::get('message')}}";
        //触发事件
        $(window).ready(function(){
            if(msg){
                layer.msg(msg,{
                    time:2000  //2毫秒
                });
            }
        });
    </script>
    <!--/sidebar-->
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{url('center/')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">下载管理</span></div>
        </div>
        <div class="result-wrap">
            <div class="result-content">
                <span class="crumb-name">下载管理</span><br><br><br>
                <form action="{{url('center/download/biaogeDownload')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <table class="tablesty">
                        <tbody>
                        <tr>
                            <td class="wid1">新币上线申请表上传：</td>
                            <td class="wid2">
                                <input style=" width:190px !important; float:left;" type='file' name='biaoge'>
                                <input class="btn-primary" style="width:40px !important; float:left;" type="submit" value="提交"></td>
                            <td class="wid1 ceshi"><a href='{{$all['biaoge_url']}}'>测试</a></td>
                        </tr>
                        </tbody>
                    </table>

                </form>
                <br> <hr><br>
                <form action="{{url('center/download/qianbaoDownload')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <table class="tablesty">
                        <tbody>
                        @foreach($currency as $k=>$v)
                            <tr>
                                <td class="wid1">{{$v['currency_name']}}钱包上传：</td>
                                <td class="wid2"><input type='file' name='{{$v['currency_id']}}'> </td>
                                <td class="wid1"><a href='{{$v['qianbao_url']}}'>测试钱包</a></td>
                            </tr>
                        @endforeach
                        <tr>
                            <td><input class="btn-primary" style=" width:60px;" type="submit" value="提交"></td>
                        </tr>
                        </tbody>
                    </table>

                </form>
                <br> <hr><br>
                <form action="{{url('center/download/guanwangUrl')}}" method="post" name="form1" enctype="multipart/form-data" >
                    {{csrf_field()}}
                    <table class="tablesty">
                        <tbody>
                        @foreach($currency as $k=>$v)
                            <tr>
                                <td class="wid1">{{$v['currency_name']}}官方网站： </td>
                                <td class="wid1"><input type="text" name="guanwang_url[{{$v['currency_id']}}]" value="{{$v['guanwang_url']}}"  style="width:300px;"/></td>
                                <input type="hidden" name="currency_id[]" value="{{$v['currency_id']}}" />
                            </tr>
                        @endforeach
                        <tr>
                            <td><input class="btn-primary" style=" width:60px;" type="submit" value="提交"></td>
                        </tr>
                        </tbody>
                    </table>

                </form>
                <br> <hr><br>
            </div>

        </div>
    </div>
    <!--/main-->
    </div>
    </body>
    </html>
@endsection