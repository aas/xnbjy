@extends('admin.mainlayout')
@section('js')

@endsection
@section('content')
<div class="main-wrap">

    <div class="crumb-wrap">
        <div class="crumb-list"><i class="icon-font"></i><a href="{:U('Index/index')}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">会员管理</span></div>
    </div>
    <div class="search-wrap">
        <div class="search-content">
        </div>
    </div>
    <div class="result-wrap">

        <div class="result-title">
            <div class="result-list">
                <a href="#" onclick="history.go(-1)">返回</a>
            </div>
        </div>
        <div class="result-content">
            <table class="result-tab" width="100%">
                <tr>
                    <th>ID</th>
                    <th>会员邮箱</th>
                    <th>状态</th>
                    <th>注册时间</th>
                </tr>

                    @if(!empty($data['data']))
                    @foreach($data['data'] as $v)
                    <tr>
                        <td>{{$v['member_id']}}</td>
                        <td>{{$v['email']}}</td>
                        <td>
                            @if($v['status']==0)

                            @elseif($v['status']==1)
                            正常
                            @elseif($v['status']==2)
                            禁用
                            @else
                            未知状态
                            @endif
                        </td>
                        <td>{{date('Y-m-d H:i:s',$v['reg_time'])}}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="4" align="center">还没有推荐其他人</td>
                    </tr>
                    @endif

            </table>
            @if(!empty($data['data']))
            <div class="list-page">
                <ul>
                    <div>
                        <a class="prev" href="{{$data['prev_page_url']}}&member_id={{$_GET['member_id']}}"><<</a>
                        @foreach($data['pageNoList'] as $v)
                            <a @if($v == $data['current_page'])
                               class="current"
                               @else
                               class="num"
                               @endif
                               href="{{urlAdmin('Member','getInvite')}}?member_id={{$_GET['member_id']}}&page={{$v}}">{{$v}}</a>
                        @endforeach
                        <a class="next" href="{{$data['next_page_url']}}&member_id={{$_GET['member_id']}}">>></a>
                    </div>
                </ul>
            </div>
            @endif

        </div>

    </div>
</div>

@endsection
@section('footer')

@endsection