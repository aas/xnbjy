@extends('admin.mainlayout')
@section('js')

@endsection
@section('content')
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{:U('Index/index')}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">查看个人账户</span></div>
        </div>
        <div class="result-wrap">
            <table class="result-tab" width="100%">
                <tr>
                    <th>ID</th>
                    <th>会员姓名</th>
                    <th>会员邮箱</th>
                    <th>会员电话</th>
                </tr>
                <tr>
                    <td>{{$member_info['member_id']}}</td>
                    <td>{{$member_info['name']}}</td>
                    <td>{{$member_info['email']}}</td>
                    <td>{{$member_info['phone']}}</td>
                </tr>
            </table>
        </div>
        <div class="result-wrap">
            <div class="result-content">
                <table class="result-tab" width="100%">
                    <tr>
                        <th>币种名称</th>
                        <th>持有数量</th>
                        <th>冻结数量</th>
                        <th>操作</th>
                    </tr>

                        @foreach($info['data'] as $v)
                        <tr>
                            <td>{{$v['currency']['currency_name']}}</td>
                            <td><input  value="{{$v['num']}}" type="text" id="num{{$v['currency_id']}}"/></td>
                            <td><input value="{{$v['forzen_num']}}" type="text" id="forzen_num{{$v['currency_id']}}"/></td>
                            <td><a onclick="update({{$v['currency_id']}},num{{$v['currency_id']}})" style="cursor: pointer;">修改</a></td>
                        </tr>
                        @endforeach

                </table>
                <div class="list-page">
                    <ul>
                        <div>
                            @if(!empty($info['prev_page_url']))
                            <a class="prev" href="{{$info['prev_page_url']}}&member_id={{$_GET['member_id']}}"><<</a>
                            @endif
                            @foreach($info['pageNoList'] as $v)
                                <a @if($v == $info['current_page'])
                                   class="current"
                                   @else
                                   class="num"
                                   @endif
                                   href="{{urlAdmin('Member','getShow')}}?member_id={{$_GET['member_id']}}&page={{$v}}">{{$v}}</a>
                            @endforeach
                            @if(!empty($info['next_page_url']))
                            <a class="next" href="{{$info['next_page_url']}}&member_id={{$_GET['member_id']}}">>></a>
                            @endif
                        </div>
                    </ul>
                </div>
            </div>

        </div>
        <div style="margin: 0 auto; text-align:center; "><br />
            <input type="button" value="返回" onclick="history.go(-1)" class="btn btn6">
        </div>

@endsection
@section('footer')
            <script>
                function update(_this,a){
                    layer.confirm('确定修改？', {
                        btn: ['确定','取消'], //按钮
                        title: '修改'
                    }, function(){
                        var member_id="{{isset($info['data'][0]['member_id'])?$info['data'][0]['member_id']:''}}";
                        var num=$("#num"+_this).val();
                        var forzen_num=$("#forzen_num"+_this).val();
                        $.post(
                                "{{urlAdmin('Member','postMemberMoney')}}",
                                {currency_id:_this,
                                 member_id:member_id,
                                 num:num,
                                 forzen_num:forzen_num,
                                '_token':'{{csrf_token()}}'
                                },
                                function(data){
                            if(data['status'] == 1){
                                layer.msg(data['msg']);
                                setTimeout(function(){location.reload();},1000);
                            }else{
                                layer.msg(data['msg']);
                            }
                        })
                    }, function(){
                        layer.msg('已取消');
                    });

                }
            </script>
            <script>
                $(".sub-menu").eq(3).show();
                $(".sub-menu").eq(3).children("li").eq(0).addClass("on");
            </script>
@endsection