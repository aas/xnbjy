@extends('admin.mainlayout')
@section('js')

@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="#">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">会员管理</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
                <form action="{{urlAdmin('Member','getIndex')}}" method="get">
                    <table class="search-tab">
                        <tr>
                            <td width="70">会员账号:</td>
                            <td><input class="common-text" placeholder="邮箱" name="email" type="text"></td>
                            <td width="70">会员ID:</td>
                            <td><input class="common-text" placeholder="ID" name="member_id" type="text"></td>
                            <td><input class="btn btn-primary btn2"  value="查询" type="submit"></td>
                        </tr>
                    </table>
                </form>

            </div>
        </div>
        <div class="result-wrap">

            <div class="result-title">
                <div class="result-list">
                    <a href="{{urlAdmin('Member','getAdd')}}"><i class="icon-font"></i>新增会员</a>
                </div>
            </div>
            <div class="result-content">
                <table class="result-tab" width="100%">
                    <tr>
                        <th>ID</th>
                        <th>会员邮箱</th>
                        <th>推荐人</th>
                        <th>会员姓名</th>
                        <th>手机号</th>
                        <th>账户余额</th>
                        <th>冻结金钱</th>
                        <th>注册时间</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                        @if(!empty($data['data']))
                        @foreach($data['data'] as $v)
                        <tr>
                            <td>{{$v['member_id']}}</td>
                            <td>{{$v['email']}}</td>
                            <td>{{$v['pid']}}</td>
                            <td>{{$v['name']}}</td>
                            <td>{{$v['phone']}}</td>
                            <td>{{$v['rmb']}}</td>
                            <td>{{$v['forzen_rmb']}}</td>
                            <td>{{date('Y-m-d H:i:s',$v['reg_time'])}}</td>
                            <td>
                                @if($v['status']==1)
                                正常
                                @endif
                                @if($v['status']==2)
                                禁用
                                @endif
                                @if($v['status']==0)
                                未填写个人信息(<a href="{{urlAdmin('Member','getSaveModify',array('member_id'=>$v['member_id']))}}">填写</a>)

                                @endif
                            </td>
                            <td>
                                <a class="link-del" href="{{urlAdmin('finance','getIndex',array('member_id'=>$v['member_id']))}}">查看财务日志</a>
                                <br>
                                <a class="link-del" href="{{urlAdmin('Member','getInvite',array('member_id'=>$v['member_id']))}}">查看邀请人员</a><br>
                                <a class="link-update" href="{{urlAdmin('Member','getEdit',array('member_id'=>$v['member_id']))}}">修改|</a>
                                <a class="link-del" href="#" onclick="del({{$v['member_id']}})">删除</a>
                                <a class="link-update" href="{{urlAdmin('Member','getShow',array('member_id'=>$v['member_id']))}}">查看会员账户</a>
                            </td>
                        </tr>
                       @endforeach
                        @else
                        <tr>
                            <td colspan="10" align="center">暂无数据...</td>
                        </tr>
                        @endif
                </table>
                <div class="list-page">
                    <ul>
                        @if($data['last_page']>1)
                        <div>
                            <a class="prev" href="{{$data['prev_page_url']}}"><<</a>
                            @foreach($data['pageNoList'] as $v)
                                <a @if($v == $data['current_page'])
                                   class="current"
                                   @else
                                   class="num"
                                   @endif
                                   href="{{urlAdmin('Member','getIndex')}}?page={{$v}}">{{$v}}</a>
                            @endforeach
                            <a class="next" href="{{$data['next_page_url']}}">>></a>
                        </div>
                        @endif
                    </ul>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('footer')
    <script>
        $(".sub-menu").eq(3).show();
        $(".sub-menu").eq(3).children("li").eq(0).addClass("on");
    </script>
    <script>
        function del(id){

            layer.confirm('确定要删除该项吗?',{btn:['确定','取消']},function(){

                console.log(id);
                $.ajax({
                    type:'post',
                    url:"{{urlAdmin('Member','postDel')}}",
                    data:{'id':id,'_token':'{{csrf_token()}}'},
                    dataType:'json',
                    success: function (res) {

                        // alert(res);
                        if(res['status']==1){

                            layer.msg(res.msg,{icon:6});
                            setTimeout("location.reload()",1500);

                        }else{

                            layer.msg(res.msg,{icon:5});

                        }

                    }

                });

            },function(){

                layer.msg('已取消!');

            });
        }
    </script>
@endsection