@extends('admin.mainlayout')
@section('js')
    <script>
        window.onload = function(){

            /********快闪重定向*********/
            var message = "{{Session::get('message')}}";
            $(window).ready(function () {
                if (message) {
                    layer.msg(message, {
                        time: 2000,

                    });
                }
            });
            /********快闪重定向*********/

        }
    </script>
@endsection
@section('content')
<div class="main-wrap">
    <div class="crumb-wrap">
        <div class="crumb-list"><i class="icon-font"></i><a href="#">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">会员管理</span></div>
    </div>
    <div class="result-wrap">
        <form action="{{urlAdmin('Member','postEdit')}}" method="post"  enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="config-items">
                <div class="result-content">
                    <table width="100%" class="insert-tab">
                        <input type="hidden" value="{{$data['member_id']}}" name="member_id" />
                        <tbody>
                        <tr>
                            <th width="15%">电子邮件：</th>
                            <td>{{$data['email']}}</td>
                        </tr>
                        <tr>
                            <th>头像上传：</th>
                            <td><input id='file' type="file"  style="width:200px;" value="上传图片" />
                                <img id='oldImg' src="/{{$data['head']}}" style="width:50px;height:auto" />
                                <input id='img' type="hidden" name="img" value="" />
                            </td>
                        </tr>
                        <tr>
                            <th> 登录密码：</th>
                            <td><input id="pwd" name="pwd" type="password">
                                <span id="showBug" style=" margin-left:10px;color:#FF0000">为空则不修改</span></td>
                        </tr>
                        <tr>
                            <th> 邀请码：</th>
                            <td>{{$data['pid']}}</td>
                        </tr>
                        <tr>
                            <th> 交易密码：</th>
                            <td><input id="pwdtrade" name="pwdtrade" type="password">
                                <span id="showBug" style=" margin-left:10px;color:#FF0000">为空则不修改</span></td>
                        </tr>
                        <tr>
                            <th> 金额：</th>
                            <td><input name="rmb" value="{{$data['rmb']}}" type="text"></td>
                        </tr>

                        <tr>
                            <th> 冻结金额：</th>
                            <td><input name="forzen_rmb" value="{{$data['forzen_rmb']}}" type="text"></td>
                        </tr>
                        <if condition="$list['status']!=0 ">
                            <tr>
                                <th>昵称：</th>
                                <td><input name="nick" id="nick" value="{{$data['nick']}}" type="text"></td>
                            </tr>
                            <tr>
                                <th> 真实姓名：</th>
                                <td><input name="name" id="name" value="{{$data['name']}}" onkeyup="value=value.replace(/[^\D]/g,'');" autocomplete="off" maxlength="8" type="text"></td>
                            </tr>
                            <tr>
                                <th> 证件类型：</th>
                                <td>
                                    <select id="cardtype" name="cardtype">
                                        <option value="1"
                                        @if($data['cardtype']==1)
                                        selected
                                        @endif
                                        >身份证</option>
                                        <option value="2"
                                        @if($data['cardtype']==2)
                                        selected
                                        @endif
                                        >护照</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th> 证件号码：</th>
                                <td><input name="idcard" id="idcard" value="{{$data['idcard']}}" type="text"></td>
                            </tr>
                            <tr>
                                <th> 手机号码：</th>
                                <td><input name="phone" id="mo" value="{{$data['phone']}}" type="text"></td>
                            </tr>
                        </if>
                        <tr>
                            <th> 注册Ip：</th>
                            <td>{{$data['ip']}}
                                <span style="margin-left:20px;">时间： {{date('Y-m-d H:i:s',$data['reg_time'])}}</span>
                            </td>
                        </tr>
                        <tr>
                            <th>最后一次登录Ip：</th>
                            <td>
                                @if(isset($data['login_ip']))
                                {{$data['login_ip']}}
                                @else
                                未登录
                                @endif
                                <span style="margin-left:20px;">时间：
                                @if(isset($data['login_time']))
                                {{date('Y-m-d H:i:s',$data['login_time'])}}
                                @else

                                @endif
                                </span>
                            </td>
                        </tr>
                        <tr>
                        <tr>
                            <th>状态</th>
                            <td>
                                <input type="radio" value="0"
                                @if($data['status']==0)
                                checked
                                @endif
                                name="status" />正常(未填写个人信息)
                                <input type="radio" value="1"
                                @if($data['status']==1)
                                checked
                                @endif
                                name="status" />正常&nbsp;<input type="radio" value="2"
                                @if($data['status']==2)
                                checked
                                @endif
                                name="status" />禁用</td>
                        </tr>
                        <th></th>
                        <td>
                            <input type="submit" value="提交" class="btn btn-primary btn6 mr10">
                            <input type="button" value="返回"  class="btn btn6" onclick="history.go(-1)">
                        </td>
                        </tr>
                        </tbody></table>
                </div>
            </div>
        </form>
    </div>
</div>

<!--/main-->

@endsection
@section('footer')
    <script>
        $(".sub-menu").eq(3).show();
        $(".sub-menu").eq(3).children("li").eq(0).addClass("on");


        var oFile = document.getElementById('file');
        var oldImg = document.getElementById('oldImg');
        var oImg = document.getElementById('img');

        oFile.onchange = function(){

            var file = this.files[0];

            var reader = new FileReader();

            reader.readAsDataURL(file);   //将文件读取为DataUrl

            //读完了之后就能调取数据了
            reader.onload = function(){
                //console.log(reader.result);
                //在div中添加dom
                //在隐藏标签中添加value
                oldImg.src = reader.result;
                oImg.value = reader.result;
            }

        }


    </script>
@endsection