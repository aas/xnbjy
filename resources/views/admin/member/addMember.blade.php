@extends('admin.mainlayout')
@section('js')
    <script>
        window.onload = function(){

            /********快闪重定向*********/
            var message = "{{Session::get('message')}}";
            $(window).ready(function () {
                if (message) {
                    layer.msg(message, {
                        time: 3000,

                    });
                }
            });
            /********快闪重定向*********/

        }
    </script>
    <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
    <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
    <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>
    <script>
        $.validator.setDefaults({
            submitHandler: function(form) {
                //alert("提交事件!");
                form.submit();
            }
        });
        $().ready(function() {
// 在键盘按下并释放及提交后验证提交表单
            $("#myform").validate({
                rules: {

                    username: {
                        required: true,
                        minlength: 2
                    },
                    pwd: {
                        required: true,
                        minlength: 5
                    },
                    repwd: {
                        required: true,
                        minlength: 5,
                        equalTo: "#pwd"
                    },
                    pwdtrade: {
                        required: true,
                        minlength: 5
                    },
                    repwdtrade: {
                        required: true,
                        minlength: 5,
                        equalTo: "#pwdtrade"
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    pid: "required"
                },
                messages: {

                    username: {
                        required: "请输入用户名",
                        minlength: "用户名必需由两个字母组成"
                    },
                    pwd: {
                        required: "请输入密码",
                        minlength: "密码长度不能小于 5 个字母"
                    },
                    repwd: {
                        required: "请再次输入密码",
                        minlength: "密码长度不能小于 5 个字母",
                        equalTo: "两次密码不一致"
                    },
                    pwdtrade: {
                        required: "请输入交易密码",
                        minlength: "密码长度不能小于 5 个字母"
                    },
                    repwdtrade: {
                        required: "请再次输入交易密码",
                        minlength: "密码长度不能小于 5 个字母",
                        equalTo: "两次交易密码不一致"
                    },
                    email: "请输入一个正确的邮箱",
                    pid: "请填写邀请码",

                }
            });
        });
    </script>
    <style>
        .error{
            color:red;
        }
    </style>
@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="#">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">会员管理</span></div>
        </div>
        <div class="result-wrap">
            <form action="{{urlAdmin('Member','postAdd')}}" method="post" id="myform" name="myform" enctype="multipart/form-data" >
                {{csrf_field()}}
                <div class="config-items">
                    <div class="config-title">
                        <h1><i class="icon-font">&#xe00a;</i>会员用户添加</h1>
                    </div>
                    <div class="result-content">
                        <table width="100%" class="insert-tab">
                            <tbody>
                            <tr>
                                <th width="15%"><i class="require-red">*</i>电子邮件：</th>
                                <td><input name="email" id="email" type="text"><span id="showBug" style="margin-left:10px;color:#FF0000"></span></td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>登录密码：</th>
                                <td><input id="pwd" name="pwd" type="password"></td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>重复密码：</th>
                                <td><input id="repwd" name="repwd" type="password"></td>
                            </tr>
                            <tr>
                                <th><i class="require-red"></i>邀请码：</th>
                                <td><input id="pid" name="pid" type="text"></td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>交易密码：</th>
                                <td><input id="pwdtrade" name="pwdtrade" type="password"></td>
                            </tr>

                            <tr>
                                <th><i class="require-red">*</i>重复交易密码：</th>
                                <td><input id="repwdtrade" name="repwdtrade" type="password"></td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <input type="button" onclick="subform()" value="提交" class="btn btn-primary btn6 mr10">
                                    <input type="button" value="返回" onclick="history.go(-1)" class="btn btn6">
                                </td>
                            </tr>
                            </tbody></table>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('footer')
    <script>
        ckemail = 0;
        $('#email').blur(function(){
            var path_Email = "{{urlAdmin('Member','getCheckEmail')}}";
            $.ajax({
                type:"get",
                dataType:"json",
                data:{email:$('#email').val()},
                url:path_Email,
                success:function(d){
                    if(d['status']==0){
                        $('#showBug').html(d['msg']);
                        $('#email').focus();
                        return false;
                    }else{
                        $('#showBug').html(d['msg']);
                        return ckemail = 1 ;
                    }
                },
            });
        });
        function subform(){
            if(ckemail==1){
                $('#myform').submit();
            }else{
                $('#email').focus();
            }
        }
    </script>
    <script>
        $(".sub-menu").eq(3).show();
        $(".sub-menu").eq(3).children("li").eq(1).addClass("on");
    </script>
@endsection