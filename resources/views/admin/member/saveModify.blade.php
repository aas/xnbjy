@extends('admin.mainlayout')
@section('js')
    <script>
        window.onload = function(){

            /********快闪重定向*********/
            var message = "{{Session::get('message')}}";
            $(window).ready(function () {
                if (message) {
                    layer.msg(message, {
                        time: 2000,

                    });

                    setTimeout('location.href="{{urlAdmin('member','getIndex')}}"',2000);
                }

            });
            /********快闪重定向*********/

        }
    </script>
@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="{{urlAdmin('public','getIndex')}}">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">会员管理</span></div>
        </div>
        <div class="result-wrap">
            <form action="{{urlAdmin('Member','postSaveModify')}}" method="post" id="myform" name="myform" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="config-items">
                    <div class="config-title">
                        <h1><i class="icon-font">&#xe00a;</i>会员用户添加</h1>
                    </div>
                    <div class="result-content">
                        <table width="100%" class="insert-tab">
                            <tbody>
                            <input type="hidden" name="member_id" value="{{$data['member_id']}}"/>
                            <tr>
                                <th width="15%"><i class="require-red">*</i>电子邮件：</th>
                                <td>{{$data['email']}}</td>
                            </tr>
                            <!-- 修改时显示下方 -->
                            <tr>
                                <th><i class="require-red">*</i>昵称：</th>
                                <td><input name="nick" id="nick" value="" type="text">
                                    <span id="showBug" style=" margin-left:10px;color:#FF0000"></span></td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>真实姓名：</th>
                                <td><input name="name" id="name" value="" onkeyup="value=value.replace(/[^\D]/g,'');" autocomplete="off" maxlength="8" type="text"></td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>证件类型：</th>
                                <td>
                                    <select id="cardtype" name="cardtype">
                                        <option value="1" selected="selected">身份证</option>
                                        <!--  <option value="2">护照</option>-->
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>证件号码：</th>
                                <td><input name="idcard" id="idcard" value="" type="text"></td>
                            </tr>
                            <tr>
                                <th><i class="require-red">*</i>手机号码：</th>
                                <td><input name="phone" id="phone" value="" type="text">
                                    <span id="moshowBug" style=" margin-left:10px;color:#FF0000"></span></td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <input type="button" onclick="subform()" value="提交" class="btn btn-primary btn6 mr10">
                                    <a href="{{urlAdmin('Member','getIndex')}}"><input type="button" value="返回"  class="btn btn6"></a>
                                </td>
                            </tr>
                            </tbody></table>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('footer')
    <script>
        cknick = 0;
        ckphone = 0;
        $('#nick').blur(function(){
            var path = "{{urlAdmin('Member','getCheckNick')}}";
            $.ajax({
                type:"get",
                dataType:"json",
                data:{nick:$('#nick').val()},
                url:path,
                success:function(d){
                    if(d['status']==0){
                        $('#showBug').html(d['msg']);
                        $('#nick').focus();
                        return false;
                    }else{
                        $('#showBug').html(d['msg']);
                        return cknick = 1 ;
                    }
                },
            });
        });
        $('#phone').blur(function(){
            var path = "{{urlAdmin('Member','getCheckPhone')}}";
            $.ajax({
                type:"get",
                dataType:"json",
                data:{phone:$('#phone').val()},
                url:path,
                success:function(d){
                    if(d['status']==0){
                        $('#moshowBug').html(d['msg']);
                        $('#phone').focus();
                        return false;
                    }else{
                        $('#moshowBug').html(d['msg']);
                        return ckphone = 1 ;
                    }
                },
            });
        });

        function subform(){
            if(cknick==1 && ckphone==1){
                $('#myform').submit();
            }else{
                alert('请填写完整信息');
            }
        }
    </script>
@endsection