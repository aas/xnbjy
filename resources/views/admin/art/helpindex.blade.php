@extends('admin.mainlayout')
@section('js')
<script>
    window.onload = function(){

    /********快闪重定向*********/
    var message = "{{Session::get('message')}}";
    var status = "{{Session::get('status')}}";
    $(window).ready(function () {
    if (message) {
    layer.msg(message, {
    time: 3000,

    });
    }
    if(status==1){

    location.href="{{url('center/art/help?article_category_id=6')}}";

    }

    });
    /********快闪重定向*********/

    }
</script>
@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">文章管理</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
                <!--article_category_id传递文章类型id-->
                <form action="{{urlAdmin('Art','getHelpIndex')}}" method="get" id="myform">
                    <table class="search-tab">
                        <tr>
                            <th width="120">选择分类:</th>
                            <td>
                                <select name="news_id" id="news_id">
                                        <option value="">全部</option>
                                        @foreach($type as $v)
                                        <option value="{{$v['id']}}">
                                            {{$v['name']}}
                                        </option>
                                        @endforeach
                                </select>
                            </td>
                            <input type="hidden" id="article_category_id" name="article_category_id" value="6">
                            <th width="70">标题:</th>
                            <td><input class="common-text" placeholder="关键字" name="keywords" value="" id="news_title" type="text"></td>
                            <td><input class="btn btn-primary btn2" name="sub" value="查询" type="submit"></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <div class="result-wrap">

            <div class="result-title">
                <div class="result-list">
                    <!--article_category_id传递文章类型id-->
                    <a href="{{urlAdmin('Art','getHelpAdd',array('art_category_id'=>6))}}"><i class="icon-font"></i>新增文章</a>
                    <a href="{{urlAdmin('Arttype','getAdd')}}"><i class="icon-font"></i>新增帮助类别</a>
                </div>
            </div>
            <div class="result-content">
                <table class="result-tab" width="100%">
                    <tr>
                        <th>ID</th>
                        <th>标题</th>
                        <th>分类</th>
                        <th>内容</th>
                        <th>标红</th>
                        <th>更新时间</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                        @foreach($data['data'] as $v)
                        <tr>
                            <td>{{$v['article_id']}}</td>
                            <td>{{$v['title']}}</td>
                            <td>{{$v['article_category']['name']}}</td>
                            <td>{{mb_substr((strip_tags(html_entity_decode($v['content']))),0,30,'utf-8')}}</td>
                            <td>
                                @if($v['sign']==1)
                                是
                                @else
                                否
                                @endif
                            </td>
                            <td>{{date('Y-m-d H:i:s',isset($v['add_time'])?$v['add_time']:'')}}</td>
                            <td>{{$v['status']}}</td>
                            <td>
                                <a class="link-update" href="{{urlAdmin('Art','getHelpEdit',array('article_id'=>$v['article_id']))}}">修改</a>
                                &nbsp;&nbsp;
                                <a class="link-del" href="{{urlAdmin('Art','getHelpDel',array('article_id'=>$v['article_id']))}}" onclick="return confirm('确认删除吗？')">删除</a>
                            </td>
                        </tr>
                        @endforeach
                </table>
                <div class="list-page">
                    <ul>
                        @if($data['last_page']>1)
                        <div>
                            <a class="prev" href="{{$data['prev_page_url']}}&article_category_id=6"><<</a>
                            @foreach($data['pageNoList'] as $v)
                                <a @if($v == $data['current_page'])
                                   class="current"
                                   @else
                                   class="num"
                                   @endif
                                   href="{{urlAdmin('Art','getHelpIndex')}}?page={{$v}}&article_category_id=6">{{$v}}</a>
                            @endforeach
                            <a class="next" href="{{$data['next_page_url']}}&article_category_id=6">>></a>
                        </div>
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('footer')
    <script>
        $(".sub-menu").eq(9).show();
        $("#myform").ready(function(e) {
            var num = parseInt($("#article_category_id").val());
            switch(6){
                case 1:
                    $(".sub-menu").eq(9).children("li").eq(0).addClass("on");
                    break;
                case 2:
                    $(".sub-menu").eq(9).children("li").eq(1).addClass("on");
                    break;
                case 6:
                    $(".sub-menu").eq(9).children("li").eq(2).addClass("on");
                    break;
                case 7:
                    $(".sub-menu").eq(9).children("li").eq(3).addClass("on");
                    break;
            }
        });
    </script>
@endsection