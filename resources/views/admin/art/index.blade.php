@extends('admin.mainlayout')
@section('js')

@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="#">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">文章管理</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
                <!--article_category_id传递文章类型id-->
                <form action="#" method="get" id="myform">
                    <table class="search-tab">
                        <tr>
                            <th width="120">选择分类:</th>
                            <td>
                                {{$type[0]['name']}}
                            </td>
                            <input type="hidden" id="article_category_id" name="article_category_id" value="{{$type[0]['id']}}">
                            <th width="70">标题:</th>
                            <td><input class="common-text" placeholder="关键字" name="title" value="" id="news_title" type="text"></td>
                            <td><input class="btn btn-primary btn2" name="sub" value="查询" type="submit"></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <div class="result-wrap">

            <div class="result-title">
                <div class="result-list">
                    <!--article_category_id传递文章类型id-->
                    <a href="{{urlAdmin('Art','getAdd',array('art_category_id'=>$type[0]['id']))}}"><i class="icon-font"></i>新增文章</a>
                </div>
            </div>
            <div class="result-content">
                <table class="result-tab" width="100%">
                    <tr>
                        <th>ID</th>
                        <th>标题</th>
                        <th>分类</th>
                        <th>内容</th>
                        <th>标红</th>
                        <th>更新时间</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    @if(!empty($data['data']))
                    @foreach($data['data'] as $v)
                        @if(!empty($v['article']))
                        @foreach($v['article'] as $v2)
                            <tr>
                                <td>{{$v2['article_id']}}</td>
                                <td>{{mb_substr((strip_tags(html_entity_decode($v2['title']))),0,14,'utf-8')}}</td>
                                <td>{{$v['name']}}</td>
                                <td>{{mb_substr((strip_tags(html_entity_decode($v2['content']))),0,30,'utf-8')}}</td>
                                <td>
                                    @if($v2['sign']==1)
                                        是
                                        <else/>
                                    @else
                                        否
                                    @endif
                                </td>
                                <td>{{date('Y-m-d H:i:s',strtotime($v2['add_time']))}}</td>
                                <td>{{$v2['status']}}</td>
                                <td>
                                    <a class="link-update" href="{{urlAdmin('Art','getEdit',array('article_id'=>$v2['article_id'],'art_category_id'=>$type[0]['id']))}}">修改</a>
                                    &nbsp;&nbsp;
                                    <a class="link-del" href="#">删除</a>
                                </td>
                            </tr>
                        @endforeach
                        @endif
                    @endforeach
                    @endif
                </table>
                <div class="list-page">
                    <ul>
                        <div>
                            <a class="prev" href="{{$data['prev_page_url']}}&art_category_id={{$_GET['art_category_id']}}"><<</a>
                            @foreach($data['pageNoList'] as $v)
                                <a @if($v == $data['current_page'])
                                   class="current"
                                   @else
                                   class="num"
                                   @endif
                                   href="{{urlAdmin('Art','getIndex')}}?art_category_id={{$_GET['art_category_id']}}&page={{$v}}">{{$v}}</a>
                            @endforeach
                            <a class="next" href="{{$data['next_page_url']}}&art_category_id={{$_GET['art_category_id']}}">>></a>
                        </div>
                    </ul></div>
            </div>

        </div>
    </div>

@endsection
@section('footer')
    <script>
        $(".sub-menu").eq(9).show();
        $("#myform").ready(function(e) {
            var num = parseInt($("#article_category_id").val());
            switch(num){
                case 1:
                    $(".sub-menu").eq(9).children("li").eq(0).addClass("on");
                    break;
                case 2:
                    $(".sub-menu").eq(9).children("li").eq(1).addClass("on");
                    break;
                case 6:
                    $(".sub-menu").eq(9).children("li").eq(2).addClass("on");
                    break;
                case 7:
                    $(".sub-menu").eq(9).children("li").eq(3).addClass("on");
                    break;
            }
        });
    </script>
@endsection