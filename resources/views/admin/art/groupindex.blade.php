@extends('admin.mainlayout')
@section('js')
    <script>
        window.onload = function(){

            /********快闪重定向*********/
            var message = "{{Session::get('message')}}";
            $(window).ready(function () {
                if (message) {
                    layer.msg(message, {
                        time: 3000,

                    });
                }
            });
            /********快闪重定向*********/

        }
    </script>
@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="#">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">文章管理</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
                <!--article_category_id传递文章类型id-->
                <form action="{{urlAdmin('Art','getGroupIndex')}}" method="get" id="myform">
                    <table class="search-tab">
                        <tr>
                            <th width="120">选择分类:</th>
                            <td>
                                团队信息
                            </td>
                            <input type="hidden" id="article_category_id" name="article_category_id" value="7">
                            <th width="70">标题:</th>
                            <td><input class="common-text" placeholder="关键字" name="keywords" value="" id="news_title" type="text"></td>
                            <td><input class="btn btn-primary btn2" name="sub" value="查询" type="submit"></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
        <div class="result-wrap">

            <div class="result-title">
                <div class="result-list">
                    <!--article_category_id传递文章类型id-->
                    <a href="{{urlAdmin('Art','getNotesAdd',array('article_category_id'=>$_GET['article_category_id']))}}"><i class="icon-font"></i>新增文章</a>
                </div>
            </div>
            <div class="result-content">
                <table class="result-tab" width="100%">
                    <tr>
                        <th>ID</th>
                        <th>标题</th>
                        <th>分类</th>
                        <th>内容</th>
                        <th>标红</th>
                        <th>更新时间</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    @if(!empty($data['data']))
                        @foreach($data['data'] as $v)
                                    <tr>
                                        <td>{{$v['article_id']}}</td>
                                        <td>{{mb_substr((strip_tags(html_entity_decode($v['title']))),0,14,'utf-8')}}</td>
                                        <td>{{$v['article_category']['name']}}</td>
                                        <td>{{mb_substr((strip_tags(html_entity_decode($v['content']))),0,30,'utf-8')}}</td>
                                        <td>
                                            @if($v['sign']==1)
                                                是
                                                <else/>
                                            @else
                                                否
                                            @endif
                                        </td>
                                        <td>{{date('Y-m-d H:i:s',!empty($v['add_time'])?$v['add_time']:'1506393221')}}</td>
                                        <td>{{$v['status']}}</td>
                                        <td>
                                            <a class="link-update" href="{{urlAdmin('Art','getNotesEdit',array('article_id'=>$v['article_id'],'article_category_id'=>$_GET['article_category_id']))}}">修改</a>
                                            &nbsp;&nbsp;
                                            <a class="link-del" style="cursor: pointer;" onclick="del({{$v['article_id']}});">删除</a>
                                        </td>
                                    </tr>
                        @endforeach
                        @else
                                <tr>
                                    <td colspan="8" align="center">查无此数据...</td>
                                </tr>
                    @endif
                </table>
                <div class="list-page">
                    <ul>
                        @if($data['last_page']>1)
                        <div>
                            <a class="prev" href="{{$data['prev_page_url']}}&article_category_id={{$_GET['article_category_id']}}"><<</a>
                            @foreach($data['pageNoList'] as $v)
                                <a @if($v == $data['current_page'])
                                   class="current"
                                   @else
                                   class="num"
                                   @endif
                                   href="{{urlAdmin('Art','getGroupIndex')}}?page={{$v}}&article_category_id={{$_GET['article_category_id']}}">{{$v}}</a>
                            @endforeach
                            <a class="next" href="{{$data['next_page_url']}}&article_category_id={{$_GET['article_category_id']}}">>></a>
                        </div>
                        @endif
                    </ul>
                </div>
            </div>

        </div>
    </div>

@endsection
@section('footer')
    <script>
        $(".sub-menu").eq(9).show();
        $("#myform").ready(function(e) {
            var num = parseInt($("#article_category_id").val());
            console.log(num);
            switch(num){
                case 1:
                    $(".sub-menu").eq(9).children("li").eq(0).addClass("on");
                    break;
                case 2:
                    $(".sub-menu").eq(9).children("li").eq(1).addClass("on");
                    break;
                case 6:
                    $(".sub-menu").eq(9).children("li").eq(2).addClass("on");
                    break;
                case 7:
                    $(".sub-menu").eq(9).children("li").eq(3).addClass("on");
                    break;
            }
        });


        /********所有ajax加载效果**********/
        $(document).ajaxStart(function(){

            //加载效果
            layer.load(2, {shade: false});

        }).ajaxStop(function(){

            //关闭加载效果
            layer.closeAll("loading");
        });
        /********所有ajax加载效果**********/

        function del(id){

            layer.confirm('确定要删除该项吗?',{btn:['确定','取消']},function(){

                layer.closeAll();
                $.ajax({
                    type:'post',
                    url:"{{urlAdmin('Art','postNotesDel')}}",
                    data:{'article_id':id,'_token':'{{csrf_token()}}'},
                    dataType:'json',
                    success: function (res) {

                        // alert(res);
                        if(res['status']==1){

                            layer.msg(res.msg,{icon:6});
                            setTimeout("location.reload()",1500);

                        }else{

                            layer.msg(res.msg,{icon:5});

                        }

                    }

                });

            },function(){

                layer.msg('已取消!');

            });
        }

    </script>
@endsection