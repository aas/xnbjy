@extends('admin.mainlayout')
@section('js')
    <script type="text/javascript" charset="utf-8" src="/kindeditor/kindeditor.js"></script>
    <script type="text/javascript" charset="utf-8" src="/kindeditor/lang/zh_CN.js"></script>
    <script>
        function checkForm(){
            editor.sync();
            var name = document.getElementById('title').value;

            if(name != ""){
                document.getElementById('myform').submit();
            }else{
                alert('请填写标题');
            }
        }

        window.onload = function(){

            /********快闪重定向*********/
            var message = "{{Session::get('message')}}";
            var status = "{{Session::get('status')}}";
            $(window).ready(function () {
                if (message) {
                    layer.msg(message, {
                        time: 3000,

                    });
                }
                if(status==1){

                    location.href="{{url('center/art/help?article_category_id=6')}}";

                }

            });
            /********快闪重定向*********/

        }

    </script>
@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="">首页</a><span class="crumb-step">&gt;</span><a class="crumb-name" href="javascript:void(0)" onclick="history.go(-1)">文章管理</a><span class="crumb-step">&gt;</span><span>修改文章</span></div>
        </div>
        <div class="result-wrap">
            <div class="result-content">
                <form action="{{urlAdmin('Art','postHelpEdit')}}" method="post" id="myform" name="myform" enctype="multipart/form-data" >
                    {{csrf_field()}}
                    <table class="insert-tab" width="100%">
                        <tbody>
                        <tr>
                            <th width="120"><i class="require-red">*</i>分类：</th>
                            <td>
                                <select name="type_id" id="news_id">
                                    <option value="6">全部</option>
                                    @foreach($type as $v)
                                        <option value="{{$v['id']}}"
                                        @if($v['id']==$data['position_id'])
                                            selected
                                        @endif
                                        >
                                            {{$v['name']}}
                                        </option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        <input type="hidden" id="article_category_id" name="article_category_id" value="">
                        <tr>
                            <th><i class="require-red">*</i>标题：</th>
                            <td><input class="common-text required" id="title" name="title" size="50" value="{{$data['title']}}" type="text"></td>
                        </tr>
                        <!--                          <tr>
                                            <th>状态：</th>
                                            <td>
                                            <select name="status" id="status" class="required">
                                                <option value="1" selected="selected">显示</option>
                                                <option value="0">不显示</option>
                                            </select>
                                            </td>
                                        </tr>-->
                        <tr>
                            <th>是否标红</th>
                            <td>
                                <label><input type="radio" name="sign" value="1"
                                    @if((isset($data['sign'])&&$data['sign']==1))
                                    checked
                                    @endif
                                    /> 是</label>
                                <label><input type="radio" name="sign" value="0"
                                    @if(isset($data['sign'])&&$data['sign']==0)
                                    checked
                                    @endif
                                    /> 否</label>

                            </td>
                        </tr>

                        <tr>
                            <th>内容：</th>
                            <input type="hidden" value="{{$data['article_id']}}" name="article_id">
                            <td><textarea id="content" name="content" cols="30" style="width:800px;height:300px;" rows="10">
                                    {{$data['content']}}
                                </textarea>
                                <script>
                                    KindEditor.ready(function(K) {
                                        window.editor = K.create('#content');
                                    });

                                </script>
                            </td>
                        </tr>
                        <tr>
                            <th></th>
                            <!--确定文章的类型-->

                            <!--确定后台成功返回的url-->

                            <td><input class="btn btn-primary btn6 mr10" value="提交"  type="button" onclick="checkForm()" >
                            <input class="btn btn6" onclick="history.go(-1)" value="返回" type="button"></td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>

@endsection
@section('footer')
    <script>
        $(".sub-menu").eq(9).show();
        $("#myform").ready(function(e) {
            var num = parseInt($("#article_category_id").val());
            switch(7){
                case 1:
                    $(".sub-menu").eq(9).children("li").eq(0).addClass("on");
                    break;
                case 2:
                    $(".sub-menu").eq(9).children("li").eq(3).addClass("on");
                    break;
                case 7:
                    $(".sub-menu").eq(9).children("li").eq(2).addClass("on");
                    break;
                default:
                    $(".sub-menu").eq(9).children("li").eq(1).addClass("on");
            }
        });
    </script>
@endsection