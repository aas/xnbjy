@extends('admin.mainlayout')
@section('js')
    <script>
        window.onload = function(){

            /********快闪重定向*********/
            var message = "{{Session::get('message')}}";
            $(window).ready(function () {
                if (message) {
                    layer.msg(message, {
                        time: 2000,

                    });
                }
            });
            /********快闪重定向*********/

        }
    </script>
@endsection
@section('content')
    <div class="main-wrap">

        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="/jscss/admin">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">消息管理</span></div>
        </div>
        <div class="search-wrap">
            <div class="search-content">
                <form action="{{urlAdmin('Message','getIndex')}}" method="get">
                    <table class="search-tab">
                        <tr>
                            <th width="70">消息标题:</th>
                            <td><input class="common-text" placeholder="关键字" name="keywords" type="text"></td>
                            <td><input class="btn btn-primary btn2"  value="查询" type="submit"></td>
                        </tr>
                    </table>
                </form>

            </div>
        </div>
        <div class="result-wrap">

            <div class="result-title">
                <div class="result-list">
                    <a href="{{urlAdmin('message','getAdd')}}"><i class="icon-font"></i>新增消息</a>
                </div>
            </div>
            <div class="result-content">
                <table class="result-tab" width="100%">
                    <tr>
                        <th>ID</th>
                        <th>消息类型</th>
                        <th>消息标题</th>
                        <th>消息内容</th>
                        <th>添加时间</th>
                        <th>发送人员</th>
                        <th>操作</th>
                    </tr>
                        @if(!empty($data['data']))
                        @foreach($data['data'] as $v)
                        <tr>
                            <td>{{$v['id']}}</td>
                            <td>
                                @if($v['u_id']==-1)
                                <span style="color:#FF0000">【{{$v['name']}}】</span>
                                @else
                                【{{$v['name']}}】
                                @endif
                            </td>
                            <td>{{$v['title']}}</td>    {{--   mb_substr=0,9    --}}
                            <td>{!! $v['content'] !!}</td>  {{--   mb_substr=0,20    --}}
                            <td>{{$v['add_time']}}</td>
                            <td>
                                @if($v['u_id']==-1)
                                    群发
                                  @else
                                    发送至个人ID为({{$v['u_id']}})
                                @endif
                            </td>
                            <td>
                                <a class="link-update" href="{{urlAdmin('Message','getEdit',array('id'=>$v['id']))}}">查看</a>
                                <a class="link-del" href="#" onclick="del({{$v['id']}})">删除</a>
                            </td>
                        </tr>
                        @endforeach
                        @else
                            <tr>
                                <td colspan="7" align="center">暂无数据...</td>
                            </tr>
                        @endif


                </table>
                <div class="list-page">
                    <ul>
                        @if($data['last_page']>1)
                        <div>
                            @if(!empty($data['prev_page_url']))
                                <a class="prev" href="{{$data['prev_page_url']}}"><<</a>
                            @endif
                            @foreach($data['pageNoList'] as $v)
                                <a @if($v == $data['current_page'])
                                   class="current"
                                   @else
                                   class="num"
                                   @endif
                                   href="{{urlAdmin('Message','getIndex')}}?&page={{$v}}">{{$v}}</a>
                            @endforeach
                            @if(!empty($data['next_page_url']))
                                <a class="next" href="{{$data['next_page_url']}}">>></a>
                            @endif
                        </div>
                        @endif
                    </ul>
                </div>
            </div>

        </div>
    </div>
    <!--/main-->

@endsection
@section('footer')
    <script>
        $(".sub-menu").eq(3).show();
        $(".sub-menu").eq(3).children("li").eq(2).addClass("on");
    </script>
    <script>
        /********所有ajax加载效果**********/
        $(document).ajaxStart(function(){

            //加载效果
            layer.load(2, {shade: false});

        }).ajaxStop(function(){

            //关闭加载效果
            layer.closeAll("loading");
        });
        /********所有ajax加载效果**********/
        function del(id){

            layer.confirm('确定要删除该项吗?',{btn:['确定','取消']},function(){

                layer.closeAll();
                $.ajax({
                    type:'get',
                    url:"{{urlAdmin('Message','getDel')}}",
                    data:{'id':id},
                    dataType:'json',
                    success: function (res) {

                        // alert(res);
                        if(res['status']==1){

                            layer.msg(res.msg,{icon:6});
                            setTimeout("location.reload()",1500);

                        }else{

                            layer.msg(res.msg,{icon:5});

                        }

                    }

                });

            },function(){

                layer.msg('已取消!');

            });
        }
    </script>
@endsection