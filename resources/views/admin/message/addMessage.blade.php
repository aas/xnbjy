@extends('admin.mainlayout')
@section('js')
    <script type="text/javascript" src="/js/jquery-1.8.2.js"></script>
    <script type="text/javascript" src="/js/jquery-2.1.1.min.js"></script>
    <script src="/js/bootstrap.min.js?v=3.4.0"></script>
    <script type="text/javascript" src="/js/layer/layer.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/messages_zh.min.js"></script>
    <script src="/js/base.js"></script>
    <script type="text/javascript" charset="utf-8" src="/kindeditor/kindeditor.js"></script>
    <script type="text/javascript" charset="utf-8" src="/kindeditor/lang/zh_CN.js"></script>
    <script type="text/javascript" charset="utf-8" src="/kindeditor/plugins/prettify.js"></script>
    <script>
        $(function(){
            if($("#all").change()){
                $('#show').hide();
            }

            $("#one").change(function(){
                $('#show').show();
            });
            $("#all").change(function(){
                $('#show').hide();
            });
        });
    </script>
    <script>
        window.onload = function(){

            /********快闪重定向*********/
            var message = "{{Session::get('message')}}";
            $(window).ready(function () {
                if (message) {
                    layer.msg(message, {
                        time: 3000,

                    });
                }
            });
            /********快闪重定向*********/

        }
    </script>
@endsection
@section('content')
    <div class="main-wrap">
        <div class="crumb-wrap">
            <div class="crumb-list"><i class="icon-font"></i><a href="/think/admin">首页</a><span class="crumb-step">&gt;</span><span class="crumb-name">会员管理</span></div>
        </div>
        <div class="result-wrap">
            <form action="#" method="post" id="subform" jump-url="{:U('Message/index')}" >
                {{csrf_field()}}
                <div class="config-items">
                    <div class="config-title">
                        <h1><i class="icon-font">&#xe00a;</i>消息添加</h1>
                    </div>
                    <div class="result-content">
                        <table width="100%" class="insert-tab">
                            <tbody>

                            <tr>
                                <th width="15%"><i class="require-red">*</i>类型</th>
                                <td>
                                    <select name="type">
                                            @foreach($type['data'] as $row)
                                            <option value="{{$row['id']}}" >{{$row['name']}}</option>
                                            @endforeach
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th width="15%"><i class="require-red">*</i>标题</th>
                                <td>
                                    <input  style=" width:300px;" name="title" id="title" value="" type="text">
                                    <span id="showBug" style=" margin-left:10px;color:#FF0000"></span>
                                </td>
                            </tr>

                            <tr>
                                <th width="15%"><i class="require-red">*</i>内容</th>
                                <td>

                                    <textarea id="content" name="content" cols="30" style="width:800px;height:300px;" rows="10">
                                    </textarea>
                                    <script>
                                        KindEditor.ready(function(K) {
                                            window.editor = K.create('#content');
                                        });

                                    </script>
                                </td>
                            </tr>
                            <tr>
                                <th width="15%"><i class="require-red">*</i>发送人员</th>
                                <td>

                                        <input type="radio"  name="radios" value="all" id="all" checked="checked" />群发
                                        <input type="radio"  name="radios" value="one" id="one" />个人
                                </td>
                            </tr>
                            <tr id="show" style="display:none">
                                <th width="15%"><i class="require-red">*</i>请输入个人ID</th>
                                <td>
                                    <input type="text"  name="u_id" value="" />
                                </td>
                            </tr>

                                <tr>
                                    <th width="15%"><i class="require-red">*</i>添加时间</th>
                                    <td><input style=" width:200px;" name="add_time" readonly="readonly" value=""  /></td>
                                </tr>

                            <tr>
                                <th></th>

                                <td>

                                        <input type="submit"  value="提交" class="btn btn-primary btn6 mr10">
                                    <input type="button" value="返回"  class="btn btn6" onclick="history.go(-1)">
                                </td>
                            </tr>
                            </tbody></table>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('footer')
    <script>

        $("#subform").validate({
            rules: {
                title: {
                    required: true,
                },
                content: {
                    required: true,
                }
            },
            messages: {
                title: {
                    required: "标题不能为空",
                },
                content: {
                    required: "内容不能为空",
                }
            },
            submitHandler:function(form){
                editor.sync();
                form.submit();
                return false;
            },
            invalidHandler: function() {  //不通过回调
                return false;
            },
        });

    </script>
    <script>
        $(".sub-menu").eq(3).show();
        $(".sub-menu").eq(3).children("li").eq(2).addClass("on");
    </script>
@endsection