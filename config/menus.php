<?php
return [
    'center/config/indexAdd', //保存系统设置
    'center/config/information_add', //保存信息设置
    'center/config/finance_edit', //财务设置模块
    'center/config/customer_edit', //客服设置
    'center/config/short_edit' ,//短信邮箱设置
    'center/config/short' ,//短信邮箱设置
    'center/fileconfig/saveEntrance_edit' ,//后台入口设置管理
    'center/fileconfig/saveDb_edit' ,//数据库配置管理
    'center/website_bank/save' ,//银行列表修改
    'center/website_bank/del' ,//银行列表删除
    'center/flash/add' ,//幻灯添加
    'center/flash/addList' ,//幻灯添加
    'center/flash/edit' ,//幻灯修改
    'center/flash/editList' ,//幻灯修改
    'center/flash/del' ,//幻灯删除
    'center/link/add' ,//友情链接添加
    'center/link/addList' ,
    'center/link/edit' ,//友情链接修改
    'center/link/editList' ,
    'center/link/del' ,//友情链接删除
    'center/member/check_email' ,//会员列表
    'center/member/show_my_invite' ,//查看邀请人员
//    'center/member/show' ,//查看财务日志
    'center/member/save' ,//会员修改
    'center/member/del' ,//删除
    'center/member/show' ,//查看会员账户
    'center/message/edit' ,//查看系统消息
    'center/message/del' ,//删除系统消息
    'center/dividend/indexEdit' ,//分红股管理
    'center/zhongchou/add' ,//众筹添加
    'center/zhongchou/jiedongbyid' ,//众筹解冻
    'center/pending/success_by' ,//提现审核--通过
    'center/pending/error_by' ,//提现审核--不通过
    'center/pending/payExcel' ,//提现审核表格导出
    'center/pay/payExcel' ,//人工充值管理表格导出
    'center/finance/payExcel' ,//财务日志表格导出
    'center/pay/admAdd', //管理员充值
//    'center/currency/add',//币种添加
    'center/currency/addList',
    'center/trade/trade',//查看交易记录
    'center/currency/savePrice',//设置交易限额
    'center/trade/order',//查看委托交易记录
    'center/currency/set_member',//向会员钱包转账
    'center/currency/set_member_qianbao',
    'center/currency/edit', //币种修改
    'center/currency/editList',
    'center/currency/del', //币种删除
    'center/download/biaogeDownload', //新币申请
    'center/download/qianbaoDownload', //钱包上传
    'center/download/guanwangUrl', //钱包地址
    'center/manage/auth_admin', //管理员管理
    'center/art/notes/edit', //文章管理修改
    'center/art/notes/add', //文章管理添加
    'center/art/notes/del', //文章管理删除
    'center/art/notes/add', //市场动态管理添加
    'center/art/notes/edit', //市场动态管理修改
    'center/art/notes/del', //市场动态管理删除
    'center/art/help/add', //帮助中心管理添加
    'center/art/help/edit', //帮助中心管理修改
    'center/art/help/del', //帮助中心管理删除
    'center/arttype/add', //新增帮助类别
    'center/arttype/edit', //新增帮助类别修改
    'center/arttype/del', //新增帮助类别删除
    'center/art/notes', //团队信息管理
    'center/index/cache', //去除缓存





];