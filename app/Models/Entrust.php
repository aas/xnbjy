<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Entrust extends BaseModel
{

    protected $table = "entrust";

    public $timestamps = false;

    public function getOne($where, $fileds = '*', $order = '')
    {
        if (!$order) {
            $order = ['bank_id' => 'desc'];
        }
        return $this->multiSelect($fileds)->multiWhere($where)->multiOrder($order)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }


    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getOrderCurrencyMemberList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->with('currency')->with('member')->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->with('currency')->with('member')->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    public function currency()
    {
        return $this->belongsTo('App\Models\Currency', 'currency_id', 'currency_id');
    }

    public function member()
    {
        return $this->belongsTo('App\Models\Member', 'member_id', 'member_id');
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }


    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }

    public function count($where=[], $field='')
    {
        return $this->multiWhere($where)->count($field);
    }

    public function sum($where=[], $field)
    {
        return $this->multiWhere($where)->sum($field);
    }

    public function max($where=[], $field)
    {
        return $this->multiWhere($where)->max($field);
    }

    public function min($where=[], $field)
    {
        return $this->multiWhere($where)->min($field);
    }



    public function manage($request){
        $content = $this->userStatus();//判断是否需要进行信息补全
        //获取主币种
        $currency = (new Trade())->getCurrencyByCurrencyId();
        $currencytype = $request['currency'];
        $status = $request['status'];
        $state = explode(',',$status);
        if (!empty($currencytype)) {
            $where['currency_id'] = $currencytype;
        }
        $where['member_id'] = session('USER_KEY_ID');

            if(count($state) < 2){
                $where['status'] = $status;
            }else{
                $where['status'] = array('in',[0,1]);
            }

        $all = (new Orders())->entrust($where);
        return [$content,$currency,$all];

    }

    //委托历史
    public function history($request){
        $content = $this->userStatus();//判断是否需要进行信息补全
        //获取主币种
        $currency = (new Trade())->getCurrencyByCurrencyId();
        $currencytype = $request['currency'];
        $status = $request['status'];
        $state = explode(',',$status);

        if(!empty($currencytype)){
            $where['currency_id'] = $currencytype;
        }
        $where['member_id'] = session('USER_KEY_ID');

        if(count($state) < 2){
            $where['status'] = $status;
        }else{
            $where['status'] = array('in',[-1,2]);
        }
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list = (new Orders())->entrust($where);
        return [$content,$currency,$list];
    }

    /**
     *  撤销方法
     */
    public function cancel($request){
        $order_id = $request['order_id'];
        if (empty($order_id)) {
            $info['status'] = 0;
            $info['info'] = '撤销订单不正确!';
            return $info;
        }
        //获取人的一个订单
        $one_order = (new Orders())->getOneOrdersByMemberAndOrderId(session('USER_KEY_ID'), $order_id,array(0,1));
        if(empty($one_order)){
            $info['status'] = -1;
            $info['info'] = '传入信息错误';
            return $info;
        }
        $info = 	$this ->cancelOrdersByOrderId($one_order);
        return $info;

    }


    /**
     * 检测是否需要进行信息填写(补全)
     */
    public function  userStatus(){
        header("Content-type:text/html;charset=utf-8");
        $list = (new Member())->getOne(['member_id'=>session('USER_KEY_ID')]);
        return $list;
    }


    /**
     *撤销订单
     * @param   int $list  订货单信息
     * @param   int $member_id  用户id
     * @param   int $order_id  订单号 id
     */
    protected  function cancelOrdersByOrderId($one_order){
        $this->start();
        try {
            $order = new Orders();
            $r[] = $order->setOrdersStatusByOrdersId(-1, $one_order['orders_id']);
            //返还资金
            switch ($one_order['type']) {
                case 'buy':
                    $money = ($one_order['num'] - $one_order['trade_num']) * $one_order['price'];
                    $r[] = $order->setUserMoney($one_order['member_id'], $one_order['currency_trade_id'], $money, 'inc', 'num');
                    $r[] = $order->setUserMoney($one_order['member_id'], $one_order['currency_trade_id'], $money, 'dec', 'forzen_num');

                    break;
                case 'sell':
                    $num = $one_order['num'] - $one_order['trade_num'];
                    $r[] = $order->setUserMoney($one_order['member_id'], $one_order['currency_id'], $num, 'inc', 'num');
                    $r[] = $order->setUserMoney($one_order['member_id'], $one_order['currency_id'], $num, 'dec', 'forzen_num');
                    break;
            }

            $this->commit();
            $info['status'] =1;
            $info['info'] = '撤销成功!';
            return $info;

        }catch (\Exception $e){

            $this->rollback();
            $info['status'] = -1;
            $info['info'] = '撤销失败!';
            return $info;
            }

    }

}
