<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Help extends BaseModel
{
    /**
     * 帮助中心首页
     */
    public function index($request){

        //$id为header传来的文章id，
        $id = $request['position_id'];
        //$article_id为帮助中心内传来的文章id
        $article_id = $request['article_id'];
        if($id) {
            $where['position_id'] = $id;
        }
        if($article_id) {
            $where['article_id'] = $article_id;
        }

        //查找到单一的文章
        $art_one = Art::where($where)->first();
        //将数据库中html标签字符串化，显示
        $art_one['title'] = strip_tags($art_one['title']);
        $art_one['content'] = strip_tags($art_one['content']);
//        dd($art_one);
        //查找6：帮助中心的title，遍历为左侧的显示title
        $art_list = Arttype::where('parent_id',6)->get();
        foreach($art_list as $k=>$v) {
            $item = Art::where('position_id',$v['id'])->first();
            $art_list[$k]['children'] = $item;
        }
        return [$art_one,$art_list];
    }
}
