<?php

namespace App\Models;

use Illuminate\Support\Facades\Input;
use function Sodium\crypto_box_publickey_from_secretkey;
use Cache;

class Config extends BaseModel
{
    public $timestamps = false;

    protected $table = "config";

    protected $guarded = [];

    const CACHE_CONFIG_ALL = 'cache_config_all';

    public function getOne($where, $fileds = '*', $order = '')
    {
        if (!$order) {
            $order = ['id' => 'desc'];
        }
        return $this->multiSelect($fileds)->multiWhere($where)->multiOrder($order)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     * 获取全部数据
     * @param array $where
     * @param string $fields
     * @param array $order
     * @return mixed
     */
    public function getAll($where = [], $fields = "*", $order = ['created_at' => 'desc'])
    {
        return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }

    public function index($request)
    {
        $all = $this->getCache();
        if ($request['logo'] == $all['logo'] && $request['localhost'] == $all['localhost'] && $request['title'] == $all['title'] && $request['name'] == $all['name'] && $request['keywords'] == $all['keywords'] && $request['xnb'] == $all['xnb'] && $request['xnb_name'] == $all['xnb_name'] && $request['bili'] == $all['bili'] && $request['desc'] == $all['desc'] && $request['copyright'] == $all['copyright'] && $request['record'] == $all['record'] && $request['address'] == $all['address'] && $request['biaoge_url'] == null ) {
          $res['state'] = 200;
          $res['msg'] = '请填写配置项！';
          return $res;
        }
        $images = $request->file('logo');//获取上传文件
        if ($images !== null) {
            $path = "upload/" . date('Y-m-d', time()) . "/";//定义路径
            $extension = $images->getClientOriginalExtension(); //获取上传图片的后缀名
            if (!in_array($extension, array('jpg','gif', 'png', 'jpeg'))) {
                $res['state'] = 400;
                $res['msg'] = '图片格式错误';
                return $res;
            }
            $fileName = str_random(8) . '.' . $extension;//重命名
            $filePath = $images->move($path, $fileName); //使用move方法移动文件.
            $data['logo'] = '/' . $filePath;
        }
        $data['localhost'] = $request['localhost'];
        $data['title'] = $request['title'];
        $data['name'] = $request['name'];
        $data['keywords'] = $request['keywords'];
        $data['xnb'] = $request['xnb'];
        $data['xnb_name'] = $request['xnb_name'];
        $data['bili'] = $request['bili'];
        $data['desc'] = $request['desc'];
        $data['copyright'] = $request['copyright'];
        $data['record'] = $request['record'];
        $data['address'] = $request['address'];
        $file = $request->file('biaoge_url');//获取上传文件
        if (isset($file)) {
            $path = "upload/biaoge/" . date('Y-m-d', time()) . "/";//定义路径
            $extension = $file->getClientOriginalExtension(); //获取上传图片的后缀名
            if (!in_array($extension, array('xls', 'xlsx'))) {
                $res['state'] = 400;
                $res['msg'] = '文件格式错误';
                return $res;
            }
            $fileName = str_random(4) . '新币申请表';//重命名
            $filePath = $file->move($path, $fileName); //使用move方法移动文件.

            $data['biaoge_url'] = '/'.$filePath;
        }
        foreach ($data as $k=>$v)
        {
            $result[] = $this->up(['key'=>$k],['value'=>$v]);
        }
        if ($result) {
            $this->generateCache();
            $res['state'] = 200;
            $res['msg'] = '操作成功！';
            return $res;
        }
        $res['state'] = 400;
        $res['msg'] = '操作失败！';
        return $res;

    }

    /**
     * 信息设置模块
     */
    public function information_add($request)
    {
        $all = $this->getCache();
        if ($request['friendship_tips'] == $all['friendship_tips'] && $request['risk_warning'] == $all['risk_warning'] && $request['withdraw_warning'] == $all['withdraw_warning'] && $request['invite_rule'] == $all['invite_rule'] && $request['VAP_rule'] == $all['VAP_rule'] && $request['disclaimer'] == $all['disclaimer'] && $request['FWTK'] == $all['FWTK'] && $request['new_coin_up'] == $all['new_coin_up'] && $request['reg_risk_warning'] == $all['reg_risk_warning']) {
            $res['state'] = 200;
            $res['msg'] = '请填写配置项！';
            return $res;
        }
       $data['friendship_tips'] = strip_tags($request['friendship_tips']);
       $data['risk_warning'] = strip_tags($request['risk_warning']);
       $data['withdraw_warning'] = strip_tags($request['withdraw_warning']);
       $data['invite_rule'] = strip_tags($request['invite_rule']);
       $data['VAP_rule'] = strip_tags($request['VAP_rule']);
       $data['disclaimer'] = strip_tags($request['disclaimer']);
       $data['FWTK'] = strip_tags($request['FWTK']);
       $data['new_coin_up'] = strip_tags($request['new_coin_up']);
       $data['reg_risk_warning'] = strip_tags($request['reg_risk_warning']);
       foreach ($data as $k=>$v) {
           $result[] = $this->up(['key'=>$k],['value'=>$v]);
       }
        if ($result) {
            $this->generateCache();
            $res['state'] = 200;
            $res['msg'] = '配置修改成功！';
            return $res;
        }
        $res['state'] = 400;
        $res['msg'] = '配置修改失败！';
        return $res;


    }

    /**
     * 财务设置模块
     */
    public function finance_edit($request)
    {
        $all = $this->getCache();
        if ($request['jiaoyi_start_hour'] == $all['jiaoyi_start_hour'] && $request['jiaoyi_start_minute'] == $all['jiaoyi_start_minute'] && $request['jiaoyi_over_hour'] == $all['jiaoyi_over_hour'] && $request['jiaoyi_over_minute'] == $all['jiaoyi_over_minute'] && $request['pay_min_money'] == $all['pay_min_money'] && $request['pay_max_money'] == $all['pay_max_money'] && $request['time_limit'] == $all['time_limit'] && $request['pay_fee'] == $all['pay_fee'] && $request['fee'] == $all['fee'] && $request['tcoin_fee'] == $all['tcoin_fee'] && $request['transaction_false'] == $all['transaction_false'] && $request['jiedong_bili'] == $all['jiedong_bili']) {
            $res['state'] = 200;
            $res['msg'] = '请填写配置项！';
            return $res;
        }
        $data['jiaoyi_start_hour'] = $request['jiaoyi_start_hour'];
        $data['jiaoyi_start_minute'] = $request['jiaoyi_start_minute'];
        $data['jiaoyi_over_hour'] = $request['jiaoyi_over_hour'];
        $data['jiaoyi_over_minute'] = $request['jiaoyi_over_minute'];
        $data['pay_min_money'] = $request['pay_min_money'];
        $data['pay_max_money'] = $request['pay_max_money'];
        $data['time_limit'] = $request['time_limit'];
        $data['pay_fee'] = $request['pay_fee'];
        $data['fee'] = $request['fee'];
        $data['tcoin_fee'] = $request['tcoin_fee'];
        $data['transaction_false'] = $request['transaction_false'];
        $data['jiedong_bili'] = $request['jiedong_bili'];
        foreach ($data as $k=>$v) {
            $result[] = $this->up(['key'=>$k],['value'=>$v]);
        }
        if ($result) {
            $this->generateCache();
            $res['state'] = 200;
            $res['msg'] = '配置修改成功！';
            return $res;
        }
        $res['state'] = 400;
        $res['msg'] = '配置修改失败！';
        return $res;

    }

    /**
     * 客服设置模块
     */
    public function customer_edit($request)
    {
        $all = $this->getCache();
        if ($request['tel'] == $all['tel'] && $request['weibo'] == $all['weibo'] && $request['weixin'] == $all['weixin'] && $request['qq1'] == $all['qq1'] && $request['qq2'] == $all['qq2'] && $request['qq3'] == $all['qq3'] && $request['qqqun1'] == $all['qqqun1'] && $request['qqqun2'] == $all['qqqun2'] && $request['qqqun3'] == $all['qqqun3'] && $request['email'] == $all['email'] && $request['business_email'] == $all['business_email'] && $request['qqqun_url'] == $all['qqqun_url']) {
            $res['state'] = 200;
            $res['msg'] = '请填写配置项！';
            return $res;
        }
        $data['tel'] = $request['tel'];
        $data['weibo'] = $request['weibo'];
        $images = $request->file('weixin');//获取上传文件
        if ($images !== null) {
            $path = "upload/weixin/" . date('Y-m-d', time()) . "/";//定义路径
            $extension = $images->getClientOriginalExtension(); //获取上传图片的后缀名
            if (!in_array($extension, array('jpg','gif', 'png', 'jpeg'))) {
                $res['state'] = 400;
                $res['msg'] = '图片格式错误';
                return $res;
            }
            $fileName = str_random(8) . '.' . $extension;//重命名
            $filePath = $images->move($path, $fileName); //使用move方法移动文件.
            $data['weixin'] = '/' . $filePath;
        }
        $data['qq1'] = $request['qq1'];
        $data['qq2'] = $request['qq2'];
        $data['qq3'] = $request['qq3'];
        $data['qqqun1'] = $request['qqqun1'];
        $data['qqqun2'] = $request['qqqun2'];
        $data['qqqun3'] = $request['qqqun3'];
        $data['email'] = $request['email'];
        $data['business_email'] = $request['business_email'];
        $data['qqqun_url'] = $request['qqqun_url'];
        foreach ($data as $k=>$v) {
            $result[] = $this->up(['key'=>$k],['value'=>$v]);
        }
        if ($result) {
            $this->generateCache();
            $res['state'] = 200;
            $res['msg'] = '配置修改成功！';
            return $res;
        }
        $res['state'] = 400;
        $res['msg'] = '配置修改失败！';
        return $res;


    }

    /**
     * 短信邮箱设置模块
     */
    public function short_edit($request)
    {
        $all = $this->getCache();
        if ($request['CODE_USER_NAME'] == $all['CODE_USER_NAME'] && $request['CODE_NAME'] == $all['CODE_NAME'] && $request['EMAIL_HOST'] == $all['EMAIL_HOST'] && $request['EMAIL_USERNAME'] == $all['EMAIL_USERNAME'] && $request['EMAIL_PASSWORD'] == $all['EMAIL_PASSWORD']) {
            $res['state'] = 200;
            $res['msg'] = '请填写配置项！';
            return $res;
        }
        $data['CODE_USER_NAME'] = $request['CODE_USER_NAME'];
        $data['CODE_NAME'] = $request['CODE_NAME'];
        $data['EMAIL_HOST'] = $request['EMAIL_HOST'];
        $data['EMAIL_USERNAME'] = $request['EMAIL_USERNAME'];
        $data['EMAIL_PASSWORD'] = $request['EMAIL_PASSWORD'];
        foreach ($data as $k=>$v) {
            $result[] = $this->up(['key'=>$k],['value'=>$v]);
        }
        if ($result) {
            $this->generateCache();
            $res['state'] = 200;
            $res['msg'] = '配置修改成功！';
            return $res;
        }
        $res['state'] = 400;
        $res['msg'] = '配置修改失败！';
        return $res;
    }

    /**
     * 重新生成缓存
     */
    public function generateCache()
    {
        Cache::forget(self::CACHE_CONFIG_ALL);
        $res = $this->getList();
        $config = [];
        if ($res) {
            $res = $res->toArray();
            foreach ($res as $v) {
                $config[$v['key']] = $v['value'];
            }
        }
        Cache::forever(self::CACHE_CONFIG_ALL, $config);
    }

    /**
     * 获取缓存
     */
    public function getCache()
    {
        if (Cache::has(self::CACHE_CONFIG_ALL)) {
            return Cache::get(self::CACHE_CONFIG_ALL);
        } else {
            $res = $this->getList();
            $config = [];
            if ($res) {
                $res = $res->toArray();
                foreach ($res as $v) {
                    $config[$v['key']] = $v['value'];
                }
            }
            Cache::forever(self::CACHE_CONFIG_ALL, $config);
            return $config;
        }
    }

}
