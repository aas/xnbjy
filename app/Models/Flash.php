<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
class Flash extends BaseModel
{
    public $timestamps = false;

    protected $table = "flash";

    protected $guarded = [];

    public function getOne($where, $fileds = '*', $order = '')
    {
        if (!$order) {
            $order = ['flash_id' => 'desc'];
        }
        return $this->multiSelect($fileds)->multiWhere($where)->multiOrder($order)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     * 获取全部数据
     * @param array $where
     * @param string $fields
     * @param array $order
     * @return mixed
     */
    public function getAll($where = [], $fields = "*", $order = ['created_at' => 'desc'])
    {
        return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }

    /**
     * 新增幻灯
     */
    public function addList($request)
    {

        if($request->isMethod('post')) {
            $images = Input::file('pic');//获取上传文件
            if ($images !== null) {
                $path = "upload/" . date('Y-m-d', time()) . "/";//定义路径
                $extension = $images->getClientOriginalExtension(); //获取上传图片的后缀名
                if (!in_array($extension, array('jpg','gif', 'png', 'jpeg'))) {
                    $res['state'] = 400;
                    $res['msg'] = '图片格式错误';
                    return $res;
                }
                $fileName = str_random(8) . '.' . $extension;//重命名
                $filePath = $images->move($path, $fileName); //使用move方法移动文件.
                $data['pic'] = '/'.$filePath;
            }
            $data['title'] = $request['title'];
            $data['sort'] = $request['sort'];
            $data['jump_url'] = $request['jump_url'];
            $data['created_at'] = date('Y-m-d H:i:s',time());
            $data['add_time'] = time();
            $result = $this->add($data);
            if ($result) {
                $res['state'] = 200;
                $res['msg'] = '操作成功！';
                return $res;
            }
            $res['state'] = 400;
            $res['msg'] = '操作失败！';
            return $res;
        }

        }


    /**
     * 修改幻灯
     */
    public function editList($request)
    {
        $where['flash_id'] = $request['flash_id'];
        $all = $this->getOne($where);
        if ($all['title'] == $request['title'] && $all['sort'] == $request['sort'] && $all['jump_url'] == $request['jump_url'] && $all['pic'] == $request['pic']) {
            $res['state'] = 0;
            $res['msg'] = '请填写修改内容！';
            return $res;
        }
        $images = Input::file('pic');//获取上传文件
        if ($images !== null) {
            $path = "upload/" . date('Y-m-d', time()) . "/";//定义路径
            $extension = $images->getClientOriginalExtension(); //获取上传图片的后缀名
            if (!in_array($extension, array('jpg','gif', 'png', 'jpeg'))) {
                $res['state'] = 400;
                $res['msg'] = '图片格式错误';
                return $res;
            }
            $fileName = str_random(8) . '.' . $extension;//重命名
            $filePath = $images->move($path, $fileName); //使用move方法移动文件.
            $data['pic'] = '/'.$filePath;
        }
        $data['title'] = $request['title'];
        $data['sort'] = $request['sort'];
        $data['jump_url'] = $request['jump_url'];
        $data['add_time'] = time();
        $data['updated_at'] = date('Y-m-d H:i:s',time());
        $where['flash_id'] = $request['flash_id'];
        $result = $this->up($where,$data);
        if ($result) {
          $res['state'] = 200;
          $res['msg'] = '修改成功！';
          return $res;
        }
        $res['state'] = 400;
        $res['msg'] = '修改失败！';
        return $res;
    }




}
