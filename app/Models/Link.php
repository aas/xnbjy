<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends BaseModel
{
    public $timestamps = false;

    protected $table = "link";

    protected $guarded = [];

    public function getOne($where, $fileds = '*', $order = '')
    {
        if (!$order) {
            $order = ['id' => 'desc'];
        }
        return $this->multiSelect($fileds)->multiWhere($where)->multiOrder($order)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     * 获取全部数据
     * @param array $where
     * @param string $fields
     * @param array $order
     * @return mixed
     */
    public function getAll($where = [], $fields = "*", $order = ['created_at' => 'desc'])
    {
        return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }
    
    /**
     * 友情链接添加
     */
    public function addList($request)
    {
        $data['name'] = $request['name'];
        $data['url'] = $request['url'];
        $data['status'] = $request['status'];
        $data['add_time'] = time();
        $data['created_at'] = date('Y-m-d',time());
        $result = $this->add($data);
        if ($result) {
            $res['state'] = 200;
            $res['msg'] = '操作成功';
            return $res;
        }else{
            $res['state'] = 400;
            $res['msg'] = '操作失败';
            return $res;
        }

    }
    
    /**
     * 友情链接的修改
     */
    public function editList($request)
    {
        $data['name'] = $request['name'];
        $data['url'] = $request['url'];
        $data['status'] = $request['status'];
        $data['updated_at'] =date('Y-m-d',time());
        $where['id'] = $request['id'];
        $result = $this->up($where,$data);
        if ($result) {
            $res['state'] = 200;
            $res['msg'] = '修改成功！';
            return $res;
        } else {
            $res['state'] = 400;
            $res['msg'] = '修改失败！';
            return $res;
        }

    }


}
