<?php

namespace App\Models;


class QianbaoAddress extends BaseModel
{
    public $timestamps = false;

    protected $table = "qianbao_address";

    protected $guarded = [];

}
