<?php

namespace App\Models;

use DB;

class Bank extends BaseModel
{
    public $timestamps = false;
    protected $table = 'bank';

    protected $guarded = [];

    public function getOne($where, $fileds = '*', $order = '')
    {
        if (!$order) {
            $order = ['id' => 'desc'];
        }
        return $this->multiSelect($fileds)->multiWhere($where)->multiOrder($order)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     * 获取全部数据
     * @param array $where
     * @param string $fields
     * @param array $order
     * @return mixed
     */
    public function getAll($where = [], $fields = "*", $order = ['created_at' => 'desc'])
    {
        return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }

    public function count($where = [], $field = '')
    {
        return $this->_count($where, $field);
    }

    public function getBanAdres()
    {
        $prefix = config('database.connections.mysql.prefix');
        $sql = 'SELECT bank.*,b.area_name as barea_name,a.area_name as aarea_name ';
        $sql.= 'FROM '.$prefix.'bank as bank JOIN '.$prefix.'areas as b ON b.area_id = bank.address ';
        $sql.= 'JOIN '.$prefix.'areas as a ON a.area_id = b.parent_id ';
        $sql.= 'WHERE bank.uid = '.session('USER_KEY_ID');
        return DB::select($sql);

    }

}