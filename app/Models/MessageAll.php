<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageAll extends BaseModel
{
    public $timestamps = false;

    protected $table = "message_all";

    protected $guarded = [];

    public function getOne($where, $fileds = '*')
    {
        return $this->multiSelect($fileds)->multiWhere($where)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }


    /**
     *
     * 所有消息列表
     *
     */
    public function getMessageAllList()
    {

        $where = [];
        $order['bank_id'] = 'desc';
        $rows = $this->getList($where, '*', $order, 2);
        if ($rows) {
            $rows = $rows->toArray();
            $rows['pageNoList'] = $this->getPageNoList($rows['last_page'], request('page', 1), 3);
            return $rows;
        }

    }

    /**
     *
     * 根据id获取一条消息
     *
     */
    public function getOneMessage($request)
    {

        $id = trim($request['id']);
        if(empty($id)){
            return '参数错误';
        }
        $where['id'] = $id;
        $data =  $this->getOne($where);
        return $data;

    }

    /**
     *
     * 新增消息表单
     *
     */
    public function addAllMessage($request)
    {
        //dd($_POST);
        $radios = trim($request['radios']);    //one为个人.all为群发
        $type = trim($request['type']);        //消息类型
        $title = trim($request['title']);
        $content = trim($request['content']);

        if(empty($radios)||empty($type)||empty($title)||empty($content))
        {
            return '表单字段不能为空!';
        }

        //判断个人还是全体
        if($radios == 'one'){
            $data['u_id'] = trim($request['u_id']);    //接收输入的个人id
            if(empty($data['u_id'])){
                return '个人ID不能为空!';
            }
        }else{
            $data['u_id'] = -1; //-1 状态代表群发
        }
        $data['type'] = $type;
        $data['title'] = $title;
        $data['content'] = $content;
        $data['add_time'] = time();

        $did = $this->add($data);
        if(!$did){
            $res ="服务器繁忙,请稍后重试";
            return $res;
        }
        $res = "添加消息成功!";
        return $res;

    }

    /**
     * 删除消息
     */
    public function deleteMessage($request)
    {

        //dd(234);
        //开启事务处理
        $this->start();
        try{

            $where1['id'] = $request['id'];
            $a = $this->del($where1);     //删除message_all表中数据

            $message_model = new Message();
            $where2['message_all_id'] = $request['id'];
            $b = $message_model->del($where2);  //删除message表中数据

            $this->commit();

        }catch (\Exception $e){
            $this->rollBack();
            //echo $e->getMessage();die();
            $data['status'] = 0;
            $data['msg'] = '服务器繁忙,请稍后重试!';
            return $data;
        }
            $data['status'] = 1;
            $data['msg'] = '删除成功!';
            return $data;
    }

    /**
     * 添加消息库
     * @param int $member_id   用户ID -1 为群发
     * @param int $type    分类  4=系统  -1=文章表系统公告 -2 个人信息
     * @param String $title       标题
     * @param String $content     内容
     * @return bool|mixed  成功返回增加Id 否则 false
     */
    public function addMessage_all($member_id,$type,$title,$content){
        $data['u_id']=$member_id;
        $data['type']=$type;
        $data['title']=$title;
        $data['content']=$content;
        $data['add_time']=time();

        $did = $this->add($data);
        if($did){
            return $did;
        }else{
            return false;
        }
    }

}
