<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Member extends BaseModel
{

    protected $table = "member";

    public $timestamps = false;

    public function getOne($where, $fileds = '*', $order = '')
    {
        return $this->multiSelect($fileds)->multiWhere($where)->multiOrder($order)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getWithMemberList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->with('member')->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->with('member')->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }


    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }

    public function getCount($where = [], $field = '')
    {
        return $this->_count($where, $field);
    }

    public function getSum($where = [], $field)
    {
        return $this->_sum($where, $field);
    }

    public function inc($where = [], $field)
    {
        return $this->_inc($where, $field);
    }

    public function dec($where = [], $field)
    {
        return $this->_dec($where, $field);
    }

    /**
     *
     * 会员列表
     *
     */
    public function getMemberList($request)
    {
        $email = trim($request['email']);         //关键词搜索
        $member_id = trim($request['member_id']);
        $where = [];
        if (!empty($email)) {
            $where['email'] = ['like', '%' . $email . '%'];
        }
        if (!empty($member_id)) {
            $where['member_id'] = $member_id;
        }
        $order['member_id'] = 'desc';
        $rows = $this->getList($where, '*', $order, 10);
        //dd($rows);
        if ($rows) {
            $rows = $rows->toArray();
            $rows['pageNoList'] = $this->getPageNoList($rows['last_page'], request('page', 1), 5);
            return $rows;
        }

    }

    /**
     *
     * 添加会员用户
     *
     */
    public function addMember($request)
    {

        $data['email'] = trim($request['email']);          //电子邮件
        $data['pwd'] = trim($request['pwd']);              //登录密码
        $data['repwd'] = trim($request['repwd']);           //重复密码
        $data['pid'] = trim($request['pid']);               //邀请码
        $data['pwdtrade'] = trim($request['pwdtrade']);    //交易密码
        $data['repwdtrade'] = trim($request['repwdtrade']); //重复交易密码

        $data['ip'] = getIp();
        $data['reg_time'] = time();
        //$data['status'] = 1;

        $message = $this->checkFileds($data);     //验证字段是否为空,如果有返回值则验证不通过
        if ($message) {
            return $message;
        }

        if ($data['pwd'] != $data['repwd']) {
            $res = '两次登录密码不一致';
            return $res;
        }

        if ($data['pwdtrade'] != $data['repwdtrade']) {
            $res = '两次交易密码不一致';
            return $res;
        }

        if ($data['pwd'] == $data['pwdtrade']) {
            $res = '支付密码不能和登录密码一样';
            return $res;
        }
        unset($data['repwd']);
        unset($data['repwdtrade']);

        $did = $this->add($data);
        if (!$did) {
            $res = "服务器繁忙,请稍后重试";
            return $res;
        }
        $res = "添加用户成功";
        return $res;

    }

    //验证表单字段是否为空
    public function checkFileds($data)
    {

        if (!$data['email']) {
            return '表单不能为空';
        }
        if (!$data['pwd']) {
            return '表单不能为空';
        }
        if (!$data['repwd']) {
            return '表单不能为空';
        }
        if (!$data['pid']) {
            return '表单不能为空';
        }
        if (!$data['pwdtrade']) {
            return '表单不能为空';
        }
        if (!$data['repwdtrade']) {
            return '表单不能为空';
        }

    }

    /**
     *
     * ajax验证邮箱
     *
     */
    public function ajaxCheckEmail($request)
    {

        $email = urldecode($request['email']);
        $data = array();
        if (!checkEmail($email)) {
            $data['status'] = 0;
            $data['msg'] = "邮箱格式错误×";
        } else {

            $where['email'] = $email;
            $r = $this->getOne($where);
            if ($r) {
                $data['status'] = 0;
                $data['msg'] = "邮箱已存在×";
            } else {
                $data['status'] = 1;
                $data['msg'] = "该邮箱可用√";
            }
        }
        return $data;

    }

    /**
     *
     * ajax验证昵称
     *
     */
    public function ajaxCheckNick($request)
    {

        $nick = urldecode($request['nick']);
        $data = array();

        if (empty($nick)) {
            $data['status'] = 0;
            $data['msg'] = "昵称不能为空!";
            return $data;
        }

        $where['nick'] = $nick;
        $r = $this->getOne($where);
        if ($r) {
            $data['status'] = 0;
            $data['msg'] = "昵称已被占用×";
        } else {
            $data['status'] = 1;
            $data['msg'] = "该昵称可用√";
        }

        return $data;

    }

    /**
     *
     * ajax验证手机
     *
     */
    public function ajaxCheckPhone($request)
    {

        $phone = urldecode($request['phone']);
        $data = array();
        if (!checkMobile($phone)) {
            $data['status'] = 0;
            $data['msg'] = "手机号不正确×";
        } else {

            $where['phone'] = $phone;
            $r = $this->getOne($where);
            if ($r) {
                $data['status'] = 0;
                $data['msg'] = "此手机已经绑定过！请更换手机号";
            } else {
                $data['status'] = 1;
                $data['msg'] = "该手机可用√";
            }
        }
        return $data;

    }

    /**
     *
     * 添加会员用户信息
     *
     */
    public function saveModify($request)
    {
        $data['nick'] = trim($request['nick']);
        $data['name'] = trim($request['name']);
        $data['cardtype'] = trim($request['cardtype']);
        $data['idcard'] = trim($request['idcard']);
        $data['phone'] = trim($request['phone']);


//        $message = $this->checkFileds($data);     //验证字段是否为空,如果有返回值则验证不通过
//        if ($message) {
//            return $message;
//        }
        $data['status'] = 1;//0=有效但未填写个人信息1=有效并且填写完个人信息2=禁用
        $where['member_id'] = trim($request['member_id']);
        $did = $this->up($where, $data);
        if (!$did) {
            $res = "服务器繁忙,请稍后重试";
            return $res;
        }
        $res = "修改会员信息成功";
        return $res;

    }

    /**
     *
     * 查询个人信息
     *
     */
    public function getOneModify($request)
    {

        $where['member_id'] = $request['member_id'];
        $data = $this->getOne($where);
        return $data;

    }

    /**
     * 根据id查询用户名
     */
    public function getMemberAllbyMemberId($memberid)
    {

        $where['member_id'] = $memberid;
        $data = $this->getOne($where);
        //$data = array_map('get_object_vars', $data);
        return $data;
    }


    /**
     *
     * ajax删除会员
     *
     */
    public function delMember($request)
    {

        if (!isset($request['id'])) {
            $data['status'] = 0;
            $data['msg'] = '传入参数有误！';
            return $data;
        }
        $where['member_id'] = $request['id'];

        //判断还有没有余额
//        $where['member_id']= $member_id;
//        $member = $M_member->where($where)->find();
//        $member_currency = M('Currency_user')->where($where)->find();
//        if($member['rmb']>0||$member['forzen_rmb']>0||$member_currency['num']>0||$member_currency['forzen_num']>0){
//            $this->error('因账户有剩余余额,禁止删除');
//            return;
//        }
//        $r[] = $M_member->delete($member_id);
//        $r[] = M('Currency_user')->where($where)->delete();
//        $r[] = M('Finance')->where($where)->delete();
//        $r[] = M('Orders')->where($where)->delete();
//        $r[] = M('Trade')->where($where)->delete();
//        $r[] = M('Withdraw')->where('uid='.$member_id)->delete();
//        $r[] = M('Pay')->where($where)->delete();
//        if($r){
//            $this->success('删除成功',U('Member/index'));
//            return;
//        }else{
//            $this->error('删除失败');
//            return;
//        }

        $did = $this->del($where);
        if ($did) {
            $data['status'] = 1;
            $data['msg'] = '删除会员成功！';
        } else {
            $data['status'] = 0;
            $data['msg'] = '删除会员失败！';
        }

        return $data;
    }

    /**
     * 修改会员表单
     */
    public function saveMember($request)
    {
        //dd($_POST);

        $member_id = trim($request['member_id']);
        $head = isset($request['img']) ? trim($request['img']) : '';
        $pwd = trim($request['pwd']);
        $pwdtrade = trim($request['pwdtrade']);
        $rmb = trim($request['rmb']);
        $forzen_rmb = trim($request['forzen_rmb']);
        $nick = trim($request['nick']);
        $name = trim($request['name']);
        $cardtype = trim($request['cardtype']);
        $idcard = trim($request['idcard']);
        $phone = trim($request['phone']);
        $status = trim($request['status']);

        $data['rmb'] = $rmb;
        $data['forzen_rmb'] = $forzen_rmb;
        $data['nick'] = $nick;
        $data['name'] = $name;
        $data['cardtype'] = $cardtype;
        $data['idcard'] = $idcard;
        $data['phone'] = $phone;
        $data['status'] = $status;

        foreach ($data as $k => $v) {
            if (empty($v)) {
                return '表单字段不完整,请重新提交';
            }
        }

        //如果有上传图片
        if (!empty($head)) {
            $imgPath = upImgs($head, 'head');
            $data['head'] = $imgPath;
            if (empty($imgPath)) {
                return '图片上传失败,请重新提交';
            }
        }


        if (!empty($pwd) && !empty($pwdtrade) && ($pwd == $pwdtrade)) {
            return '交易密码不能和密码一致';
        } else {
            $data['pwd'] = md5($pwd);
            $data['pwdtrade'] = md5($pwdtrade);
        }

        //判断昵称是否重复
        $info = DB::select("select * from xnb_member WHERE nick='$nick' AND member_id <> $member_id");
        if ($info) {
            return '昵称重复!';
        }

        //判断电话是否重复
        $telinfo = DB::select("select * from xnb_member WHERE phone='$phone' AND member_id <> $member_id");
        if ($telinfo) {
            return '手机号重复!';
        }

        $where['member_id'] = $member_id;
        $did = $this->up($where, $data);
        //dd(123);
        if (!$did) {
            $res = "服务器繁忙,请稍后重试";
            return $res;
        }
        $res = "修改会员成功";
        return $res;

    }

    /**
     *
     * 显示自己推荐列表
     *
     */
    public function showMyInvite($request)
    {

        $where['pid'] = $request['member_id'];
        $order['member_id'] = 'desc';
        $rows = $this->getList($where, '*', $order, 2);
        //dd($rows);
        if ($rows) {
            $rows = $rows->toArray();
            $rows['pageNoList'] = $this->getPageNoList($rows['last_page'], request('page', 1), 10);
            return $rows;
        }

    }


    /**
     *
     * 查看个人币种
     *
     */
    public function show($request)
    {
        $member_id = $request['member_id'];

        $rows = CurrencyUser::Join('currency', 'currency_user.currency_id', '=', 'currency.currency_id')->where('member_id', $member_id)->orderBy('member_id', 'desc')->paginate(3);
        //dd($rows);

        if ($rows) {
            $rows = $rows->toArray();
            $rows['pageNoList'] = $this->getPageNoList($rows['last_page'], request('page', 1), 5);
            return $rows;
        }
    }


    /**
     *
     * 修改币种
     *
     */
    public function updateMemberMoney($request)
    {

        $member_id = $request['member_id'];
        $currency_id = $request['currency_id'];
        $num = $request['num'];
        $forzen_num = $request['forzen_num'];
        if (empty($member_id) || empty($currency_id)) {
            $data['info'] = "参数不全";
            $data['status'] = 0;
        }
        $where['member_id'] = $member_id;
        $where['currency_id'] = $currency_id;

        $currency_user_model = new CurrencyUser();
        $data['num'] = $num;
        $data['forzen_num'] = $forzen_num;
        $did = $currency_user_model->up($where, $data);
        if (!$did) {
            $data['status'] = 0;
            $data['msg'] = "修改失败";

        } else {
            $data['status'] = 1;
            $data['msg'] = "修改成功";
        }
        return $data;

    }


    public function modify($request)
    {
        if (!$request['code']) {
            $data['status'] = 0;
            $data['info'] = '请填写验证码';
            return $data;
        }
        if (!checkPhoneCode($request['code'])) {
            $data['status'] = 0;
            $data['info'] = '验证码错误';
            return $data;
        }
        $input['name'] = $request['name'];
        $input['nick'] = $request['nick'];
        $input['cardtype'] = $request['cardtype'];
        $input['idcard'] = $request['idcard'];
        $input['phone'] = $request['phone'];
        $input['status'] = 1;
        $input = trim_fileds($input);
        $where['member_id'] = session('USER_KEY_ID');
        $res = $this->up($where, $input);
        $data['status'] = 0;
        $data['info'] = '服务器繁忙,请稍后重试';
        if ($res) {
            addSession('procedure', 2);//SESSION 跟踪第二步
            addSession('STATUS', 1);
            $data['status'] = 1;
            $data['info'] = "提交成功";
        }
        return $data;
    }


    /**
     * ajax验证昵称是否存在
     */
    public function checkNick($request)
    {
        $nick = urldecode($request['nick']);
        $where['nick'] = $nick;
        $r = $this->getOne($where);
        $data['msg'] = "";
        $data['status'] = 1;
        if ($r) {
            $data['msg'] = "昵称已被占用";
            $data['status'] = 0;
        }
        return $data;
    }

    /**
     * ajax手机验证
     */
    function checkPhone($request)
    {
        $phone = urldecode($request['phone']);
        $data = [];
        if (!checkMobile($phone)) {
            $data['msg'] = "手机号不正确！";
            $data['status'] = 0;
        } else {
            $where['phone'] = $phone;
            $r = $this->getOne($where);
            $data['msg'] = "";
            $data['status'] = 1;
            if ($r) {
                $data['msg'] = "此手机已经绑定过！请更换手机号";
                $data['status'] = 0;
            }
        }
        return $data;
    }

    /**
     * ajax验证手机验证码
     */
    public function sandPhone($request)
    {
        $configModel = new Config();
        $config = $configModel->getCache();
        $phone = urldecode($request['phone']);
        if (empty($phone)) {
            $data['status'] = 0;
            $data['info'] = "手机号码不能为空";
            return $data;
        }
        if (!preg_match("/^1[34578]{1}\d{9}$/", $phone)) {
            $data['status'] = -1;
            $data['info'] = "手机号码不正确";
            return $data;
        }

//        $user_phone = $this->getOne(['phone' => $phone], 'phone');
//        if (!empty($user_phone)) {
//            $data['status'] = -2;
//            $data['info'] = "手机号码已经存在";
//            return $data;
//        }
        $r = sandPhone($phone, $config['CODE_NAME'], $config['CODE_USER_NAME'], $config['CODE_USER_PASS']);
        if (!$r[1]) {
            $data['status'] = 1;
            $data['info'] = "发送成功";
            return $data;
        } else {
            $data['status'] = -3;
            $data['info'] = chuanglan_status($r[1]);
            return $data;
        }
    }


}
