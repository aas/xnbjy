<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Bonus extends BaseModel
{
    public $timestamps = false;


    protected $guarded = [];

    public function getOne($where, $fileds = '*', $order = '')
    {
        if (!$order) {
            $order = ['id' => 'desc'];
        }
        return $this->multiSelect($fileds)->multiWhere($where)->multiOrder($order)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     * 获取全部数据
     * @param array $where
     * @param string $fields
     * @param array $order
     * @return mixed
     */
    public function getAll($where = [], $fields = "*", $order = ['created_at' => 'desc'])
    {
        return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }

    /**
     * 添加
     */
    public function addList($request)
    {
        if ($request->isMethod('post')) {

            if (!is_numeric($request['num'])) {
                $res['state'] = 400;
                $res['msg'] = '分红数值不正确！';
                return $res;
            }
            $count = 0;
            $num = $request['num']; //分红的份额
            $text = $request['text']; //分红说明
            $member = Member::where('status',1)->get();
            if(empty($member)){
                $res['state'] = 400;
                $res['msg'] = '没有需要分红的人数！';
                return $res;
            }
                foreach ($member as $v) {
                    $item = $v['member_id'];
                    $list[] = DB::table('currency_user')
                        ->select('currency_id', 'cu_id', 'member_id', 'num')
                        ->where('num', '>', 0)
                        ->where('currency_id', $request['currency_id'])
                        ->where('member_id', $item)
                        ->get();

                }
            $list = array_filter($list); //过滤数组中的空单元
            foreach ($list as $k=>$v) {
                foreach ($v as $kk=>$vv){
                    $count += $vv->num;
                }
            }

            if (empty($list)) {
                $res['state'] = 400;
                $res['msg'] = '没有需要分红的人数！';
                return $res;
            }
            foreach ($list as $k => $v){
                foreach ($v as $kk=>$vv){
                    $currency_num = $num * ($vv->num / $count );
                    $currency_num = round($currency_num,4);
                if ($request['set_currency_id'] == 0){
                          DB::table('member')
                         ->where('member_id',$vv->member_id)
                         ->update(['rmb' => $currency_num]);
                    //数据入库
                        $re = DB::table('finance')->insert(['member_id' => $vv->member_id ,'type'=> 10,'content' => $text,'money' => $currency_num , 'money_type' => 1 , 'currency_id' => 0 ]);

                }else{
                     DB::table('currency_user')
                        ->where('currency_id',$vv->currency_id)
                        ->where('member_id',$vv->member_id)
                        ->update(['num' => $currency_num]);
                    //数据入库
                     $re = DB::table('finance')->insert(['member_id' => $vv->member_id ,'type'=> 10,'content' => $text,'money' => $currency_num , 'money_type' => 1 , 'currency_id' => $vv->currency_id]);
                }
                DB::table('message')->insert(['member_id' => $vv->member_id , 'type' => -2 , 'title' => '分红奖励' , 'content' => $text]);
            }
            }
            if ($re) {
                $res['state'] = 200;
                $res['msg'] = '分红成功！';
                return $res;
            }else{
                $res['state'] = 400;
                $res['msg'] = '分红失败！';
                return $res;
            }

        }
        }



}