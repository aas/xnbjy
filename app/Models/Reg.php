<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Reg extends BaseModel
{
    /**
     * 前台注册
     */
    public function register($request)
    {
        if($request->isMethod('post')) {

            $data['email'] = $request['email'];
            $data['pwd'] = md5($request['pwd']);
            $data['pid'] = $request['pid'];
            $data['pwdtrade'] = md5($request['pwdtrade']);
            $data['reg_time'] = time();
            $data['ip'] = get_ip();

            $member = new Member();
            $result = $member->add($data);
            if ($result) {
                $res['status'] = 1;
                $res['msg'] = '注册成功,请登录!';
                return $res;
            }
            $res['status'] = 0;
            $res['msg'] = '注册失败,请稍后重试！';
            return $res;
        }

    }

    /**
     * ajax验证邮箱
     * @param string $email 规定传参数的结构
     *
     */
    public function checkEmail($request){
        $email = urldecode($request['email']);
        if(!check_email($email)){
            $res['status'] = 0;
            $res['msg'] = "邮箱格式错误";
            return $res;
        }else{
            $member = new Member();
            $r = $member->getOne(['email'=>$email]);
            if(!empty($r)){
                $res['status'] = 0;
                $res['msg'] = "邮箱已存在";
                return $res;
            }else{
                $res['status'] = 1;
                $res['msg'] = "";
                return $res;
            }
        }
    }
}
