<?php
namespace App\Models;
/**
 * Created by PhpStorm.
 * User: zxj
 * Date: 2017/9/4
 * Time: 12:53
 */

class Websitebank extends BaseModel{

    protected $table = "website_bank";

    public $timestamps = false;

    public function getOne($where, $fields = '*', $order = '')
    {
        if (!$order) {
            $order = ['bank_id' => 'desc'];
        }
        return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getWithMemberList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->with('member')->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->with('member')->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }


    public function member()
    {
        return $this->hasOne('App\Models\Member', 'id', 'member_id');
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }

    public function count($where = [], $field = '')
    {
        return $this->_count($where, $field);
    }

    public function areas()
    {
        return $this->hasOne('App\Models\Areas', 'area_id', 'address');
    }
    

    /**
     *
     * 银行卡会员列表
     *
     */
    public function getWebsiteBankList()
    {

        $where = [];
        $order['bank_id'] = 'desc';
        $rows = $this->getList($where, '*', $order, 10);
        if ($rows) {
            $rows = $rows->toArray();
            $rows['pageNoList'] = $this->getPageNoList($rows['last_page'], request('page', 1), 3);
            return $rows;
        }

    }

    /**
     *
     * 添加银行卡
     *
     */
    public function addBank($request)
    {

        $data['bank_name'] = trim($request['bank_name']);
        $data['bank_adddress'] = trim($request['bank_adddress']);
        $data['bank_no'] = trim($request['bank_no']);
        $data['status'] = 1;


        $message = $this->checkFileds($data);     //验证字段是否为空,如果有返回值则验证不通过
        if ($message) {
            return $message;
        }

        $did = $this->add($data);
        if(!$did){
            $res ="服务器繁忙,请稍后重试";
            return $res;
        }
        $res = "添加用户成功";
        return $res;

    }

    /**
     * @param $data
     * @return string
     * 修改银行卡
     */
    public function saveBank($request)
    {
        $data['bank_name'] = trim($request['bank_name']);
        $data['bank_adddress'] = trim($request['bank_adddress']);
        $data['bank_no'] = trim($request['bank_no']);
        $data['status'] = trim($request['status']);


        $message = $this->checkFileds($data);     //验证字段是否为空,如果有返回值则验证不通过
        if ($message) {
            return $message;
        }

        $where['bank_id'] = $request['bank_id'];
        $did = $this->up($where,$data);
        if(!$did){
            $res ="服务器繁忙,请稍后重试";
            return $res;
        }
        $res = "修改银行卡成功";
        return $res;

    }

    /**
     *
     * ajax删除银行卡
     *
     */
    public function delBank($request)
    {

        if(!isset($request['id'])){
            $data['status'] = 0;
            $data['msg'] = '传入参数有误！';
            return $data;
        }
        $where['bank_id'] = $request['id'];
        $did = $this->del($where);
        if($did){
            $data['status'] = 1;
            $data['msg'] = '删除成功！';
        }else{
            $data['status'] = 0;
            $data['msg'] = '删除失败！';
        }

        return $data;
    }

    /**
     * @param $data
     * @return string
     * 查询单个银行卡
     */
    public function getOneBank($request)
    {
        $where['bank_id'] = $request['bank_id'];
        $data = $this->getOne($where);
        return $data;
    }


    //验证表单字段是否为空
    public function checkFileds($data)
    {

        if (!$data['bank_name']) {
            return '收款人不能为空';
        }
        if (!$data['bank_adddress']) {
            return '开户行不能为空';
        }
        if (!$data['bank_no']) {
            return '账号不能为空';
        }

    }










}