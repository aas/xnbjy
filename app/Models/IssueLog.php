<?php

namespace App\Models;

/**
 * Class IssueLog
 * @package App\Models
 * 众筹记录
 */
class IssueLog extends BaseModel
{
    public $timestamps = false;

    protected $table = "issue_log";

    //protected $guarded = [];

    public function getOne($where, $fileds = '*')
    {
        return $this->multiSelect($fileds)->multiWhere($where)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }

    public function count($where=[], $field='')
    {
        return $this->multiWhere($where)->count($field);
    }


    public function sum($where=[], $field)
    {
        return $this->multiWhere($where)->sum($field);
    }


    public function issue()
    {
        return $this->belongsTo('App\Models\Issue', 'iid', 'id');
    }


    public function getMyZhongChouList($pageSize=10)
    {
        $where['uid'] = session('USER_KEY_ID');
        $order['id'] = 'desc';
        return $this->with('issue')->withCertain('issue', ['id','currency_id','title'])->multiWhere($where)->multiOrder($order)->paginate($pageSize)->toArray();
    }

    /**
     * 众筹记录
     */
    public function zhongchouLog($request)
    {
        $where = [];
        $iid = trim($request['iid']);         //分类搜索
        $name = trim($request['name']);       //用户姓名
        $uid = trim($request['uid']);         //用户id
        $email = trim($request['email']);     //用户email
        if (!empty($iid)){
            $where['issue_log.iid'] = $iid;
        }
        if(!empty($name)){
            $where['member.name'] = ['like','%'.$name.'%'];
        }
        if (!empty($uid)){
            $where['member.member_id'] = $uid;
        }
        if (!empty($email)){
            $where['member.email'] = $email;
        }
        //$where['issue_log.id'] = 30;

        $data = $this->leftJoin('member','member.member_id','=','issue_log.uid')->leftJoin('issue','issue.id','=','issue_log.iid')->select('issue_log.*','member.name','issue.title','issue_log.num','issue_log.price',DB::raw('xnb_issue_log.num*xnb_issue_log.price as count'))->multiWhere($where)->orderBy('add_time','desc')->paginate(5)->toArray();
        //dd($data);
        //通过buy_currency_id查询属于哪种币，并新增字段buy_name表示
        $currency_model = new Currency();
        foreach ($data['data'] as $k=>$v)
        {
            $data['data'][$k]['buy_name'] = $v['buy_currency_id']==0?'人民币':$currency_model->getCurrencyNameById($v['buy_currency_id'])['currency_name'];
        }

        if ($data) {
            $data['pageNoList'] = $this->getPageNoList($data['last_page'], request('page', 1), 10);
            return $data;
        }



    }


}
