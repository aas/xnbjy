<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends BaseModel
{
    public $timestamps = false;

    protected $table = "admin";

    protected $guarded = [];

    public function getOne($where, $fileds = '*')
    {
        return $this->multiSelect($fileds)->multiWhere($where)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }

    public function login($request)
    {
        $username = trim($request['username']);
        $pwd = trim($request['pwd']);
        if (empty($username) || empty($pwd)) {
            $this->error = '请填写完整信息';
            return false;
        }
        $admin = $this->getOne(['username' => $username]);
        if ($admin['password'] != md5($pwd)) {
            $this->error = '登录密码不正确';
            return false;
        }
        if(empty($admin['nav'])) {
            $res['status'] = 0;
            $res['msg'] = '此账号尚未分配权限！';
            return $res;
        }
        addSession('admin_userid', $admin['admin_id']);
        $nav = new Nav();
        $str = explode(",",$admin['nav']);
        //dd($str);
        $where['nav_id'] = ['in', $str];

        $result = $nav->getList($where);
        $ruls[] = 'center/index';
        $menus = [];
        if ($result) {
            $result = $result->toArray();
            foreach ($result as $k=>$v){
                $ruls[] = $v['nav_url'];
                $value[$v['cat_id']][]=$v;
            }
            foreach ($value as $k=>$v){
                $menus[$k."_nav"] = $v;
            }
        }
        addSession('menus', $menus);
        addSession('ruls', $ruls);
        return true;
    }
}
