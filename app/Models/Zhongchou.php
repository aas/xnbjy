<?php
namespace App\Models;
use App\Http\Controllers\Home\CommonController;
use Illuminate\Support\Facades\DB;
use function PHPSTORM_META\map;

/**
 * Created by PhpStorm.
 * User: zxj
 * Date: 2017/9/4
 * Time: 12:53
 */

class Zhongchou extends BaseModel{

    protected $table = "issue";

    public $timestamps = false;

    public function getOne($where, $fields = '*', $order = '')
    {

        return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getWithMemberList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->with('member')->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->with('member')->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }


    public function member()
    {
        return $this->hasOne('App\Models\Member', 'id', 'member_id');
    }


    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }

    /**
     * @param $totalPage
     * @param $pageNo
     * @param $pageSize
     * @return array
     *
     */

    public function getPageNoList($totalPage, $pageNo, $pageSize)
    {
        $disPageList = 0;//定义最大链接显示数额
        $disPageList = $pageNo + $pageSize - 1;//最大链接显示数额赋值，公式为"当前页数 + 最大链接显示数额 -1"
        //前页面导航页数
        $pageNoList = array();
        //循环显示出当前页面导航页数
        if ($disPageList <= $totalPage) {
            for ($i = $pageNo; $i <= $disPageList; $i++) {
                $pageNoList[] = $i;
            }
        } else {
            if ($totalPage < $pageSize) {
                for ($i = 1; $i <= $totalPage; $i++) {
                    $pageNoList[] = $i;
                }
            } else {
                $start = $totalPage - $pageSize + 1;
                for ($i = $start; $i <= $totalPage; $i++) {
                    $pageNoList[] = $i;
                }
            }
        }
        return $pageNoList;
    }

    /**
     * 修正众筹表 计算剩余数量  修改状态
     */
    public function checkZhongchou()
    {
        $where = [];
        $fields = ['id','add_time','end_time','num','num_nosell','zhongchou_success_bili','status'];
        $order = [];
        $pageSize = '';
        $list = $this->getList($where, $fields, $order, $pageSize)->toArray();

        foreach($list as $k=>$v){
            $where['id'] = $v['id'];
            if($v['status']==3){

            }
        }

    }

    /**
     * 获取众筹列表
     */
    public function getZhongchouList()
    {


        $list = $this->leftjoin('currency','issue.currency_id','=','currency.currency_id')->select('issue.*','currency.currency_name as name')->orderBy('ctime','desc')->paginate(3);

        if ($list) {
            $list = $list->toArray();
            $list['pageNoList'] = $this->getPageNoList($list['last_page'], request('page', 1), 3);
            return $list;
        }

        foreach ($list['data'] as $k=>$v){
            $list['data'][$k]['zhongchou_success_bili'] = $v['zhongchou_success_bili']*100;
        }

        return $list;

    }





    /**
     * 新增众筹
     */
    public function addZhongchou($request)
    {

        $data['title'] = $request['title'];
        $data['currency_id'] = $request['currency_id'];
        $data['buy_currency_id'] = $request['buy_currency_id'];
        $data['is_forzen'] = $request['is_forzen'];
        $data['num'] = $request['num'];
        $data['num_nosell'] = $request['num_nosell'];
        $data['price'] = $request['price'];
        $data['limit'] = $request['limit'];
        $data['min_limit'] = $request['min_limit'];
        $data['limit_count'] = $request['limit_count'];
        $data['zhongchou_success_bili'] = $request['zhongchou_success_bili'];
        $data['add_time'] = $request['add_time'];
        $data['end_time'] = $request['end_time'];
        $data['info'] = $request['info'];

        foreach ($data as $k=>$v){
            if(!isset($v)){
                return '表单不能为空';
            }
        }

        $data['remove_forzen_bili'] = $request['remove_forzen_bili'];
        $data['remove_forzen_cycle'] = $request['remove_forzen_cycle'];

        $data['add_time']=strtotime($data['add_time'])?strtotime($data['add_time']):time();
        $data['end_time']=strtotime($data['end_time'])?strtotime($data['end_time']):time();
        $data['ctime']=time();

        $data['zhongchou_success_bili'] = $data['zhongchou_success_bili']/100;

        //如果有上传图片
        if(!empty($request['url1']))
        {
            $imgPath = upImgs($request['url1'],'zhongchou');
            if(empty($imgPath))
            {
                return '图片1上传失败,请重新提交';
            }
            $data['url1'] = $imgPath;
        }

        if(!empty($request['url2']))
        {
            $imgPath = upImgs($request['url2'],'zhongchou');
            if(empty($imgPath))
            {
                return '图片2上传失败,请重新提交';
            }
            $data['url2'] = $imgPath;
        }

        //文件上传
        if(!empty($_FILES['wenjian_url'])){
            $fileName = downloadFile($_FILES['wenjian_url']['tmp_name'],'upload/zhongchou/wenjian');
            if(empty($fileName))
            {
                return '文件上传失败,请重新提交';
            }
            $data['wenjian_url'] = $fileName;
        }

        $did = $this->add($data);
        if(!$did){
            $res ="服务器繁忙,请稍后重试";
            return $res;
        }
        $res = "添加众筹成功";
        return $res;

    }


    /**
     *  根据id获取众筹信息
     *
     */
    public function getZhongchouByid($request)
    {

        $where['id'] = $request['id'];
        $fields = '*';
        $order['id'] = 'desc';
        $data =  $this->getOne($where, $fields, $order);
        return $data;
    }

    /**
     *
     * 修改众筹
     */
    public function saveZhongchou($request)
    {

        $data['title'] = $request['title'];
        $data['currency_id'] = $request['currency_id'];
        $data['buy_currency_id'] = $request['buy_currency_id'];
        $data['is_forzen'] = $request['is_forzen'];
        $data['num'] = $request['num'];
        $data['num_nosell'] = $request['num_nosell'];
        $data['price'] = $request['price'];
        $data['limit'] = $request['limit'];
        $data['min_limit'] = $request['min_limit'];
        $data['limit_count'] = $request['limit_count'];
        $data['zhongchou_success_bili'] = $request['zhongchou_success_bili'];
        $data['add_time'] = $request['add_time'];
        $data['end_time'] = $request['end_time'];
        $data['info'] = $request['info'];

        foreach ($data as $k=>$v){
            if(!isset($v)){
                return $k.'表单不能为空';
            }
        }

        $data['remove_forzen_bili'] = $request['remove_forzen_bili'];
        $data['remove_forzen_cycle'] = $request['remove_forzen_cycle'];

        if(empty($request['id'])){
            return '接收参数错误，请重试！';
        }

        $data['add_time']=strtotime($data['add_time'])?strtotime($data['add_time']):time();
        $data['end_time']=strtotime($data['end_time'])?strtotime($data['end_time']):time();
        $data['ctime']=time();

        $data['zhongchou_success_bili'] = $data['zhongchou_success_bili']/100;

        //如果有上传图片
        if(!empty($request['url1']))
        {
            $imgPath = upImgs($request['url1'],'zhongchou');
            if(empty($imgPath))
            {
                return '图片1上传失败,请重新提交';
            }
            $data['url1'] = $imgPath;
        }

        if(!empty($request['url2']))
        {
            $imgPath = upImgs($request['url2'],'zhongchou');
            if(empty($imgPath))
            {
                return '图片2上传失败,请重新提交';
            }
            $data['url2'] = $imgPath;
        }

        //文件上传
        if(($_FILES['wenjian_url']['size']>0)){
            $fileName = downloadFile($_FILES['wenjian_url']['tmp_name'],'upload/zhongchou/wenjian');
            if(empty($fileName))
            {
                return '文件上传失败,请重新提交';
            }
            $data['wenjian_url'] = $fileName;
        }

        $where['id'] = $request['id'];
        $did = $this->up($where,$data);
        if(!$did){
            $res ="服务器繁忙,请稍后重试";
            return $res;
        }
        $res = "修改众筹成功";
        return $res;

    }


    /**
     * ajax删除众筹
     */
    public function delZhongchou($request)
    {

        if(!isset($request['id'])){
            $data['status'] = 0;
            $data['msg'] = '传入参数有误！';
            return $data;
        }
        $where['id'] = $request['id'];
        $did = $this->del($where);
        if($did){
            $data['status'] = 1;
            $data['msg'] = '删除成功！';
        }else{
            $data['status'] = 0;
            $data['msg'] = '删除失败！';
        }

        return $data;
    }


    /**
     *  手动开始众筹
     */
    public function zhongchouStart($request)
    {
        if(empty($request['id'])){
            //abort(404);
            return ['status'=>0,'msg'=>'参数错误!'];
        }
        $where['id'] = $request['id'];
        $data['status'] = 0;
        $data['add_time'] = time();
        $data['end_time'] = time();

        $did = $this->up($where,$data);
        if($did){
            $res['status'] = 1;
            $res['msg'] = '开启众筹成功！';
        }else{
            $res['status'] = 0;
            $res['msg'] = '开启众筹失败！';
        }

        return $res;
    }

    /**
     * 手动结束众筹
     */
    public function zhongchouEnd($request)
    {
        if(empty($request['id'])){
            return ['status'=>0,'msg'=>'参数错误!'];
        }
        $where['id'] = $request['id'];
        $data['status'] = 3;
        $data['add_time'] = time();
        $data['end_time'] = time();

        $did = $this->up($where,$data);
        if($did){
            $res['status'] = 1;
            $res['msg'] = '结束众筹成功！';
        }else{
            $res['status'] = 0;
            $res['msg'] = '结束众筹失败！';
        }

        return $res;
    }


    /**
     * 根据id获取众筹奖励信息
     */
    public function getAwardsMessageById($request)
    {
        $where['id'] = $request['id'];
        $fields = 'id,zc_awards_currency_id,zc_awards_one_ratio,zc_awards_two_ratio,zc_awards_status';
        $order['id'] = 'desc';
        $data = $this->getOne($where, $fields , $order);
        return $data;
    }

    /**
     * 提交众筹奖励
     */
    public function saveAwards($request)
    {
        //dd($request['id']);
        $id = isset($request['id'])?intval($request['id']):'';
        if(empty($id)){
            return '参数错误!';
        }
        $where['id'] = $id;
        $data['zc_awards_currency_id'] = isset($request['zc_awards_currency_id'])?trim($request['zc_awards_currency_id']):'';
        $data['zc_awards_one_ratio'] = isset($request['zc_awards_one_ratio'])?trim($request['zc_awards_one_ratio']):'';
        $data['zc_awards_two_ratio'] = isset($request['zc_awards_two_ratio'])?trim($request['zc_awards_two_ratio']):'';
        $data['zc_awards_status'] = isset($request['zc_awards_status'])?trim($request['zc_awards_status']):'';
        if(empty($data['zc_awards_currency_id'])){
            return '请填写币种';
        }
        if(!isset($data['zc_awards_one_ratio'])){
            return '请填写一级比例';
        }
        if(!isset($data['zc_awards_two_ratio'])){

            return '请填写二级比例';
        }
        if(!isset($data['zc_awards_status'])){

            return '请填写是否开启状态';
        }
        if($request['zc_awards_one_ratio']>100 || $request['zc_awards_two_ratio'] > 100){
            return '比例不能大于100%';
        }

        $did = $this->up($where,$data);
        if(!$did){
            return '保存失败！';
        }

        return '保存成功！';


    }

    /**
     *
     * 获取众筹的id和title
     *
     */
    public function getZhongChouIdAndTitle()
    {
        $data = $this->select('id','title')->get();
        return $data;
    }

    /**
     * 解冻众筹by id
     */
    public function jiedongById($request)
    {

        if(empty($request['id'])){
            return ['status'=>0,'msg'=>'参数错误!'];
        }

        $where['id'] = $request['id'];

        $log_model =  new IssueLog();
        $log = $log_model->getOne($where);
        if($log['status']==1){
            return ['status'=>0,'msg'=>'该记录已处理!'];
        }

        //开启事务处理
        $this->start();

        try{

            $step[] = $log_model->up($where,['deal'=>0]);
            $step[] = $log_model->up($where,['add_time'=>time()]);
            $CurrencyUser_model = new CurrencyUser();
            //dd($log);
            $step[] = $CurrencyUser_model->where('member_id','=',$log['uid'])->where('currency_id','=',$log['cid'])->increment('num', $log['deal']);    //数量自增
            $step[] = $CurrencyUser_model->where('member_id','=',$log['uid'])->where('currency_id','=',$log['cid'])->decrement('forzen_num', $log['deal']);  //冻结数量自减
            $step[] = $log_model->up($where,['status'=>1]);

            $this->commit();
        }catch (\Exception $e){
            $this->rollBack();

            return ['status'=>0,'msg'=>'解冻众筹失败!'];
        }

        return ['status'=>1,'msg'=>'解冻众筹成功!'];

    }

    /**
     *
     * 解冻众筹by iid
     *
     */
    public function jiedongByIid($request)
    {

        //dd($_POST);
        if(empty($request['iid'])){
            return ['status'=>0,'msg'=>'请选择一个分类!'];
        }
        $where['iid'] = $request['iid'];
        $log_model =  new IssueLog();
        $log = $log_model->getList($where);

        $CurrencyUser_model = new CurrencyUser();


        //开启事务处理
        $this->start();

        try{

            foreach ($log as $k=>$v){
                if($v['status']==1){
                    continue;
                }

                $step[] = $log_model->up($where,['deal'=>0]);
                $step[] = $log_model->up($where,['add_time'=>time()]);
                $step[] = $CurrencyUser_model->where('member_id','=',$v['uid'])->where('currency_id','=',$v['cid'])->increment('num', $v['deal']);    //数量自增
                $step[] = $CurrencyUser_model->where('member_id','=',$v['uid'])->where('currency_id','=',$v['cid'])->decrement('forzen_num', $v['deal']);  //冻结数量自减
                $step[] = $log_model->up($where,['status'=>1]);

            }

            $this->commit();
        }catch (\Exception $e){
            $this->rollBack();

            return ['status'=>0,'msg'=>'解冻众筹失败!'];
        }

        return ['status'=>1,'msg'=>'解冻众筹成功!'];

    }


    /**
     * 开始解冻,修改所有未解冻账号时间为0，使解冻程序运行
     */
    public function jiedong_start($request)
    {

        if(empty($request['iid'])){
            return ['status'=>0,'msg'=>'请选择一个分类!'];
        }
        $iid = $request['iid'];
        $log_model =  new IssueLog();
        $log = $log_model->whereRaw("num=deal and iid=$iid")->get()->toArray();

        if(!$log){
            return ['status'=>0,'msg'=>'无对应记录!'];
        }
        $CurrencyUser_model = new CurrencyUser();

        //开启事务处理
        $this->start();
        try{

            foreach ($log as $k=>$v){
                $list=$this->getIssueRemoveForzenBiLiByIssueId($v['iid']);
                $v['remove_forzen_bili'] = $list['remove_forzen_bili']/100;    //解冻比例
                $v['remove_forzen_cycle'] = $list['remove_forzen_cycle'];      //解冻周期
                $r[] = $log_model->where('id','=',$v['id'])->decrement('deal', $v['num']*$v['remove_forzen_bili']);
                $r[] = $log_model->where('id','=',$v['id'])->update('addtime',time());

                $r[] = $CurrencyUser_model->where('member_id','=',$v['uid'])->where('currency_id','=',$v['cid'])->increment('num', $v['num']*$v['remove_forzen_bili']);
                $r[] = $CurrencyUser_model->where('member_id','=',$v['uid'])->where('currency_id','=',$v['cid'])->increment('forzen_num', $v['num']*$v['remove_forzen_bili']);
            }

            $this->commit();
        }catch (\Exception $e){
            $this->rollBack();
            return ['status'=>0,'msg'=>'无记录!'];
        }


            return ['status'=>1,'msg'=>'操作成功!'];

    }

    /**
     * 根据认筹id查找解冻比例
     * @param int $id Issue Id
     * @return 解冻比例
     */
    private function getIssueRemoveForzenBiLiByIssueId($id){

        $where['id'] = $id;
        $fields = 'is_forzen,remove_forzen_bili';
        $list = $this->getOne($where, $fields, $order = '');
        if($list['is_forzen']==0){    //0冻结，1可用
            return $list;
        }else{
            return 0;
        }

    }

    /******前台认购中心******/
    public function index($request)
    {
        $all = (new CommonController())->checkZhongchou();
        $list = DB::table('issue')
            ->join('currency','issue.currency_id','=','currency.currency_id')
            ->select('issue.*',DB::raw('xnb_currency.currency_name as name,xnb_currency.currency_logo as logo'))
            ->orderBy('end_time','desc')
            ->paginate(10);
        //算进度
        foreach ($list as $key=>$vo) {
            $bili=(($vo->num-$vo->deal)/$vo->num)*100;
            if ($bili<0.001) {
                $bili=0.001;
            }
            if ($bili>100) {
                $bili=100;
            }
            $list[$key]->bl = $bili;
            if ($vo->buy_currency_id == 0) {
                $list[$key]->currency_type = '人民币';
            } else {
                $list[$key]->currency_type = $vo->name;
            }
        }
        return $list;

    }

    /**
     * 众筹详情
     */
    public function details($request)
    {
        (new CommonController())->checkZhongchou();
        $list = DB::table('issue')
            ->select('issue.*',DB::raw('xnb_currency.currency_name as name'))
            ->join('currency','issue.currency_id','=','currency.currency_id')
            ->orderBy('ctime','desc')
            ->where('id',$request['id'])
            ->first();
        $currency = new Currency();
        $where['currency_id'] = $list->buy_currency_id;
        $list->buy_name = $list->buy_currency_id == 0 ? "人民币":$currency->getOne($where,'currency_name')['currency_name'];
        $list->buy_mark = $list->buy_currency_id == 0 ? "RMB":$currency->getOne($where,'currency_mark')['currency_mark'];
        $list->zhongchou_success_bili = $list->zhongchou_success_bili * 100;
        $uid = session('USER_KEY_ID');

        if (!empty($uid)){
            $list->buy_count  = $this->getIssuecountById($uid,$request['id']);
        }
        if(!$list){
//            $this->redirect('Public:404');
            return false;
        }
        $list->info = html_entity_decode($list->info);
        //查询个人记录
        $where2['uid'] = session('USER_KEY_ID');
        $where2['iid'] = $list->id;
        $issutLog = new IssueLog();
        $field = ['*','num*price as count'];
        $log = $issutLog->getList($where2,$field);
        $num_buy = $issutLog->sum($where2,'num');
        //查询账户余额
        if (!empty($uid)) {
            if ($list->buy_currency_id != 0) {
                $buy_num = (new CurrencyUser())->getOne(['member_id'=>$uid,'currency_id'=>$list->buy_currency_id],'num');
                $buy_num = $buy_num['num'];
            }else{
                $buy_num = (new Member())->getOne(['member_id'=>$uid],'rmb');
                $buy_num = $buy_num['rmb'];
            }
        }else{
            $buy_num = 0;
        }
        return [$list,$log,$num_buy,$buy_num];

    }



    //获取会员一次众筹有几次记录
    public function getIssuecountById($uid,$iid){
        if (empty($uid)){
            return 0;
        }
        $list = count((new IssueLog())->getList(['uid'=>$uid,'iid'=>$iid]));
        if($list){
            return $list;
        }else{
            return false;
        }
    }

    //处理众筹
    public function run($request){
        $num = intval($request['num']);
        $id = $request['id'];
        $uid = session('USER_KEY_ID');
        $buy_currency_id = $request['buy_currency_id'];
//    	$member=$this->member;
        $currencyUser = new CurrencyUser();
        $member = new Member();
        if ($buy_currency_id != 0) {
            $buy_num = $currencyUser->getOne(['member_id'=>$uid,'currency_id'=>$buy_currency_id],'num');
            $buy_num = $buy_num['num'];
        }else{
            $buy_num = $member->getOne(['member_id'=>$uid],'rmb');
            $buy_num = $buy_num['rmb'];
        }
//        $config=$this->config;
        $issue = (new Issue())->getOne(['id'=>$id]);
        $where['uid'] = session('USER_KEY_ID');
        $where['iid'] = $id;
        $issueLog = new IssueLog();
        $num_buy = $issueLog->sum($where,'num');

        //获取会员一次众筹有几次记录
        $count = $this->getIssuecountById($uid,$id);
        if($issue['limit_count'] <= $count){
            $data['status'] = 0;
            $data['info'] = '认筹次数不能超过限购次数!';
            return $data;
        }
        if($issue['limit'] < $num){
            $data['status'] = 0;
            $data['info'] = '认筹数量不能超过限购数量!';
            return $data;

        }
        if($issue['min_limit'] > $num){
            $data['status'] = 0;
            $data['info'] = '认筹数量不能小于最小认筹数量!';
            return $data;
        }
        if($issue['deal'] < $num){
            $data['status'] = 0;
            $data['info'] = '认筹数量不能超过剩余数量!';
            return $data;
        }
        if($issue['limit'] < $num_buy + $num){
            $data['status'] = 0;
            $data['info'] = '您已超过总认筹限购!';
            return $data;
        }
        if($buy_num < $num*$issue['price']){
            $data['status'] = 0;
            $data['info'] = '您的账户余额不足!';
            return $data;
        }
        //修改会员表人民币字段
//    	$rs=M('Member')->where("member_id=$uid")->setDec('rmb',$num*$issue['price']*$config['bili']);
        if($buy_currency_id != 0){
            $rs = CurrencyUser::where(['member_id'=>$uid,'currency_id'=>$issue['buy_currency_id']])->decrement('num',$num*$issue['price']);
        }else{
            $rs = Member::where('member_id',$uid)->decrement('rmb',$num*$issue['price']);
        }

        //添加财务日志表
        if($rs){
            //添加认购记录
            $arr['iid'] = $id;
            $arr['uid'] = $uid;
            $arr['cid'] = $issue['currency_id'];
            $arr['num'] = $num;
            $arr['deal'] = $num;
            $arr['price'] = $issue['price'];
            $arr['add_time'] = time();
            $arr['buy_currency_id'] = $buy_currency_id;
            $arr['status'] = 0;
            $issueLog->add($arr);
            //修改会员币种数量
            if ($issue['is_forzen'] == 0 ) {
                CurrencyUser::where(['member_id'=>$uid,'currency_id'=>$issue['currency_id']])->increment('forzen_num',$num);
            }else{
                CurrencyUser::where(['member_id'=>$uid,'currency_id'=>$issue['currency_id']])->increment('num',$num);

            }
            $trade = new Trade();
            $trade->addFinance($uid, 8, '众筹扣款'.$num*$issue['price'], $num*$issue['price'], 2, $buy_currency_id);
            $trade->addFinance($uid, 9, '众筹获取'.$num, $num, 1, $issue['currency_id']);
            //添加信息表
            (new MessageAll())->addMessage_all($uid, -2, '认购成功', '您参与的众筹项目'.$issue['title'].'已成功,扣除交易币'.$num*$issue['price'].',获取众筹币'.$num);
            //添加众筹推荐人员奖励
            if ($issue['zc_awards_status'] == 1 ) {
                $all = $this->setAwards($issue,$uid,$num);
            }
            $data['status'] = 0;
            $data['info'] = '认筹成功!';
            return $data;
        }else{
            //添加信息表
            (new MessageAll())->addMessage_all($uid, -2, '众筹失败', '您参与的众筹项目'.$issue['title'].'未成功');
            $data['status'] = 0;
            $data['info'] = '认筹失败!';
            return $data;
        }
    }

    public function setAwards($arr,$uid,$num){
        //查找此用户下推荐人 一代 二代
        $member = new Member();
        $currencyUser = new CurrencyUser();
        $u_info = $member->getOne(['member_id'=>$uid],['member_id','pid']);
        if(empty($u_info['pid'])){
            return true;
        }
        $one_info = $member->getOne(['member_id'=>$u_info['pid'],['status,member_id,pid']]);

        $one_where['member_id'] = $one_info['member_id'];
        $one_where['currency_id'] = $arr['zc_awards_currency_id'];
        $num = $arr['zc_awards_one_ratio']/100*$num;

        if ($one_info['status'] == 1 && $u_info['is_lock'] == 0 ) { //只有用户正常情况才会有推荐奖励
            $r = CurrencyUser::where($one_where)->increment('num',$num);
            (new Trade())->addFinance($one_info['member_id'], 12, '众筹推荐奖励'.$num, $num, 1, $arr['zc_awards_currency_id']);
        }

        if(empty($one_info['pid'])){
            return true;
        }
        $two_info = M('Member')->field('status,member_id,pid')->where("member_id = '{$one_info['pid']}'")->find();
        $two_info = $member->getOne(['member_id'=>$one_info['pid']],['status,member_id,pid']);
        $two_where['member_id'] = $two_info['member_id'];
        $two_where['currency_id'] = $arr['zc_awards_currency_id'];
        $num = $arr['zc_awards_two_ratio']/100*$num;
        if ($two_info['status'] == 1 && $two_info['is_lock'] == 0 ) {//只有用户正常情况才会有推荐奖励
            $r = M('Currency_user')->where($two_where)->setInc('num',$num);
            $r = CurrencyUser::where($two_where)->increment('num',$num);
            (new Trade())->addFinance($two_info['member_id'], 12, '众筹推荐奖励'.$num, $num, 1, $arr['zc_awards_currency_id']);
        }
        if($r){
            return true;
        }
    }

}