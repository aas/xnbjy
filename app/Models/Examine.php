<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Examine extends BaseModel
{
    public $timestamps = false;

    protected $table = "examine_pwdtrade";

    protected $guarded = [];

    public function getOne($where, $fileds = '*')
    {
        return $this->multiSelect($fileds)->multiWhere($where)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }

    function getPageNoList($totalPage, $pageNo, $pageSize)
    {
        $disPageList = 0;//定义最大链接显示数额
        $disPageList = $pageNo + $pageSize - 1;//最大链接显示数额赋值，公式为"当前页数 + 最大链接显示数额 -1"
        //前页面导航页数
        $pageNoList = array();
        //循环显示出当前页面导航页数
        if ($disPageList <= $totalPage) {
            for ($i = $pageNo; $i <= $disPageList; $i++) {
                $pageNoList[] = $i;
            }
        } else {
            if ($totalPage < $pageSize) {
                for ($i = 1; $i <= $totalPage; $i++) {
                    $pageNoList[] = $i;
                }
            } else {
                $start = $totalPage - $pageSize + 1;
                for ($i = $start; $i <= $totalPage; $i++) {
                    $pageNoList[] = $i;
                }
            }
        }
        return $pageNoList;
    }

    /**
     *
     * 审核列表
     */
    public function getExaminList($request)
    {

        $keywords = isset($request['u_id'])?trim($request['u_id']):'';         //关键词搜索
        $where = [];
//        if(!empty($keywords)){
//            $where['email'] = ['like','%'.$email.'%'];
//        }
        if (!empty($keywords)){
            $where['u_id'] = $keywords;
        }

        $order['id'] = 'desc';
        $rows = $this->getList($where, '*', $order, 10);
        if ($rows) {
            $rows = $rows->toArray();
            $rows['pageNoList'] = $this->getPageNoList($rows['last_page'], request('page', 1), 5);
            return $rows;
        }
    }

    //根据id获取单条详细数据
    public function getOneExamin($request)
    {
        $id = $request['id'];
        if(empty($id)){
            return '参数错误!';
        }
        $where['id'] = $id;
        $data = $this->getOne($where);
        return $data;

    }

    //修改审核状态
    public function saveExamin($request)
    {
        $where = [];
        $id = $request['id'];
        $status = $request['status'];
        $where['id'] = $id;
        if(empty($id)){
            return '参数错误!';
        }
        if($status!=0){
            $data['examine_time'] = time();
            $data['examine_name'] = session('admin_user');
        }

        $data['status'] = $status;
        $did = $this->up($where,$data);
        if(!$did){
            $res ="服务器繁忙,请稍后重试";
            return $res;
        }else{
            $messageall_model = new MessageAll();
            $list = $this->getOne($where);
            if($data['status']==1){       //审核通过后修改个人密码

                $member_data['pwdtrade'] = $list['pwdtrade'];
                $member_model = new Member();
                $member_model->up(['member_id'=>$list['u_id']],$member_data);
                //审核成功后增加个人消息提示

                $messageall_model->addMessage_all(
                    $list['u_id'],-2,
                    "您申请的修改支付密码已审核通过",
                    "您申请的修改支付密码已审核通过,审核通过时间为( ".date('Y-m-d H:i:s',$data['examine_time'])." )"
                );
            }
            if($data['status']==2){
                //审核未通过后添加个人消息提示

                $messageall_model->addMessage_all(
                    $list['u_id'],-2,
                    "很抱歉您申请的修改支付密码审核未通过",
                    "很抱歉您申请的修改支付密码审核未通过,请重新审核。或联系客服"
                );
            }

            return '审核成功!';

        }


    }


}
