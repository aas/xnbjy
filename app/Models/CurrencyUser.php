<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

/**
 * Created by PhpStorm.
 * User: zxj
 * Date: 2017/9/20
 * Time: 12:53
 */
class CurrencyUser extends BaseModel
{
    public $timestamps = false;

    protected $table = "currency_user";

    protected $guarded = [];

    public function getOne($where, $fileds = '*')
    {
        return $this->multiSelect($fileds)->multiWhere($where)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }


    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getWithCurrencyList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->with('currency')->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->with('currency')->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    public function currency()
    {
        return $this->hasOne('App\Models\Currency', 'currency_id', 'currency_id');
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }

    public function inc($where = [], $field)
    {
        return $this->_inc($where, $field);
    }

    public function dec($where = [], $field)
    {
        return $this->_dec($where, $field);
    }


    //连表查看币种信息(currencyUser表和Currency表)
    public function getCurrencyUserWithCurrency($request)
    {
        $where['member_id'] = $request['member_id'];
        $order['member_id'] = 'desc';
        $rows = $this->getWithCurrencyList($where, '*', $order, 10);
        if ($rows) {
            $rows = $rows->toArray();
            $rows['pageNoList'] = $this->getPageNoList($rows['last_page'], request('page', 1), 5);

        }
        return $rows;
    }


    public function getMemberCurrencyPmByCurrencyId($currency)
    {
        $where['currency_id'] = $currency;
        $list = $this->select('*')->orderBy('num', 'desc')->where($where)->limit(200)->get();
        $list = array_map('get_object_vars', $list);
        $member_model = new Member();
        foreach ($list as $k => $v) {
            $list[$k]['member'] = $member_model->getMemberAllbyMemberId($v['member_id']);
            $list[$k]['allmoney'] = $v['num'] + $v['forzen_num'];
        }
        return $list;
    }

    public function getUserCurrencyData()
    {
        $prefix = config('database.connections.mysql.prefix');
        $where['member_id'] = session('USER_KEY_ID');
        $fields = "{$prefix}currency_user.*,({$prefix}currency_user.num+{$prefix}currency_user.forzen_num) as count,{$prefix}currency.currency_name,{$prefix}currency.currency_mark";
        $data['currency_user'] = $this->select(DB::raw($fields))
            ->leftJoin('currency', 'currency.currency_id', '=', 'currency_user.currency_id')
            ->where('currency_user.member_id',session('USER_KEY_ID'))->orderBy('sort', 'desc')->get()->toArray();
        $data['allmoneys'] = 0;
        $trade = new Trade();
//        dd($data['currency_user']);
        foreach ($data['currency_user'] as $k => $v) {
            $Currency_message = $trade->getCurrencyMessageById($v['currency_id']);
            $allmoney = $data['currency_user'][$k]['count'] * $Currency_message['new_price'];
            $data['allmoneys'] += $allmoney;
        }
        return $data;
    }

    //获取个人账户指定币种金额
    public function getUserMoneyByCurrencyId($currencyId){
        $where['member_id'] = session('USER_KEY_ID');
        $where['currency_id'] = $currencyId;
        return $this->_getOne($where,'num,forzen_num,chongzhi_url');
    }

}
