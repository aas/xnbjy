<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;

class Manage extends BaseModel
{
    public $timestamps = false;

    protected $table = "admin";

    protected $guarded = [];

    public function getOne($where, $fileds = '*', $order = '')
    {
        if (!$order) {
            $order = ['id' => 'desc'];
        }
        return $this->multiSelect($fileds)->multiWhere($where)->multiOrder($order)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     * 获取全部数据
     * @param array $where
     * @param string $fields
     * @param array $order
     * @return mixed
     */
    public function getAll($where = [], $fields = "*", $order = ['created_at' => 'desc'])
    {
        return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }

    //判断权限
    private function auth(){
        $admin=$this->admin;
        if($admin['admin_id']!=1){
            $this->error ='只有超级管理员可以进入此页面!';
            return false;
        }
    }

    public function add_admin($request)
    {
        if($request->isMethod('post')){
            $data['username'] = $request['username'];
            $data['password'] = md5($request['password']);
            $data['pwd_show'] = $request['pwd_show'];
            $re = Manage::where('username',$data['username'])->get();
            if ($re->isEmpty()) {
              $result =  $this->add($data);
              if ($result) {
                  $this->error = '添加成功！';
                  return false;
              }
                $this->error = '添加失败！';
                return false;
            }
            $this->error = '该用户名称已存在！';
            return false;
        }
    }

    public function edit_admin($request)
    {
        $all = Manage::where('admin_id',$request['admin_id'])->first();
        if ($all['username'] == $request['username'] && $request['password'] == "" && $all['pwd_show'] == $request['pwd_show']) {
            $this->error = '请填写修改信息！';
            return false;
        }
        $data['username'] = $request['username'];
        if (!empty($request['password'])) {
            $data['password'] = md5($request['password']);
            $data['pwd_show'] = $request['password'];
        }
        $where['admin_id'] = $request['admin_id'];
        $result = $this->up($where,$data);
        if ($result) {
            $this->error = '修改成功！';
            return false;
        }
        $this->error = '修改失败！';
        return false;
    }

    /**
     * 权限修改
     */
    public function auth_admin($request)
    {
            if ($request['admin_id'] && !empty($request['nav'])) {
                $data['nav'] = implode(',', $request['nav']);
            } else {
                $data['nav'] = "";
            }
            $data['admin_id'] = $request['admin_id'];
            $where['admin_id'] = $request['admin_id'];
            $rs = $this->up($where, $data);
            if ($rs) {
                $this->error = '修改成功!';
                return false;
            } else {
                $this->error = '修改失败!';
                return false;
            }


    }

    /**
     * 密码修改
     */
    public function edit_pwd($request)
    {
          $admin_id = Session('admin_userid');
          $re = Manage::where('admin_id',$admin_id)->first();
          $old_pwd = md5($request['old_pwd']);
          if ($old_pwd !== $re['password']) {
              $this->error = '原始密码错误!';
              return false;
          }
          $where['admin_id'] = $admin_id;
          $data['password'] = md5($request['password']);
          $result = $this->up($where,$data);
          if ($result) {
              $this->error = '修改成功!';
              return false;
          }
        $this->error = '修改失败!';
        return false;
    }

}
