<?php

namespace App\Models;

use App\Http\Requests\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class Download extends BaseModel
{
    /**
     * 表格上传
     */
    public function biaogeDownload($request)
    {
        $file = $request->file('biaoge');//获取上传文件
        if (isset($file)) {
            $path = "upload/biaoge/" . date('Y-m-d', time()) . "/";//定义路径
            $extension = $file->getClientOriginalExtension(); //获取上传图片的后缀名
            if (!in_array($extension, array('xls', 'xlsx'))) {
                $res['state'] = 400;
                $res['msg'] = '文件格式错误';
                return $res;
            }
            $fileName = str_random(4) . '新币申请表';//重命名
            $filePath = $file->move($path, $fileName); //使用move方法移动文件.
            $url = '/' . $filePath;
            $result = DB::table('config')->where('key', 'biaoge_url')->update(['value' => $url]);
            if ($result) {
                $res['state'] = 200;
                $res['msg'] = '上传成功！';
                return $res;
            }
            $res['state'] = 400;
            $res['msg'] = '上传失败！';
            return $res;
        }

        $res['state'] = 400;
        $res['msg'] = '上传文件不能为空！';
        return $res;
    }

    /**
     * 钱包上传
     */
    public function qianbaoDownload($request)
    {

        $file = $request->file();//获取上传文件

        if (!empty($file)) {
            foreach ($file as $k => $v) {
                $file = $v;
                $path = "upload/qianbao/" . date('Y-m-d', time()) . "/";//定义路径
                $extension = $file->getClientOriginalExtension(); //
                if (!in_array($extension, array('zip', 'rar', 'CAB', 'ISO'))) {
                    $res['state'] = 400;
                    $res['msg'] = '文件格式错误';
                    return $res;
                }
                $fileName = str_random(8) . '.' . $extension;//重命名
                $filePath = $file->move($path, $fileName); //使用move方法移动文件.
                $url = '/' . $filePath;
                $result = DB::table('currency')->where('currency_id', $k)->update(['qianbao_url' => $url]);
            }
            if ($result) {
                $res['state'] = 200;
                $res['msg'] = '上传成功！';
                return $res;
            }
            $res['state'] = 400;
            $res['msg'] = '上传失败！';
            return $res;
        }
            $res['state'] = 400;
            $res['msg'] = '钱包上传不能为空！';
            return $res;

    }

    /**
     * 币种官方网站
     */
    public function guanwangUrl($request)
    {
        //返回带有currency_id的一维数组
        $guanwang_url = $request['guanwang_url'];
        foreach ($guanwang_url as $k => $v) {
            if ($v != '') {
                $data[$k] = $v;
            }else{
                $res['state'] = 400;
                $res['msg'] = '提交网址都不为空！';
                return $res;
            }

        }
        foreach ($data as $k=>$v) {
            $pat = "/http:[\/]{2}[a-z]+[.]{1}[a-z\d\-]+[.]{1}[a-z\d]*[\/]*[A-Za-z\d]*[\/]*[A-Za-z\d]*/";
            $re = preg_match($pat,$v);
            if (!$re) {
                $res['state'] = 400;
                $res['msg'] = '网站地址格式错误！';
                return $res;
            }
            $result[] = DB::table('currency')->where('currency_id', $k)->update(['guanwang_url' => $v]);
        }

        if ($result) {
            $res['state'] = 200;
            $res['msg'] = '操作成功！';
            return $res;
        }
        $res['state'] = 400;
        $res['msg'] = '操作失败！';
        return $res;

    }
}