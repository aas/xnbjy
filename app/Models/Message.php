<?php

namespace App\Models;

class Message extends BaseModel
{
    public $timestamps = false;

    protected $table = "message";

    protected $guarded = [];

    public function getOne($where, $fileds = '*')
    {
        return $this->multiSelect($fileds)->multiWhere($where)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }


    /**
     * 系统消息列表
     */
    public function getMessageList($request)
    {
        $where = [];
        $field_name = [];
        $keywords = trim($request['keywords']) ? trim($request['keywords']) : '';   //关键词搜索
        if ($keywords) {
            if (is_numeric($keywords)) {
                $where['id'] = $keywords;
                $keywords = '';
                //$field_name = 'id';
            } else {
                $where['title'] = ['like', '%' . $keywords . '%'];
                $field_name = 'title';
            }
        }

        $rows = MessageAll::Join('message_category','message_all.type','=','message_category.id')->where($field_name,'like','%'.$keywords.'%')->select('message_all.*','message_category.name')->orderBy('add_time','desc')->paginate(3);

        if ($rows) {
            $rows = $rows->toArray();
            $rows['pageNoList'] = $this->getPageNoList($rows['last_page'], request('page', 1), 5);
            return $rows;
        }


    }

    /**
     *
     * 新增消息
     *
     */
    public function addMessage($request)
    {
         $radios = trim($request['radios']);    //one为个人.all为群发
         $type = trim($request['type']);        //消息类型
         $title = trim($request['title']);
         $content = trim($request['content']);

        if(empty($radios)||empty($type)||empty($title)||empty($content))
        {
            return '字段不能为空!';
        }

        //判断个人还是全体
        if($radios == 'one'){
            $data['u_id'] = trim($request['u_id']);
        }else{
            $data['u_id'] = -1; //-1 状态代表群发
        }
        $data['type'] = $type;
        $data['title'] = $title;
        $data['content'] = $content;
        $data['add_time'] = time();

        $did = $this->add($data);
        if(!$did){
            $res ="服务器繁忙,请稍后重试";
            return $res;
        }
        $res = "添加用户成功";
        return $res;

    }

    /**
     * @param $data
     * @return string
     * 修改消息
     */
    public function saveMessage($request)
    {
        $data['bank_name'] = trim($request['bank_name']);
        $data['bank_adddress'] = trim($request['bank_adddress']);
        $data['bank_no'] = trim($request['bank_no']);
        $data['status'] = trim($request['status']);


        $message = $this->checkFileds($data);     //验证字段是否为空,如果有返回值则验证不通过
        if ($message) {
            return $message;
        }

        $where['bank_id'] = $request['bank_id'];
        $did = $this->up($where,$data);
        if(!$did){
            $res ="服务器繁忙,请稍后重试";
            return $res;
        }
        $res = "修改银行卡成功";
        return $res;

    }

    /**
     *
     * ajax删除银行卡
     *
     */
    public function delOneMessage($request)
    {

        if(!isset($request['id'])){
            $data['status'] = 0;
            $data['msg'] = '传入参数有误！';
            return $data;
        }
        $where['message_all_id'] = $request['id'];
        $did = $this->del($where);
        if($did){
            $data['status'] = 1;
            $data['msg'] = '删除成功！';
        }else{
            $data['status'] = 0;
            $data['msg'] = '删除失败！';
        }

        return $data;
    }

    /**
     * @param $data
     * @return string
     * 查询单个银行卡
     */
    public function getOneBank($request)
    {
        $where['bank_id'] = $request['bank_id'];
        $data = $this->getOne($where);
        return $data;
    }


    //验证表单字段是否为空
    public function checkFileds($data)
    {

        if (!$data['bank_name']) {
            return '收款人不能为空';
        }
        if (!$data['bank_adddress']) {
            return '开户行不能为空';
        }
        if (!$data['bank_no']) {
            return '账号不能为空';
        }

    }

    public function category()
    {
        return $this->belongsTo('App\Models\MessageCategory','type','id');
    }

    public function getMessageWithCategory($where,$order=[],$pageSize=0)
    {
        if ($pageSize) {
            return $this->with('category')
                ->multiWhere($where)
                ->multiOrder($order)->paginate($pageSize);
        }
        return $this->with('category')
            ->multiWhere($where)
            ->multiOrder($order)->first();
    }


}
