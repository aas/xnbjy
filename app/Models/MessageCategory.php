<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MessageCategory extends BaseModel
{
    public $timestamps = false;

    protected $table = "message_category";

    protected $guarded = [];

    public function getOne($where, $fileds = '*')
    {
        return $this->multiSelect($fileds)->multiWhere($where)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }


    function getPageNoList($totalPage, $pageNo, $pageSize)
    {
        $disPageList = 0;//定义最大链接显示数额
        $disPageList = $pageNo + $pageSize - 1;//最大链接显示数额赋值，公式为"当前页数 + 最大链接显示数额 -1"
        //前页面导航页数
        $pageNoList = array();
        //循环显示出当前页面导航页数
        if ($disPageList <= $totalPage) {
            for ($i = $pageNo; $i <= $disPageList; $i++) {
                $pageNoList[] = $i;
            }
        } else {
            if ($totalPage < $pageSize) {
                for ($i = 1; $i <= $totalPage; $i++) {
                    $pageNoList[] = $i;
                }
            } else {
                $start = $totalPage - $pageSize + 1;
                for ($i = $start; $i <= $totalPage; $i++) {
                    $pageNoList[] = $i;
                }
            }
        }
        return $pageNoList;
    }

    /**
     *
     * 所有消息列表
     *
     */
    public function getMessageCategoryList()
    {

        $where = [];
        $order['id'] = 'desc';
        $rows = $this->getList($where, '*', $order, 3);
        if ($rows) {
            $rows = $rows->toArray();
            $rows['pageNoList'] = $this->getPageNoList($rows['last_page'], request('page', 1), 3);
            return $rows;
        }

    }

}
