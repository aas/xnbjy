<?php
namespace App\Models;

class SmsLog extends BaseModel
{

    protected $table = 'sms_log';
    public $timestamps = false;

    const DEFAULT_CONNECT_SMS_TIME = 90;

    const DEFAULT_CONNECT_SMS_PHONE = 5;

    const DEFAULT_CONNECT_SMS_IP = 20;

    const DEFAULT_CONNECT_SMS_USED_TIME = 30;

    public function __construct()
    {
        parent::__construct();
    }

    public function getOne($where, $fileds = '*', $order = '')
    {
        if (!$order) {
            $order = ['id' => 'desc'];
        }
        return $this->multiSelect($fileds)->multiWhere($where)->multiOrder($order)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }


    /**
     * 增加短信记录
     *
     * @param
     * @return int
     */
    public function addSms($data)
    {
        return $this->add($data);
    }

    /**
     * 查询单条记录
     *
     * @param
     * @return array
     */
    public function getSmsInfo($where)
    {
        if (empty($where)) {
            return false;
        }
        return $this->getOne($where);
    }

    /**
     * 查询记录
     *
     * @param
     * @return array
     */
    public function getSmsList($where = array())
    {
        return $this->getList($where, '*', ['id' => 'desc']);
    }

    /**
     * 取得记录数量
     *
     * @param
     * @return int
     */
    public function getSmsCount($where)
    {
        return $this->where($where)->count();
    }

    /**
     * 获取手机短信验证码
     */
    public function getSmsCaptcha($request)
    {
        $ajaxData = ['state' => false, 'msg' => '验证码或手机号码不正确'];
        $phone = $request['phone'];
        $logType = $request['type'];//短信类型:1预约成功,2绑定就诊人,3改签,4充值成功 5.绑定就诊卡
        if (strlen($phone) == 11) {
            $ajaxData = $this->sendCaptcha($phone, $logType);
        }
        return $ajaxData;
    }

    /**
     * 验证验证码
     * @param string $phone
     * @param string $captcha
     * @param string $log_type
     * @return array
     */
    public function checkSmsCaptcha($request)
    {
        $state = 1;
        $msg = '手机验证码验证成功';
        $where['log_phone'] = $request['phone'];;
        $where['log_captcha'] = $request['captcha'];;
        $where['log_type'] = intval($request['type']);
        $smsLog = new SmsLog();
        $res = $smsLog->getSmsInfo($where);
        if (empty($res) || ($res['add_time'] < time() - 1800)) {//半小时内进行验证为有效
            $state = 0;
            $msg = '验证码错误或已过期，重新输入';
        }
        $ajaxData = ['state' => $state, 'msg' => $msg];
        return $ajaxData;
    }


    /**
     * 发送手机动态码
     * @param string $phone
     * @param string $logType 短信类型:1预约成功,2绑定就诊人,3改签,4充值成功
     * @return array
     */
    public function sendCaptcha($phone, $logType, $dataArr = [], $uid = 0)
    {
        $state = true;
        $msg = '手机动态码发送成功';
        $sms_log = $this->ipCaptcha($logType);
        $now = time();
        if (!empty($sms_log) && ($sms_log['add_time'] > $now - self::DEFAULT_CONNECT_SMS_TIME)) {//同一IP[n]秒内只能发一条短信
            $state = false;
            $msg = '同一IP地址' . self::DEFAULT_CONNECT_SMS_TIME . '秒内，请勿多次获取验证码！';
        }
        $where['log_phone'] = $phone;
        $where['log_type'] = $logType;
        $sms_log = $this->getSmsInfo($where);
        if ($state && !empty($sms_log) && ($sms_log['add_time'] > $now - self::DEFAULT_CONNECT_SMS_TIME)) {//同一手机号IP[n]秒内只能发一条短信
            $state = false;
            $msg = '同一手机号' . self::DEFAULT_CONNECT_SMS_TIME . '秒内，请勿多次获取验证码！';
        }
        $time24 = $now - 60 * 60 * 24;
        $where = [];
        $where['log_phone'] = $phone;
        $where['add_time'] = ['<', $time24];
        $num = $this->getSmsCount($where);
        if ($state && $num >= self::DEFAULT_CONNECT_SMS_PHONE) {//同一手机号24小时内只能发5条短信
            $state = false;
            $msg = '同一手机号24小时内，请勿多次获取验证码！';
        }
        $where = [];
        $where['log_ip'] = request()->ip();
        $where['add_time'] = ['<', $time24];
        $num = $this->getSmsCount($where);
        if ($state && $num >= self::DEFAULT_CONNECT_SMS_IP) {//同一IP24小时内只能发20条短信
            $state = false;
            $msg = '同一IP24小时内，请勿多次获取验证码！';
        }
        if ($state == true) {
            $logData = [];
            $captcha = rand(100000, 999999);
            $logMsg = [];
            //短信类型:1预约成功,2绑定就诊人,3改签,4充值成功
            switch ($logType) {
                case 1:
                    $appointmentInfo = $dataArr;
                    //您已成功预约{1}的{2}医生，预约时间：{3}。
                    $logMsg = [$appointmentInfo['depart_name'], $appointmentInfo['doctor_name'], date('Y-m-d H:i', $appointmentInfo['mark_time'])];
                    break;
                case 2:
                    $member = new Member();
                    $memberInfo = $member->getOne(['mobile' => $phone]);
                    if ($memberInfo) {
                        $state = false;
                        $msg = '当前手机号已被绑定，请更换其他号码。';
                    }
                    //您的验证码为{1}，请于{2}分钟内正确输入，如非本人操作，请忽略此短信。
                    $logMsg = [$captcha, self::DEFAULT_CONNECT_SMS_USED_TIME];
                    break;
                case 3:
                    //您已改签成功，预约{1}的{2}医生，预约时间：{3}。
                    $appointmentInfo = $dataArr;
                    $logMsg = [$appointmentInfo['depart_name'], $appointmentInfo['doctor_name'], date('Y-m-d H:i', $appointmentInfo['mark_time'])];
                    break;
                case 4:
                    $chargeInfo = $dataArr;
                    $res = apiCurl('memberAccountQuery', $chargeInfo['card_no']);
                    $money = $res['基本余额'] ? $res['基本余额'] : 0;
                    //充值成功，您的就诊卡{1}充值金额{2}，卡内余额{3}。
                    $logMsg = [$chargeInfo['card_no'], $chargeInfo['money'], $money];
                    break;
                case 5:
                    //验证码为{1}，您正在绑定就诊卡，请于{2}分钟内正确输入，请确认是本人操作。
                    $logMsg = [$captcha, self::DEFAULT_CONNECT_SMS_USED_TIME];
                    break;
                default:
                    $state = false;
                    $msg = '参数错误';
                    break;
            }
            if ($state == true) {
                $logData['user_id'] = $uid ? $uid : request('mid', 0);
                $smsTmpl = new SmsTmpl();
                $tmpl = $smsTmpl->getSmsTmplById($logType);
                $result = sendSMS($phone, $logMsg, $tmpl['tmpl_id']);
                if ($result) {
                    $logData['log_phone'] = $phone;
                    $logData['log_captcha'] = $captcha;
                    $logData['log_ip'] = request()->ip();
                    $logData['log_msg'] = json_encode($logMsg);
                    $logData['log_type'] = $logType;
                    $logData['add_time'] = $now;
                    $this->addSms($logData);
                } else {
                    $state = false;
                    $msg = '手机短信发送失败';
                }
            }
        }
        $state_data = [
            'state' => $state,
            'sms_time' => self::DEFAULT_CONNECT_SMS_TIME,
            'msg' => $msg
        ];
        return $state_data;
    }

    /**
     * 按IP查询手机验证码
     * @param string $logType
     * @return array
     */
    public function ipCaptcha($logType = '')
    {
        $where = array();
        $where['log_ip'] = request()->ip();
        $logType = intval($logType);
        if ($logType > 0) {
            $where['log_type'] = $logType;//短信类型:1预约成功,2绑定就诊人,3改签,4充值成功
        }
        $res = $this->getSmsInfo($where);
        return $res;
    }
}
