<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Arttype extends BaseModel
{
    public $timestamps = false;

    protected $table = "article_category";

    protected $guarded = [];

    public function getOne($where, $fileds = '*')
    {
        return $this->multiSelect($fileds)->multiWhere($where)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     * 类型表和文章表关联
     */
    public function ArttypewithArtList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->with('article')->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->with('article')->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    public function article()
    {
        return $this->hasMany('App\Models\Art','position_id','id');
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }

    //获取当前帮助分类列表
    public function getArttypeHelpList($request)
    {
        $data = $this->getList(['parent_id'=>6])->toArray();
        return $data;
    }

    //添加当前帮助分类
    public function AddArttypeHelp($request)
    {

        $name = isset($request['name'])?trim($request['name']):'';
        $keyword = isset($request['keyword'])?trim($request['keyword']):'';
        $sort = isset($request['sort'])?trim($request['sort']):'';

        if(!isset($name)||!isset($keyword)||!isset($sort)){
            $res['status'] = 0;
            $res['msg'] = '表单填写不完整!';
            return $res;
        }

        $data['name'] = $name;
        $data['keywords'] = $keyword;
        $data['sort'] = $sort;
        $data['parent_id'] = 6;

        $did = $this->add($data);
        if(!$did){
            $res['status'] = 0;
            $res['msg'] = '服务器繁忙,请稍后重试!';
            return $res;
        }

        $res['status'] = 1;
        $res['msg'] = '添加成功!';
        return $res;

    }

    //根据id获取当前分类
    public function getOneArttype($request)
    {
        $id = isset($request['id'])?trim($request['id']):'';
        $info = $this->getOne(['id'=>$id]);
        return $info;
    }

    //根据id修改当前分类
    public function EditArttypeHelp($request)
    {

        $name = isset($request['name'])?trim($request['name']):'';
        $keyword = isset($request['keyword'])?trim($request['keyword']):'';
        $sort = isset($request['sort'])?trim($request['sort']):'';
        $fenlei_id = isset($request['fenlei_id'])?trim($request['fenlei_id']):'';

        if(!isset($fenlei_id)){
            $res['status'] = 0;
            $res['msg'] = '分类参数有误!';
            return $res;
        }

        if(!isset($name)||!isset($keyword)||!isset($sort)){
            $res['status'] = 0;
            $res['msg'] = '表单填写不完整!';
            return $res;
        }

        $data['name'] = $name;
        $data['keywords'] = $keyword;
        $data['sort'] = $sort;
        $data['parent_id'] = 6;

        $did = $this->up(['id'=>$fenlei_id],$data);
        if(!$did){
            $res['status'] = 0;
            $res['msg'] = '服务器繁忙,请稍后重试!';
            return $res;
        }

        $res['status'] = 1;
        $res['msg'] = '修改成功!';
        return $res;

    }

    //删除分类
    public function delOneArttype($request)
    {

        $id = isset($request['id'])?trim($request['id']):'';
        if(empty($id)){
            $res['status'] = 0;
            $res['msg'] = '传入参数有误！';
            return $res;
        }
        $where['id'] = $id;
        $did = $this->del($where);
        if($did){
            $res['status'] = 1;
            $res['msg'] = '删除成功！';
        }else{
            $res['status'] = 0;
            $res['msg'] = '删除失败！';
        }
        return $res;

    }

}
