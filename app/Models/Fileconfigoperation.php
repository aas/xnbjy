<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Fileconfigoperation extends BaseModel
{

    /**
     * 后台入口操作
     */
    public function saveEntrance_edit($request)
    {
        if ($request->isMethod('post')) {
           $res['state'] = 0;
           $res['msg'] = '此功能暂未开放...';
           return $res;
        }

    }

    /**
     * 数据库入口--表前缀修改
     */
    public function saveDb_edit($request)
    {
        if ($request->isMethod('post')) {
            $res['state'] = 0;
            $res['msg'] = '此功能暂未开放...';
            return $res;
        }

    }

}
