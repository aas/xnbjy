<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fill extends BaseModel
{
    public $timestamps = false;

    protected $table = "fill";

    protected $guarded = [];

    public function getOne($where, $fileds = '*')
    {
        return $this->multiSelect($fileds)->multiWhere($where)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }


    /**
     * 网银支付日志
     */
    public function getFillList()
    {
        $where['uid'] = session('USER_KEY_ID');       //$_SESSION['USER_KEY_ID'];
        $order['id'] = 'desc';
        $list = $this->getList($where, $fields = '*', $order = '', 5);
        if ($list) {
            $list = $list->toArray();
            $list['pageNoList'] = $this->getPageNoList($list['last_page'], request('page', 1), 5);
            return $list;
        }
    }


}
