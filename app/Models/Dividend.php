<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Dividend extends BaseModel
{
    public $timestamps = false;

    protected $table = 'dividend_config';

    protected $guarded = [];

    public function getOne($where, $fileds = '*', $order = '')
    {
        if (!$order) {
            $order = ['bank_id' => 'desc'];
        }
        return $this->multiSelect($fileds)->multiWhere($where)->multiOrder($order)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }


    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }


    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }


    public function indexEdit($request)
    {

        $re = Dividend::get();
        foreach ($re as $k=>$v)
        {
            $rst[$v['name']] = $v['value'];
        }
        if($rst['dividend_id'] !== $request['dividend_id'] || $rst['num1'] !== $request['num1'] || $rst['num2'] !==$request['num2'] || $rst['num3'] !== $request['num3'] || $rst['num4'] !== $request['num4'] || $rst['money1'] !== $request['money1'] || $rst['money2'] !== $request['money2'] || $rst['money3'] !== $request['money3']  )
        {
            $data['dividend_id'] = $request['dividend_id'];
            $data['num1'] = $request['num1'];
            $data['num2'] = $request['num2'];
            $data['num3'] = $request['num3'];
            $data['num4'] = $request['num4'];
            $data['money1'] = $request['money1'];
            $data['money2'] = $request['money2'];
            $data['money3'] = $request['money3'];
            foreach ($data as $k=>$v){
                $result[] = DB::table('dividend_config')
                    ->where('name',$k)
                    ->update(['value' => $v]);
            }
            if($result){
                $res['state'] = 200;
                $res['msg'] = '配置修改成功！';
                return $res;

            }
                $res['state'] = 400;
                $res['msg'] = '配置修改失败！';
                return $res;

        }

        $res['state'] = 400;
        $res['msg'] = '已配置信息未进行修改！';
        return $res;
    }


}
