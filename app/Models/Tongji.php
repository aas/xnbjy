<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;


class Tongji extends BaseModel
{
    public $timestamps = false;

    //protected $table = "admin";

    protected $guarded = [];

    public function getOne($where, $fileds = '*')
    {
        return $this->multiSelect($fileds)->multiWhere($where)->first();
    }

    /**
     * 按条件查询全部数据,根据配置显示条数显示
     */
    public function getList(array $where = [], $fields = '*', $order = '', $pageSize = '')
    {
        if ($pageSize) {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->paginate($pageSize);
        } else {
            return $this->multiSelect($fields)->multiWhere($where)->multiOrder($order)->get();
        }
    }

    /**
     *插入数据
     */
    public function add($data)
    {
        return $this->_add($data);
    }


    /***
     * @param array $id
     * @param array $data
     * 更新数据
     */
    public function up($where, $data)
    {
        return $this->_updata($where, $data);
    }

    /**
     * @param $id
     * 删除数据
     */
    public function del($where)
    {
        return $this->_del($where);
    }


    //根据id获取email
    public function getMemberEmailById($id)
    {

        $data = DB::select("select email from xnb_member where member_id = $id limit 1");
        if(!empty($data)){
            return $data[0]->email;
        }

        return '';
    }

    //获取所有排名
    public function getAllPaiming()
    {

        $data = DB::table('member')->select(DB::raw('count(member_id) as num,pid'))->groupBy('pid')->orderBy('num','desc')->get();
        //去掉pid为空的
        $arr = [];
        foreach($data as $k=>$v)
        {
            if(!empty($v->pid))
            {
                $arr[] = $v;
            }

        }

        //生成一个新数组,包含num,pid,email三项
        foreach($arr as $k=>$v)
        {
            $email = $this->getMemberEmailById($v->pid);
            $v->email = $email;
        }
        return $arr;

    }

    //获取最新月排名
    public function getMonth()
    {
        //获取本月第一天/最后一天的时间戳
        $year = date("Y");
        $month = date("m");
        $allday = date("t");
        $start_time = strtotime($year."-".$month."-1");
        $end_time = strtotime($year."-".$month."-".$allday);

        $data = DB::table('member')->select(DB::raw('count(member_id) as num,pid'))->whereBetween('reg_time',[$start_time,$end_time])->groupBy('pid')->orderBy('num','desc')->get();

        //去掉pid为空的
        $arr = [];
        foreach($data as $k=>$v)
        {
            if(!empty($v->pid))
            {
                $arr[] = $v;
            }

        }

        //生成一个新数组,包含num,pid,email三项
        foreach($arr as $k=>$v)
        {
            $email = $this->getMemberEmailById($v->pid);
            $v->email = $email;
        }
        return $arr;

    }

    //获取最新周排名
    public function getZhou()
    {

        //获取本周第一天/最后一天的时间戳
        $year = date("Y");
        $month = date("m");
        $day = date('w');
        $nowMonthDay = date("t");
        $firstday = date('d') - $day;
        if(substr($firstday,0,1) == "-"){
            $firstMonth = $month - 1;
            $lastMonthDay = date("t",$firstMonth);
            $firstday = $lastMonthDay - substr($firstday,1);
            $time_1 = strtotime($year."-".$firstMonth."-".$firstday);
        }else{
            $time_1 = strtotime($year."-".$month."-".$firstday);
        }

        $lastday = date('d') + (7 - $day);
        if($lastday > $nowMonthDay){
            $lastday = $lastday - $nowMonthDay;
            $lastMonth = $month + 1;
            $time_2 = strtotime($year."-".$lastMonth."-".$lastday);
        }else{
            $time_2 = strtotime($year."-".$month."-".$lastday);
        }

        $data = DB::table('member')->select(DB::raw('count(member_id) as num,pid'))->whereBetween('reg_time',[$time_1,$time_2])->groupBy('pid')->orderBy('num','desc')->get();

        //去掉pid为空的
        $arr = [];
        foreach($data as $k=>$v)
        {
            if(!empty($v->pid)){
                $arr[] = $v;
            }

        }

        //生成一个新数组,包含num,pid,email三项
        foreach($arr as $k=>$v)
        {
            $email = $this->getMemberEmailById($v->pid);
            $v->email = $email;
        }
        return $arr;

    }


    //不同币种的充值统计
    public function getCurrencyPayTongjiByCurrencyId($currency_id,$where='')
    {

        $where['currency_id'] = $currency_id;
        $data = Pay::select(DB::raw('count(pay_id) as allnum,sum(money) as allmoney'))->groupBy('currency_id')->multiWhere($where)->get();
        if($data){
            $data = $data->toArray();
        }
        //dd($data);
        //$data = array_map('get_object_vars', $data);   //对象转数组
        return $data;

    }

    //不同币种的提现统计
    public function getCurrencyTibiTongjiByCurrencyId($currency_id,$where='')
    {

        $where['currency_id'] = $currency_id;

        if(isset($where['member_id'])){
            $where['user_id'] = $where['member_id'];
        }

        foreach ($where as $k=>$v){
            if($k!='memberid'){
                $arr[$k] = $v;
            }
        }

        //dd($arr);
        $data = Tibi::select(DB::raw('count(id) as allnum,sum(num) as allmoney'))->multiWhere($arr)->groupBy('currency_id')->get();
        if($data){
            $data = $data->toArray();
        }
        //dd($data);
        return $data;

    }

    //挂单统计
    public function getCurrencyOrderTongji($where='')
    {

        $list = Orders::select(DB::raw('count(orders_id) as num,sum(num*price) as jiaoyiprice,sum(num) as jiaoyinum,currency_trade_id,currency_id'))->multiWhere($where)->groupBy('currency_id')->get();
        //$list = array_map('get_object_vars', $data);   //对象转数组

        $currency_model = new Currency();

        foreach ($list as $k=>$v){
            $list[$k]['currency_trade_name'] = $currency_model->getCurrencynameById($v['currency_trade_id']);
            $list[$k]['currency_name'] = $currency_model->getCurrencynameById($v['currency_id']);
            $list[$k]['allmoney'] = $v['jiaoyiprice'];
        }

        return $list;

    }

    //成交统计
    public function getCurrencyTradeTongji($where='')
    {

        $where['type'] = 'sell';
        $list = Trade::select(DB::raw('count(trade_id) as num,sum(num*price) as jiaoyiprice,sum(num) as jiaoyinum,currency_trade_id,currency_id'))->multiWhere($where)->groupBy('currency_id')->get();
        //$list = array_map('get_object_vars', $data);   //对象转数组
        $currency_model = new Currency();

        foreach ($list as $k=>$v){
            $list[$k]['currency_trade_name'] = $currency_model->getCurrencynameById($v['currency_trade_id']);
            $list[$k]['currency_name'] = $currency_model->getCurrencynameById($v['currency_id']);
            $list[$k]['allmoney'] = $v['jiaoyiprice'];
        }

        //dd($list);
        return $list;

    }

}
