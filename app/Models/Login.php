<?php

namespace App\Models;

use App\Support\email;

class Login extends BaseModel
{
    /**
     * 登录
     */
    public function email($request)
    {
        $email = $request['email'];
        if ((checkEmail($email) || checkMobile($email)) == false) {
            $res['status'] = 2;
            $res['msg'] = "请输入正确的手机或者邮箱!";
            return $res;
        }
        $info = Member::where('email', $email)->first();
        if (empty($info)) {
            $res['status'] = 2;
            $res['msg'] = "邮箱或者手机不存在!";
            return $res;
        }
        if ($info['status'] == 2) {
            $res['status'] = 2;
            $res['msg'] = "非常抱歉您的账号已被禁用!";
            return $res;
        }

        //获取下方能用到的参数
        $new_ip = get_ip();
        $old_login_ip = $info['login_ip'] ? $info['login_ip'] : $info['ip'];
        if (!empty($info['idcard'])) {
            if ($new_ip != $old_login_ip) {
                $res['status'] = 4;
                $res['msg'] = '检测到当前登录ip与上次不一致请验证！';
                return $res;
            }
        }
        $res['msg'] = '';
        return $res;

    }

    public function login($request)
    {
        $email = $request['email'];
        $pwd = md5($request['pwd']);
        $member = new Member();
        $info = $member->getOne(['email' => $email]);
        $card = $request['year'] . $request['month'] . $request['day'];
        $idcard = substr($info['idcard'], 6, 8);
        if (!empty($info['idcard']) && $card != 19800101) {
            if ($card != $idcard) {
                $res['status'] = 2;
                $res['msg'] = "生日与您当前填写不符!";
                return $res;
            }
        }
        //验证密码
        if ($info['pwd'] != $pwd) {
            //$this->error('密码输入错误');
            $res['status'] = 3;
            $res['msg'] = "密码输入错误!";
            return $res;
        }

        $data['login_ip'] = get_ip();
        $data['login_time'] = time();
        $member_id = $info['member_id'];
        $result = $member->up(['member_id' => $member_id], $data);
        if ($result) {
            session(['USER_KEY_ID' => $info['member_id']]);
            session(['USER_KEY' => $info['email']]);//用户名
            session(['STATUS' => $info['status']]);//用户状态
            session()->save();
            $res['status'] = 1;
            $res['msg'] = '登录成功！';
            return $res;
        }
        $res['status'] = 2;
        $res['msg'] = '登录失败,请稍后再试！';
        return $res;
    }

    /**
     * 验证码
     */
    public function code()
    {
        generate_captcha();
    }

    /**
     * 忘记密码
     */
    public function findPwd($request)
    {
        if ($request->isMethod('post')) {
            $email = $request['email'];
            $code = $request['captcha'];
            if (!check_captcha($code)) {
                $res['status'] = 2;
                $res['msg'] = '验证码错误！';
                return $res;
            }
        }
        $info = (new Member())->getOne(['email'=>$email]);
        if (!$info) {
            $res['status'] = 2;
            $res['msg'] = "用户不存在!";
            return $res;
        }
        $token = strtoupper(md5($request['email']) . md5(time()));//大写md5当前邮箱+当前时间
        $url = urlHome('Login','postResetPwd',['key'=>$token]);
        $content = "<div>";
        $content .= "您好，<br><br>请点击链接：<br>";
        $content .= "<a target='_blank' href='{$url}' >重置您的密码</a>";
        $content .= "<br><br>如果链接无法点击，请复制并打开以下网址：<br>";
        $content .= "<a target='_blank' href='{$url}'>{$url}</a>";
        $config = (new Config())->getCache();
        $r = setPostEmail($config['EMAIL_HOST'], $config['EMAIL_USERNAME'], $config['EMAIL_PASSWORD'], $config['name'] . '团队', $request['email'], $config['name'] . '团队[密码找回]', $content);
        if($r) {
        $member_id = $info['member_id'];
        $data['member_id'] = $info['member_id'];
        $data['token'] = $token;
        $data['add_time'] = time();
        $result = (new Findpwd())->add($data);
        if($result){
            $res['status']=1;
            $res['msg']="邮箱已发送!";
            return $res;
        }
        } else{
            $res['status'] = 2;
            $res['msg'] = "服务器繁忙,请稍后重试!";
            return $res;
        }

    }

    /**
     * 根据发送邮箱地址显示修改密码界面
     */
    public function resetPwd($request){
        if($request->isMethod('post')){
            $captcha = $request['captcha'];
            if (check_captcha($captcha) == false) {
                $data['status'] = 0;
                $data['msg'] = '验证码错误!';
                return $data;
            }
            $where['token'] = $request['key'];
            $findpwd = new Findpwd();
            $member = new Member();
            $findpwd_info = $findpwd->getOne($where);
            $member_info = $member->getOne(['member_id'=>$findpwd_info['member_id']]);
            if(!empty($member_info['idcard'])) {
                if($member_info['idcard'] != $request['idcard']) {
                    $data['status'] = 0;
                    $data['msg'] = "请输入正确的身份证号!";
                    return $data;
                }
            }
            $pwd = md5($request['pwd']);
            $whereMember['member_id'] = $member_info['member_id'];
            $r = $member->up($whereMember,['pwd'=>$pwd]);
            if(!$r){
                $data['status'] = 0;
                $data['msg'] = "服务器繁忙,请稍后重试!";
                return $data;
            }
            $findpwd->del($findpwd_info['id']);
            $data['status'] = 1;
            $data['msg'] = "修改成功!";
            return $data;

        }
    }

}
