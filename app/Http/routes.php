<?php

const CTR = 'Controller';

/**
 * 前台路由
 */
require 'Routes/home.php';

/**
 * 后台路由
 */
require 'Routes/admin.php';


/**
 * 接口路由
 */
require 'Routes/api.php';

