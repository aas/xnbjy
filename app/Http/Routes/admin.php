<?php
//后台管理员路由
$login= array(
    'prefix'=>'center',
    'namespace' => 'Admin',
    'middleware' => ['web']
);
Route::group($login, function () {

//    Route::get('/' , function () {
//        return phpinfo();
//    });
    Route::get('/' , 'LoginController@getLogin');
    Route::get('login' , 'LoginController@getLogin');
    Route::post('login' , 'LoginController@postLogin');
    Route::get('logout' , 'LoginController@getLogout');

});

$admin= array(
    'prefix'=>'center',
    'namespace' => 'Admin',
    'middleware' => ['web','checkAdminlogin']
);
Route::group($admin, function () {

    Route::get('index' , 'IndexController@getIndex');                                 //后台首页
    Route::post('index/cache' , 'IndexController@postIndex_cache');                           //去除缓存
    Route::get('test' , 'TestController@getIndex');                                          //后台首页


    /**系统管理模块**/
    Route::get('config/index' , 'ConfigController@getIndex');                                 //系统设置模块
    Route::post('config/indexAdd' , 'ConfigController@postIndexAdd');
    Route::get('config/information' , 'ConfigController@getInformation');                     //信息设置模块
    Route::post('config/information_add' , 'ConfigController@postInformation_add');
    Route::get('config/finance' , 'ConfigController@getFinance');                             //财务设置模块
    Route::post('config/finance_edit' , 'ConfigController@postFinance_edit');
    Route::get('config/customer' , 'ConfigController@getCustomer');                           //客服设置模块
    Route::post('config/customer_edit' , 'ConfigController@postCustomer_edit');
    Route::get('config/short' , 'ConfigController@getShort');                                 //短信邮箱设置模块
    Route::post('config/short_edit' , 'ConfigController@postShort_edit');
    Route::get('fileconfig/saveEntrance' , 'FileconfigoperationController@getSaveEntrance');  //入口配置管理模块
    Route::post('fileconfig/saveEntrance_edit' , 'FileconfigoperationController@postSaveEntrance_edit');
    Route::get('fileconfig/saveDb' , 'FileconfigoperationController@getSaveDb');              //数据库配置管理模块
    Route::post('fileconfig/saveDb_edit' , 'FileconfigoperationController@postSaveDb_edit');

    /**常用操作模块**/
    Route::get('flash/index' , 'FlashController@getIndex');                                   //幻灯管理模块
    Route::get('flash/add' , 'FlashController@getAdd');                                       //幻灯管理添加操作
    Route::post('flash/addList' , 'FlashController@postAddList');
    Route::post('flash/upload' , 'FlashController@postUpload');                               //图片上传
    Route::get('flash/edit' , 'FlashController@getEdit');                                     //幻灯管理修改操作
    Route::post('flash/editList' , 'FlashController@postEditList');
    Route::get('flash/del' , 'FlashController@getDel');                                         //删除
    Route::get('index/info' , 'IndexController@getInfo');                                     //全站统计信息
    Route::get('link/index' , 'LinkController@getIndex');                                     //友情链接模块
    Route::get('link/add' , 'LinkController@getAdd');                                         //添加
    Route::post('link/addList' , 'LinkController@postAddList');
    Route::get('link/edit' , 'LinkController@getEdit');                                       //修改
    Route::post('link/editList' , 'LinkController@postEditList');
    Route::get('link/del' , 'LinkController@getDel');                                         //删除

    /**分红管理模块**/
    Route::get('bonus/index' , 'BonusController@getIndex');                                   //添加分红奖励模块
    Route::post('bonus/addList' , 'BonusController@postAddList');                             //添加分红
    Route::get('bonus/bonuslist' , 'BonusController@getBonuslist');                           //分红列表模块
    Route::get('dividend/index' , 'DividendController@getIndex');                             //分红股模块
    Route::post('dividend/indexEdit' , 'DividendController@postIndexEdit');

    /**财务管理模块**/
    Route::get('pending/index' , 'PendingController@getIndex');                               //提现审核模块
    Route::get('pending/payExcel' , 'PendingController@getPayExcel');                         //表格导出
    Route::post('pending/success_by' , 'PendingController@postSuccess_by');                   //通过操作
    Route::post('pending/error_by' , 'PendingController@postError_by');                       //不通过操作

    Route::get('pay/payByMan' , 'PayController@getPayByMan');                                 //人工充值管理
    Route::post('pay/payEdit' , 'PayController@postPayEdit');                                 //审核操作
    Route::get('pay/payExcel' , 'PayController@getPayExcel');                                 //表格导出
    Route::get('finance/index' , 'FinanceController@getIndex');                               //财务日志
    Route::get('finance/payExcel' , 'FinanceController@getPayExcel');                         //表格导出

    Route::get('finance/count' , 'FinanceController@getCount');                               //财务明细
    Route::get('pay/adm' , 'PayController@getAdm');                                           //管理员充值管理
    Route::post('pay/admAdd' , 'PayController@postAdmAdd');                                   //管理员充值添加
    Route::get('pay/fill' , 'PayController@getFill');                                         //第三方充值记录

    /**钱包币种管理**/
    Route::get('currency/index' , 'CurrencyController@getIndex');                             //币种管理
    Route::post('currency/savePrice' , 'CurrencyController@postSavePrice');                   //设置交易限额
    Route::get('currency/set_member' , 'CurrencyController@getSet_member');                   //向会员钱包转账
    Route::post('currency/set_member_qianbao' , 'CurrencyController@postSet_member_qianbao');
    Route::get('currency/add' , 'CurrencyController@getAdd');                                 //币种添加
    Route::post('currency/addList' , 'CurrencyController@postAddList');
    Route::get('currency/edit' , 'CurrencyController@getEdit');                               //币种修改
    Route::post('currency/editList' , 'CurrencyController@postEditList');
    Route::get('currency/del' , 'CurrencyController@getDel');                                 //删除
    Route::get('currency/chongzhi' , 'CurrencyController@getChongzhi');                       //充币记录
    Route::get('currency/tibi' , 'CurrencyController@getTibi');                               //提币记录
    Route::get('currencyUser/member_chongzhi' , 'CurrencyUserController@getMemberChongzhi');  //会员钱包充值列表
    Route::get('currencyUser/member_tibi' , 'CurrencyUserController@getMemberTibi');          //会员钱包提币列表
    Route::get('download/index' , 'DownloadController@getIndex');                             //下载管理
    Route::post('download/biaogeDownload' , 'DownloadController@postBiaogeDownload');         //新币上传申请表
    Route::post('download/qianbaoDownload' , 'DownloadController@postQianbaoDownload');       //钱包上传
    Route::post('download/guanwangUrl' , 'DownloadController@postGuanwangUrl');               //币种官方网站




    /**管理员管理模块**/
    Route::get('manage/index' , 'ManageController@getIndex');                      //管理员管理
    Route::post('manage/index' , 'ManageController@postIndex');
    Route::get('manage/add_admin' , 'ManageController@getAdd_admin');
    Route::post('manage/add_admin' , 'ManageController@postAdd_admin');
    Route::get('manage/edit_admin' , 'ManageController@getEdit_admin');
    Route::post('manage/edit_admin' , 'ManageController@postEdit_admin');
    Route::post('manage/del_admin' , 'ManageController@postDel_admin');
    Route::get('manage/auth_admin' , 'ManageController@getAuth_admin');
    Route::post('manage/auth_admin' , 'ManageController@postAuth_admin');

    /**密码修改**/
    Route::get('manage/edit_pwd' , 'ManageController@getEdit_pwd');
    Route::post('manage/edit_pwd' , 'ManageController@postEdit_pwd');







    /***银行管理***/
    Route::get('/', 'PublicController@getIndex');                                  //用户首页
    Route::get('Website_bank/index', 'WebsitebankController@getIndex');            //银行列表
    Route::get('website_bank/add', 'WebsitebankController@getAdd');                //添加银行
    Route::post('website_bank/add', 'WebsitebankController@postAdd');              //添加银行提交
    Route::get('website_bank/save', 'WebsitebankController@getEdit');              //修改银行卡页面
    Route::post('website_bank/save', 'WebsitebankController@postEdit');            //处理修改银行卡
    Route::post('website_bank/del', 'WebsitebankController@postDel');              //删除银行卡

    /***会员管理***/
//    Route::Controller("member", "MemberController");                             //会员管理
//    Route::Controller("message", "MessageController");                           //系统消息
//    Route::Controller("examine", "ExamineController");                           //交易密码管理

    Route::get('member', 'MemberController@getIndex');                             //会员列表
    Route::get('member/add', 'MemberController@getAdd');                           //添加会员
    Route::post('member/add', 'MemberController@postAdd');                         //添加会员提交
    Route::get('member/save', 'MemberController@getEdit');                         //修改会员
    Route::post('member/save', 'MemberController@postEdit');                       //修改会员提交
    Route::post('member/del', 'MemberController@postDel');                         //删除会员
    Route::get('member/check_email', 'MemberController@getCheckEmail');            //ajax验证邮箱
    Route::get('member/save_modify', 'MemberController@getSaveModify');            //修改个人信息
    Route::post('member/save_modify', 'MemberController@postSaveModify');          //修改个人信息
    Route::get('member/check_nick', 'MemberController@getCheckNick');              //ajax验证昵称
    Route::get('member/check_phone', 'MemberController@getCheckPhone');            //ajax验证手机
    Route::get('member/show_my_invite', 'MemberController@getInvite');             //查看邀请人
    Route::get('member/show', 'MemberController@getShow');                         //查看个人账户
    Route::post('member/update_member_money', 'MemberController@postMemberMoney'); //修改个人账户
    Route::get('message', 'MessageController@getIndex');                           //系统消息列表
    Route::get('message/add', 'MessageController@getAdd');                         //新增消息
    Route::post('message/add', 'MessageController@postAdd');                       //新增消息提交
    Route::get('message/edit', 'MessageController@getEdit');                       //消息详情
    Route::get('message/del', 'MessageController@getDel');                         //删除消息

    Route::get('examine', 'ExamineController@getIndex');                           //交易密码管理列表
    Route::get('examine/save', 'ExamineController@getEdit');                       //修改审核
    Route::post('examine/save', 'ExamineController@postEdit');                     //修改审核

    /***交易管理***/
    Route::Controller("trade", "TradeController");                                 //交易管理

    /***文章管理***/
    Route::get('art/notes', 'ArtController@getNotesIndex');                        //官方公告列表
    Route::get('art/notes/add', 'ArtController@getNotesAdd');                      //官方公告添加
    Route::post('art/notes/add', 'ArtController@postNotesAdd');                    //官方公添加
    Route::get('art/notes/edit', 'ArtController@getNotesEdit');                    //官方公告修改
    Route::post('art/notes/edit', 'ArtController@postNotesEdit');                  //官方公告修改
    Route::post('art/notes/del', 'ArtController@postNotesDel');                    //官方公告删除

    Route::get('art/group', 'ArtController@getGroupIndex');                        //团队管理列表
    Route::get('art/group/add', 'ArtController@getGroupAdd');                      //团队管理添加
    Route::post('art/group/add', 'ArtController@getGroupAdd');                     //团队管理添加

    Route::get('art/help', 'ArtController@getHelpIndex');                          //帮助列表
    Route::get('art/help/add', 'ArtController@getHelpAdd');                        //添加文章
    Route::post('art/help/add', 'ArtController@postHelpAdd');                      //添加文章
    Route::get('art/help/edit', 'ArtController@getHelpEdit');                      //修改文章
    Route::post('art/help/edit', 'ArtController@postHelpEdit');                    //修改文章
    Route::get('art/help/del', 'ArtController@getHelpDel');                        //删除文章

    Route::get('arttype/add', 'ArttypeController@getAdd');                         //添加帮助分类
    Route::post('arttype/add', 'ArttypeController@postAdd');                       //添加帮助分类提交
    Route::get('arttype/edit', 'ArttypeController@getEdit');                       //修改帮助分类
    Route::post('arttype/edit', 'ArttypeController@postEdit');                     //修改帮助分类提交
    Route::get('arttype/del', 'ArttypeController@getDel');                         //删除帮助分类

    /***统计***/
    //Route::Controller("tongji", "TongjiController");                             //统计推广
    Route::get('tongji/tuiguang', 'TongjiController@getTuiGuang');                 //推广排名
    Route::get('tongji/shuju', 'TongjiController@getShuju');                       //数据统计
    Route::get('tongji/xiangxi', 'TongjiController@getXiangxi');                   //详细统计
    Route::get('tongji/yue', 'TongjiController@getYue');                           //余额排名

    /***众筹管理***/
    Route::get('zhongchou','ZhongchouController@getIndex');                        //众筹管理
    Route::get('zhongchou/log','ZhongchouController@getLog');                      //众筹记录
    Route::get('zhongchou/add','ZhongchouController@getAdd');                      //添加众筹
    Route::post('zhongchou/add','ZhongchouController@postAdd');                    //添加众筹

    Route::get('zhongchou/edit','ZhongchouController@getEdit');                    //修改众筹
    Route::post('zhongchou/edit','ZhongchouController@postEdit');                  //修改众筹
    Route::post('zhongchou/del','ZhongchouController@postDel');                    //删除众筹

    Route::post('zhongchou/start','ZhongchouController@postZhongchouStart');       //开启众筹
    Route::post('zhongchou/end','ZhongchouController@postZhongchouEnd');           //结束众筹

    Route::get('zhongchou/awardslist','ZhongchouController@getAwardsList');        //众筹奖励列表
    Route::get('zhongchou/awardsadd','ZhongchouController@getAwardsAdd');          //添加众筹奖励
    Route::post('zhongchou/awardsadd','ZhongchouController@postAwardsAdd');        //添加众筹奖励

    Route::post('zhongchou/jiedongbyid','ZhongchouController@postJiedongById');    //解冻单个众筹
    Route::post('zhongchou/jiedongbyiid','ZhongchouController@postJiedongByIid');  //解冻全部一类众筹
    Route::post('zhongchou/jiedong_start','ZhongchouController@postJiedong_start'); //开始解冻

});