<?php
//前台路由
$home = array(
    'namespace' => 'Home',
    'middleware' => ['web']
);
Route::group($home, function () {

    /**首页**/
    Route::get('/', 'IndexController@getIndex');

    Route::controllers([
        'register' => 'Reg' . CTR,  //注册
        'login' => 'Login' . CTR,  //登录
        'orders' => 'Orders' . CTR,  //订单
        'trade' => 'Trade' . CTR,  //交易
        'zc' => 'Zhongchou' . CTR,  //众筹
        'dow' => 'Dow' . CTR,  //下载中心
        'help' => 'Help' . CTR,  //帮助中心
        'art' => 'Art' . CTR,  //最新动态
        'market' => 'Market' . CTR,  //行情中心
    ]);
    
    Route::group(['middleware' => 'checkHomelogin'], function () {
        /**用户中心**/
        Route::controllers([
            'modify-member' => 'ModifyMember' . CTR,  //个人信息
            'safe' => 'Safe' . CTR,  //安全信息
            'user' => 'User' . CTR,  //用户中心
            'finance' => 'Finance' . CTR,  //财务
            'fill-by-bank' => 'FillByBank' . CTR,  //在线网银支付
            'fill' => 'Fill' . CTR,  //网银支付
            'entrust' => 'Entrust' . CTR,  //委托管理
            'pay' => 'Pay' . CTR,  //充值管理
        ]);
    });


});