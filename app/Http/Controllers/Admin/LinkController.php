<?php

namespace App\Http\Controllers\Admin;

use App\Models\Link;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class LinkController extends BaseController
{
    /**
     * 初始化
     */
    public function __construct(Link $link)
    {
        parent::__construct();
        $this->link = $link;
    }

    /**
     * 友情链接
     */
    public function getIndex()
    {
//        $data = $this->link->getAll();
        $data = Link::paginate(10);
        return view('admin.link.index',compact('data'));
    }

    /**
     * 添加操作
     */
    public function getAdd()
    {
        return view('admin.link.add');
    }

    public function postAddList()
    {
        $res = $this->link->addList(request());
        if ($res['state'] == 200 || $res['state'] == 400) {
            return back()->with('message',$res['msg'])->withInput();
        }
    }

    /**
     * 修改操作
     */
    public function getEdit()
    {
        $where['id'] = request('id');
        $data = $this->link->getOne($where);
        return view('admin.link.edit',compact('data'));
    }

    public function postEditList()
    {
        $res = $this->link->editList(request());
        if ($res['state'] == 200 || $res['state'] == 400) {
            return back()->with('message',$res['msg'])->withInput();
        }
    }

    /**
     * 删除
     */
    public function getDel()
    {
        $where['id'] = $_GET['id'];
        $re = $this->link->del($where);
        if($re){
            $res['state'] = 200;
            $res['msg'] = '删除成功！';
            return $res;
        }
        $res['state'] = 400;
        $res['msg'] = '删除失败,请稍后再试！';
        return $res;

    }

}
