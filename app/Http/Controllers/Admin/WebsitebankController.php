<?php
namespace App\Http\Controllers\Admin;
use App\Models\Websitebank;
use Session;

class WebsitebankController extends BaseController
{

    public function __construct(Websitebank $websitebank)
    {
        parent::__construct();
        $this->websitebank = $websitebank;
    }

    //银行列表
    public function getIndex()
    {
        $data = $this->websitebank->getWebsiteBankList();
        return view('admin.websitebank.index',compact('data'));
    }

    //添加银行卡页面
    public function getAdd()
    {

        return view('admin.websitebank.addBank');

    }

    //添加银行卡处理
    public function postAdd()
    {
        $res = $this->websitebank->addBank(request());
        return redirect()->back()->with('message', $res)->withInput();   //家昌版快闪重定向
    }

    //修改银行卡页面
    public function getEdit()
    {
        $data = $this->websitebank->getOneBank(request());
        return view('admin.websitebank.saveBank',compact('data'));
    }

    //修改银行卡提交
    public function postEdit()
    {
        $res = $this->websitebank->saveBank(request());
        return redirect()->back()->with('message', $res)->withInput();   //家昌版快闪重定向
    }

    //ajax删除银行卡
    public function postDel()
    {
        $res = $this->websitebank->delBank(request());
        return ajax_return($res);
    }

}
