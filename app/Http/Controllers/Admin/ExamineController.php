<?php
namespace App\Http\Controllers\Admin;
use App\Models\Examine;
use Session;

class ExamineController extends BaseController
{

    public function __construct(Examine $examine)
    {
        parent::__construct();
        $this->examine = $examine;
    }

    //交易审核列表
    public function getIndex()
    {
        $list = $this->examine->getExaminList(request());
        return view('admin.examine.index',compact('list'));
    }


    //修改审核页面
    public function getEdit()
    {
        $list = $this->examine->getOneExamin(request());
        return view('admin.examine.saveExamine',compact('list'));
    }

    //修改审核提交
    public function postEdit()
    {
        $res = $this->examine->saveExamin(request());
        return redirect()->back()->with('message', $res)->withInput();  
    }



}
