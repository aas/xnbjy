<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Config;

class ConfigController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Config $config)
    {
        parent::__construct();
        $this->config = $config;
    }
    /**
     * 公共模块
     */
    public function common()
    {
        $data = $this->config->getCache();
        return $data;
    }

    /**
     * 系统设置模块
     */
    public function getIndex()
    {

        $data = $this->common();
        return view('admin.config.index',compact('data'));
    }

    public function postIndexAdd()
    {
        $res = $this->config->index(request());
        if ($res['state'] == 200 || $res['state'] == 400) {
            return back()->with('message',$res['msg'])->withInput();
        }
    }

    /**
     * 信息设置模块
     */
    public function getInformation()
    {
        $data = $this->common();
        return view('admin.config.information',compact('data'));
    }

    public function postInformation_add()
    {
        $res = $this->config->information_add(request());
        if ($res['state'] == 200 || $res['state'] == 400) {
            return back()->with('message',$res['msg'])->withInput();
        }
    }

    /**
     * 财务设置模块
     */
    public function getFinance()
    {
        $data = $this->common();
        return view('admin.config.finance',compact('data'));
    }

    public function postFinance_edit()
    {
        $res = $this->config->finance_edit(request());
        if ($res['state'] == 200 || $res['state'] == 400) {
            return back()->with('message',$res['msg'])->withInput();
        }
    }

    /**
     * 客服设置模块
     */
    public function getCustomer()
    {
        $data = $this->common();
        return view('admin.config.customerservice',compact('data'));
    }

    public function postCustomer_edit()
    {
        $res = $this->config->customer_edit(request());
        if ($res['state'] == 200 || $res['state'] == 400) {
            return back()->with('message',$res['msg'])->withInput();
        }
    }

    /**
     * 短信邮箱设置模块
     */
    public function getShort()
    {
        $data = $this->common();
        return view('admin.config.shortmessage',compact('data'));
    }

    public function postShort_edit()
    {
        $res = $this->config->short_edit(request());
        if ($res['state'] == 200 || $res['state'] == 400) {
            return back()->with('message',$res['msg'])->withInput();
        }
    }
}
