<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Fileconfigoperation;

class FileconfigoperationController extends BaseController
{
    /**
     *实例化
     */
    public function __construct(Fileconfigoperation $fileconfigoperation)
    {
        parent::__construct();
        $this->Fileconfigoperation = $fileconfigoperation;
    }

    /**
     * 后台入口配置管理
     */
    public function getSaveEntrance()
    {
        return view('admin.fileconfigoperation.saveEntrance');
    }

    public function postSaveEntrance_edit()
    {
        $res = $this->Fileconfigoperation->saveEntrance_edit(request());
        if ($res['state'] == 0) {
            return back()->with('message',$res['msg'])->withInput();
        }
   }

    /**
     * 后台配置管理模块
     */
    public function getSaveDb()
    {
        $result = Config();
        $data = $result['database']['connections']['mysql'];
        return view('admin.fileconfigoperation.saveDb',compact('data'));
    }

    public function postSaveDb_edit()
    {
        $res = $this->Fileconfigoperation->saveDb_edit(request());
        if ($res['state'] == 0) {
            return back()->with('message',$res['msg'])->withInput();
        }
    }
}
