<?php
namespace App\Http\Controllers\Admin;
use App\Models\Currency;
use App\Models\Orders;
use App\Models\Trade;
use Session;

class TradeController extends BaseController
{

    public function __construct(Orders $orders,Currency $currency,Trade $trade)
    {
        parent::__construct();
        $this->orders = $orders;
        $this->currency = $currency;
        $this->trade = $trade;
    }

    //委托列表
    public function getOrderIndex()
    {
        $list = $this->orders->getOrderCurrencyMemberListFenye(request());   //列表数据
        $currency_type = $this->currency->getCurrencyName();                 //币种
        return view('admin.trade.orders',compact('list','currency_type'));
    }

    //挂单列表
    public function getTradeIndex()
    {
        $list = $this->trade->getTradeCurrencyMemberListFenye(request());    //列表数据
        $currency_type = $this->currency->getCurrencyName();                 //币种
        return view('admin.trade.trade',compact('list','currency_type'));
    }


    //ajax撤销订单
    public function postCancel()
    {
        $res = $this->orders->cancelOrder(request());
        return ajax_return($res);
    }

}
