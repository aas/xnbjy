<?php

namespace App\Http\Controllers\Admin;

use App\Models\Currency;
use App\Models\CurrencyUser;
use App\Models\Finance;
use App\Models\Member;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class FinanceController extends BaseController
{
    /**
     *实例化
     */
    public function __construct(Finance $finance)
    {
        parent::__construct();
        $this->finance = $finance;
    }

    /**
     * 财务管理--财务日志
     */
    public function getIndex()
    {
        //筛选
        $where =[];
        $id = '';
        $type = DB::table('finance_type')->get();
        $type_id = request('type_id');
        $name = request('name');
        $member_id = request('member_id');
        if(!empty($type_id)){
            $where['type'] = $type_id;
        }
        if(!empty($name)){
            $uid = Member::where('name',$name)->get();
            foreach ($uid as $k=>$v){
               $id = $v['member_id'];
            }
            $where['member.member_id']= $id;
        }
        if(!empty($member_id)){
            $where["member.member_id"] = $member_id;
        }
            $result = DB::table('finance')
                ->leftJoin('member','finance.member_id','=','member.member_id')
                ->leftJoin('finance_type','finance.type','=','finance_type.id')
                ->leftJoin('currency','finance.currency_id','=','currency.currency_id')
                ->select(DB::raw("xnb_member.name as username , xnb_finance_type.name as typename "),'finance.*','currency.currency_name')
                ->where($where)
                ->orderBy('finance.add_time','desc')
                ->paginate(10);

        foreach ($result as $k=>$v){
            if($v->currency_id == 0){
                $result[$k]->currency_name = '人民币';
            }
        }


        return view('admin.finance.index',compact('type','type_id','name','member_id','result'));
    }

    /**
     * 表格导出
     */
    public function getPayExcel()
    {
        $res = $this->finance->payExcel(request());
        $this->ajaxReturn($res);
    }


    /**
     * 财务管理--财务明细
     */
    public function getCount()
    {
        $pay = Finance::where('type',6)->sum('money');
        $pay_admin = Finance::whereIn('type',[3,13])->where('currency_id',0)->sum('money');
        $draw =  Finance::where('type',23)->sum('money');
        //统计人民币总额
        $rmb_count = Member::sum('rmb');
        $forzen_rmb_count = Member::sum('forzen_rmb');
        $rmb = $rmb_count + $forzen_rmb_count;
        //分币种统计
        $currency = DB::table('currency')->select('currency_id','currency_name')->get();
        foreach ($currency as $k=>$v){
            $currency_user[$k]['num'] = CurrencyUser::where('currency_id',$v->currency_id)->sum('num');
            $currency_user[$k]['forzen_num'] = CurrencyUser::where('currency_id',$v->currency_id)->sum('forzen_num');
            $currency_user[$k]['name'] = $v->currency_name;
        }

        return view('admin.finance.count',compact('pay','pay_admin','draw','rmb','currency_user'));
    }




}
