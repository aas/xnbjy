<?php
namespace App\Http\Controllers\Admin;
use App\Models\Art;
use App\Models\Arttype;
use Session;

class ArttypeController extends BaseController
{

    public function __construct(Art $art,Arttype $arttype)
    {
        parent::__construct();
        $this->art = $art;
        $this->arttype = $arttype;
    }

    //添加帮助公告类型
    public function getAdd()
    {
        $list = $this->arttype->getArttypeHelpList(request());
        return view('admin.arttype.add',compact('list'));
    }

    //添加帮助类型提交
    public function postAdd()
    {
        $res = $this->arttype->AddArttypeHelp(request());
        return redirect()->back()->with('message', $res['msg'])->withInput();   
    }

    //修改帮助类型
    public function getEdit()
    {
        $list = $this->arttype->getArttypeHelpList(request());
        $data = $this->arttype->getOneArttype(request());
        return view('admin.arttype.edit',compact('list','data'));
    }

    //修改帮助类型提交
    public function postEdit()
    {
        $res = $this->arttype->EditArttypeHelp(request());
        return redirect()->back()->with('message', $res['msg'])->withInput();   
    }

    //删除类型
    public function getDel()
    {
        $res = $this->arttype->delOneArttype(request());
        return redirect()->back()->with('message', $res['msg'])->withInput();  
    }

}
