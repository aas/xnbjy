<?php

namespace App\Http\Controllers\Admin;
use App\Http\Requests;
use App\Models\Config;
use App\Models\CurrencyUser;
use App\Models\Manage;
use App\Models\Member;
use App\Models\Issue;
use App\Models\Pay;
use App\Models\Withdraw;
use App\Models\Currency;
use Illuminate\Support\Facades\Cache;

class IndexController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Member $member,Issue $issue,Pay $pay,Withdraw $withdraw,Currency $currency,CurrencyUser $currencyUser)
    {
        parent::__construct();
        $this->member = $member;
        $this->issue = $issue;
        $this->pay = $pay;
        $this->withdraw = $withdraw;
        $this->currency = $currency;
        $this->currencyUser = $currencyUser;

    }
    /**
     * 后台首页
     */
    public function getIndex(){

        $data = (new Config())->getCache();
        $sql = phpversion();
        $id = session('admin_userid');
        $re = Manage::where('admin_id',$id)->pluck('username');
        return view('admin.index.index',compact('data','sql','re'));
    }


    /**
     *去除缓存
     */
    public function postIndex_cache()
    {
        $path = base_path('storage.framework.views');

        if (file_exists($path)) {
            $this->del_dir($path);
            $res['state'] = 200;
            $res['msg'] = '缓存清理成功！';
            return $res;
        }
    }

    /**
     * 常用操作--全站统计信息
     */
    public function getInfo()
    {

        $count = $this->member->getCount(request());
        $issue_count = $this->issue->get_issue_count(request());
        $pay_money_count = $this->pay->get_money_count(request());
        $withdraw_money_count = $this->withdraw->withdraw_money_count(request());
        $pay_count = $this->pay->get_pay_count(request());
        $withdraw_count = $this->withdraw->get_withdraw_count(request());
//        $currency_u_info = $this->currency->getOneWithUser(request());
//        dd($currency_u_info['currencyUser']['currency_id']);
        $currency_u_info = $this->currency->get_currency_u_info(request());

        return view('admin/index/infoStatistics',compact('count','issue_count','pay_money_count','withdraw_money_count','pay_count','withdraw_count','currency_u_info'));
    }


    /**
     * 删除文件和目录
     * @param type $path 要删除文件夹路径
     * @param type $fileName 要删除的目录名称
     */
    function del_dir($dir) {
        if (!is_dir($dir)) {
            return false;
        }
        $handle = opendir($dir);
        while (($file = readdir($handle)) !== false) {
            if ($file != "." && $file != "..") {
                is_dir("$dir/$file") ? del_dir("$dir/$file") : @unlink("$dir/$file");
            }
        }
    }
}
