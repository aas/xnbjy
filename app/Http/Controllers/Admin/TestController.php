<?php
namespace App\Http\Controllers\Admin;
use App\Models\Orders;
use Illuminate\Support\Facades\DB;
use Session;

class TestController extends BaseController
{

    public function __construct(Orders $orders)
    {
        parent::__construct();
        $this->orders = $orders;
    }

    function getPageNoList($totalPage, $pageNo, $pageSize)
    {
        $disPageList = 0;//定义最大链接显示数额
        $disPageList = $pageNo + $pageSize - 1;//最大链接显示数额赋值，公式为"当前页数 + 最大链接显示数额 -1"
        //前页面导航页数
        $pageNoList = array();
        //循环显示出当前页面导航页数
        if ($disPageList <= $totalPage) {
            for ($i = $pageNo; $i <= $disPageList; $i++) {
                $pageNoList[] = $i;
            }
        } else {
            if ($totalPage < $pageSize) {
                for ($i = 1; $i <= $totalPage; $i++) {
                    $pageNoList[] = $i;
                }
            } else {
                $start = $totalPage - $pageSize + 1;
                for ($i = $start; $i <= $totalPage; $i++) {
                    $pageNoList[] = $i;
                }
            }
        }
        return $pageNoList;
    }

    //测试
    public function getIndex()
    {
        //dd(123);
//        $where = [];
//        $order['orders_id'] = 'desc';
//        //$res = $this->orders->getList($where,'*',$order,'');
//        //dd($res);
//        $rows = $this->orders->getOrderCurrencyMemberList($where,'*',$order,5);
//        if ($rows) {
//            $rows = $rows->toArray();
//            $rows['pageNoList'] = $this->getPageNoList($rows['last_page'], request('page', 1), 5);
//
//        }
//        dd($rows);
//        return view('admin.websitebank.index',compact('data'));

        //$data = DB::select('select * from xnb_member WHERE member_id = 78');
        $data = DB::table('currency')->where('currency_id','=',30)->increment('currency_all_money',2);
        dd($data);
    }



}
