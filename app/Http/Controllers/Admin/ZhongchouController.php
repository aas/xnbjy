<?php
namespace App\Http\Controllers\Admin;
use App\Models\Currency;
use App\Models\Finance;
use App\Models\IssueLog;
use App\Models\Zhongchou;
use Session;

class ZhongchouController extends BaseController
{

    public function __construct(Zhongchou $zhongchou,Currency $currency,Finance $finance,IssueLog $issueLog)
    {
        parent::__construct();
        $this->zhongchou = $zhongchou;
        $this->currency = $currency;
        $this->finance = $finance;
        $this->issueLog = $issueLog;
    }

    //众筹列表
    public function getIndex()
    {
         $this->zhongchou->checkZhongchou();
         $list = $this->zhongchou->getZhongchouList();
        return view('admin.zhongchou.index',compact('list'));
    }

    //添加众筹
    public function getAdd()
    {
        $currency = $this->currency->getCurrencyAll();
        return view('admin.zhongchou.add',compact('currency'));
    }

    //添加众筹
    public function postAdd()
    {


       $res =  $this->zhongchou->addZhongchou(request());
       return redirect()->back()->with('message', $res)->withInput();  
    }

    //修改众筹
    public function getEdit()
    {
        $currency = $this->currency->getCurrencyAll();
        $data = $this->zhongchou->getZhongchouByid(request());
        return view('admin.zhongchou.edit',compact('currency','data'));
    }

    //修改众筹
    public function postEdit()
    {

        $res =  $this->zhongchou->saveZhongchou(request());
        return redirect()->back()->with('message', $res)->withInput();   
    }

    //删除众筹
    public function postDel()
    {
        $res = $this->zhongchou->delZhongchou(request());
        return ajax_return($res);
    }

    //开启众筹
    public function postZhongchouStart()
    {
        $res = $this->zhongchou->zhongchouStart(request());
        return ajax_return($res);
    }

    public function postZhongchouEnd()
    {
        $res = $this->zhongchou->zhongchouEnd(request());
        return ajax_return($res);
    }

    //众筹记录
    public function getLog()
    {
        $data = $this->issueLog->zhongchouLog(request());
        $issue =  $this->zhongchou->getZhongChouIdAndTitle();
        return view('admin.zhongchou.log',compact('data','issue'));
    }

    //众筹推荐奖励列表
    public function getAwardsList()
    {
        $data = $this->finance->zhongchouAwardsList(request());
        return view('admin.zhongchou.awardsList',compact('data'));
    }

    //添加众筹奖励
    public function getAwardsAdd()
    {
        $currency = $this->currency->getCurrencyAll();
        $list = $this->zhongchou->getAwardsMessageById(request());
        return view('admin.zhongchou.awards',compact('currency','list'));
    }
    //添加众筹奖励
    public function postAwardsAdd()
    {
        $res = $this->zhongchou->saveAwards(request());
        return redirect()->back()->with('message', $res)->withInput();  
    }


    //解冻单条众筹记录
    public function postJiedongById()
    {
        $res = $this->zhongchou->jiedongById(request());
        return ajax_return($res);
    }

    //解冻一类众筹
    public function postJiedongByIid()
    {
        $res = $this->zhongchou->jiedongByIid(request());
        return ajax_return($res);

    }

    //开始解冻
    public function postJiedong_start()
    {
       $res = $this->zhongchou->jiedong_start(request());
       return ajax_return($res);
    }


}
