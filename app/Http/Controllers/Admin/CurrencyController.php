<?php

namespace App\Http\Controllers\Admin;

use App\Models\Currency;
use App\Models\CurrencyUser;
use App\Models\Member;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Bitcoin;
use Illuminate\Support\Facades\DB;
class CurrencyController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Currency $currency)
    {
        parent::__construct();
        $this->currency = $currency;
    }


    /**
     * 钱包币种管理--币种管理
     */
    public function getIndex()
    {
        $result = $this->currency->get_index(request());
      return view('admin.currency.index',compact('result'));
    }

    /**
     * 设置交易限额
     */
    public function postSavePrice()
    {
        $res = $this->currency->savePrice(request());
         return $res;
    }

    /**
     * 向会员钱包转账
     */
    public function getSet_member()
    {
        $currency = $this->currency->get_set_member(request());
        $data['qianbao_blance'] = $currency['qianbao_blance'];
        $data['currency_id'] = $currency['currency_id'];
        $data['currency_name'] = $currency['currency_name'];
        return view('admin.currency.set_member_currencyForQianbao',compact('data'));
     }

    public function postSet_member_qianbao()
    {
        $res = $this->currency->set_member_qianbao(request());
        if (!$res) {
         return back()->with('message',$this->currency->error)->withInput();
        }
     }

    /**
     * 添加币种
     */
    public function getAdd()
    {
        $data = Currency::get();
       return view('admin.currency.add',compact('data'));
    }

    public function postAddList()
    {
       $res = $this->currency->addList(request());
       if($res['status'] == 200 || $res['status'] == 400) {
         return back()->with('message',$res['msg'])->withInput();
       }
    }

    /**
     * 修改币种
     */
    public function getEdit()
    {
        $currency_id = request('currency_id');
        $result = Currency::where('currency_id',$currency_id)->get();
        foreach ($result as $k=>$v) {
            $data = $v;
        }
        $res = Currency::get();
      return view('admin.currency.edit',compact('data','res'));
    }

    public function postEditList()
    {
        $res = $this->currency->editList(request());
        if ($res['state'] == 200 || $res['state'] == 400) {
           return back()->with('message',$res['msg'])->withInput();
        }
    }


        /**
         * 删除
         */
        public function getDel()
    {
        $where['currency_id'] = $_GET['currency_id'];
        $result = CurrencyUser::where('currency_id',$where['currency_id'])->sum("num");
        if ($result > 0) {
            $res['state'] = 1;
            $res['msg'] = '该币种尚有用户持有，不能删除';
            return $res;
        } else {
            $re = $this->currency->del($where);
            if($re){
                $res['state'] = 200;
                $res['msg'] = '删除成功！';
                return $res;
            }
            $res['state'] = 400;
            $res['msg'] = '删除失败,请稍后再试！';
            return $res;
        }

    }


    /**
     * 钱包币种管理--充币记录
     */
    public function getChongzhi()
    {
        //读取币种表
        $curr = Currency::get();
        $where = [];
        $member_id = "";
        $cuid = request('cuid');
        $email = request('email');
        if(!empty($cuid)){
            $where['tibi.currency_id'] = $cuid;
        }
        if(!empty($email)){
            $name = Member::where('email','like',$email."%")->get();
            foreach ($name as $k=>$v) {
                $member_id = $v['member_id'];
            }
            $where['tibi.user_id'] = $member_id;
        }
        $result =DB::table('tibi')
            ->join('member','tibi.user_id','=','member.member_id')
            ->join('currency','tibi.currency_id','=','currency.currency_id')
            ->select('tibi.*','member.email','currency.currency_name')
            ->where($where)
            ->whereIn('tibi.status',[2,3])
            ->paginate(10);
        return view('admin.currency.chongzhi_index',compact('curr','result','email','cuid'));
    }

    /**
     * 钱包币种管理--提币记录
     */
    public function getTibi()
    {
        //读取币种表
        $curr = Currency::get();
        $where = [];
        $member_id = "";
        $cuid = request('cuid');
        $email = request('email');
        if(!empty($cuid)){
            $where['tibi.currency_id'] = $cuid;
        }
        if(!empty($email)){
            $name = Member::where('email','like',$email.'%')->get();
            foreach ($name as $k=>$v) {
                $member_id = $v['member_id'];
            }
            $where['tibi.user_id'] = $member_id;
        }
        $result =DB::table('tibi')
            ->join('member','tibi.user_id','=','member.member_id')
            ->join('currency','tibi.currency_id','=','currency.currency_id')
            ->select('tibi.*','member.email','currency.currency_name')
            ->where($where)
            ->whereIn('tibi.status',[0,1])
            ->paginate(10);
        return view('admin.currency.tibi_index',compact('curr','result','email','cuid'));
    }


}
