<?php

namespace App\Http\Controllers\Admin;


use App\Models\Config;
use App\Models\Currency;
use App\Models\Download;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DownloadController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Download $download)
    {
        parent::__construct();
        $this->download = $download;
    }

    /**
     * 钱包币种管理--下载管理
     */
    public function getIndex()
    {
        $currency = Currency::get();
        $all = (new Config())->getCache();
        return view('admin.download.index',compact('currency','all'));
    }

    /**
     * 新币上传申请表
     */
    public function postBiaogeDownload()
    {
        $res = $this->download->biaogeDownload(request());
        if ($res['state'] == 200 || $res['state'] == 400) {
           return back()->with('message',$res['msg'])->withInput();
        }
    }

    /**
     * 钱包上传
     */
    public function postQianbaoDownload()
    {
        $res = $this->download->qianbaoDownload(request());
        if ($res['state'] == 200 || $res['state'] == 400) {
            return back()->with('message',$res['msg'])->withInput();
        }
    }

    /**
     * 币种官方网址
     */
    public function postGuanwangUrl()
    {
        $res = $this->download->guanwangUrl(request());
        if ($res['state'] == 200 || $res['state'] == 400) {
            return back()->with('message',$res['msg'])->withInput();
        }
    }
}
