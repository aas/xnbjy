<?php
namespace App\Http\Controllers\Admin;
use App\Models\CurrencyUser;
use App\Models\Member;
use Session;

class MemberController extends BaseController
{

    public function __construct(Member $member,CurrencyUser $currencyUser)
    {
        parent::__construct();
        $this->member = $member;
        $this->currencyUser = $currencyUser;
    }

    //会员列表
    public function getIndex()
    {
        $data = $this->member->getMemberList(request());
        return view('admin.member.index',compact('data'));
    }

    //添加会员页面
    public function getAdd()
    {
        return view('admin.member.addMember');
    }

    //添加会员处理
    public function postAdd()
    {
        $res = $this->member->addMember(request());
        return redirect()->back()->with('message', $res)->withInput();  
    }



    //添加会员个人信息页面
    public function getSaveModify()
    {
        $data = $this->member->getOneModify(request());
        return view('admin.member.saveModify',compact('data'));
    }

    //添加会员个人信息提交
    public function postSaveModify()
    {
        $res = $this->member->saveModify(request());
        return redirect()->back()->with('message', $res)->withInput(); 
    }

    //ajax判断邮箱
    public function getCheckEmail()
    {
        $res = $this->member->ajaxCheckEmail(request());
        return ajax_return($res);
    }

    //ajax验证昵称是否存在
    public function getCheckNick()
    {
        $res = $this->member->ajaxCheckNick(request());
        return ajax_return($res);
    }


    //ajax手机验证
    public function getCheckPhone()
    {
        $res = $this->member->ajaxCheckPhone(request());
        return ajax_return($res);
    }


    //修改会员页面
    public function getEdit()
    {
        $data = $this->member->getOneModify(request());
        return view('admin.member.saveMember',compact('data'));
    }

    //修改会员提交
    public function postEdit()
    {
        $res = $this->member->saveMember(request());
        return redirect()->back()->with('message', $res)->withInput();   //家昌版快闪重定向
    }

    //ajax删除会员
    public function postDel()
    {
        $res = $this->member->delMember(request());
        return ajax_return($res);
    }

    //会员推荐人页面
    public function getInvite()
    {
        $data = $this->member->showMyInvite(request());
        return view('admin.member.show_my_invit',compact('data'));
    }

    //查看个人币种信息(两张表currency_user表和currency表)
    public function getShow()
    {
        $info = $this->currencyUser->getCurrencyUserWithCurrency(request());
        $member_info = $this->member->getOneModify(request());
        return view('admin.member.show',compact('info','member_info'));
    }

    //ajax修改币种
    public function postMemberMoney()
    {
        $data = $this->member->updateMemberMoney(request());
        return ajax_return($data);
    }



}
?>