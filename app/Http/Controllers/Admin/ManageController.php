<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use App\Models\Manage;
use App\Models\Nav;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;

class ManageController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Manage $manage)
    {
        parent::__construct();
        $this->manage = $manage;
    }

    /**
     * 管理员管理
     */
    public function getIndex()
    {
        $id = session('admin_userid');
        if ($id !== 1) {
            abort(404);
        }
        $name = request('username');
        $where = [];
        if (!empty($name)) {
            $where['username'] = $name;
        }
        $data = Admin::where($where)->paginate(5);
        return view('admin.manage.index',compact('data','name'));
    }

    public function postIndex()
    {
        $re = $this->manage->index(request());
        if (!$re) {
            return back()->with('message',$this->manage->error)->withInput();
        }
    }

    /**
     * 新增管理员
     */
    public function getAdd_admin()
    {
        return view('admin.manage.addAdmin');
    }

    public function postAdd_admin()
    {
        $re = $this->manage->add_admin(request());
        if (!$re) {
            return back()->with('message',$this->manage->error)->withInput();
        }
    }

    /**
     * 管理员修改
     */
    public function getEdit_admin()
    {
        $admin = request('admin_id');
        $list = Manage::where('admin_id',$admin)->first();
        return view('admin.manage.editAdmin',compact('list'));
    }

    public function postEdit_admin()
    {
        $re = $this->manage->edit_admin(request());
        if (!$re) {
            return back()->with('message',$this->manage->error)->withInput();
        }
    }

    /**
     * 删除操作
     */
    public function postDel_admin()
    {
        $where['admin_id'] = request('admin_id');
        $re = $this->manage->del($where);
        if($re){
            $res['state'] = 200;
            $res['msg'] = '删除成功！';
            return $res;
        }
        $res['state'] = 400;
        $res['msg'] = '删除失败,请稍后再试！';
        return $res;

    }

    /**
     * 权限修改
     */
    public function getAuth_admin()
    {

            $where['admin_id'] = request('admin_id');
            $admin_id = request('admin_id');
            $admin = Manage::where($where)->first();
            $list = explode(',', $admin['nav']);
            $nav = Nav::get();
            foreach ($nav as $k => $v) {
                if (in_array($v['nav_id'], $list)) {
                    $nav[$k]['status'] = 1;
                }
            }

        return view('admin.manage.showNav',compact('nav','admin_id'));
    }

    public function postAuth_admin()
    {
        $re = $this->manage->auth_admin(request());
        if (!$re) {
            return back()->with('message',$this->manage->error)->withInput();
        }
    }

    /**
     * 密码修改
     */
    public function getEdit_pwd()
    {
        $admin_id = Session('admin_userid');
        $data = Manage::where('admin_id',$admin_id)->first();
        return view('admin.manage.pwdUpdate',compact('data'));
    }
    public function postEdit_pwd()
    {
        $re = $this->manage->edit_pwd(request());
        if (!$re) {
            return back()->with('message',$this->manage->error)->withInput();
        }
    }


}
