<?php
namespace App\Http\Controllers\Admin;
use App\Models\Art;
use App\Models\Arttype;
use Session;

class ArtController extends BaseController
{

    public function __construct(Art $art,Arttype $arttype)
    {
        parent::__construct();
        $this->art = $art;
        $this->arttype = $arttype;
    }

    //官方公告列表
    public function getNotesIndex()
    {
        $data = $this->art->getNotesList(request());
        return view('admin.art.notesindex',compact('data','type'));
    }

    //官方公告添加
    public function getNotesAdd()
    {
        $type = $this->arttype->getOne(['id'=>request('article_category_id')]);
        return view('admin.art.notesadd',compact('type'));

    }

    //官方公告添加
    public function postNotesAdd()
    {

        $res = $this->art->AddOneNote(request());

//        if($res['status']==1){
//
//            return redirect()->action('Admin\ArtController@getNotesIndex')->with('message', $res['msg'])->withInput();
//
//        }

        return redirect()->back()->with('message', $res['msg'])->with('status',$res['status'])->with('url',$res['url'])->withInput();

    }

    //官方公告修改页面
    public function getNotesEdit()
    {
        $data = $this->art->getOneNote(request());
        $type = $this->arttype->getOne(['id'=>request('article_category_id')]);
        return view('admin.art.notesedit',compact('data','type'));
    }

    //官方公告修改提交
    public function postNotesEdit()
    {

        $res = $this->art->saveOneNote(request());

//        if($res['status']==1){
//
//            return redirect()->action('Admin\ArtController@getNotesIndex')->with('message', $res['msg'])->withInput();
//
//        }

        return redirect()->back()->with('message', $res['msg'])->with('status',$res['status'])->with('url',$res['url'])->withInput();

    }

    //官方公告删除
    public function postNotesDel()
    {
        $res = $this->art->deleteOneArticle(request());
        return ajax_return($res);
    }


    //团队信息管理列表
    public function getGroupIndex()
    {
        $data = $this->art->GroupArticleList(request());
        return view('admin.art.groupindex',compact('data'));
    }

    //团队信息添加
    public function getGroupAdd()
    {
        return view('admin.art.groupadd');
    }


    //帮助中心列表
    public function getHelpIndex()
    {
        $data = $this->art->getHelpIndex(request());
        $type = $this->arttype->getList(['parent_id'=>6])->toArray(); //查找一级分类
        return view('admin.art.helpindex',compact('data','type'));
    }


    //帮助中心添加页面
    public function getHelpAdd()
    {
        $type = $this->arttype->getList(['parent_id'=>6])->toArray(); //查找一级分类
        return view('admin.art.helpadd',compact('type'));
    }


    //添加帮助中心文章
    public function postHelpAdd()
    {
        $res = $this->art->AddOneHelp(request());
        return redirect()->back()->with('message', $res['msg'])->with('status',$res['status'])->withInput();

    }

    //修改帮助中心
    public function getHelpEdit()
    {
        $data = $this->art->getOneArticle(request());
        $type = $this->arttype->getList(['parent_id'=>6])->toArray(); //查找一级分类
        return view('admin.art.helpedit',compact('data','type'));
    }

    //修改帮助中心
    public function postHelpEdit()
    {
        $res = $this->art->SaveOneHelp(request());
        return redirect()->back()->with('message', $res['msg'])->with('status',$res['status'])->withInput();

    }

    public function getHelpDel()
{
$res = $this->art->deleteOneArticle(request());
return redirect()->back()->with('message', $res['msg'])->withInput();
}


}
