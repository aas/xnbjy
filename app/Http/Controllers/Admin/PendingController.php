<?php

namespace App\Http\Controllers\Admin;

use App\Models\Areas;
use App\Models\Config;
use App\Models\Withdraw;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PendingController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Withdraw $withdraw,Config $config)
    {
        parent::__construct();
        $this->withdraw = $withdraw;
        $this->config = $config;
    }

    /**
     * 财务管理--提现审核
     */
    public function getIndex()
    {
        $keyname = request('keyname');
        $status = request('status');
        $where = [];

        if (!empty($keyname)) {
            $where['bank.cardname'] = $keyname;
        }
        if (!empty($status)) {
            $where['withdraw.status'] = $status;
        }
        $data = $this->config->getCache();
        ob_clean();

        $page = request('page',1);
        $pageSize = 3;
        $offSite = ($page - 1) * $pageSize;

        $counSql = 'SELECT count(w.withdraw_id) as records FROM xnb_withdraw w LEFT JOIN xnb_bank b ON w.bank_id=b.id LEFT JOIN xnb_areas ab ON ab.area_id=b.address LEFT JOIN xnb_areas a ON a.area_id=ab.parent_id  LEFT JOIN xnb_member m ON m.member_id=w.uid ';

        $sql = 'SELECT w.*,b.cardname,b.bank_branch,b.cardnum,b.bankname,ab.area_name barea_name,ab.parent_id, ';
        $sql.= 'a.area_name aarea_name,m.nick,m.email FROM xnb_withdraw w LEFT JOIN xnb_bank b ON w.bank_id=b.id ';
        $sql.= 'LEFT JOIN xnb_areas ab ON ab.area_id=b.address LEFT JOIN xnb_areas a ON ';
        $sql.= 'a.area_id=ab.parent_id LEFT JOIN xnb_member m ON m.member_id=w.uid ';
        if (isset($where['bank.cardname'])) {
            $sql.= 'WHERE b.cardname like %'.$keyname.'% ';
            $counSql.= 'WHERE b.cardname like %'.$keyname.'% ';
        }
        if (isset($where['withdraw.status'])) {
            $sql.= 'WHERE w.status = '.$status.' ';
            $counSql.= 'WHERE w.status = '.$status.' ';
        }
        $sql.= ' GROUP BY w.withdraw_id ORDER BY w.status desc,w.add_time desc LIMIT '.$offSite.','.$pageSize.'';
        $result = DB::select($sql);
        //总条数
        $count = DB::select($counSql);
        $records = $count ? $count[0]->records : 0;
        //总页数
        $totalPage = ceil($records/$pageSize);
        if ($result) {
            $rows['data'] = object_array($result);
            $rows['last_page'] =intval(ceil(($records+$pageSize-1)/$pageSize));
            $rows['pageNoList'] = getPageNoList($totalPage,$page , 5);
            $rows['pre_page'] = $page - 1 ? $page - 1 : 1;
            $rows['next_page'] = $page + 1 ? $page + 1 : 1;
            $rows['page'] = request('page',1);
        }
        return view('admin.pending.index', compact('data', 'status', 'keyname', 'rows'));
    }

    /**
     * 表格导出
     */
    public function getPayExcel()
    {
        $res = $this->withdraw->payExcel(request());
        $this->ajaxReturn($res);
    }

    /**
     * 提现审核 通过操作
     */
    public function postSuccess_by()
    {
        $res = $this->withdraw->success_by(request());
        $this->ajaxReturn($res);
    }

    /**
     * 提现审核 不通过操作
     */
    public function postError_by()
    {
        $res = $this->withdraw->error_by(request());
        $this->ajaxReturn($res);
    }
}
