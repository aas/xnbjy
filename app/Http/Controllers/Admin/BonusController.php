<?php

namespace App\Http\Controllers\Admin;

use App\Models\Bonus;
use App\Models\Currency;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Finance;
class BonusController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Bonus $bonus)
    {
       parent::__construct();
       $this->bonus = $bonus;
    }

    /**
     * 分红管理--添加分红奖励
     */
    public function getIndex()
    {
        $result = Currency::where('status', '=', 0)->get();
        foreach ($result as $k => $v) {
            $money = DB::table('finance')
                ->where('currency_id',$v['currency_id'])
                ->where('type',10)
                ->sum('money');
            $sumMoney = DB::table('finance')
                ->where('currency_id',$v['currency_id'])
                ->where('type',11)
                ->sum('money');
            $data[$k]['money'] = $money;
            $data[$k]['sumMoney'] = $sumMoney;
            $data[$k]['currency_id'] = $v['currency_id'];
            $data[$k]['currency_name'] = $v['currency_name'];
        }
        return view('admin.bonus.index', compact('result','data'));

    }
    public function postAddList()
    {
        $res = $this->bonus->addList(request());
        if ($res['state'] == 400 || $res['state'] == 200) {
            return back()->with('message',$res['msg'])->withInput();
        }
    }

    /**
     * 分红管理--分红列表
     */
    public function getBonuslist()
    {
        $data = DB::table('finance')
            ->where('type',10)
            ->join('member','finance.member_id','=','member.member_id')
            ->select('member.name','finance.currency_id','finance.money','finance.add_time')
            ->paginate(15);
        foreach ($data as $k=>$v) {
            if ($v->currency_id == 0) {
               $data[$k]->currency_id = '人民币';
            } else {
                $result = Currency::where('currency_id',$v->currency_id)->first();
                $data[$k]->currency_id = $result['currency_name'];
            }
        }

        return view('admin.bonus.bonuslist',compact('data'));
    }
}
