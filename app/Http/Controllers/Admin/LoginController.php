<?php
namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use Illuminate\Support\Facades\Session;

class LoginController extends BaseController
{

    public function __construct(Admin $admin)
    {
        parent::__construct();
        $this->admin = $admin;
    }

    public function getLogin()
    {
        return view('admin.login.login');
    }

    public function postLogin()
    {
        $res = $this->admin->login(request());
        if (!$res) {
            return redirect()->back()->with('message', $this->admin->error)->withInput();
        }
        if ($res['status'] == 200) {
            return redirect('login')->with('message',$res['msg'])->withInput();
        }
        return redirect(urlAdmin('index','getIndex'));
    }

    public function getLogout()
    {

        session::forget('admin_userid');
        return redirect(urlAdmin('login','getLogin'));

    }

}
?>