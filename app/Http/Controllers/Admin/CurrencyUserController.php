<?php

namespace App\Http\Controllers\Admin;

use App\Models\Currency;
use App\Models\Member;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CurrencyUserController extends BaseController
{
    /**
     * 钱包币种管理--会员钱包充值列表
     */
    public function getMemberChongzhi()
    {
        $curr = Currency::get();
        $where = [];
        $member_id = "";
        $cuid = request('cuid');
        $email = request('email');
        if (!empty($cuid)) {
            $where['currency_user.currency_id'] = $cuid;
        }
        if (!empty($email)) {
            $name = Member::where('email','like',$email.'%')->get();
            foreach ($name as $k=>$v) {
                $member_id = $v['member_id'];
            }
            $where['currency_user.member_id'] = $member_id;
        }
        $result = DB::table('currency_user')
            ->join('member','currency_user.member_id','=','member.member_id')
            ->join('currency','currency_user.currency_id','=','currency.currency_id')
            ->select('currency_user.*','member.email','currency.currency_name')
            ->where($where)
            ->orderBy('currency_user.cu_id','desc')
            ->paginate(10);

        return view('admin.currencyuser.MemberQianbaoChongzhiUrl',compact('curr','result','email','cuid'));
    }

    /**
     * 钱包币种管理--会员钱包提币列表
     */
    public function getMemberTibi()
    {
        $curr = Currency::get();
        $where = [];
        $member_id = "";
        $cuid = request('cuid');
        $email = request('email');
        if(!empty($cuid)){
            $where['qianbao_address.currency_id'] = $cuid;
        }
        if(!empty($email)){
            $name = Member::where('email','like',$email.'%')->get();
            foreach ($name as $k=>$v) {
                $member_id = $v['member_id'];
            }
            $where['qianbao_address.user_id'] = $member_id;
        }
        $result = DB::table('qianbao_address')
            ->join('member','qianbao_address.user_id','=','member.member_id')
            ->join('currency','qianbao_address.currency_id','=','currency.currency_id')
            ->select('qianbao_address.*','member.email','currency.currency_name')
            ->where($where)
            ->orderBy('qianbao_address.id','desc')
            ->paginate(10);
        return view('admin.currencyuser.MemberQianbaoTibiUrl',compact('curr','result','email','cuid'));
    }
}
