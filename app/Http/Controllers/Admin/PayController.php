<?php

namespace App\Http\Controllers\Admin;

use App\Models\Currency;
use App\Models\Finance;
use App\Models\Member;
use App\Models\Pay;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
class PayController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Pay $pay)
    {
        parent::__construct();
        $this->pay = $pay;

    }

    /**
     * 财务管理--人工充值管理
     */
    public function getPayByMan()
    {
        $status = request('status');
        $member_name = request('member_name');
        $result = $this->pay->get_payByMan(request());
        foreach ($result as $k=>$v) {
            $result[$k]->status = payStatus($v->status);
//            $result[$k]['pay_reward'] = $v['money']*$this->config['pay_reward']/100;
        }
        return view('admin.pay.payByMan',compact('result','status','member_name'));
    }

    /**
     * 表格导出
     */
    public function getPayExcel()
    {
        $res = $this->pay->payExcel(request());
        return $res;
    }

    /**
     *  财务管理--人工充值管理修改操作
     */
    public function postPayEdit()
    {
        $res = $this->pay->payEdit(request());
       return $res;
    }



    /**
     * 财务管理--管理员充值管理
     */
    public function getAdm()
    {
        $data = Currency::get();
        $type_id = request('type_id');
        $email = request('email');
        $member_id = request('member_id');
        if (!empty($type_id)) {
            $where['currency_id'] = $type_id;
        }
        if (!empty($email)) {
            $uid = Member::where('email','like',$email.'%')->first();
            $where["member_id"] = $uid['member_id'];
        }
        if(!empty($member_id)){
            $where["member_id"] = $member_id;
        }
        $where['type'] = 3;
        $result = Pay::where($where)->paginate(5);
        foreach ($result as $k=>$v) {
            $member = Member::where('member_id',$v['member_id'])->first();
            $currency = Currency::where('currency_id',$v['currency_id'])->first();
            $result[$k]['email'] = $member['email'];
            $result[$k]['currency_name'] = $currency['currency_name'];
        }
        return view('admin.pay.admRecharge',compact('data','result','type_id','email','member_id'));
    }

    public function postAdmAdd()
    {
        $res = $this->pay->admAdd(request());
        if (!$res) {
            return back()->with('message', $this->pay->error)->withInput();
        }
    }

    /**
     * 财务管理--第三方充值记录
     */
    public function getFill()
    {
        $result = DB::table('fill')->paginate(10);
        return view('admin.pay.fill',compact('result'));
    }

}
