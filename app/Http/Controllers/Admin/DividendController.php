<?php

namespace App\Http\Controllers\Admin;

use App\Models\Currency;
use App\Models\Dividend;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DividendController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Dividend $dividend)
    {
      parent::__construct();
      $this->dividend = $dividend;
    }
    /**
     * 分红管理--分红股管理
     */
    public function getIndex()
    {
        $data = Dividend::all();
        foreach ($data as $k=>$v){
            $list[$v['name']] = $v['value'];
        }
        $currency = DB::table('currency')
            ->select('currency_id','currency_name')
            ->get();
        return view('admin.dividend.index',compact('list','currency'));
    }

    public function postIndexEdit()
    {
        $res =  $this->dividend->indexEdit(request());
        if ($res['state'] == 200 || $res['state'] == 400) {
            return back()->with('message',$res['msg'])->withInput();
        }
    }
}
