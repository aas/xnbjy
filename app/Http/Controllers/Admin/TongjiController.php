<?php
namespace App\Http\Controllers\Admin;
use App\Models\Currency;
use App\Models\CurrencyUser;
use App\Models\Member;
use App\Models\Tongji;
use Illuminate\Support\Facades\DB;
use Session;

class TongjiController extends BaseController
{

    public function __construct(Tongji $tongji,Currency $currency,CurrencyUser $currencyUser,Member $member)
    {
        parent::__construct();
        $this->tongji = $tongji;
        $this->currency = $currency;
        $this->currencyUser = $currencyUser;
        $this->member = $member;
    }

    //推广排名
    public function getTuiGuang()
    {

        $memberid = request('userid');
        $stime = strtotime(request('stime'));
        $etime = strtotime(request('etime'));
        if(!empty($memberid)){
            $where['pid']=$memberid;
        }
        if(!empty($stime)){
            $where['reg_time']=['>=',$stime];
        }
        if(!empty($etime)){
            $where['reg_time']=['<=',$stime];
        }
        if(!empty($etime)&&!empty($stime)){
            $where['reg_time'] = ['glt',[['>',$stime],['<=',$etime]]];
        }
        if(empty($memberid)){
            $where['pid'] = ['!=',''];
        }
        $list = $this->getMemberTuijianPaiming($where);

        //获取本月第一天/最后一天的时间戳
        $year = date("Y");
        $month = date("m");
        $allday = date("t");
        $start_time = strtotime($year."-".$month."-1");
        $end_time = strtotime($year."-".$month."-".$allday);

        $monthWhere['reg_time'] = ['between',[$start_time,$end_time]];
        $monthWhere['pid'] = ['!=',''];
        $month_data = $this->getMemberTuijianPaiming($monthWhere);


        //获取本周第一天/最后一天的时间戳
        $year = date("Y");
        $month = date("m");
        $day = date('w');
        $nowMonthDay = date("t");
        $firstday = date('d') - $day;
        if(substr($firstday,0,1) == "-"){
            $firstMonth = $month - 1;
            $lastMonthDay = date("t",$firstMonth);
            $firstday = $lastMonthDay - substr($firstday,1);
            $time_1 = strtotime($year."-".$firstMonth."-".$firstday);
        }else{
            $time_1 = strtotime($year."-".$month."-".$firstday);
        }

        $lastday = date('d') + (7 - $day);
        if($lastday > $nowMonthDay){
            $lastday = $lastday - $nowMonthDay;
            $lastMonth = $month + 1;
            $time_2 = strtotime($year."-".$lastMonth."-".$lastday);
        }else{
            $time_2 = strtotime($year."-".$month."-".$lastday);
        }
        $weekWhere['reg_time'] = ['between',[$time_1,$time_2]];
        $weekWhere['pid'] = ['!=',''];
        $week_data = $this->getMemberTuijianPaiming($weekWhere);

        return view('admin.tongji.tuiguang',compact('list','month_data','week_data'));

    }

    //获取推荐排名信息
    public function getMemberTuijianPaiming($where='')
    {

        $list = $this->member->select(DB::raw('count(member_id) as num'),'pid')->orderBy('num','desc')->multiWhere($where)->groupBy('pid')->get();

        foreach ($list as $k=>$v){
            $list[$k]['pname']=$this->getMemberEailByMemberId($v['pid']);
        }

        return $list;

    }

    //根据id获取名称
    public function getMemberEailByMemberId($id)
    {
        $where['member_id'] = $id;
        $list = $this->member->select('email')->where($where)->first();
        if($list){
            return $list['email'];
        }
        return '';
    }


    //数据统计
    public function getShuju()
    {
        $where = [];
        //$a = null;
        $memberid = request('userid');
        //dd($memberid);

        $stime = strtotime(request('stime'));
        $etime = strtotime(request('etime'));
        if(!empty($memberid)){
            $where['member_id'] = $memberid;
        }
        if(!empty($stime)){
            $where['add_time'] = ['>=',$stime];
        }
        if(!empty($etime)){
            $where['add_time'] = ['<=',$stime];
        }
        if(!empty($etime)&&!empty($stime)){
            $where['add_time']=['between',[$stime,$etime]];
        }
        $list = $this->currency->getCurrencyAll();
        //dd($where);
        //充值统计+提现统计
        foreach ($list as $k=>$v){
            $alltj[$k]['currency_name'] = $v['currency_name'];
            $alltj[$k]['currency_id'] = $v['currency_id'];
            $alltj[$k]['currency_mark'] = $v['currency_mark'];
            $alltj[$k]['paytj'] = $this->tongji->getCurrencyPayTongjiByCurrencyId($v['currency_id'],$where);
            $alltj[$k]['tibitj'] = $this->tongji->getCurrencyTibiTongjiByCurrencyId($v['currency_id'],$where);
        }

        //挂单统计
        $order = $this->tongji->getCurrencyOrderTongji($where);

        //成交统计
        $trade = $this->tongji->getCurrencyTradeTongji($where);
        return view('admin.tongji.shuju',compact('alltj','order','trade'));
    }

    //详细统计
    public function getXiangxi()
    {
        $wherepaytj = [];
        $where = [];

        $memberid = request('userid');
        $stime = strtotime(request('stime'));
        $etime = strtotime(request('etime'));
        if(!empty($memberid)){
            $where['member_id'] = $memberid;
            $wherepaytj['member_id'] = $memberid;
        }
        if(!empty($stime)){
            $where['reg_time'] = ['>=',$stime];
        }
        if(!empty($etime)){
            $where['add_time'] = ['<=',$stime];
        }
        if(!empty($etime)&&!empty($stime)){
            $where['add_time'] = ['between',[$stime,$etime]];
        }
        if(empty($memberid)){
            //$where['pid'] = ['!=',''];
        }



        //获取所有币种类型
        $list = $this->currency->getCurrencyAll2();

        foreach ($list as $k=>$v){
            $alltj[$k]['paytj'] = $this->currency->getCurrencyNumByCurrencyId($v['currency_id'],$wherepaytj); //余额统计,冻结统计
            $alltj[$k]['membertj'] = $this->currency->getCurrencyMemberNumByCurrencyId($v['currency_id'],$wherepaytj);//用户余额
            $alltj[$k]['cztxtj'] = $this->currency->getCurrencyTibiByCurrencyId($v['currency_id'],$where);//充值提现
            $where['currency_id'] = $v['currency_id'];
            $order[] = $this->tongji->getCurrencyOrderTongji($where);
            $trade[] = $this->tongji->getCurrencyTradeTongji($where);
            $list[$k]['currency'] = $this->currency->getCurrencynameById($v['currency_id']);
            $list[$k]['trade_currency'] = $this->currency->getCurrencynameById($v['trade_currency_id']); //挂单交易
        }
        return view('admin.tongji.xiangxi',compact('list','order','trade','alltj'));
    }

    //余额排名
    public function getYue()
    {
        //获取所有币种类型
        $currency_list = $this->currency->getCurrencyAll2();
        $currency = request('currency_id')?request('currency_id'):29;
        $cur = $this->currency->getCurrencynameById($currency);
        $cur['currency_id'] = $currency;
        $list = $this->currencyUser->getMemberCurrencyPmByCurrencyId($currency);

        return view('admin.tongji.yue',compact('cur','list','currency_list'));
    }


}
