<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Flash;
class FlashController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Flash $flash)
    {
        parent::__construct();
        $this->flash = $flash;
    }

    /**
     * 常用设置--幻灯管理模块
     */
    public function getIndex()
    {
        $data = $this->flash->getAll();
//        $data = Flash::paginate(5);
        return view('admin.flash.index',compact('data'));
    }
    /**
     * 添加模块
     */
    public function getAdd()
    {
        return view('admin.flash.add');
    }

    public function postAddList()
    {
        $res = $this->flash->addList(request());
        if ($res['state'] == 200 || $res['state'] == 400 || $res['state'] == 0) {
          return back()->with('message',$res['msg'])->withInput();
        }
    }
    

    /**
     * 修改
     */
    public function getEdit()
    {
        $where['flash_id'] = request('flash_id');
        $data = $this->flash->getOne($where);
        return view('admin.flash.edit',compact('data'));
    }

    public function postEditList()
    {
     $res = $this->flash->editList(request());
     if ($res['state'] == 200 || $res['state'] == 400 || $res['state'] == 0) {
         return back()->with('message',$res['msg'])->withInput();
     }
    }

    /**
     * 删除
     */
    public function getDel()
    {
        $where['flash_id'] = $_GET['flash_id'];
        $re = $this->flash->del($where);
        if($re){
            $res['state'] = 200;
            $res['msg'] = '删除成功！';
            return $res;
        }
        $res['state'] = 400;
        $res['msg'] = '删除失败,请稍后再试！';
        return $res;

    }

}
