<?php
namespace App\Http\Controllers\Admin;
use App\Models\Message;
use App\Models\MessageAll;
use App\Models\MessageCategory;
use Session;

class MessageController extends BaseController
{

    public function __construct(Message $message,MessageAll $messageAll,MessageCategory $messageCategory)
    {
        parent::__construct();
        $this->message = $message;
        $this->messageAll = $messageAll;
        $this->messageCategory = $messageCategory;
    }

    //系统消息列表
    public function getIndex()
    {
        $data = $this->message->getMessageList(request());
        //dd($data);
        return view('admin.message.index',compact('data'));
    }

    //新增消息
    public function getAdd()
    {
        $type = $this->messageCategory->getMessageCategoryList();
        return view('admin.message.addMessage',compact('type'));
    }

    //新增消息处理
    public function postAdd()
    {
        $res = $this->messageAll->addAllMessage(request());
        return redirect()->back()->with('message', $res)->withInput();   
    }

    //修改消息
    public function getEdit()
    {
        $list = $this->messageAll->getOneMessage(request());     //消息详情
        $type = $this->messageCategory->getMessageCategoryList();  //消息类型
        return view('admin.message.saveMessage',compact('list','type'));
    }

    //修改消息提交
    public function postEdit()
    {
        $res = $this->websitebank->saveBank(request());
        return redirect()->back()->with('message', $res)->withInput();   
    }

    //ajax删除消息
    public function getDel()
    {
        $res = $this->messageAll->deleteMessage(request());
        return ajax_return($res);
    }

}
