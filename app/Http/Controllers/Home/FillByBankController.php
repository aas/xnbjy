<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FillByBankController extends BaseController
{

    public function __construct()
    {
        parent::__construct();

    }

    public function getIndex()
    {
        return view('home.fillbybank.index');
    }

}
