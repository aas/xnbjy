<?php

namespace App\Http\Controllers\Home;

use App\Models\Art;
use App\Models\Config;
use App\Models\Currency;
use App\Models\Flash;
use App\Models\Issue;
use App\Models\Link;
use App\Models\Trade;
use DB;

class IndexController extends BaseController
{
    /**
     * 实例化
     */

    public function __construct(Art $art, Config $config, Flash $flash, Trade $trade, Link $link, Currency $currency)
    {
        parent::__construct();
        $this->art = $art;
        $this->config = $config;
        $this->flash = $flash;
        $this->currency = $currency;
        $this->trade = $trade;
        $this->link = $link;

    }


    public function getIndex()
    {
        //页面右方公告，提示，资信
        $info1 = $this->art->info(1);
        $info_red1 = $this->art->info_red(1);//标红
        $info2 = $this->art->info(2);         //非标红
        $info_red2 = $this->art->info_red(2);//标红

        $flash = $this->flash->getList([], '*', ['sort' => 'desc'], 6);
        //币种
        $currency = $this->currency->getList(['is_line' => 1], '*', ['sort' => 'desc']);
        if ($currency) {
            $currency = $currency->toArray();
            foreach ($currency as $k => $v) {
                $list = $this->trade->getCurrencyMessageById($v['currency_id']);
                $v = (array)$v;
                $currency[$k] = array_merge($list, $v);
                $new_price = $list['new_price'] ? $list['new_price'] : 0;
                $currency[$k]['currency_all_money'] = floatval($v['currency_all_num']) * $new_price;
            }
        }
        //*********选择进盟币,安全可信赖begin*******
        $all_money = $this->trade->sum([], 'money');
        $config = $this->config->getCache();
        $all_money = $config['transaction_false'] + $all_money;
        $all_money = (string)round($all_money);
        for ($i = 0; $i < strlen($all_money); $i++) {
            $arr[strlen($all_money) - 1 - $i] = $all_money[$i];
        }
        //*********选择进盟币,安全可信赖end*******

        $link_info = $this->link->getList();
        if ($link_info) {
            $link_info = $link_info->toArray();
            //截断友情链接url头，统一写法
            foreach ($link_info as $k => $v) {
                $url = "";
                $url = trim($v['url'], 'https://');
                $link_info[$k]['url'] = $url;
            }
        }

        //*******众筹begin*******//
        $issue_list =  $issue_list = DB::table('issue')
            ->leftJoin('currency', 'issue.currency_id', '=', 'currency.currency_id')
            ->select('issue.*', DB::raw('(xnb_currency.currency_name) as name'))
            ->orderBy('id', 'desc')
            ->get();

        return view('home.index.index', compact('info1', 'info2', 'info_red1', 'info_red2', 'flash', 'currency', 'arr', 'link_info', 'issue_list'));
    }
}
