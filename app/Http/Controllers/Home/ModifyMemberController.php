<?php

namespace App\Http\Controllers\Home;

use App\Models\Config;
use App\Models\Member;

class ModifyMemberController extends BaseController
{

    public function __construct(Member $member, Config $config)
    {
        parent::__construct();
        $this->member = $member;
        $this->config = $config;
    }

    /**
     * 添加个人信息
     */
    public function getModify()
    {
        //判断是否是已经完成reg基本注册
        $this->checkLogin();
        return view('home.modifymember.modify');
    }

    public function postModify()
    {
        //判断是否是已经完成reg基本注册
        $this->checkLogin();
        $data = $this->member->modify(request());
        $this->ajaxReturn($data);
    }

    /**
     * ajax验证昵称是否存在
     */
    public function getAjaxCheckNick()
    {
        $data = $this->member->checkNick(request());
        $this->ajaxReturn($data);
    }

    /**
     * ajax手机验证
     */
    function getAjaxCheckPhone()
    {
        $data = $this->member->checkPhone(request());
        $this->ajaxReturn($data);
    }

    /**
     * ajax验证手机验证码
     */
    public function postAjaxSandPhone()
    {
        $data = $this->member->sandPhone(request());
        $this->ajaxReturn($data);
    }

    //验证前台登录
    public function checkLogin()
    {
        if (!session('USER_KEY') || !session('USER_KEY_ID')) {
            return redirect(urlHome('index','getIndex'));
        }
        if (session('STATUS') != 0) {
            return redirect(urlHome('index','getIndex'));
        }
    }

}