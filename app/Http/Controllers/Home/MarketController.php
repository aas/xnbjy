<?php

namespace App\Http\Controllers\Home;

use App\Models\Currency;
use App\Models\Market;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class MarketController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Market $market,Currency $currency)
    {
        parent::__construct();
        $this->market = $market;
        $this->currency = $currency;
    }

    /**
     * 行情中心
     */

    public function getIndex()
    {

        if (!empty(request('coin'))) {
            $whereMark['currency_mark'] = request('coin');
        }
        $whereMark['is_line'] = 1;
        $field = ['currency_id','currency_name','currency_logo','currency_mark'];
        $order = ['sort'=>''];
        $liCurrency = $this->currency->getList($whereMark,$field,$order);
        //判断 有没有 可以交易的币种
        if ($liCurrency->isEmpty()) {
            return $this->error('交易币种正在紧张筹备中！敬请期待', urlHome('Index', 'getIndex'));
        }
        $result = $this->market->index(request());
        $Deal = $result[0];
        $sell = $result[1];
        $listCurrency = $result[2];
        $liCurrency = $result[3];
        $buy = $result[4];
        $count = $result[5];
        return view('home.market.index', compact('$result', 'Deal', 'sell', 'listCurrency', 'liCurrency', 'buy', 'count'));
    }


    public function getCheck()
    {
        $res = $this->market->check(request());
        $this->ajaxReturn($res);
    }

    public function getGetMarket()
    {
        $res = $this->market->getMarket(request());
        $this->ajaxReturn($res);
    }
}
