<?php

namespace App\Http\Controllers\Home;

use App\Models\Config;
use App\Models\Reg;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class RegController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Reg $reg)
    {
        parent::__construct();
        $this->reg = $reg;
    }

    /**
     * 注册
     */
    public function getIndex()
    {
        $pid = intval(request('member_id'));
        return view('home.reg.reg',compact('pid'));
    }

    public function postIndex()
    {
        $re = $this->reg->register(request());
        if ($re['status'] == 0) {
            return back()->with('message',$re['msg'])->withInput();
        }
        if ($re['status'] == 1) {
            return redirect(urlHome('login','getIndex'))->with('message', $re['msg']);
        }
    }

    /**
     * ajax验证邮箱
     * @param string $email 规定传参数的结构
     *
     */
    public function getCheckEmail()
    {
        $re = $this->reg->checkEmail(request());
        $this->ajaxReturn($re);
    }

    public function getSuccess()
    {
        if(session('USER_KEY_ID')){
            return redirect(urlHome('User','getIndex'));
        }
        //判断步骤并重置
        if(session('procedure')==2){
            session('procedure',null);
            return view('home.reg.regSuccess');
        }
        if(session('procedure')==1){
            return redirect(urlHome('Reg','getIndex'));
        }
    }

    /**
     * 显示服务条款
     */
    public function terms(){
        return view('home.reg.terms');
    }

}
