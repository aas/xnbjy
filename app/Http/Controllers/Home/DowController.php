<?php

namespace App\Http\Controllers\Home;

use App\Models\Dow;
use App\Http\Requests;

class DowController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Dow $dow)
    {
        parent::__construct();
        $this->dow = $dow;
    }

    /**
     * 下载中心
     */
    public function getIndex()
    {
        $result = $this->dow->index(request());
        return view('home.dow.index',compact('result'));
    }

    /**
     * 首页面我要上新币
     */
    public function getNewcoin()
    {
        return view('home.dow.newcoin');
    }
}
