<?php

namespace App\Http\Controllers\Home;

use App\Models\Fill;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FillController extends BaseController
{

    public function __construct(Fill $fill)
    {
        parent::__construct();
        $this->fill = $fill;
    }

    //网银支付日志
    public function getIndex()
    {
        $list = $this->fill->getFillList();
        return view('home.fill.index',compact('list'));
    }




}
