<?php

namespace App\Http\Controllers\Home;

use App\Models\Currency;
use App\Models\Finance;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class FinanceController extends BaseController
{
    public function __construct(Finance $finance,Currency $currency)
    {
        parent::__construct();
        $this->finance = $finance;
        $this->currency = $currency;
    }


    //财务日志
    public function getIndex()
    {
        $list = $this->finance->getFinanceLog();
        $count = $this->finance->getFinanceNum();
        return view('home.finance.index',compact('count','list'));
    }

}
