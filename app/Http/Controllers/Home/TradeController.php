<?php

namespace App\Http\Controllers\Home;

use App\Models\Currency;
use App\Models\Trade;

class TradeController extends BaseController
{
    public function __construct(Trade $trade,Currency $currency)
    {
        parent::__construct();
        $this->trade = $trade;
        $this->currency = $currency;
    }

    public function getIndex()
    {
        //获取主币种
        $currency=$this->currency->getCurrencyByCurrencyId();
        $currencytype = request('currency');
        if($currencytype){
            $where['currency_id'] =$currencytype;
        }
        $where['member_id'] = session('USER_KEY_ID');
        $rows = $this->trade->getTradeListWithCurrency($where);
        return view('home.trade.mydeal',compact('currency','rows'));
    }

    /**
     * 交易大厅--买入
     */
    public function postBuy()
    {
        $result = $this->trade->buy(request());
        $this->ajaxReturn($result);
    }

    /**
     * 交易大厅--卖出
     */
    public function postSell()
    {
        $result = $this->trade->sell(request());
        $this->ajaxReturn($result);
    }
}
