<?php

namespace App\Http\Controllers\Home;

use App\Models\Config;
use App\Models\Help;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HelpController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Help $help)
    {
        parent::__construct();
        $this->help = $help;
    }

    /**
     * 帮助中心首页
     */
    public function getIndex()
    {

        $result = $this->help->index(request());
        $art_one = $result[0];
        $art_list = $result[1];

        return view('home/help/index',compact('art_one','art_list'));
    }
}
