<?php

namespace App\Http\Controllers\Home;

use App\Models\Art;
use App\Models\Config;
use App\Http\Requests;

class Artcontroller extends BaseController
{
    /**
     *实例化
     */
    public function __construct(Art $art)
    {
        parent::__construct();
        $this->art = $art;
    }

    /**
     * 最新动态
     */
    public function getIndex()
    {
        $result = $this->art->index(request());
        return view('home.art.index',compact('result'));
    }
    
    /**
     * 文章详情
     */
    public function getDetails()
    {
        $result = $this->art->details(request());
        $art_one = $result[0];
        $art_list = $result[1];
        return view('home.art.details',compact('art_one','art_list'));
    }
}
