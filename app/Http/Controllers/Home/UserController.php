<?php

namespace App\Http\Controllers\Home;

use App\Models\Areas;
use App\Models\Art;
use App\Models\Bank;
use App\Models\Config;
use App\Models\Currency;
use App\Models\CurrencyUser;
use App\Models\ExaminePwdtrade;
use App\Models\Finance;
use App\Models\IssueLog;
use App\Models\Member;
use App\Models\Message;
use App\Models\Pay;
use App\Models\Websitebank;
use App\Models\Withdraw;
use DB;

class UserController extends CommonController
{
    public function __construct(
        CurrencyUser $currencyUser,
        Member $member,
        Areas $areas,
        ExaminePwdtrade $examinePwdtrade,
        IssueLog $issueLog,
        Currency $currency,
        Config $config,
        Pay $pay,
        Websitebank $websitebank,
        Art $art,
        Withdraw $withdraw,
        Bank $bank,
        Finance $finance,
        Message $message
    )
    {
        parent::__construct();
        $this->currencyUser = $currencyUser;
        $this->member = $member;
        $this->areas = $areas;
        $this->examinePwdtrade = $examinePwdtrade;
        $this->issueLog = $issueLog;
        $this->currency = $currency;
        $this->config = $config;
        $this->pay = $pay;
        $this->websitebank = $websitebank;
        $this->art = $art;
        $this->withdraw = $withdraw;
        $this->bank = $bank;
        $this->finance = $finance;
        $this->message = $message;
    }

    /**
     * 账户资产
     */
    public function getIndex()
    {
        $u_info = $this->member->getOne(['member_id' => session('USER_KEY_ID')], 'rmb,forzen_rmb');
        $currencyData = $this->currencyUser->getUserCurrencyData();
        $allmoneys = $currencyData['allmoneys'] + $u_info['rmb'] + $u_info['forzen_rmb'];
        $currency_user = $currencyData['currency_user'];
        return view('home.user.index', compact('u_info', 'allmoneys', 'currency_user'));
    }


    public function getZhongchou()
    {
        $rows = $this->issueLog->getMyZhongChouList();
        $currency = $this->currency->getList();
        if ($rows['data']) {
            foreach ($rows['data'] as $k => $v) {
                foreach ($currency as $m) {
                    if ($v['issue']['currency_id'] == $m['currency_id']) {
                        $rows['data'][$k]['issue']['currency_name'] = $m['currency_name'];
                    }
                }

            }
            $rows['pageNoList'] = getPageNoList($rows['last_page'], request('page', 1), 5);
        }
        return view('home.user.zhongchou', compact('rows'));
    }

    /**
     * 修改会员信息
     */
    public function getUpdatePassword()
    {
        if (!session('STATUS')) {
            return $this->error('请填写个人信息', urlHome('ModifyMember', 'getModify'));
        }
        return view('home.user.update_password');
    }

    public function checkPwd($pwd)
    {
        $pattern = "/^[1-9|a-z|A-Z]{6,20}$/";
        if (preg_match($pattern, $pwd)) {
            return true;
        } else {
            return false;
        }
    }

    public function postUpdatePassword()
    {
        if (!(request('oldpwd') && request('pwd') && request('repwd'))) {
            $data['status'] = 2;
            $data['info'] = '请输入相应密码';
            $this->ajaxReturn($data);
        }
        $oldPwd = md5(trim(request('oldpwd')));
        $newPwd = md5(trim(request('pwd')));
        $rePwd = md5(trim(request('repwd')));
        if (!$this->checkPwd(request('oldpwd')) || !$this->checkPwd(request('pwd')) || !$this->checkPwd(request('repwd'))) {
            $data['status'] = 2;
            $data['info'] = '请输入6-20位密码';
            $this->ajaxReturn($data);
        }
        if ($rePwd != $newPwd) {
            $data['status'] = 2;
            $data['info'] = '两次输入的密码不一致';
            $this->ajaxReturn($data);
        }
        $r = $this->member->getOne(['member_id' => session('USER_KEY_ID'), 'pwd' => $oldPwd]);
        if (!$r) {
            $data['status'] = 2;
            $data['info'] = '原始密码输入错误';
            $this->ajaxReturn($data);
        }
        if ($newPwd === $oldPwd) {
            $data['status'] = 2;
            $data['info'] = '新密码不能和密码一样';
            $this->ajaxReturn($data);
        }
        $data['pwd'] = $newPwd;
        $s = $this->member->up(['member_id' => session('USER_KEY_ID')], $data);
        if (!$s) {
            $data['status'] = 2;
            $data['info'] = '服务器繁忙请稍后重试';
            $this->ajaxReturn($data);
        }
        $data['status'] = 1;
        $data['info'] = '修改成功..请重新登录';
        session()->forget('USER_KEY_ID', 'USER_KEY', 'STATUS');
        session()->save();
        $this->ajaxReturn($data);
    }

    /**
     * 币种与人民币兑换
     * @param number $rmb 人民币数量
     * @param number $bili 兑换比例
     * @param unknown $currency_id 兑换币种ID
     */
    public function rmbChangeCurrency($rmb = 0, $bili = 1, $currency_id)
    {
        $this->member->start();
        try {
            $this->member->dec(['member_id' => session('USER_KEY_ID')], 'rmb', $rmb);
            $this->currencyUser->inc(['member_id' => session('USER_KEY_ID'), 'currency_id' => $currency_id], 'num', $rmb / $bili);
            $this->member->commit();
            return $this->success('兑换成功');
        } catch (\Exception $e) {
            $this->member->rollBack();
            return $this->error('兑换失败');
        }
    }

    /**
     * 修改支付密码
     */
    public function postUpdatePwdTrade()
    {
        $member_id = session('USER_KEY_ID');
        $info = $this->member->getOne(['member_id' => $member_id]);
        $pwd = request('oldpwd_b');
        $oldpwdtrade = request('oldpwdtrade_b');
        $data['pwdtrade'] = request('pwdtrade');
        $repwdtrade = request('repwdtrade');
        $data['add_time'] = time();
        $data['u_id'] = $member_id;
        $data['idcard'] = $info['idcard'];
        $data['idcardPositive'] = null;//判断后赋值
        $data['idcardSide'] = null;//判断后赋值
        $data['idcardHold'] = null;//判断后赋值
        $examine = $this->examinePwdtrade->getList(['u_id' => $member_id, 'status' => 0]);
        if (count($examine)) {
            return $this->error("您已提交过,正在审核中..");
        }
        if (!$this->checkPwd($pwd)) {
            return $this->error("密码输入位数不正确");
        }
        if (!$this->checkPwd($oldpwdtrade)) {
            return $this->error("交易密码输入位数不正确");
        }
        if (!$this->checkPwd($data['pwdtrade'])) {
            return $this->error("新密码输入位数不正确");
        }
        if ($data['pwdtrade'] != $repwdtrade) {
            return $this->error("两次支付密码输入不一致");
        }
        if ($info['pwd'] != md5($pwd)) {
            return $this->error("密码输入错误");
        }
        if ($info['pwdtrade'] != md5($oldpwdtrade)) {
            return $this->error("原始支付密码输入错误");
        }
        if ($info['pwd'] == md5($data['pwdtrade'])) {
            return $this->error("支付密码不能与登录密码一致");
        }
        // 上传文件
        $pic_1 = $this->upload('pic_1', getRandNum(15));
        if (!$pic_1) {
            return $this->error('图片1上传失败');
        }
        $pic_2 = $this->upload('pic_2', getRandNum(15));
        if (!$pic_2) {
            return $this->error('图片2上传失败');
        }
        $pic_3 = $this->upload('pic_3', getRandNum(15));
        if (!$pic_3) {
            return $this->error('图片3上传失败');
        }
        $idcardPositive = ltrim($pic_1);
        $idcardSide = ltrim($pic_2);
        $idcardHold = ltrim($pic_3);
        $data['pwdtrade'] = request('pwdtrade') ? md5(request('pwdtrade')) : '';
        $data['idcardPositive'] = $idcardPositive ? $idcardPositive : '';//判断后赋值
        $data['idcardSide'] = $idcardSide ? $idcardSide : '';//判断后赋值
        $data['idcardHold'] = $idcardHold ? $idcardHold : '';//判断后赋值

        $r = $this->examinePwdtrade->add($data);
        if (!$r) {
            return $this->error('服务器繁忙,请稍后重试');
        }
        return $this->success('申请成功,审核后会以系统通知通知您', urlHome('User', 'getIndex'));
    }

    public function upload($file, $name)
    {
        $file = request()->file($file);
        $pic = '/upload/user/authentication/' . $name . '.' . $file->extension();
        $res = $file->move(public_path('upload/user/authentication/'), $name . '.' . $file->extension());
        if (!$res) {
            return false;
        }
        return $pic;
    }

    //个人信息
    public function getUpdateMessage()
    {
        //判断是否需要进行信息补全
        $list = $this->member->getOne(['member_id' => session('USER_KEY_ID')]);
        if ($list['status'] == 0) {
            addSession('STATUS', 0);
            return $this->error('请填写个人信息', urlHome('ModifyMember', 'getModify'));
        }
        $where['area_id'] = ['in', [$list['city'], $list['province']]];
        $res = $this->areas->getList($where, 'area_name');
        $list['area_name_city'] = isset($res[0]) ? $res[0]['area_name'] : '';
        $list['area_name_province'] = isset($res[1]) ? $res[1]['area_name'] : '';
        $areas = $this->areas->getList(['area_type' => 1]);
        return view('home.user.update_massage', compact('areas', 'list'));
    }

    //个人信息
    public function postUpdateMessage()
    {
        $member_id = session('USER_KEY_ID');
        $list = $this->member->getOne(['member_id' => $member_id]);
        $data['nick'] = request('nick', '');
        $data['province'] = intval(request('province'));
        $data['city'] = intval(request('city'));
        $data['job'] = request('job', '');
        $data['head'] = request('head', '');
        $data['profile'] = html_entity_decode(request('profile', ''));
        if ($data['nick'] != $list['nick']) {
            $where = null;
            $where['member_id'] = ['<>', $member_id];
            $where['nick'] = $data['nick'];
            if ($this->member->getOne($where, 'nick')) {
                $data['status'] = 2;
                $data['info'] = '昵称重复';
                $this->ajaxReturn($data);
            }
        }
        if (empty($data['province'])) {
            $data['status'] = 2;
            $data['info'] = '请填写所在省份';
            $this->ajaxReturn($data);
        }
        if (empty($data['city'])) {
            $data['status'] = 2;
            $data['info'] = '请填写所在城市';
            $this->ajaxReturn($data);
        }
        $r = $this->member->up(['member_id' => $member_id], $data);
        if (!$r) {
            $data['status'] = 2;
            $data['info'] = '服务器繁忙,请稍后重试';
            $this->ajaxReturn($data);
        }
        $data['status'] = 1;
        $data['info'] = '修改成功';
        $this->ajaxReturn($data);
    }

    /**
     * 充值
     */
    public function getPay()
    {
        $config = $this->config->getCache();
        $member = $this->member->getOne(['member_id' => session('USER_KEY_ID')]);
        $order_num = $this->pay->count(['member_name' => $member['name']]);
        //随机数
        $num = 0.01 * rand(10, 99);
        $fee = floatval($config['pay_fee'] + 0.01 * $order_num + $num);
        //支付表
        $where['member_name'] = $member['name'];
        $where['member_id'] = $member['member_id'];
        $where['type'] = ['<>', 3];
        $rows = $this->pay->getList($where, '*', ['pay_id' => 'desc'], 5)->toArray();
        if (count($rows['data'])) {
            foreach ($rows['data'] as $k => $v) {
                $rows['data'][$k]['status'] = payStatus($v['status']);
            }
            $rows['pageNoList'] = getPageNoList($rows['last_page'], request('page', 1), 5);
        }

        $bank = $this->websitebank->getList(['status' => 1]);
        //充值说明
        $art = $this->art->getOne(['article_id' => 102]);
        $art['content'] = html_entity_decode($art['content']);
        return view('home.user.pay', compact('rows', 'fee', 'bank', 'art', 'member'));
    }

    /**
     *  提现显示信息及添加信息
     */
    public function getDraw()
    {
        $where['uid'] = session('USER_KEY_ID');
        //提示文章显示
        $art = $this->art->getOne(['position_id' => 120]);
        $art['content'] = html_entity_decode($art['content']);
        //查找省份
        $areas = $this->areas->getList(['parent_id' => 1]);
        //查找当前登录人的提现地址
        $bank_info = object_array($this->bank->getBanAdres());
        //检测是否有10个地址
        $count = $this->bank->_count($where);
        $num = $count > 10 ? 2 : 1;
        //显示提现记录
        $draw_info = $this->withdraw->getWithdrawWithBank();
        //显示可用余额
        $member = $this->member->getOne(['member_id' => $where['uid']], 'rmb,phone,name');
        $auth = $member['name'];
        $rmb = $member['rmb'];
        return view('home.user.draw', compact('num', 'rmb', 'draw_info', 'bank_info', 'auth', 'areas', 'art', 'member'));
    }

    /**
     * 邀请好友
     */
    public function getInvit()
    {
        //我的邀请
//         $my_invit = M('Member')->field('email,status,reg_time')->where(array('pid'=>session('USER_KEY_ID')))->select();
        $my_invit = $this->member->getPageList(['pid' => session('USER_KEY_ID')], 'email,status,reg_time', '', 5, 5);
        if ($my_invit['data']) {
            foreach ($my_invit['data'] as $k => $vo) {
                $my_invit['data'][$k]['status_name'] = $vo['status'] ? "已填写个人信息" : "未填写个人信息";
            }
        }
        $where['position_id'] = ['in', [121, 122]];
        $res = $this->art->getList($where, 'title,content');
        $info = $res[0];
        $info1 = $res[1];
        $info['title'] = html_entity_decode($info['title']);
        $info['content'] = html_entity_decode($info['content']);
        $info1['title'] = html_entity_decode($info1['title']);
        $info1['content'] = html_entity_decode($info1['content']);
        //邀请获得总金额
        $count = sprintf("%.2f", $this->finance->_sum(['member_id' => session('USER_KEY_ID'), 'type' => 12], 'money'));
        return view('home.user.invit', compact('my_invit', 'info', 'info1', 'count'));
    }

    /**
     * 邀请好友
     */
    public function postInvit()
    {
        $config = $this->config->getCache();
        $emails = request('emails');
        $list = explode(";", $emails);
        $arr = [];
        if ($list) {
            foreach ($list as $k => $vo) {
                if ($this->member->_getOne(['email' => $vo])) {
                    $data['status'] = 0;
                    $data['info'] = "您输入的" . $vo . "邮箱已经注册";
                    $this->ajaxReturn($data);
                } else {
                    $arr[] = $vo;
                }
            }
            $url = urlHome('Reg', 'getIndex', ['member_id' => session('USER_KEY_ID')]);
            foreach ($arr as $vo) {
                $content = "<div>";
                $content .= "您好，<br><br>请点击链接：<br>";
                $content .= "<a target='_blank' href='{$url}' >完成注册邀请</a>";
                $content .= "<br><br>如果链接无法点击，请复制并打开以下网址：<br>";
                $content .= "<a target='_blank' href='{$url}' >{$url}</a>";
                if (setPostEmail($config['EMAIL_HOST'], $config['EMAIL_USERNAME'], $config['EMAIL_PASSWORD'], $config['name'] . '团队', $vo, $config['name'] . '团队[注册邀请]', $content)) {
                    $data['status'] = 0;
                    $data['info'] = "邮箱" . $vo . "发送失败";
                    $this->ajaxReturn($data);
                }
            }
            $data['status'] = 1;
            $data['info'] = "发送成功";
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 0;
            $data['info'] = "请输入发送邮箱";
            $this->ajaxReturn($data);
        }
    }

    /**
     * 系统消息
     */
    public function getSysmessage()
    {
        $where['member_id'] = session('USER_KEY_ID');
        $order['status'] = 'asc';
        $order['add_time'] = 'desc';
        $rows = $this->message->getMessageWithCategory($where, $order, 10);
        if ($rows) {
            $rows = $rows->toArray();
            $rows['pageNoList'] = getPageNoList($rows['last_page'], request('page', 1), 9);
        }
        return view('home.user.system_massage', compact('rows'));
    }

    /**
     *显示详细系统消息界面
     */
    public function getShowSystem()
    {
        $where['member_id'] = session('USER_KEY_ID');
        $where['message_id'] = intval(request('message_id'));
        $where['message_all_id'] = intval(request('message_all_id'));
        $list = $this->message->getMessageWithCategory($where);
        if (!$list) {
            abort(404);
        }
        //判断状态为0则是 未读 执行语句否则不执行标为已读
        if ($list['status'] == 0) {
            $status = $this->message->_updata($where, ['status' => 1]);
            if (!$status) {
                return $this->error('服务器繁忙请稍后重试');
            }
        }
        //右侧部分
        $where = null;
        $where['member_id'] = session('USER_KEY_ID');
        $right = $this->message->getMessageWithCategory($where, ['add_time' => 'desc'], 4)->toArray();
        return view('home.user.show_system', compact('list', 'right'));
    }

    /**
     * 添加提现银行信息
     * post
     * return ajax
     */
    public function postInsert()
    {
        $member = $this->member->_getOne(['member_id'=>session('USER_KEY_ID')]);
        //判断post是否为空
        $info['bname'] = request('new_label');
        $info['address'] = request('shi');
        $info['cardnum'] = request('account');
        $info['bankname'] = request('bank');
        $info['cardname'] = $member['name'];
        $info['uid'] = session('USER_KEY_ID');
        $info['bank_branch'] = request('bank_branch');
        $info['status'] = 0;
        $info = trim_fileds($info);
        if (empty($info['bname'])) {
            $data['status'] = 0;
            $data['info'] = '请填写标签';
            $this->ajaxReturn($data);
        }
        if (empty($info['bankname'])) {
            $data['status'] = 2;
            $data['info'] = '请选择银行';
            $this->ajaxReturn($data);
        }
        if (empty($info['address'])) {
            $data['status'] = 3;
            $data['info'] = '请选择开户地址';
            $this->ajaxReturn($data);
        }
        if (empty($info['bank_branch'])) {
            $data['status'] = 7;
            $data['info'] = '请选择开户支行';
            $this->ajaxReturn($data);
        }
        if (empty($info['cardnum'])) {
            $data['status'] = 4;
            $data['info'] = '请输入银行卡号';
            $this->ajaxReturn($data);
        }
        if (16 > strlen(request('account')) || strlen(request('account')) > 19) {

            $data['status'] = 5;
            $data['info'] = '请输入有效银行卡号';
            $this->ajaxReturn($data);
        }
        $re = $this->bank->_add($info);
        if ($re > 0) {
            $data['status'] = 1;
            $data['info'] = '操作成功';
            $this->ajaxReturn($data);
        } else {
            $data['status'] = 6;
            $data['info'] = '服务器繁忙,请稍后重试';
            $this->ajaxReturn($data);
        }
    }


    /**
     * 删除提现地址
     */
    public function postDelete()
    {
        $where['bank_id'] = intval(request('id'));
        //查询选择地址是否有提现记录
        $count = $this->withdraw->_count($where);
        //有记录，不许删除
        if ($count) {
            $arr['status'] = -1;
            $arr['info'] = "该地址尚有提现记录，无法删除！";
            $this->ajaxReturn($arr);
        }
        $re = $this->bank->_del(['id'=>$where['bank_id']]);
        if ($re) {
            $arr['status'] = 1;
            $arr['info'] = "操作成功";
            $this->ajaxReturn($arr);
        } else {
            $arr['status'] = 0;
            $arr['info'] = "服务器繁忙,请稍后重试";
            $this->ajaxReturn($arr);
        }
    }

    /**
     * 提现金额
     */
    public function postWithdraw()
    {
        //查询手续率$config['fee']
        $config = $this->config->getCache();
        //查找member_id对应的交易密码
        $where['member_id'] = session('USER_KEY_ID');
        //查找member表uid对应信息（交易密码，可以金额，冻结金额）
        $mem_data = $this->member->_getOne($where, 'pwdtrade,rmb,forzen_rmb');
        //交易密码
        $data['bank_id'] = request('select_bank');
        $data['all_money'] = floatval(request('money'));//提现金额
        $pwdtrade = md5(request('pwdtrade'));
        if (request('code') != session('code')) {
            $info['status'] = 11;
            $info['info'] = '验证码不正确';
            $this->ajaxReturn($info);
        }
        if (empty($data['all_money'])) {
            $info['status'] = 0;
            $info['info'] = "请填写提现金额";
            $this->ajaxReturn($info);
        }
        //单笔在100至50000在之间
        if ($data['all_money'] < 10 || $data['all_money'] > 500000) {
            $info['status'] = 2;
            $info['info'] = "提现金额超出限制";
            $this->ajaxReturn($info);
        }
        //单日是否超出50W限制
        $res = $this->maxwithdeaw_oneday(floatval(request('money')));
        if (!$res) {
            $info['status'] = 3;
            $info['info'] = "本次提现金额超出单日提现金额最大金额";
            $this->ajaxReturn($info);
        }
        //验证密码
        if (empty($pwdtrade)) {
            $info['status'] = 4;
            $info['info'] = "请填写交易密码";
            $this->ajaxReturn($info);
        }
        if ($pwdtrade != $mem_data['pwdtrade']) {
            $info['status'] = 5;
            $info['info'] = "交易密码填写错误";
            $this->ajaxReturn($info);
        }

        //验证是否选取地址
        if (empty($data['bank_id'])) {
            $info['status'] = 6;
            $info['info'] = "请选择提现地址";
            $this->ajaxReturn($info);
        }

        if ($data['all_money'] > $mem_data['rmb']) {
            $info['status'] = 6;
            $info['info'] = "账户余额不足";
            $this->ajaxReturn($info);
        }

        if ($mem_data['rmb'] < 100) {
            $info['status'] = 7;
            $info['info'] = "现金少于100，不能提现";
            $this->ajaxReturn($info);
        }

        //应付手续费
        $data['withdraw_fee'] = floatval(request('money')) * $config['fee'] * 0.01;
        //实际金额
        $data['money'] = floatval(request('money')) - $data['withdraw_fee'];
        //加时间
        $data['add_time'] = time();
        //加订单号
        $data['order_num'] = session('USER_KEY_ID') . "-" . $data['add_time'];
        //加uid辨明身份
        $data['uid'] = session('USER_KEY_ID');
        //保存可用金额修改信息
        $data_mem['rmb'] = $mem_data['rmb'] - floatval(request('money'));
        //保存冻结金额的修改信息
        $data_mem['forzen_rmb'] = $mem_data['forzen_rmb'] + floatval(request('money'));

        $res = $this->member->_updata($where, $data_mem);

        $re = $this->withdraw->_add($data);

        if ($re) {
            if ($res) {
                $info['status'] = 1;
                $info['info'] = "提现成功，24小时内到账 ";
                $this->ajaxReturn($info);
            } else {
                $info['status'] = 8;
                $info['info'] = "服务器繁忙,请稍后重试";
                $this->ajaxReturn($info);
            }
        } else {
            $info['status'] = 9;
            $info['info'] = "服务器繁忙,请稍后重试";
            $this->ajaxReturn($info);
        }
    }

    /**
     * 限制单日最多提现50万
     * @param float $num
     * @return boolean
     */
    private function maxwithdeaw_oneday($num)
    {
        //单日0时0分
        $time = strtotime(date('Y-M-d', time()));
        //从0时0分到当前时间
        $where['add_time'] = ['between', [$time, time()]];

        $where['uid'] = session('USER_KEY_ID');//
        //总钱数
        $money = $this->withdraw->_sum($where, 'all_money');
        //之前的总提现数是否超过了500000
        if ($money >= 500000) {
            return false;
        }
        //本次提现是否会超出500000
        $money_now = $money + $num;
        if ($money_now >= 500000) {
            return false;
        }
        return true;
    }

    public function postChexiaoByid()
    {
        $id = request('id');
        if (empty($id)) {
            $data['status'] = 0;
            $data['info'] = "参数错误";
            $this->ajaxReturn($data);
        }
        $where['withdraw_id'] = $id;
        //查询出对应id的提现金额,对应会员的会员id
        $money = $this->withdraw->_getOne($where, 'uid,all_money,status');
        //对应会员的可用金额和冻结金额
        $rmb = $this->member->_getOne(['member_id' => $money['uid']], 'rmb,forzen_rmb');
        //判断用户冻结金额是否出现负数
        if ($rmb['forzen_rmb'] < 0) {
            $data['status'] = 4;
            $data['info'] = "撤销失败，冻结金额有误，请联系管理员处理";
            $this->ajaxReturn($data);
        }
        //查状态是否在可操作的状态
        if ($money['status'] != 3) {
            $data['status'] = 5;
            $data['info'] = "请勿重复操作";
            $this->ajaxReturn($data);
        }
        //加回可用金额
        $money_back['rmb'] = $rmb['rmb'] + $money['all_money'];
        //减去冻结金额
        $money_back['forzen_rmb'] = $rmb['forzen_rmb'] - $money['all_money'];
        //修改数据库
        $re = $this->member->_updata(['member_id' => $money['uid']], $money_back);
        if (!$re) {
            $data['status'] = 2;
            $data['info'] = "撤销失败";
            $this->ajaxReturn($data);
        }
        $res = $this->withdraw->_del($where);
        if (!$res) {
            $data['status'] = 3;
            $data['info'] = "撤销失败";
            $this->ajaxReturn($data);
        }
        $data['status'] = 1;
        $data['info'] = "撤销成功";
        $this->ajaxReturn($data);
    }

    /**
     * 省级联动
     */
    public function postCity()
    {
        $area_id = intval(request('id'));
        if (!empty($area_id)) {
            $city_list = $this->areas->getList(['parent_id' => $area_id]);
//           foreach ($city_list as $vo) {
//               $op[] = '<option value="'.$vo['area_id'].'">'.$vo['area_name'].'</option>'; ;
//           }
//           $this->ajaxReturn($op);
            $this->ajaxReturn($city_list);
        }
    }

    /**
     * ajax上传图片方法
     */
    function postAddPicForAjax()
    {

        $file = request()->file('Filedata');
        $path = public_path('upload/member/head/');
        // 上传文件
        $info = $file->move($path, $file->getClientOriginalName());
        if (!$info) {// 上传错误提示错误信息
            $arr['status'] = 0;
            $arr['info'] = '上传失败';
            $this->ajaxReturn($arr);
        } else {
            // 上传成功
//            $pic_1=$info['head']['savepath'].$info['head']['savename'];
//            $pic=ltrim($pic_1,".");
            $arr['status'] = 1;
            $arr['info'] = '/upload/member/head/' . $file->getClientOriginalName();
            $this->ajaxReturn($arr);
        }
    }

    public function substitution_center()
    {

        $this->display();
    }

    public function duihuan()
    {

        $this->display();
    }

}
