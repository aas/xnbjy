<?php

namespace App\Http\Controllers\Home;

use App\Models\Currency;
use App\Models\Orders;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OrdersController extends BaseController
{
    public function __construct(Orders $orders)
    {
        parent::__construct();
        $this->orders = $orders;
    }

    /**
     * 交易显示页面
     */
    public function getIndex()
    {
        $result = $this->orders->index(request());
        $buy_record = $result[0];
        $sell_record = $result[1];
        $currency_message = $result[2];
        $currency_trade = $result[3];
        $user_currency_money = $result[4];
        $user_orders = $result[5];
        $buy_num = $result[6];
        $sell_num = $result[7];
        $currency = $result[8];
        $trade = $result[9];
        return view('home.orders.index', compact('buy_record', 'sell_record', 'currency_message', 'currency_trade', 'user_currency_money', 'user_orders', 'buy_num', 'sell_num', 'currency', 'trade'));

    }


    //交易大厅
    public function getCurrencyTrade()
    {
        $currency = $this->orders->getCurrencyTrade();
        return view('home.orders.currency_trade', compact('currency'));
    }


    /**
     * 获取挂单记录
     */
    public function postOrders()
    {
        $result = $this->orders->getOrders(request());
        $this->ajaxReturn($result);
    }

    /**
     * 获取k线图
     */
    public function getOrdersKline()
    {
        $data =  $this->orders->getOrdersKline(request());
        $this->ajaxReturn($data);;
    }


}
