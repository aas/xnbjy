<?php

namespace App\Http\Controllers\Home;

use App\Models\Currency;
use App\Models\Entrust;
use App\Models\Orders;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class EntrustController extends BaseController
{
    public function __construct(Entrust $entrust,Currency $currency,Orders $orders)
    {
        parent::__construct();
        $this->entrust = $entrust;
        $this->currency = $currency;
        $this->orders = $orders;
    }

    //委托管理
    public function getManage()
    {
        if (!session('STATUS')) {
            return $this->error('请填写个人信息', urlHome('ModifyMember', 'getModify'));
        }
        //获取主币种
        $currency = $this->currency->getCurrencyByCurrencyId();
        $currencytype = request('currency');
        $status = request('status');
        if (!empty($currencytype)) {
            $where['currency_id'] = $currencytype;
        }

        $where['status'] = ['in', [0,1]];
        $where['member_id'] = session('USER_KEY_ID');

        if (!empty($status)) {
            $where['status'] = ['in', $status];
        }
        $rows = $this->orders->getOrderListWithCurrency($where);
        return view('home.entrust.manage',compact('rows','currency'));
    }

    public function getHistory()
    {
        if (!session('STATUS')) {
            return $this->error('请填写个人信息', urlHome('ModifyMember', 'getModify'));
        }
        //获取主币种
        $currency = $this->currency->getCurrencyByCurrencyId();
        $currencytype = request('currency');
        $status = request('status');
        if (!empty($currencytype)) {
            $where['currency_id'] = $currencytype;
        }
        $where['status'] = ['in', [-1,2]];
        $where['member_id'] = session('USER_KEY_ID');
        if (!empty($status)) {
            $where['status'] = ['in', $status];
        }
        $rows = $this->orders->getOrderListWithCurrency($where);
        return view('home.entrust.history',compact('rows','currency'));
    }

    /**
     *  撤销方法
     */
    public function postCancel()
    {
        $order_id = request('order_id');
        if (empty($order_id)) {
            $info['status'] = 0;
            $info['info'] = '撤销订单不正确';
            $this->ajaxReturn($info);
        }
        //获取人的一个订单
        $one_order = $this->orders->getOneOrdersByMemberAndOrderId(session('USER_KEY_ID'), $order_id, array(0, 1));
        if (empty($one_order)) {
            $info['status'] = -1;
            $info['info'] = '传入信息错误';
            $this->ajaxReturn($info);
        }
        $info = $this->orders->cancelOrdersByOrderId($one_order);
        $this->ajaxReturn($info);

    }

}
