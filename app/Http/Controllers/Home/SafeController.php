<?php

namespace App\Http\Controllers\Home;

use App\Models\Member;

class SafeController extends BaseController
{
    //
    public function __construct(Member $member)
    {
        parent::__construct();
        $this->member = $member;
    }

    public function getIndex()
    {
        $member_id = session('USER_KEY_ID');
        $u_info = $this->member->getOne(['member_id' => $member_id]);
        return view('home.safe.index', compact('u_info'));
    }


}
