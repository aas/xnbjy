<?php

namespace App\Http\Controllers\Home;

use App\Models\CurrencyUser;
use App\Models\Issue;
use App\Models\IssueLog;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
class CommonController extends BaseController
{

    /**
     * 获取当前币种的信息
     * @param int $id 币种id
     * @return 24H成交量 24H_done_num  24H成交额 24H_done_money 24H涨跌 24H_change 7D涨跌  7D_change
     * @return 最新价格 new_price 买一价 buy_one_price 卖一价 sell_one_price 最高价 max_price 最低价 min_price
     */

    public function getCurrencyMessageById($id){
        $data['24H_done_num'] = '';
        $data['24H_done_money'] = '';
        $where['currency_id'] = $id;
        $time = time();
        //一天前的时间
        $old_time = strtotime(date('Y-m-d',$time));
        //最新价格
        $rs = DB::table('trade')->where($where)->orderBy('add_time','desc')->first();
        if ($rs == null) {
            $data['new_price'] = '';
        }else{
            $data['new_price'] = $rs->price;
        }

        //判断价格是升是降
        $re = DB::table('trade')->where($where)->where('add_time','<',$old_time)->orderBy('add_time','desc')->first();
        if ($re == null) {
            $data['new_price_status'] = 1;
        }else {
            if($re->price > $rs->price){
                //说明价格下降
                $data['new_price_status'] = 0;
            }else{
                $data['new_price_status'] = 1;
            }
        }

        //24H涨跌
        $re = DB::table('trade')->where($where)->where('add_time','<',$time-60*60*24)->orderBy('add_time','desc')->first();
        if ($re == null) {
            $data['24H_change'] = 100;
        } else {
            if ($re->price != 0){
                $data['24H_change'] = sprintf("%.2f", ($rs->price-$re->price)/$re->price*100);
                if($data['24H_change'] == 0) {
                    $data['24H_change'] = 100;
                }
                }else {
                    $data['24H_change'] = 100;
                }
        }

        //7D涨跌
        $re = DB::table('trade')->where($where)->where('add_time','<',$time-60*60*24*7)->orderBy('add_time','desc')->first();
        if ($re == null) {
            $data['7D_change'] = 100;
        } else {
            if ($re->price != 0) {
                $data['7D_change'] = sprintf("%.2f", ($rs->price-$re->price)/$re->price*100);
                if($data['7D_change'] == 0) {
                    $data['7D_change'] = 100;
                }
            }else {
                $data['7D_change'] = 100;
            }
        }

//        //24H成交量
//        $rs = DB::table('trade')->where($where)->where('add_time','>',$time-60*60*24)->sum('num');
//        $data['24H_done_num'] = $rs;
        //24H成交额
        $all = DB::table('trade')->where($where)->where('add_time','>',$time-60*60*24)->get();
        if (empty($all)) {
            $data['24H_done_num'] ='';
            $data['24H_done_money'] ='';
        } else {
            foreach ($all as $k=>$v) {
                $num = $v->num;
                $price = $v->price;
                $rs = $num*$price;
            }
            $data['24H_done_num'] += $num;
            $data['24H_done_money'] += $rs;
        }

        //最低价
        $data['min_price'] = $this->getminPriceTrade($id);
        //最高价
        $data['max_price'] = $this->getmaxPriceTrade($id);
        //买一价
        $data['buy_one_price'] = $this->getOneOrdersByPrice($id, 'buy');
        //卖一价
        $data['sell_one_price']=$this->getOneOrdersByPrice($id, 'sell');
        //返回
        return $data;
    }

    /**
     * 获取一个挂单记录价格 买一 卖一
     * @param unknown $currencyid
     * @param unknown $type
     * @param unknown $order
     */
    protected function getOneOrdersByPrice($currencyid,$type){
        $where['currency_id'] = $currencyid;
        $where['type'] = $type;
//        $where['status']=array('in',array(0,1));
        switch ($type){
            case 'buy': $order='desc';
                break;
            case 'sell':$order='asc';
                break;
        }
        $orders = DB::table('orders')->where($where)->whereIn('status',[0,1])->orderBy('price',$order)->first();
        if ($orders != null) {
            return $orders->price;
        }else{
            return '';
        }

    }
    /**
     * 返回最高价
    @param int $currency_id 币种ID
     */
    protected function getMaxPriceTrade($currency_id){
        $order='desc';
        $trade= $this->getTradeByPrice($currency_id,$order);
        return $trade;
    }
    /**
     * 返回最低价
    @param int $currency_id 币种ID
     */
    protected function getminPriceTrade($currency_id){
        $order='asc';
        $trade= $this->getTradeByPrice($currency_id,$order);
        return $trade;
    }

    /**
     * 指定价格一个成交记录
     * @param int $currency_id 币种ID
     * @param char $order 排序
     */
    private function getTradeByPrice($currency_id,$order){
        $where['currency_id'] = $currency_id;
        $re = DB::table('trade')->where($where)->orderBy('price',$order)->first();
        if ($re == null) {
          return '';
        }
        return $re->price;
    }


    //修正众筹表 计算剩余数量  修改状态
    public function checkZhongchou(){
        $list = Issue::select('id','add_time','end_time','num','num_nosell','zhongchou_success_bili','status')->get();
        foreach($list as $k=>$v){
            $where['id'] = $v['id'];
            if($v['status'] == 3) {
                Issue::where($where)->update(['end_time'=>time()]);
                continue;
            }
            if($v['add_time'] > time()) {
                Issue::where($where)->update(['status'=>0]);
            }
            if($v['add_time'] <time() && $v['end_time'] > time()) {
                Issue::where($where)->update(['status'=>1]);
            }
            if($v['end_time'] < time()) {
                Issue::where($where)->update(['status'=>2]);
                Issue::where($where)->update(['end_time'=>time()]);
            }
            $num = IssueLog::where('iid',$v['id'])->sum('num');
            Issue::where($where)->update(['deal'=>$v['num']-$num-$v['num_nosell']]);
            $limit_num = $v['num']*$v['zhongchou_success_bili']-$v['num_nosell'];
            if($num >= $limit_num){
                Issue::where($where)->update(['status'=>2]);
            }
        }
    }


}
