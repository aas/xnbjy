<?php

namespace App\Http\Controllers\Home;

use App\Models\Zhongchou;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ZhongchouController extends BaseController
{
    /**
     * 实例化
     */
    public function __construct(Zhongchou $zhongchou)
    {
        parent::__construct();
        $this->zhongchou = $zhongchou;
    }

    /**
     * 认购中心首页
     */
    public function getIndex()
    {
        $result = $this->zhongchou->index(request());
        return view('home.zhongchou.index',compact('result'));
    }

    /**
     * 众筹详情
     */
    public function getDetails()
    {
        if(empty(request('id'))){
            abort(404);
        }
        $re = $this->zhongchou->details(request());
        if (!$re) {
            abort(404);
        }
        $list = $re[0];
        $log = $re[1];
        $num_buy = $re[2];
        $buy_num = $re[3];
        $id = $list->id;
        return view('home.zhongchou.details',compact('id','list','log','num_buy','buy_num'));

    }

    public function postRun()
    {
        $re = $this->zhongchou->run(request());
        $this->ajaxReturn($re);
    }

}
