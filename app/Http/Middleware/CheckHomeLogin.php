<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 2017/10/23
 * Time: 16:39
 */

namespace App\Http\Middleware;

use Closure;

class CheckHomeLogin
{
    public function handle($request, Closure $next)
    {
        if (!session('USER_KEY_ID')) {
            return redirect(urlHome('login', 'getIndex'));
        }
        return $next($request);
    }
}