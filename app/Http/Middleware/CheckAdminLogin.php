<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2015/8/6
 * Time: 13:29
 */

namespace App\Http\Middleware;

use Closure;

class CheckAdminLogin
{


    public function handle($request, Closure $next)
    {
        if (!(session('admin_userid') && session('ruls'))) {
            return redirect(urlAdmin('login', 'getLogin'));
        }
        $url = [$request->path(), $request->path() . '?' . $_SERVER['QUERY_STRING']];
        //二级菜单权限检查
        if (!array_intersect($url, session('ruls'))) {
            //三级菜单权限检查
            if (!array_intersect($url, config('menus'))) {
                abort(404);
            }
        }
        return $next($request);
    }

}

